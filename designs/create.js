import { Alert, BackHandler, Text, ToastAndroid } from "react-native";
import { Button, Col, Grid, Left, Right, View } from 'native-base';

import GestureRecognizer from 'react-native-swipe-gestures';
import Header from './header';
import { NavigationActions } from "react-navigation";
import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';

export default class Create extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    constructor(props) {
        super(props);
        this.state = { step: 0, processing: false };
        this.getQuestion = this.getQuestion.bind(this);
        this.isValid = this.isValid.bind(this);
        this.backer = this.backer.bind(this);
        this._goRight = this._goRight.bind(this);
        this._goLeft = this._goLeft.bind(this);
        this._back = this._back.bind(this);

    }

    componentDidMount() {
        /**
         * Prevent default closing on pressing back
         */
        BackHandler.addEventListener('hardwareBackPress', this.backer);
    }

    _back(){
        this.props.navigation.dispatch(NavigationActions.back())
    }

    backer() {
        Alert.alert(
            'Are you sure?',
            'You will lose your currently filled data.',
            [
                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                { text: 'OK', onPress: this._back.debounce(50) },
            ],
            { cancelable: true }
        )
        return true;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backer);
    }

    isValid() {
        if (this.criteria.length > 0) {
            this.criteria.filter((checkIn, key) => this[key](checkIn));
        }
        return true;
    }

    getQuestion(step, setValidation) {
        let steps = this.props.questions
        let comp = steps.filter(Boolean)[step];
        if (comp) {
            !setValidation ? this.isValid = comp.validation : '';
            return comp.component();
        }
        return false;
    }

    _goRight() {
        if (!this.isValid || (this.isValid && this.isValid()))
            this.getQuestion(this.state.step + 1, true) && this.setState({ step: this.state.step + 1 })
    }

    _goLeft() {
        this.state.step !== 0 && this.setState({ step: this.state.step - 1 })
    }

    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        };
        return <GestureRecognizer config={config}
            onSwipeLeft={this._goRight.debounce(50)}
            onSwipeRight={this._goLeft.debounce(50)}><View style={{ backgroundColor: 'white' }}>
                <View style={{ padding: 10, backgroundColor: 'white' }}>
                    {this.getQuestion(this.state.step)}
                </View>
                <View style={{ padding: 10, backgroundColor: 'white' }}e>
                    <Grid style={{ backgroundColor: 'white', }}>
                        <Col>
                            <Button disabled={Boolean(this.state.processing)} style={{ marginLeft: 10, paddingHorizontal: 15  }} primary onPress={this._goLeft.debounce(50)}><Text  style={{color:'white'}}>{'Previous'}</Text></Button>
                        </Col>
                        {(this.props.exists || !this.getQuestion(this.state.step + 1, true)) && <Col style={{ alignItems: 'center' }}>
                            <Button disabled={Boolean(this.state.processing)} style={{ marginLeft: 20, paddingHorizontal: 15 }} onPress={()=>{
                                if(!this.isValid || (this.isValid && this.isValid())) {
                                    this.setState({ processing: 'Processing...' });
                                    ToastAndroid.show('Processing...', ToastAndroid.LONG);
                                    this.props.onSave.debounce(50)();
                                }
                            }} success><Text style={{color:'white'}}>{'    Save   '}</Text></Button>
                        </Col>}
                        <Right>
                            {this.getQuestion(this.state.step + 1, true) && <Button disabled={Boolean(this.state.processing)} style={{ marginRight: 10, paddingHorizontal: 15, paddingLeft: 15,paddingRight:15 }} primary onPress={this._goRight.debounce(50)}><Text style={{color:'white'}}>{'   Next   '}</Text></Button>}
                        </Right>
                    </Grid>
                </View>
            </View>
        </GestureRecognizer>;
    }
} 
