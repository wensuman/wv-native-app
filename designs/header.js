import { Alert, Platform } from 'react-native';
import { Body, Button, Header,Text, Icon, Left, Right, Title, View } from 'native-base';

import { NavigationActions } from 'react-navigation';
import React from 'react';
import _ from 'lodash';

export default (props) => {
    
    return <View style={{ marginTop: Number(Platform.Version) < 20 ? 24 : 0}}>
        <Header style={{backgroundColor: 'orange'}} >
            <Left>
                {props.menu && <Button onPress={() => props.navigation.navigate('DrawerOpen')} transparent>
                    <Icon name='menu' />
                </Button>}
                {!props.menu && props.navigation && <Button onPress={props.alertOnBack ? () => {
                    Alert.alert(
                        'Are you sure?',
                        'You will lose your currently filled data.',
                        [
                            { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                            { text: 'OK', onPress: () => { props.navigation.dispatch(NavigationActions.back()) } },
                        ],
                        { cancelable: true }
                    )
                } : () => { props.navigation.dispatch(NavigationActions.back()) }} transparent>
                    <Icon name='arrow-back' />
                </Button>}
            </Left>
            <Body style={{ alignSelf: "center" }}>
                <Title>{_.capitalize(props.title || 'Insert title')}</Title>
            </Body>
            {props.onRight && <Right>
                <Button onPress={props.alertOnRight ? () => {
                    Alert.alert(
                        'Are you sure?',
                        props.alertOnRight.length > 4 ? props.alertOnRight : 'You will lose your data.',
                        [
                            { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                            { text: 'OK', onPress: props.onRight.debounce() },
                        ],
                        { cancelable: true }
                    )
                } : props.onRight} transparent>
                    {props.rightText? <Text>{props.rightText}</Text> : <Icon name={props.icon || 'trash'} />}
                </Button>
            </Right>}
        </Header>
    </View>
}