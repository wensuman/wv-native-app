import { Alert, AsyncStorage, ScrollView, StyleSheet, DeviceEventEmitter, NetInfo } from 'react-native';
import { DrawerItems, DrawerNavigator, SafeAreaView } from "react-navigation";

import ATTMain from './screens/att';
import Auth from './screens/auth/login';
import LogOut from './screens/auth/logout';
import FamilyMain from './screens/family';
import GroupMain from "./screens/group";
import HomeScreenMain from './screens/home';
import InstituteMain from "./screens/institute";
import MonitorMain from './screens/health';
import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import SurveyMain from './screens/survey';
import SyncMain from './screens/sync';
import _ from 'lodash';
import { Asset, FileSystem, Font } from 'expo';


Array.prototype.ofLang = function (lang) {
  return this.map((ob) => { return { label: ob.label[lang || 0], value: ob.value } })
}

Function.prototype.debounce = function(time){
  return _.debounce(this, time || 500);
}

Array.prototype.display = function () {
  return _.values(this).map(_.capitalize).join(', ');
}

String.prototype.display = function () {
  return _.capitalize(this.toString());
}

Number.prototype.display = function(){
  return this.toString();
}

Object.prototype.toString = function(){
  return JSON.stringify(this);
}

const headerVards = {
  headerMode: 'none',
  drawerWidth: 300,
  contentOptions: {
    activeTintColor: 'orange',
    itemStyle: {
      borderRadius: 4,
      borderWidth: 0.5,
      borderColor: '#d6d7da'
    }
  },
  contentOptions: {
    itemsContainerStyle: {
      paddingTop: 24
    }
  }
};
const SignedIn = DrawerNavigator(
  {
    Dashboard: {
      screen: HomeScreenMain
    },
    Family: {
      screen: FamilyMain
    },
    Group: {
      screen: GroupMain
    },
    Institute: {
      screen: InstituteMain
    },
    ATT: {
      screen: ATTMain
    },
    Survey: {
      screen: SurveyMain
    },
    Monitor: {
      screen: MonitorMain
    },
    Sync: {
      screen: SyncMain
    },
    LogOut:{
      screen:LogOut
    }
  }, headerVards
)

export default class App extends React.Component {

  state = {
    isSignedIn: false,
    ready: false,
  }

  async componentDidMount(){
    let token = await AsyncStorage.getItem('@wv-data:token');
    token && this.setState({isSignedIn: true});
    DeviceEventEmitter.addListener('logged-out',()=>{this.setState({isSignedIn: false})});
    this.setState({ready:true});
  }
  
  onSignIn(){
    this.setState({isSignedIn: true});
  }


  async componentWillMount() {
    NetInfo.isConnected.fetch().then(async isConnected => {
      isConnected && await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
      });
    });
  }

  render() {
    if(this.state.ready){
      const  {isSignedIn} = this.state;
      return isSignedIn ? <SignedIn /> : <Auth onSignIn={this.onSignIn.bind(this)}/>;
    }
    return <Spinner visible={true} textContent="Checking..."/>

  }

}