import { Body, CheckBox, Col, Content, Input as Inp, Item, Label, List, ListItem, Picker, View } from 'native-base';
import { Text, TextInput, TouchableOpacity } from 'react-native';
import React from 'react';
import _ from 'lodash';

import Option from '../module/options';

const months =[
    {label:'Baisakh', value: '1'},
    {label:'Jestha', value: '2'},
    {label:'Asadh', value: '3'},
    {label:'Shrawan', value: '4'},
    {label:'Bhadra', value: '5'},
    {label:'Aswin', value: '6'},
    {label:'Kartik', value: '7'},
    {label:'Mangsir', value: '8'},
    {label:'Poush', value: '9'},
    {label:'Magh', value: '10'},
    {label:'Falgun', value: '11'},
    {label:'Chaitra', value: '12'},
] ;

export const isQuestion = (label) => {
    try {
        label = _.trim(label.toLowerCase());
        if (_.endsWith(label, '?')) {
            return '';
        }
        return (label.startsWith('is') || label.startsWith('how') || label.startsWith('are') || label.startsWith('wh') || label.startsWith('ha') || label.startsWith('do') || label.startsWith('did')) ? ' ? ' : ''
    } catch (error) {
        return ''
    }
}

export const Input = (props) => {
    return (
        <Item style={{ borderWidth: 2, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }} stackedLabel>
            <Label key="1"> {props.label}</Label>
            <Inp {...props} regular key="2" value={props.value || ''} onChangeText={props.onChange} />
        </Item>
    )
}
export const loopMap = function (integ) {
    integ = String(integ);
    return { label: integ, value: integ };
}

export class DatePick extends React.Component {

    state = {};

    constructor(props) {
        super(props);
        this.componentDidUpdate = this.componentDidUpdate.bind(this);
        const date = this.props.value ? this.props.value.split('/') : [];
        this.state = { year: date[0], month: date[1], day: date[2] };
    }

    componentDidUpdate() {
        if (this.yearIsValid(this.state.year) && this.monthIsValid(this.state.month) && this.dayIsValid(this.state.day)) {
            let complete = [this.state.year, this.state.month, this.state.day].join('/');
            complete != this.props.value && this.props.onChange && this.props.onChange(complete);
        }
    }

    yearIsValid(year) {
        return year;
    }
    monthIsValid(month) {
        return month;
    }
    dayIsValid(day) {
        return day;
    }

    render() {
        return (<View style={{ flex: 1, flexDirection: 'row',justifyContent: 'space-between' }}>
            <View style={{ width: '32%', height: 80 }} >
                <Select prepend={false} label="Year" key="year" onChange={year => this.setState({ year })} data={_.range(1980, (new Date().getFullYear() + 58)).reverse().map(loopMap)} selected={this.state.year} />
            </View>
            <View style={{ width: '32%', height: 80 }} >
                <Select prepend={false} label="Month" key="month" onChange={month => this.setState({ month })} data={months} selected={this.state.month} />
            </View>
            <View style={{ width: '32%', height: 80 }} >
                <Select  prepend={false} label="Day" key="day" onChange={day => this.setState({ day })} data={_.range(1, 32).map(loopMap)} selected={this.state.day} />
            </View>
        </View>);
    }
}

export default Input;

export const Number = (props) => {
    let caller = props.onChange;
    return (
        <Item style={{ borderWidth: 2, borderBottomColor: 'black', borderStyle: 'solid' }} stackedLabel>
            <Label key="1"> {props.label}</Label>
            <Inp defaultValue='' {...props} regular key="2" value={props.value || ''} keyboardType="phone-pad" onChangeText={caller} />
            {props.warning != '' && <Text style={{ color: 'red' }}>{props.warning}</Text>}
        </Item>
    )
}

export class CheckList extends React.Component {
    state = {}

    constructor(props) {
        super(props)
    }

    render() {
        let data = this.props.data ? this.props.data : []

        if (this.state.search) {
            data = data.filter(f => f.value == parseInt(this.state.search) || (f.label || '').toLowerCase().includes(this.state.search.toLowerCase()))
        }

        if (this.props.limit) {
            data = data.slice(0, this.props.limit - 1);
        }

        return (
            <List>
                <Label>{this.props.label}</Label>
                {this.props.searchable && <Input placeholder="Search by ID or name" value={this.state.search} onChange={search => this.setState({ search })} />}
                {data.map((item, index) => <ListItem style={{ marginLeft: 0 }} key={index}>
                    <CheckBox {...this.props} onPress={() => {
                        let data = [];
                        if (this.props.selected && this.props.selected.includes(item.value)) {
                            data = this.props.selected.filter(filter => filter != item.value)
                        } else {
                            data = [item.value].concat(this.props.selected || []);
                        }
                        this.props.onChange(data)
                    }} checked={this.props.selected && this.props.selected.includes(item.value)} />
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        let data = this.props.selected && this.props.selected.includes(item.value) ? this.props.selected.filter(filter => filter != item.value) : [item.value].concat(this.props.selected || []);
                        this.props.onChange(data)
                    }}>
                        <Body>
                            <Text> {item.label}</Text>
                        </Body>
                    </TouchableOpacity>
                </ListItem>)}
            </List>
        )
    }
}

export class Select extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        if (this.props.multiple) {
            return <CheckList {...this.props} />;
        }
        let datam = this.props.data || [{ label: 'Yes', value: 'yes' }, { label: 'No', value: 'no' }];

        if(this.props.prepend !== false){
             datam = [{ label: this.props.placeholder || 'Select an option', value: '--' }].concat(datam)
        }

        return (
            <View style={{ borderWidth: 1, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }} stackedLabel>
                <Label>{this.props.label}</Label>
                <Picker {...this.props} placeholder={this.props.label} selectedValue={this.props.selected} onValueChange={this.props.onChange}>
                    {datam.map((item) => <Picker.Item key={item.value} label={item.label} value={item.value} />)}
                </Picker>
            </View>
        );
    }
}