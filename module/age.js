export default (birthday)=>{
    let diff = Date.now() - (new Date(birthday)).getTime();
    let ageDate = new Date(diff); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}