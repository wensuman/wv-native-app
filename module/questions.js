export const labels = {
    project: {
        label: [
            "Select a project",
            "परियोजना छान्नुहोस्",
        ]
    },
    vitamin: {
        label: [
            "Did your child take vitamin capsule?",
            "के तपाईंको बच्चाले भिटामिन क्याप्सुल खानुभयो ?"
        ]
    },
    participated_before: {
        label: [
            "When did you participate in this survey before?",
            "यस भन्दा पहिले पनि तपाई यो सर्बेक्षणमा सहभागी हुनु भएको थियो ?"
        ]
    },
    day_of_age: {
        label: [
            "No of days since child birth?",
            "बच्चा जन्मे देखिको दिन "
        ]
    },
    pdherth_applicable: {
        label: [
            'Child is in severe state. Recommend this child to PD Hearth.',
            'यो बच्चाको  अवस्था गम्भीर रहेछ / यो बच्चालाई PD hearth को लागि सिफारिस गर्नुहोस्  .',
        ]
    },
    status: {
        label: [
            'Status',
            'अवस्था '
        ]
    },
    1: {
        label: [
            "Did you visit Health post/PHC-ORC for ANC checkup?",
            "के तपाई गर्भवती जाँचको लागि स्वास्थ्य संस्था वा गाउँ घर क्लिनिकमा जानु भयो "
        ]
    },
    2: {
        label: [
            "Did you receive any iron/folate supplements during this period?",
            "के तपाईले गर्भवती भएको बेला कुनै आइरन चक्की/फोलेट खानुभयो ? "
        ]
    },
    3: {
        label: [
            "How many iron/folic tablets did you take?",
            "यदी खानुभयो भने गर्भवती भएको बेला कतिवटा आइरन चक्की/फोलेट खानुभयो "
        ]
    },
    4: {
        label: [
            "Did you eat addditional food during pregnancy?",
            "के तपाईले  गर्भवती भएको बेलामा अरुबेला भन्दा थप खानेकुरा खानुभयो "
        ]
    },
    5: {
        label: [
            "Weight of mother (In Kg)?",
            "आमाको तौल कति किलोग्राम छ ? (किलोग्राम)"
        ]
    },
    6: {
        label: [
            "MUAC? (cm)",
            "आमाको MUAC को नाप कति सेन्टिमिटर छ ? "
        ]
    },
    7: {
        label: [
            "Did you take adequate rest during the day?",
            "के तपाईले गर्भवती भएको बेलामा दिनमा पुग्दो मात्रामा आराम गर्नु भयो ?"
        ]
    },
    8: {
        label: [
            "How many hours do you take rest in a day?",
            "तपाईले दिनमा कति घण्टा आराम गर्नुहुन्थ्यो ? "
        ]
    },
    9: {
        label: [
            "When was the child born?",
            "तपाइको यो बच्चा कुन ठाउमा जन्माउनु भयो ?"
        ]
    },
    10: {
        label: [
            "What is the sex of child?",
            "बच्चाको लिंग के हो "
        ]
    },
    11: {
        label: [
            "What did you use on umbilical cord?",
            "बच्चा जन्मिएपछि अम्बिलिकल कर्ड (umbilical cord) मा के प्रयोग गर्नुभयो "
        ]
    },
    12: {
        label: [
            "How much did the baby weigh at birth?",
            "यो बच्चा जन्मदा कति तौलको थियो ?"
        ]
    },
    13: {
        label: [
            "After you gave birth to the baby, did anyone check on your health in a health facility, on the  day of delivery or within 7 days?",
            "तपाई सुत्केरी भइसके पछि सुत्केरी भएकै दिन वा सात दिन भित्र तपाइले स्वास्थ्य संस्थामा आफ्नो स्वास्थ्य जांच गराउनुभयो ? "
        ]
    },
    14: {
        label: [
            "Did you ever breastfeed?",
            "तपाईले बच्चालाइ कहिल्यै स्तनपान गराउनु भएको छ ? "
        ]
    },
    15: { 
        label: [
            "How long after birth did you first breast feed ?",
            "  यो बच्चा जन्मिएको कति समयपछि तपाईले बच्चालाई स्तनपान गराउनु भयो? "
        ]
    },
    16: {
        label: [
            "Did you feed your child with any pre-lacteal feed before initiation of breast milk on the first  day of birth (Example – ghee, honey, etc)?",
            "बच्चा जन्मेको दिन तपाइले बच्चालाई स्तनपान गराउनु भन्दा पहिले अन्य कुनै खानेकुरा खुवाउनु भएको थियो ? जस्तै: घिउ, मह आदी"
        ]
    },
    17: {
        label: [
            "Are you currently breastfeeding?",
            "के तपाई अहिले बच्चालाई स्तनपान गराई रहनु भएको छ "
        ]
    },
    18: {
        label: [
            "Do you feed your baby anything else other than breast milk?",
            "के यो बच्चालाई तपाई (दिउसो वा राती को समयमा)ले आफ्नो दुध बाहेक अन्य कुनै खानेकुरा जस्तै सुद्द दुध, जुश, सुप, आदि खुवाउनुहुन्छ ?"
        ]
    },
    19: {
        label: [
            "What did you feed your children on last 24  hours (Types of liquid and food, the child consumed yesterday (within past 24 hours)?",
            "गएको २४ घण्टामा तपाइले आफ्नो बच्चालाई के के ठोस वा तरल खानेकुरा खुवाउनु भयो ?"
        ]
    },
    20: {
        label: [
            "How many times did you breastfeed your child in last 24 hours?",
            "गएको २४ घन्टामा तपाइले बच्चालाई कति पटक स्तनपान गराउनुभयो ?"
        ]
    },
    21: {
        label: [
            "Did your child have loose stools (3times in 24 hours) in the past 2 weeks?",
            "गएको २ हप्ताको अवधिमा तपाइको बच्चाले २४ घन्टामा ३ वा तिन पटक भन्दा बढी पटक पातलो दिसा गरेको थियो वा झाडापखाला लागेको थियो "
        ]
    },
    22: {
        label: [
            "During the episode of diarrhea, was the baby given ORS and or Zinc?",
            "के त्यो झाडापखाला भएको बेलामा बच्चालाई पुनर्जलीय झोल र/वा जिंक चक्की खुवाइएको थियो ?"
        ]
    },
    23: {
        label: [
            "Please tell me if your child has been ill with a cough at any time in the last two weeks?",
            "गएको २ हप्तामा कुनै पनि बेला तपाइको बच्चा खोकी लागि बिरामी भएको थियो ?"
        ]
    },
    24: {
        label: [
            "When your child had the illness with a cough, did he/she have trouble breathing, or  breathe faster than usual with quick, short breaths ?",
            "त्यो खोकी लागि बिरामी भएको बेलामा के तपाइको बच्चालाई श्वास फेर्न समस्या भएको थियो (सामान्य अवस्थामा भन्दा छिटो वा ढिलो श्वास फेर्ने गरेको थियो ?"
        ]
    },
    25: {
        label: [
            "Where and whom did you seek treatment?",
            "कहाँ र को बाट उपचार गराउनु भएको थियो "
        ]
    },
    26: {
        label: [
            "Do you have Hand washing station with soap and water in your household?",
            "के तपाइको घरमा साबुन र पानी सहितको कुनै हात धुने ठाउँ छ ?"
        ]
    },
    27: {
        label: [
            "Where do you dispose the stool of your children?",
            "तपाइले बच्चाको दिसालाई कुन ठाउमा बिसर्जन गर्नु हुन्छ ?"
        ]
    },
    28: {
        label: [
            "Do you have safe place for children to play and eat (no any animal faeces/ rubbish on  surrounding?",
            "तपाईको घरमा बच्चाको लागि सुरक्षित खाने र खेल्ने ठाउँ छ (वरिपरी पशुपन्छी र अन्यको फोहोर नभएको) ? "
        ]
    },
    29: {
        label: [
            "When did you wash your hands within these 24 hours?",
            "गएको २४ घण्टा भित्र तपाइले कुन कुन बेलामा हात धुनुभयो ?"
        ]
    },
    30: {
        label: [
            "Did your child take Deworming tablet in past six month?",
            "के तपाइले गएको ६ महिना भित्र बच्चालाई जुकाको ट्याब्लेट खुवाउनुभयो ?"
        ]
    },
    31: {
        label: [
            "Weight of the child ( In Kg )?",
            "बच्चाको तौल कति छ ?  ( किलोग्राममा लेख्नुहोस् )"
        ]
    },
    32: {
        label: [
            "What is the Height/length of the children ( In cm )?",
            "बच्चाको उचाई/लम्बाई कति छ ? ( सेन्टिमिटरमा लेख्नुहोस् )"
        ]
    },
    33: {
        label: [
            "Different Immunization child have received?",
            "तपाइको बच्चालाई कुन कुन खोप लगाई सक्नुभएको छ ?"
        ]
    },
    34: {
        label: [
            "Did you have completely immunized your child as per the age?",
            "के तपाइँले आफ्नो बच्चालाई उमेर अनुसार लगाउनु पर्ने पुरै खोप लगाउनु भयो ?"
        ]
    },
    35: {
        label: [
            'Is child vaccine with BCG (Bacilllus-Calmette-Guerin) ie. Copenhagen strain',
            'के बच्चालाई बिसिजी खोप लगाउनु भयो ?(Bacilllus-Calmette-Cuerin) ie. Copenhagen strain'
        ]
    },
    36: {
        label: [
            'Is child vaccine with Pentavalent Vaccine i.e. DPT- HepB-HiB(Diphtheria, Pertussis, Trtanus, Hepatitis B and Hemophilus Influenza?',
            'के यो बच्चालाइ डीपीटी खोप  लगाउनु भयो ?'
        ]
    },
    37: {
        label: [
            'Is child vaccine with OPV (Oral Polia Vaccine)?',
            'के यो बच्चालाई पोलियो थोपा खुवाउनु भयो  (Oral Polia Vaccine)?',
        ]
    },
    38: {
        label: [
            'Is child vaccine with PCV (Pneumococcal Conjugated Vaccine)?',
            'के यो बच्चालाई पीसीभी खोप  (Pneumococcal Conjugated Vaccine) लगाउनु भयो ?'
        ]
    },
    39: {
        label: [
            'Is child vaccine with IPV (Injectable Polio Vaccine)?(y/n)',
            'के यो बच्चालाई  आइपीभी  खोप (Injectable Polio Vaccine) लगाउनु भयो ?(छ /छैन )'
        ]
    },
    40: {
        label: [
            'Is child vaccine with MR (Measles-Rubella)?',
            'के यो बच्चालाई दादुरा/रुबेलाको खोप (Measles-Rubella) लगाउनु भयो ?'
        ]
    },
    42: {
        label: [
            'Is vaccine with JE (Japanese Encephalitis) i.e SA-14-12-2 ?',
            'के यो बच्चालाइ जापानिज इन्सेफलाईटिसको  खोप (Japanese Encephalitis) लगाउनु भयो  ?'
        ]
    },
    43: {
        label: [
            'Is child vaccine with Td?',
            'के यो बच्चालाइ Td को  खोप  लगाउनु भयो?'
        ]
    },
    //Schoolsite 
    Schoolsite: {
        label: [
            "Site of the school?",
            "विद्यालय क्षेत्र"
        ]
    },
    building: {
        label: [
            "How many separate buildings are located on the school site (don’t include toilet)?",
            "विद्यालय हातामा कति वटा भवनहरु छन्? (सौचालय समाबेस नगर्नुहोला)"
        ]
    },
    classroom: {
        label: [
            "How many separate classrooms are located on the school site (don’t include toilet)?",
            "विद्यालयमा कति वटा छुट्टाछुट्टै कक्षाकोठाहरु छन्? (सौचालय समाबेस नगर्नुहोला)"
        ]
    },
    school_hazards: {
        label: [
            "The school site location is safe from hazards:",
            "विद्यालय रहेको स्थानको सुरक्षा अवस्था "
        ]
    },
    landslide: {
        label: [
            "Is the school site located in landslide prone area (steep slope)?",
            "के विद्यालय रहेको स्थान पहिरोको जोखिमपूर्ण ठाउंमा  रहेको छ? (भिरालो ठाउँ )"
        ]
    },
    hightension: {
        label: [
            "Is there high tension electric line or transmission tower near the school site (horizontal distance 6m or less)?",
            "के विद्यालय क्षेत्र नजिकै  बिजुलीको हाई टेन्सन लाईन वा ट्रान्समिसन टावर छ? ( बिद्यालयबाट सिधा दुरी ६ मिटर वा कम भएको)"
        ]
    },
    bigtree: {
        label: [
            "Is there a big tree located within 10 metres from a building on the school site?",
            "विद्यालयको १० मिटर क्षेत्र भित्रमा कुनै ठुलो रुख छ?"
        ]
    },
    hazardous_site: {
        label: [
            "Is access to the school site hazardous to children? (eg. steep, unstable or slippery entry/exit points)",
            "बालबालिकाहरुलाई विद्यालय क्षेत्रमा  कुनै खतरा वा जोखिम  छ? (जस्तै: जाने/आउने ठाउँ तिर. भिरालो, चिप्लो ठाउँ आदि ) "
        ]
    },
    school_resilent: {
        label: [
            "School has earthquake resilient buildings?",
            " विद्यालयको भूकम्प सुरक्षा अवस्था"
        ]
    },
    resilent_building: {
        label: [
            "How many of the school buildings have an earthquake resilient structure (approved design / drawings from a govt or NGO engineer)?",
            "विद्यालयका कति वटा भवनहरु  भूकम्प  प्रतिरोधी  संरचनाका छन्? ( सरकार वा  NGO इन्जिनियर बार संरचनाको स्वीकृत  भएको) "
        ]
    },
    resilent_toilets: {
        label: [
            "How many of the separate toilet blocks have an earthquake resilient structure (approved design / drawings from a govt or NGO engineer)?",
            "विद्यालयका कति वटा सौचालय भवनहरु  भूकम्प  प्रतिरोधी  संरचनाका छन्? ( सरकार वा  NGO इन्जिनियर बार संरचनाको स्वीकृत  भएको)"
        ]
    },
    School_Classrooms: {
        label: [
            "(this section to have the following set of questions for every classroom)",
            "(यो खण्डमा प्रत्येक कक्षाकोठाको लागि  प्रश्न छन)"
        ]
    },
    features: {
        label: [
            "The school has child friendly features in the classrooms",
            "विद्यालयको कक्षाकोठामा बाल मैत्री बिशेषताहरु :"
        ]
    },
    seating_1to3: {
        label: [
            "(a). Appropriate seating arrangement for grade 1 - 3 (Carpet, cushion & Low height table;  Desk with bench/chair)",
            "क)कक्षा १-३ को  लागि उपयुक्त बसाइ ब्यबस्थापन  ( कार्पेट, कुशन र सानो हाईटको टेबल: डेस्क  संगै बेन्च/कुर्सि)"
        ]
    },
    seating_4plus: {
        label: [
            "(b).  Appropriate Sitting arrangements for grades 4 and above  (desk with separate bench or chair;  Combined desk with bench)",
            "ख) कक्षा ४ र माथिको  लागि उपयुक्त बसाइ ब्यबस्थापन  ( कार्पेट, कुशन र सानो हाईट को टेबल: डेस्क  संगै बेन्च/कुर्सि)"
        ]
    },
    door_lock: {
        label: [
            "(c).  Classroom door locks and/or door bolts are at an accessible height for all children?",
            "ग) कक्षाकोठाको ढोकाको  चुक्कुल बालबालिका संग पहुँचयोग्य छन्?"
        ]
    },
    steps_hight: {
        label: [
            "(d). Steps providing access to the classroom are of appropriate height (less than 17cm)?",
            "घ) कक्षाकोठा जाने खुड्किलो  उपयुक्त उचाईको छ ? (१७ सेन्टीमिटर  भन्दा कम)"
        ]
    },
    exit_doors: {
        label: [
            "(e).  Is there at least two exit doors in for the classroom?",
            "ङ) के कक्षाकोठामा कम्तिमा २ वटा निस्किने ढोका छन्?"
        ]
    },
    doors_open: {
        label: [
            "(f).  Access doors to the classroom open outward (to the outside of the classroom)?",
            "च) कक्षा कोठाको ढोका बाहिर पट्टि खुल्ने  रहेको छ ?"
        ]
    },
    disable_friendly: {
        label: [
            "The school has disability friendly infrastructure",
            "विद्यालयमा अपांग मैत्री भौतिक संरचना "
        ]
    },
    ramps: {
        label: [
            "(a). Are there Ramps for access to classrooms?",
            "कक्षाकोठाहरुमा जान  अपाङ्ग मैत्री बाटो (रयाम्प) छ?"
        ]
    },
    handrails: {
        label: [
            "(b). Are there handrails to support access to classrooms?",
            "कक्षाकोठा जाने बाटोमा  समाएर जान बनाएको  रेलिंग  (handrails)छ?"
        ]
    },
    spacefor_child: {
        label: [
            "Classrooms have sufficient space for children (at least 25 sqm)",
            "कक्षाकोठामा बालबालिकाको लागि प्रसस्त ठाउँ छ ? (कम्तिमा २५ स्क्वायर मिटर)"
        ]
    },
    toilet_section: {
        label: [
            "Toilets",
            "सौचालय"
        ]
    },
    toilet_no: {
        label: [
            "How many separate toilet blocks are in the school?",
            "विद्यालयमा कति वोटा छुट्टाछुट्टै सौचालय छन्?"
        ]
    },
    girls_toilet_separate: {
        label: [
            "Does the school have separate toilets for girls?",
            "के छात्राको लागि छुट्टै सौचालय छ?"
        ]
    },
    mhm: {
        label: [
            "The school has a menstrual hygiene management system",
            "विद्यालयमा महिनावारी सरसफाई ब्यबस्थापन "
        ]
    },
    mhm_bin: {
        label: [
            "(a).  Does the school have disposal bin/container in girls toilet?",
            "क) विद्यालयको छात्राको सौचालयमा  फोहोर फाल्ने डस्टबिन छ?"
        ]
    },
    incinerator_mhm: {
        label: [
            "(b).  Does the school have incinerator or rubbish collection?",
            "ख) विद्यालयमा फोहोर संकलन गर्ने भाडा छ?"
        ]
    },
    mhm_other: {
        label: [
            "(c).  Mention if there is any other things for menstrual hygiene management",
            "महिनावारी सरसफाई ब्यबस्थापन  बारे कुनै  थप कुराहरु छन् भने उल्लेख गर्नुहोला"
        ]
    },
    childfriendly_toilet: {
        label: [
            "The toilet facility is child friendly",
            "बाल मैत्री सौचालयको सुबिधा"
        ]
    },
    doorlock_toilet: {
        label: [
            "Is the door lock and/or door bolt of toilet in an accessible height for children?",
            "के सौचालयको  ढोकाको लक र ढोकाको चुक्कुल बोल्ट सबै बालबालिकाले भेटाउने गरि राखिएको छन् ?"
        ]
    },
    commode_size: {
        label: [
            "Is the size of pan/commode  appropriate for children?",
            "के प्यान / कमोडको  आकार  बालबालिकालाई उपयुक्त छ?"
        ]
    },
    disablefriendly_toilet: {
        label: [
            "Disable friendly features in toilet",
            "सौचालयमा अपांग मैत्री बिशेषताहरु"
        ]
    },
    ramps_to_toilet: {
        label: [
            "Is there a ramp access to the toilet block?",
            "सौचालयमा जाने बाटो अपाङ्ग मैत्री बनाएको छ (रयाम्प) छ?"
        ]
    },
    handrails_toilet: {
        label: [
            "Does the toilet cubicle have a handrail ?",
            "सौचालयमा  साहयता रेलिंग छ? (handrail)"
        ]
    },
    access_toilet: {
        label: [
            "Does the toilet block enable internal access (eg wheelchair access)?",
            "सौचालय भित्र पर्याप्त ( व्हील  चेयर राख्न मिल्ने) ठाउँ छ ?"
        ]
    },
    clean_toilet: {
        label: [
            "Is the toilet facility clean  (eg no faeces visible; no stink)?",
            "सौचालयको सफा सुग्गर रहेको छ? (जस्तै बाहिरै दिसा नभएको, नगनाउने)"
        ]
    },
    water_toilet: {
        label: [
            "Is a water source available at the toilet block site?",
            "सौचालयमा पानीको ब्यवस्था छ?"
        ]
    },
    handwash_toilet: {
        label: [
            "Is a hand washing station with soap located at the toilet block site?",
            "सौचालय प्रयोग पछी साबुन सहितको हात धुने ठाउ छ?"
        ]
    },
    handwash_visual: {
        label: [
            "Does visual displays encourage handwashing (eg footprints from toilet to hand washing station)?",
            "हात धुन प्रेरित गर्ने खालका कुनै दृस्य अथवा संकेत छन् ? (जस्तै सौचालय देखि हात धुने ठाउँ सम्म पाइला बनाएको)"
        ]
    },
    water: {
        label: [
            "Clean Drinking Water",
            "सफा खाने पानी"
        ]
    },
    water_access: {
        label: [
            "Children can access clean drinking water at the school  (the questions below pop up when ‘yes’ is selected)",
            "विद्यालयमा बालबालिकको लागि सफा खाने पानी उपलब्ध छ ? (यदी छ भने मात्र तलका प्रश्न हरु आउनेछन)"
        ]
    },
    water_adequate: {
        label: [
            "Children can access adequate quantity of clean drinking water throughout the year (5 ltr per person per day).",
            "बालबालिकालाई वर्ष भरि विद्यालयमा प्रसस्त मात्रामा सफा खाने पानी उपलब्ध हुन्छ ? (प्रतेक दिन प्रति विद्यार्थी ५ लिटर)"
        ]
    },
    water_forall: {
        label: [
            "Clean drinking water is accessible to all children (incl. disability)",
            "सफा खाने पानी सम्पूर्ण बालबालिका का लागि छ ? ( अपांगहरुलाई पनि समेटेर)"
        ]
    },
    waste: {
        label: [
            "Waste Management",
            "फोहोर ब्यबस्थापन"
        ]
    },
    waste_mgmt: {
        label: [
            "The school has a waste management / garbage disposal system  (the questions below pop up when ‘yes’ is selected)",
            "विद्यालयमा फोहोर ब्यबस्थापन गर्ने पद्दति (सिस्टम) छ ?(यदी यो प्रश्नको उत्तर हो भने तल प्रश्न हरु आउनेछन)"
        ]
    },
    garbage_bin: {
        label: [
            "Garbage bins are located throughout the school",
            "विद्यालयमा फोहोर फाल्ने डस्टबिनहरु ठाउँ ठाउमा राखिएका छन् ?"
        ]
    },
    clean_ground: {
        label: [
            "The school grounds are free from rubbish / garbage",
            "विद्यालयको प्रांगण (ground)  फोहोर मैला मुक्त छ ?"
        ]
    },
    garbage_point: {
        label: [
            "The school has a garbage disposal point",
            "विद्यालयमा फोहोर मैला संकलन गर्ने / फाल्ने निश्चित स्थान छ ?"
        ]
    },
    waste_mechanism: {
        label: [
            "The waste management system includes a disposal mechanism",
            "फोहोर ब्वास्थापन पद्धतिमा फोहोर नस्ट गर्ने / फाल्ने पद्धति पनि पर्छ |"
        ]
    },
    incinerator: {
        label: [
            "Incinerator",
            "फोहोर जलाउने  ठाउ छ ?"
        ]
    },
    collection: {
        label: [
            "Wastecollection",
            "फोहोर सङ्कलन गर्ने  गरिन्छ ?"
        ]
    },
    landfill: {
        label: [
            "Landfill",
            "फोहोर खाल्डोमा  पुर्ने  गरिन्छ ?"
        ]
    },
    other: {
        label: [
            "other",
            "अन्य थप  केहिकुरा छ ?"
        ]
    },
    other_details: {
        label: [
            "Pls specify other in details ",
            "अन्य कुरा के हो बिस्तृत मा खुलाउनुहोस  ?"
        ]
    },
    playgroundarea: {
        label: [
            "Safe playground area",
            "सुरक्षित खेल्ने ठाउँ"
        ]
    },
    playground_size: {
        label: [
            "Is there adequate space for students to play outdoor games (hint: consider size of space and whether the surface is flat)",
            "विद्यार्थीहरुलाई बिद्यालयमा बाहिरि खेल खेल्न प्रसस्त ठाउँ / खेलमैदान छ? ( ठाउको आकार र सतह समथल छ कि छैन अवलोकन गर्नुहोला)"
        ]
    },
    playground_safe: {
        label: [
            "The playground is free from harmful/hazardous objects (eg flaky stones, rubble, building materials).",
            "उक्त खेलमैदान हानिकारक/जोखिमपूर्ण तत्वहरुबाट मुक्त अथवा सुरक्षित छ? (जस्तै: चुचे ढुंगा, भवन निर्माण सामग्री)"
        ]
    },
    playgroundfance: {
        label: [
            "What kind of fancing is there to protect the playground?",
            "खेलमैदानलाई सुरक्षित राख्न के कस्तो बार लगाएको छ?"
        ]
    },
    assembly: {
        label: [
            "Safe Assembly Space",
            "भेला हुने सुरक्षित स्थान "
        ]
    },
    assemblyspace: {
        label: [
            "Has the school  identified an assembly space (the questions below pop up when ‘yes’ is selected)",
            "यस विद्यालयमा प्रकोप/विपतको बेला भेला हुनको लागि कुनै सुरक्षित स्थानको पहिचान गरेको छ? "
        ]
    },
    assembly_adequat: {
        label: [
            "Can the assembly space accommodate all enrolled children?",
            "त्यस्तो सुरक्षित  स्थानमा सबै बालबालिकाहरु अटाउने गरि ठाउँ छ ?"
        ]
    },
    assembly_safe: {
        label: [
            "Is the assembly space free from harmful/hazardous objects (eg flaky stones, rubble, building materials).",
            "त्यस्तो सुरक्षित  तोकिएको स्थान हानिकारक/जोखिमपूर्ण तत्वहरु बाट मुक्त छ? (जस्तै: चुचे ढुंगा, भवन निर्माण सामग्री)"
        ]
    },
    assembly_access: {
        label: [
            "Is the assembly space accessible - within 50 meters from classroom block(s)",
            "त्यस्तो सुरक्षित  तोकिएको स्थान सबैको लागि पहुँचयोग्य छ ? (कक्षा कोठा देखि ५० मिटरको दुरीमा)"
        ]
    },
    drreducation: {
        label: [
            "Pillar 3:  Risk Reduction and Resilience Education  (questions below are to be included in head teacher interview or school observation surveys)",
            "खण्ड ३: जोखिम न्यूनीकरण र उत्थानसिलाता (resilience) शिक्षा (तलका प्रश्नहरु  प्रधानाध्यापक सँगको अन्तरबार्ता वा विद्यालयको अवलोकन सर्बेक्षणमा समाबेस गर्ने)"
        ]
    },
    drreducation1: {
        label: [
            "Teachers and Students trained on DRR",
            " बिपत जोखिम न्यूनीकरण (DRR) तालिम प्राप्त गरेका शिक्षक र विद्यार्थीहरु "
        ]
    },
    drr_train: {
        label: [
            "Teachers and students have been trained on DRR  (if yes to this question, then questions below appear)",
            "शिक्षक र बिद्दार्थीले DRR सम्बन्धि तालिम प्राप्त गरेका छन् ? "
        ]
    },
    drr_drill: {
        label: [
            "Teachers and students have participate in disaster preparedness drills / simulations",
            "शिक्षक र विद्यार्थीहरु बिपत पूर्व तयारीको अभ्यासमा (ड्रिलमा) सहभागी हुनु भएको थियो ?"
        ]
    },
    firstaidkit: {
        label: [
            "First Aid Training & Kit",
            "प्राथमिक उपचारको तालिम र सामाग्री"
        ]
    },
    fak_teacher_train: {
        label: [
            "Have teachers received first aid training facilitated by the Red Cross  (if yes to this question, then questions below appear)?",
            "यस बिद्यालयका शिक्षकहरुले रेडक्रसले संचालन गरेको प्राथमिक उपचारको तालिम लिनु भएको छ? "
        ]
    },
    fak_student_train: {
        label: [
            "Have students received first aid training facilitated by the Red Cross?",
            "यहाका विद्यार्थीहरुले रेडक्रसले संचालन गरेको प्राथमिक उपचारको तालिम लिनु भएको छ? "
        ]
    },
    fak_team: {
        label: [
            "Has a first aid team been formed and responds to first aid needs of students / teachers?",
            "यस बिद्यालयमा प्राथमिक उपचारको टोलि गठन गरिएको छ ? र विद्यार्थी/शिक्षकलाई आवस्यक पर्दा प्राथमिक उपचारको सेवा प्रदान गर्छ ?"
        ]
    },
    fak_eqqiped: {
        label: [
            "Do the school have an equipped first aid kit?",
            "विद्यालयमा प्राथमिक उपचारको सामाग्री छन् ?"
        ]
    },
    fak_accessible: {
        label: [
            "Is the first aid kit is accessible (ask to see it)at need?",
            "प्राथमिक उपचारको सामाग्रीहरु आवश्यक परेको बेलामा पहुँचयोग्य छन ? (अवलोकन गर्ने) "
        ]
    },
    drr_awareness: {
        label: [
            "Awareness Raising Events on DRR",
            "विपद जोखिम न्यूनीकरणको जनचेतना अभिबृद्धि कार्यक्रम"
        ]
    },
    drr_iec: {
        label: [
            "ARE the DRR IEC materials  on display in the school (eg  display boards, fliers, posters, jingles, wall painting, etc)?",
            "बिद्यालयमा DRR IEC सामग्रीहरु प्रदर्शित छन् अथवा देखिने गरि राखिएको छ ?  (जस्तै  प्रदर्सनी बोर्ड, पोस्टर, भित्ते पेन्टिङ आदि)"
        ]
    },
    drr_bcc: {
        label: [
            "Does the school promot DRR messages through behaviour change and communication methodologies (eg drama, singing competition, school assembly announcements)?",
            "व्यवहार परिवर्तन तथा संचार (BCC) बिधिबाट  यस विद्यालयले DRR सम्बन्धि सन्देशहरु प्रबर्धन गर्छ ? (जस्तै नाटक, गायन प्रतियोगिता, विद्यालयको भेलामा सूचना प्रवाह) ?"
        ]
    },
    drr_participate: {
        label: [
            "Are the students participate in DRR awareness raising activities?",
            "बिद्दार्थीहरु विपद जोखिम न्यूनीकरणको जनचेतना अभिबृद्धि कार्यक्रममा सहभागी भएका छन्?"
        ]
    },
    after_club: {
        label: [
            "Does the school have an after-school club (child club) supporting DRR awareness raising activities?",
            "यस विद्यालय विपद जोखिम न्यूनीकरणको जनचेतना अभिबृद्धि कार्यक्रममा साहयता पुर्याउने बाल क्लब ( after-school club) छ?"
        ]
    },
    drr_curriculum: {
        label: [
            "School Curriculum includes DRR",
            "विद्यालयको पाठ्यक्रममा DRRको समाबेस"
        ]
    },
    drr_curri_exist: {
        label: [
            "Has the school integrated DRR into curriculum  (if yes to this question, then questions below appear)?",
            "विद्यालयले DRR लाई पाठ्यक्रममा समाबेस गरेको छ? (यदी यो प्रश्नको उत्तर हो भने तल प्रश्न हरु आउनेछन)"
        ]
    },
    toolkits_drr: {
        label: [
            "Do the teachers have access to toolkits, exemplars or other learning materials focusing on DRR?",
            "यस विद्यालयका शिक्षकहरुको पहुँचमा DRR केन्द्रित विभिन्न उपकरण,  नमुना वा अन्य सिकाईका सामग्रीहरु छन् ?"
        ]
    },
    eca_drr: {
        label: [
            "Does the school support extra curricula activities for students which include DRR components ?",
            "विद्यार्थीको लागि DRR संग सम्बन्धित बिषय समाबेस गरि अतिरिक्त क्रियाकलाप सन्चालन गर्न विद्यालयले सहयोग गर्छ ?"
        ]
    },
    materials_drr: {
        label: [
            "Does the classrooms have locally developed DRR materials on display?",
            "स्थानीय रुपमा तयार गरिएका DRR सम्बन्धि सामग्री कक्षाकोठामा प्रदर्शित छन् ?"
        ]
    },
    outcome3: {
        label: [
            "#/% schools incorporating SBDRM into SIP",
            ""
        ]
    },
    outcome4: {
        label: [
            "#/% schools with basic handwashing facilities ",
            ""
        ]
    },
    water_sufficiency: {
        label: [
            "Does the school have sufficient water? (Observe)",
            "के विद्यालयमा प्रसस्त पानी छ?"
        ]
    },
    water_toilet_running: {
        label: [
            "Is there running water facility in toilet  (especially for children)? (Observe) ",
            "के सौचालयमा पानी आउने धाराको  सुबिधा छ? (बिशेषगरि बालबालिकाहरुको लागि)"
        ]
    },
    handwash_point: {
        label: [
            "Is there designated hand washing point with water facility? (Observe)",
            "के बिद्यालयमा कुनै पानीको सुबिधा सहितकोको हातधुने स्थान तोकिएको छ ?"
        ]
    },
    handwash_soap: {
        label: [
            "Is there soap available for handwashing at the hand washing point? (observe)",
            "के हात धुन तोकिएको स्थानमा साबुनको ब्यबथा गरिएको छ ?"
        ]
    },
    handwash_footprint: {
        label: [
            "Is there footprint and nudges printed to direct children to hand washing point? (Observe) ",
            "के बालबालिकाहरु हात धुने स्थानसम्म पुग्न निर्देशित गर्ने पदचाप वा चित्रहरु कोरिएका छन् ?"
        ]
    },
    outcome5: {
        label: [
            "# / % of schools with improved prioritized minimum enabling conditions (PMEC) results.",
            ""
        ]
    },
    teacher_available: {
        label: [
            "Is there qualified teachers available for all classes?",
            "के त्यहाँ सबै कक्षाको लागि योग्य शिक्षकहरु छन्?"
        ]
    },
    textbook_available: {
        label: [
            "Do all children have textbook?",
            "के सबै बालबालिकाको पाठ्य पुस्तिका छ?"
        ]
    },
    classroom_available: {
        label: [
            "Is there safe classroom available for all grades?",
            "के सबै कक्षाकोलागि सुरक्षित कक्षाकोठा छ?"
        ]
    },
    bookcorner_estd: {
        label: [
            "Is there book corner with supplimentary books established in each classroom?",
            "के प्रत्येक कक्षाकोठामा पुरक किताबहरु भएको किताब कुना निर्माण गरिएको छ?"
        ]
    },
    girls_toilet: {
        label: [
            "Is there separate laterine for girls with handwashing facility?",
            "के बालिकाहरुकोलागि हातधुने सुबिधासहितको  छुट्टै सौचालय छ?"
        ]
    },
    smc_start: {
        label: [
            "SMC",
            ""
        ]
    },
    sdmc: {
        label: [
            "School Disaster Management Committee",
            "विद्यालय प्रकोप ब्यबस्थापन समिति"
        ]
    },
    sdmc_formed: {
        label: [
            "Has a School Disaster Management Committee formed and is operating  (if yes to this question, then questions below appear)?",
            "विद्यालय बिपत ब्यबस्थापन समिति गठन भई कार्य संचालन गरि रहेको आएको छ? (यदी यो प्रश्नको उत्तर हो भने तल प्रश्न हरु आउनेछन)"
        ]
    },
    sdmc_female: {
        label: [
            "Does the School Disaster Management Committee members include at least one female?",
            "विद्यालय बिपत ब्यबस्थापन समितिमा कम्तिमा एक जना महिला सदस्य हुनुहुन्छ ?"
        ]
    },
    sdmc_dalit: {
        label: [
            "Does the School Disaster Management Committee members include at least one dalit (low cast)?",
            "विद्यालय बिपत ब्यबस्थापन समितिमा कम्तिमा एक जना दलित सदस्य हुनुहन्छ ?"
        ]
    },
    sdmc_student: {
        label: [
            "Does the School Disaster Management Committee members include student representation?",
            "विद्यालय प्रकोप ब्यबस्थापन समितिमा विद्यार्थीको प्रतिनिधि राखिएको छ?"
        ]
    },
    sdmc_training: {
        label: [
            "Do the School Disaster Management Committee focal point of the SMC have received training on DRM?",
            "बिद्यालय बिपत व्यवस्थापन समितिको लागि विद्यालय व्यवस्थापन समितिले तोकेको मुख्य जिम्बेवार व्यक्ति (focal point) ले बिपत जोखिम व्यवस्थापन (DRM) को तालिम लिनु भएकोछ?"
        ]
    },
    sdmc_meet: {
        label: [
            "Do the School Disaster Management Committee meet at least once every two months?",
            "विद्यालय बिपत ब्यबस्थापन समितिको बैठक  कम्तिमा दुइ महिनाको एक पटक बस्छ ?"
        ]
    },
    sdmc_minutes: {
        label: [
            "Do the School Disaster Management Committee maintain a record of meetings (meeting minutes)?",
            "विद्यालय बिपत ब्यबस्थापन समितिले बैठकका निर्णयहरु लिखित रुपमा राख्छ (माईनुट)?"
        ]
    },
    sbdrm: {
        label: [
            "School Based Disaster Risk Management   (SBDRM)",
            "विद्यालयमा आधारित बिपत जोखिम ब्यबस्थापन"
        ]
    },
    sbdrm_plan: {
        label: [
            "Does the school have a Disaster Risk Management plan developed by the SMC  (if yes to this question, then questions below appear)?",
            "यस बिद्यालयमा SMC ले तयार गरेको बिपत जोखिम ब्यबस्थापनको योजना छ? "
        ]
    },
    smc_assessment: {
        label: [
            "Has the SMC supported a risk assessment of the school?",
            "SMCले  विद्यालयको जोखिम मुल्यांकनमा सयोग पुराएको छ?"
        ]
    },
    sbdrm_sip: {
        label: [
            "Does the SMC update SBDRM plan annually, integrating with the school improvement plan (SIP)?",
            "SMCले SBDRM योजनालाई विद्यालय सुधार योजना (SIP) मा एकीकृत गरि बार्षिक रुपमा अध्यावधिक (अपडेट) गर्ने  गर्छ ?"
        ]
    },
    drm_budget: {
        label: [
            "Does the SMC allocate budget to DRM in the SIP?",
            "SMCले विद्यालय सुधार योजनामा  DRMलाई बजेट छुट्याउछ ?"
        ]
    },
    sbdrm_support: {
        label: [
            "Does the SBDRM plan include provisions for psycho-social support in the advent of a disaster?",
            "SBDRM योजनामा बिपतको अवस्थामा मनो-सामाजिक साहयता दिने प्रावधान समाबेस गरिएको छ?"
        ]
    },
    smc_drill: {
        label: [
            "Does SMC support the school to conduct evacuation drills with students (at least once annually)?",
            "SMCले बिद्यालयलाई बिद्दार्थीहरु संग मिलेर बिपतको बेलामा कसरि बच्ने र बचाउने भनी पूर्व अभ्यास गर्न सहायता गर्छ? (कम्तिमा बर्षमा एक चोटी)"
        ]
    },
    household_plan: {
        label: [
            "Does SMC support parents/caregivers of students to develop a household disaster risk management plan?",
            "विद्यार्थीको घर/परिवारमा बिपत जोखिम ब्यबस्थाप योजना बनाउन SMCले  अभिभावकलाई साहयता गर्छ ?"
        ]
    },
    sdrm: {
        label: [
            "Pillar 2:  School Disaster Management  ",
            "खण्ड २: विद्यालय विपद ब्यबस्थापन"
        ]
    },
    smc_form: {
        label: [
            "School Management Committee",
            "विद्यालय ब्यबस्थापन समिति"
        ]
    },
    smc_formed: {
        label: [
            "Has a School Management Committee (SMC) been formed (constituted) and is operating  (if yes to this question, then questions below appear)",
            "विद्यालय ब्यबस्थापन समिति गठन भई कार्य संचालनमा गरि रहेको छ? "
        ]
    },
    smc_female: {
        label: [
            "Does the SMC members include at least one female",
            "विद्यालय ब्यबस्थापन समितिमा कम्तिमा एक जना महिला सदस्य हुनुहुन्छ ?"
        ]
    },
    smc_dalit: {
        label: [
            "Does the SMC members include at least one dalit (low caste)?",
            "विद्यालय ब्यबस्थापन समितिमा कम्तिमा एक जना दलित सदस्य हुनुहुन्छ छ?"
        ]
    },
    meeting_freq: {
        label: [
            "How often does the SMC hold meeting?",
            "सामान्यतया विद्यालय ब्यबस्थापन समितिको वैठक कति समयको अन्तरलमा बस्ने गरेको छ ? "
        ]
    },
    smc_minutes: {
        label: [
            "Does the SMC maintains a record of meetings (meeting minutes)",
            "विद्यालय ब्यबस्थापन समितिले बैठकका निर्णयहरु लिखित रुपमा राख्छ (माईनुट)?"
        ]
    },
    smc_trainig: {
        label: [
            "Do the SMC members have received child protection training",
            "विद्यालय ब्यबस्थापन समितिका सदस्यहरुले बाल संरक्षणको तालिम लिनु भएको छ?"
        ]
    },
    smc_scpc: {
        label: [
            "Do the school have a School Child Protection Committee (SCPC) operating within the oversight of the SMC?",
            "विद्यालयमा विद्यालय ब्यबस्थापन समितिको निरिक्षणमा विद्यालय बाल संरक्षण समिति संचालनमा रहेको छ?"
        ]
    },
    complain_box: {
        label: [
            "Does the SMC manage a message/complaints box that allows children to anonymously",
            "विद्यालय ब्यबस्थापन समितिले बालबालिकाहरुको पहिचान नखुल्ने गरि उनीहरुको संदेश/गुनासो संकलन गर्न बाकस वा पेटिकाको व्यवस्था गरिएको छ?"
        ]
    },
    complain_response: {
        label: [
            "Does the SCPC or SMC have a process for responding to complaints concerning child protection?",
            "विद्यालय बाल संरक्षण समिति वा विद्यालय ब्यबस्थापन समितिको बाल संरक्षण सम्बन्धि आएका गुनासोहरुको सम्बोधन गर्ने कुनै प्रक्रिया छ ?"
        ]
    },
    school_tool: {
        label: [
            "School tool (Interview with teachers)",
            "शिक्षक संगको अन्तरवार्ता"
        ]
    },
    outcome1: {
        label: [
            "% of trained teachers using the skills acquired to teach reading",
            "% of trained teachers using the skills acquired to teach reading"
        ]
    },
    training_received: {
        label: [
            "Have you received training on Teaching Learning Methodogy 1 and 2  from WV ?",
            "तपाईले WV बाट शिक्षण सिकाई पद्दति १ र २ को तालिम लिनुभएको छ?"
        ]
    },
    module_completion: {
        label: [
            "Have you completed at least 75% sessions in each module?",
            "तपाईले प्रत्येक मोडुलको कम्तिमा ७५% सेसन पुरा गर्नु भयो?"
        ]
    },
    skills_used: {
        label: [
            "What skills are you using out of 5 core skills to teach reading to children? ",
            "बालबालिकालाई अध्यापन गर्दा पढाई सिकाईका ५ सिप मध्ये तपाइले कुन कुन सिपको प्रयोग गर्ने गर्नुभएको छ ? "
        ]
    },
    reasons_noskills: {
        label: [
            "If all skills are not used, please mention the reason?",
            "यदी सबै ५ सिपहरु प्रयोग भएको छैन भने, त्यो किन होला कृपया कारणहरु बताई दिनुहोस न |"
        ]
    },
    other_reasons: {
        label: [
            "What are the other reasons",
            "अन्य कारण के होलान हुन् |"
        ]
    },
    reasons_teachread: {
        label: [
            "What could be done to make teaching reading more effective? ",
            "सिकाइ पढाइ अझै प्रभावकारी गराउन केके गर्नुपर्छ?"
        ]
    },
    outcome2: {
        label: [
            " % reading clubs achieving minimum standards",
            " % reading clubs achieving minimum standards"
        ]
    },
    readingcamp: {
        label: [
            "Reading camp facilitator",
            "पढाई शिबिर सहजकर्ता"
        ]
    },
    rc_safe: {
        label: [
            "Is the location of reading club safe? ",
            "पढाई क्लब रहेको स्थान सुरक्षित छ?"
        ]
    },
    re_disturb: {
        label: [
            "Do children get distracted from surrounding environment? ",
            "के बाहिरी वातावरणले बालबालिको ध्यानमा बाधा पुर्याउछ ?"
        ]
    },
    rc_attendance: {
        label: [
            "Has the reading club maintained attendance register?",
            "के पढाई क्लबले  हाजिरी पुस्तिका अध्यावधिक गर्ने गरेको छ?"
        ]
    },
    re_basicmaterial: {
        label: [
            "Is the reading club equipped with basic materials? (seating, make & take journals)",
            "के पढाई क्लब आधारभूत सामग्रीले भरिपूर्ण छ? (बसाई, जर्नल,  बनाउने र लैजाने सामग्रीहरु)"
        ]
    },
    book_accessiblity: {
        label: [
            "Is the book bank accessible to the children?",
            "के किताब बैंक बालबालिकाहरुको पहुँचमा छ?"
        ]
    },
    materials_display: {
        label: [
            "Has the learning materials (eg. books, charts, cards) properly displayed?",
            "के सिकाई सामग्री (जस्तै: किताब, कार्ड, चार्ट) ठिक तरिकाले प्रदर्सन गरिएको छ?"
        ]
    },
    book_register: {
        label: [
            "Has the reading club maintained book bank register?",
            "के पढाई क्लबले  किताब बैंकको लगबुक अध्यावधिक गर्ने गरेको छ?"
        ]
    },
    attendence_percent: {
        label: [
            "Does the attendance of children exceed 90% in the previous week?",
            "के बालबालिकाहरुको उपस्थिति अघिल्लो हप्ता  ९०%  छ?"
        ]
    },
    materials_locallg: {
        label: [
            "Are there learning materials developed in local language? ",
            "के सिकाई सामग्री स्थानीय भाषामा बनाइएको छ?"
        ]
    },
    agenda_display: {
        label: [
            "Is the agenda for reading club session displayed/visible?",
            "के पढाई क्लबको सेसन एजेण्डा सबैले देख्नेगरि राखिएको छ ?"
        ]
    },
    threeday_training: {
        label: [
            "Have you received 3 days initial training on reading club management?  ",
            "के तपाईले पढाई शिबिर ब्यबस्थापन सम्बन्धि ३ दिने प्रारम्भिक तालिन लिनु भएको छ?"
        ]
    },
    rc_lb_standard: {
        label: [
            "Has the reading club received all books as per LB standard?",
            "के पढाई क्लबले मापदण्ड अनुसारको सबै किताबहरु पाएको छ?"
        ]
    },
    agenda_prepare: {
        label: [
            "Do you have practice of preparing agenda for reading club session?",
            "के तपाईले पढाई क्लबको सेसन  एजेन्डा तयार पार्ने गर्नु भएको छ ?"
        ]
    },
    parents_support: {
        label: [
            "Are parents of the reading club children supportive?",
            "के पढाई क्लबमा आउने बालबालिकाका अभिभावकहरु  सहयोगी छन्?"
        ]
    },
    sustaining_plan: {
        label: [
            "Do you have a plan for sustaining this reading club?",
            "के यो पढाई क्लबलाई दिगो बनाउने योजना बनाउनु भएको छ?"
        ]
    },
    outcome6: {
        label: [
            " Proportion of users who are satisfied with the education services they have received ",
            ""
        ]
    },
    parents_group: {
        label: [
            "",
            ""
        ]
    },
    children_school: {
        label: [
            "Does your son/daughter study in School? (If no, replace with next sample)",
            "के तपाइको छोरा/छोरि विद्यालयमा पढ्नु हुन्छ?"
        ]
    },
    satis_par_teacher: {
        label: [
            "How satisfied are you with the quality/qualification of the teacher of School?",
            "तपाई विद्यालयका शिक्षकको योग्यता प्रति  कत्तिको सन्तुष्ट हुनु हुन्छ?"
        ]
    },
    satis_par_textbook: {
        label: [
            "How satisfied are you with the availability of textbook for your son/daughter?",
            "तपाई आफ्ना छोरा/छोरीको पाठ्यपुस्तिकाको उपलब्ध प्रति कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_par_safety: {
        label: [
            "How satisfied are you with the classroom safety of your son/daughter at school?",
            "तपाईको छोरा/छोरीले पढ्ने कक्षाकोठा सुरक्षित छन् भन्नेकुरामा  कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_par_readmater: {
        label: [
            "How satisfied are you with the availability of supplimentary books & reading materials at school?",
            "तपाईको छोरा/छोरी पढ्ने बिद्यालयमा उपलब्ध  पुरक किताब र पदाई सामग्री प्रति कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_par_girltoilet: {
        label: [
            "How satisfied are you with the management of separate laterine for girls at school?",
            "तपाई विद्यालयमा बालिकाहरुकोलागि गरिएको  सौचालय ब्यबस्था प्रति  कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_par_overall: {
        label: [
            "How do you rate the overall educational services provided in the school in scale of 1 to 10? ",
            "तपाई विद्यालयको समग्र शैक्षिक सुबिधालाई  १ देखि १० मध्ये कति नम्बर दिनुहुन्छ?"
        ]
    },
    student_group: {
        label: [
            "Student group",
            "बिद्दार्थी समुह"
        ]
    },
    grade: {
        label: [
            "Which grade are you studying in?",
            "तपाई कति कक्षामा पढ्नु हुन्छ?"
        ]
    },
    gender: {
        label: [
            "Gender of the student",
            "बिद्दार्थीको लिंग"
        ]
    },
    age: {
        label: [
            "Age of the student",
            "बिद्दार्थीको उमेर"
        ]
    },
    satis_std_teacher: {
        label: [
            "How satisfied are you with the quality/qualification of the teacher of ……. School?",
            "तपाई........विद्यालयका शिक्षकको योग्यता प्रति तपाई कत्तिको सन्तुष्ट हुनु हुन्छ?"
        ]
    },
    satis_std_books: {
        label: [
            "How satisfied are you with the availability of textbook in your class?",
            "तपाई आफ्नो कक्षामा पाठ्यपुस्तिकाको उपलब्धता प्रति कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_std_classroom: {
        label: [
            "How satisfied are you with the safety of your classroom ?",
            "तपाई आफ्नो कक्षाकोठाको सुरक्षा प्रति कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_std_readmater: {
        label: [
            "How satisfied are you with the book cornor established in your classroom with supplimentary books & reading materials?",
            "तपाईको कक्षाकोठामा उपलब्ध पुरक किताब र पढाई सामग्री बारे कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_std_girltoilet: {
        label: [
            "How satisfied are you with the management of separate laterine for girls at school?",
            "तपाई विद्यालयमा बालिकाहरुकोलागि गरिएको  सौचालय ब्यबस्था प्रति  कत्तिको सन्तुष्ट हुनुहुन्छ?"
        ]
    },
    satis_std_overall: {
        label: [
            "How do you rate the overall educational services provided in the school in scale of 1 to 10? ",
            "तपाई विद्यालयको समग्र शैक्षिक सुबिधालाई  १ देखि १० मध्ये कति नम्बर दिनुहुन्छ?"
        ]
    },
    health: {
        label: [
            "Health related questions",
            "स्वास्थ्य सम्बन्धि प्रश्न"
        ]
    },
    u5_number: {
        label: [
            "How many U5 children do you have your own?",
            "पाँच बर्ष मुनिको बालबालिका कति छन्?"
        ]
    },
    name: {
        label: [
            "Name of the child?",
            "बाल/बालिका को नाम:"
        ]
    },
    sex_child: {
        label: [
            "What is the sex of $$ ?",
            " $$ को लिंग:"
        ]
    },
    DOB: {
        label: [
            "Child must be 0-59 months",
            "बच्चा ०-५९ महिनाको हुनु पर्छ"
        ]
    },
    height_child: {
        label: [
            "What  is the height/length in centimeter of $$? (if child is upto 23 months measure the child lying down and if the child is older than 23 months, measure the child standing up. ",
            "$$  को लम्बाई सेन्टीमिटरमा कति छ?  "
        ]
    },
    health_goal_2_3: {
        label: [
            "What is weight in kilograms?",
            "को किलोग्राममा कति तौल छ?"
        ]
    },
    health_goal_4: {
        label: [
            "Prevalance of disease and infection among under five children especially pneumonia (C1B.0091)",
            "Prevalance of disease and infection among under five children especially pneumonia (C1B.0091)"
        ]
    },
    cough_child: {
        label: [
            "At any time in the last two weeks, has $$ had an illness with a cough? ",
            "बिगत २ हप्तामा भित्रमा, $$ लाई रुघा/खोकी लागेको बेलामा अरु कुनै बिरामी भएको थियो?"
        ]
    },
    breath_child: {
        label: [
            "When $$ had an illness with a cough, did he/she breathe faster than usual with short, rapid breaths or have difficulty breathing? ",
            "त्यसरी $$ लाई रुघा/खोकी लागेर बिरामी भएको बेलामा, सधै भन्दा छिटो छिटो सास फेर्ने, सास फेर्न गारो हुने भएको थियो?"
        ]
    },
    chest_child: {
        label: [
            "Was the fast or difficult breathing due to a problem in the chest or a blocked or runny nose? ",
            "त्यसरी सास फेर्न असजिलो वा गारो हुनाको कारण छातीको समस्याले अथवा नाक बन्द भएर वा सिंगान बग्ने भएर हो ?"
        ]
    },
    cough_drink: {
        label: [
            "I would like to know how much $$ was given to drink during the illness with cough, including breast milk. Was he/she given less than usual to drink, about the same amount, or more than usual? ",
            "रुघा/खोकी लागेर बिरामी परेको बेलामा कत्तिको मात्रामा झोलिलो कुरा (आमाको दुध सहित)  दिईएको थियो? उनलाई अघि पछी भन्दा कम वा उत्तिकै मात्रामा अथवा सामान्य भन्दा बढी मात्रामा दिएको थियो ?"
        ]
    },
    cough_eat: {
        label: [
            "I would like to know how much $$ was given to eat during the illness with cough, was he/she given less than usual to eat, about the same amount, or more than usual? ",
            "रुघा/खोकी लागेर बिरामी परेको बेलामा कत्तिको मात्रामा खाने कुरा दिईएको थियो? उनलाई अघि पछी भन्दा कम वा उत्तिकै मात्रामा अथवा सामान्य भन्दा बढी मात्रामा दिएको थियो ?"
        ]
    },
    fever_child: {
        label: [
            "In the last two weeks, has $$ been ill with a fever at any time? ",
            "बिगत २ हप्ता भित्रमा, $$ लाई कुनै बेला ज्वोरो आएको थियो ?"
        ]
    },
    fever_child_drink: {
        label: [
            "I would like to know how much $$ was given to drink during the illness with fever, including breast milk. Was he/she given less than usual to drink, about the same amount, or more than usual? ",
            "ज्वरो आएर बिरामी परेको बेलामा कत्तिको मात्रामा झोलिलो कुरा (आमाको दुध सहित)  दिईएको थियो? उनलाई अघि पछी भन्दा कम वा उत्तिकै मात्रामा अथवा सामान्य भन्दा बढी मात्रामा दिएको थियो ?"
        ]
    },
    fever_child_eat: {
        label: [
            "I would like to know how much $$ was given to eat during the illness with fever, was he/she given less than usual to eat, about the same amount, or more than usual?",
            "ज्वरो आएर बिरामी परेको बेलामा कत्तिको मात्रामा खाने कुरा दिईएको थियो? उनलाई अघि पछी भन्दा कम वा उत्तिकै मात्रामा अथवा सामान्य भन्दा बढी मात्रामा दिएको थियो ?"
        ]
    },
    health_outcome12: {
        label: [
            "% of children aged 0–59 months with a ‘presumed pneumonia’ (ARI) episode in the past two weeks that were taken to an appropriate health-care provider   (C1B.0072)",
            "% of children aged 0–59 months with a ‘presumed pneumonia’ (ARI) episode in the past two weeks that were taken to an appropriate health-care provider   (C1B.0072)"
        ]
    },
    chest_treatment: {
        label: [
            "Did you seek any advice or treatment for the illness from any source? ",
            "त्यसरी छोरा छोरी बिरामी परेको बेलामा कहिँ कतै बाट कुनै सल्लाह सुझाव वा उपचार लिनुभयो? "
        ]
    },
    chest_treat_center: {
        label: [
            "From where did you seek advice or treatment?",
            "कहाँ कहाँ बाट सल्लाह सुझाव वा उपचार लिनुभयो ?"
        ]
    },
    health_goal_5: {
        label: [
            "Prevalance of disease and infection among under five children especially Diarrhoea (C1B.0087)",
            "Prevalance of disease and infection among under five children especially Diarrhoea (C1B.0087)"
        ]
    },
    diarrhoea_child: {
        label: [
            "In the last two weeks, has $$ had diarrhoea? ",
            "बिगत २ हप्ता भित्रमा, $$ लाई झाडा पखाला लागेको थियो?"
        ]
    },
    diarrhoea_drink: {
        label: [
            "I would like to know how much $$ was given to drink during the diarrhoea, including breast milk. Was he/she given less than usual to drink, about the same amount, or more than usual? ",
            "झाडा पखाला लागेको बेलामा कत्तिको मात्रामा झोलिलो कुरा (आमाको दुध सहित)  दिईएको थियो? उनलाई अघि पछी भन्दा कम वा उत्तिकै मात्रामा अथवा सामान्य भन्दा बढी मात्रामा दिएको थियो ?"
        ]
    },
    diarrhoea_eat: {
        label: [
            "I would like to know how much $$ was given to eat during the diarrhoea, was he/she given less than usual to eat, about the same amount, or more than usual? ",
            "झाडा पखाला लागेको बेलामा कत्तिको मात्रामा खाने कुरा दिईएको थियो? उनलाई अघि पछी भन्दा कम वा उत्तिकै मात्रामा अथवा सामान्य भन्दा बढी मात्रामा दिएको थियो ?"
        ]
    },
    health_outcome13: {
        label: [
            "Proportion of children under 5 with diarrhoea who received correct management of diarrhoea  (C1B.0067)",
            "Proportion of children under 5 with diarrhoea who received correct management of diarrhoea  (C1B.0067)"
        ]
    },
    diarrhoea_fluid_spcl: {
        label: [
            "During the episode of diarrhoea, was $$ given to drink a fluid made from a special packet called (local name for ORS packet solution)? ",
            "झाडा पखाला लागेको बेलामा $$ लाई जीबन जल, पुनर्जलीय झोल जस्ता केहि पिउने कुरा बनाएर खान दिनु भएको थियो?"
        ]
    },
    diarrhoea_fluid_prepak: {
        label: [
            "During the episode of diarrhoea, was $$ given to drink a pre-packaged ORS fluid for diarrhoea? ",
            "झाडा पखालाको अवधि $$ लाइ केहि पिउने ORS packet solution बनाएर दिनु भएको थियो?"
        ]
    },
    diarrhoea_fluid_bygovt: {
        label: [
            "*During the episode of diarrhoea, was $$ given to drink a government recommended homemade fluid X? * ",
            "झाडा पखालाको अवधि $$ लाइ केहि पिउन सरकारबाट सिफारिस गरेको घरायसी पानी बनाएर दिनु भएको थियो?"
        ]
    },
    health_goal_6: {
        label: [
            "Prevalence of Low Birth Weight Baby (Birth Weight less than 2500 grams)  (C1A.21338)",
            "Prevalence of Low Birth Weight Baby (Birth Weight less than 2500 grams)  (C1A.21338)"
        ]
    },
    birthweight_taken: {
        label: [
            "Was $$ weighed at birth?",
            "जन्मेको बेलामा $$ को तौल लिएको थियो?"
        ]
    },
    birth_weight: {
        label: [
            "How much did $$ weigh? (If a card is available, record weight from card.)",
            "जन्मेको बेला $$ को तौल कति थियो? (कार्ड छ भने कार्ड बाट तौल टिप्नुहोला)"
        ]
    },
    weight_record: {
        label: [
            "Was birth weight from health card or mother’s recall? Do Not read question",
            "जन्मेको बेलाको तौल स्वस्थ कार्ड बाट टिपेको हो कि आमाले भनेर टिपिएको हो ? "
        ]
    },
    u5member_end: {
        label: [
            "",
            ""
        ]
    },
    pregnant_women: {
        label: [
            "",
            ""
        ]
    },
    health_goal_7: {
        label: [
            "Weight gain based on gestational period Low MUAC (< 23 cm) ",
            "Weight gain based on gestational period Low MUAC (< 23 cm) "
        ]
    },
    muac: {
        label: [
            "What is the MUAC size in centemeter? (use appropriate measurement tool)(record in cm)",
            "MUAC साईज सेन्टीमिटर मा कति हो?"
        ]
    },
    pregnant_weight: {
        label: [
            "What is your weight? (for pregnant women) (measure it)",
            ""
        ]
    },
    pregnant_height: {
        label: [
            "What is your height? For pregnant women (measure it)",
            ""
        ]
    },
    health_outcome_1: {
        label: [
            "Proportion of mothers who report that they had four or more antenatal visits while they were pregnant  and to be disaggregated by age (C1C.0156) (Name is used to remind the interviewer to use the name of the youngest living child.)",
            "Proportion of mothers who report that they had four or more antenatal visits while they were pregnant  and to be disaggregated by age (C1C.0156) (Name is used to remind the interviewer to use the name of the youngest living child.)"
        ]
    },
    anc_visit: {
        label: [
            "Did you see anyone for antenatal care during your pregnancy with ?",
            ""
        ]
    },
    health_provider: {
        label: [
            "Whom did you see? (Probe: Anyone else? Check all that apply.)",
            "कस कसलाई गर्भ जाच को लागि देखाउनु भएको थियो ?"
        ]
    },
    health_provider_o: {
        label: [
            "Who was that other service provider?",
            "अन्य सेवा प्रदायक को थिए?"
        ]
    },
    anc_times: {
        label: [
            "How many times did you see him/ her/them for ANC check-up in total?",
            "गर्बाअवस्थामा त्यस्तो (ANC) जाच जम्मा कति पटक गराउनु भयो ?"
        ]
    },
    health_outcome5: {
        label: [
            "Proportion of pregnant women who consume iron and folic acid tablet during pregnancy (C1A.0032)",
            "Proportion of pregnant women who consume iron and folic acid tablet during pregnancy (C1A.0032)"
        ]
    },
    iron_taken: {
        label: [
            "During your last pregnancy, did you receive any iron/folate supplements?",
            "अन्तिम पटक गर्भवती हुँदा, तपाइले आइरन/फोलिक चक्की खानु भयो?"
        ]
    },
    iron_compliance: {
        label: [
            "If yes, how many iron/folic tablets did you take? ",
            "तपाईले जम्मा कतिवटा  चक्की खानु भयो होला ?"
        ]
    },
    pregnant_end: {
        label: [
            "",
            ""
        ]
    },
    health_outcome_2: {
        label: [
            "% of mothers of children aged 0-23 months who delivered with Skilled Birth Attendant and to be disaggregated by age (C) (S) (C1C.0152)",
            ""
        ]
    },
    Under_24: {
        label: [
            "",
            ""
        ]
    },
    under_24months: {
        label: [
            "Is the child under 24 months?",
            "के  बच्चा २४ महिना भन्दा कम उमेरको हो? "
        ]
    },
    delivery_place: {
        label: [
            "Where did you deliver this child?",
            "तपाइले यो बच्चा कहाँ जन्माउनुभएको हो ?"
        ]
    },
    delivery_type: {
        label: [
            "How was the child delivered? Normal or Syrgical?",
            "जन्म कसरि भएको हो ? सामान्य प्रसुती कि अपरेसन गरेर हो?"
        ]
    },
    delivery_helper: {
        label: [
            "Who assisted with the delivery? ",
            "कसको सहयोगमा जन्माउनु भएको थियो ?"
        ]
    },
    health_outcome_3: {
        label: [
            "Proportion of mothers of children aged 0–23 months who received at least 2 post-natal visit from a trained health care worker during the first week after birth (C) (S) (C1C.0160)",
            "Proportion of mothers of children aged 0–23 months who received at least 2 post-natal visit from a trained health care worker during the first week after birth (C) (S) (C1C.0160)"
        ]
    },
    pnc_visit: {
        label: [
            "After you gave birth to $$, did anyone check on your or your baby\'s health during that first week? ",
            "$$,लाई जन्म दिएपछि, पहिलो हप्ता भित्रमा कसैले तपाई तथा तपाइको बच्चाको स्वास्थ्य जाच गर्नु भएको थियो ?"
        ]
    },
    pnc1_visittime: {
        label: [
            "How long after that delivery did the first check take place? ",
            "डेलिभरी भएको कति समय पछि पहिलो पटक त्यस्तो स्वास्थ्य जाच गराउनुभएको थियो ?"
        ]
    },
    pnc1_provider: {
        label: [
            "Who checked on your health at that time? ",
            "त्यति बेलामा कसले तपाईको स्वस्थ जाच गर्नु भएको थियो?"
        ]
    },
    pnc2_visit: {
        label: [
            "Had you been for second PNC visit within one week of the dilivery? ",
            "प्रसूतिको एक हप्ता भित्रमा दोश्रो पटक पनि PNC जाच गराउनुभएको थियो? "
        ]
    },
    pnc2_visittime: {
        label: [
            "How long after that delivery did the second check take place? ",
            "डेलिभरी भएको कति समय पछि दोश्रो पटक त्यस्तो स्वास्थ्य जाच गराउनुभएको थियो ?"
        ]
    },
    pnc2_provider: {
        label: [
            "Who checked on your health at that time? ",
            "त्यो दोश्रो पटकमा कसले तपाईको स्वस्थ जाच गर्नु भएको थियो?"
        ]
    },
    pnc2_babycheck: {
        label: [
            "Did they also check the baby during this second visit? ",
            "के दोस्रो पटकको स्वास्थ्य जाचमा  बच्चाको पनि जाच गरेको थियो?"
        ]
    },
    health_outcome4: {
        label: [
            "Proportion of women who increased food consumption during most recent pregnancy  (C1A.0048)",
            "Proportion of women who increased food consumption during most recent pregnancy  (C1A.0048)"
        ]
    },
    food_pregnancy: {
        label: [
            "Did you eat extra food when you were pregnant with $$? Was it less than usual, or same as usual, or more than usual?",
            " $$ गर्भमा रहेको बेलामा  तपाईले थप खाने कुरा कत्तिको खाने गर्नु भएको थियो ? सामान्य अवस्थामा भन्दा कम अथवा सामान्य अवस्थामा जस्तै  वा सो भन्दा  बढी खाने कुरा खाने गर्नु भएको थियो ?"
        ]
    },
    lessfood_reasons: {
        label: [
            "Why did you eat less than usual during pregnancy?",
            "किन सामान्य अवस्थामा भन्दा पनि कम मात्रामा खाने कुरा खानु भएको होला?"
        ]
    },
    health_outcome6: {
        label: [
            "Proportion of children under 2 years receiving early initiation of breastfeeding (early initiation of BF)  (C1A.0033)",
            "Proportion of children under 2 years receiving early initiation of breastfeeding (early initiation of BF)  (C1A.0033)"
        ]
    },
    ever_breastfeed: {
        label: [
            "Did you ever breastfeed $$? ",
            "तपाईले बच्चालाई स्तनपान गराउनु भएको थियो ?"
        ]
    },
    first_breastfeed: {
        label: [
            "How long after birth did you first breast feed $$?",
            "बच्चा जन्मिएको कति समय पछि पहिलो पटक स्तनपान गराउनु भयो?"
        ]
    },
    pre_lacteal: {
        label: [
            "Did you feed $$ with any pre-lacteal feed before nitiation of breast milk on the first day of birth? (Example – ghee, honey, etc) ",
            "तपाईले बच्चा जन्मिएको दिन पहिलो पटक स्तनपान गराउनु अघि केहि कुरा खुवाउनु भयो?"
        ]
    },
    current_breastfed: {
        label: [
            "Is baby still being breastfed?",
            "$$लाइ अझै स्तनपान गराउदै हुनुहुन्छ?"
        ]
    },
    complimentary: {
        label: [
            "Please clarify to the respondent, I am interested in whether $$ had the item even if it was combined with other foods.",
            " बिगत २४ घण्टा भित्रमा $$ लाई के के खाने कुरा खुवाउनु भयो ? "
        ]
    },
    drink_water: {
        label: [
            "Did baby drink plain water yesterday, during the day or night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई  पानी पीलाउनु भयो? "
        ]
    },
    drink_formula: {
        label: [
            "Did baby drink infant formula yesterday, during the day or the night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई बाल पोषण (lactogen) खुवाउनु भयो? "
        ]
    },
    formula_times: {
        label: [
            "How many times did baby drink infant formula? ",
            "कति पटक बाल पोषण  (lactogen) खुवाउनु भयो? "
        ]
    },
    drink_milk: {
        label: [
            "Did baby drink milk, such as tinned, powdered or fresh animal milk yesterday, during the day or night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई दुध पीउन दिनु भयो (जस्तै टिनको, पाउडर वा शुद्ध दुध)?"
        ]
    },
    milk_times: {
        label: [
            "How many times did $$ drink milk, such as tinned, powdered or fresh animal milk? ",
            " बिगत २४ घण्टा भित्रमा $$ कति पटक दुध पिलाउनु भयो (जस्तै टिनको, पाउडर वा शुद्ध दुध)?"
        ]
    },
    drink_juice: {
        label: [
            "Did $$ drink juice or juice drinks yesterday, during the day or night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई फलफूलको जुस पिलाउनु भएको थियो?"
        ]
    },

    drink_vitamin: {
        label: [
            "Did $$ drink or eat vitamin or mineral supplements yesterday, during the day or night?",
            " बिगत २४ घण्टा भित्रमा $$ लाई भिटामिन वा मिनरल भएको खाने कुरा खुवाउनु भयो?"
        ]
    },
    drink_ORS: {
        label: [
            "Did $$ drink ORS (oral rehydration solution) yesterday, during the day or night?",
            " बिगत २४ घण्टा भित्रमा $$ लाई ORS (oral rehydration solution) पिलाउनु भयो?"
        ]
    },
    drink_liquids: {
        label: [
            "Did $$ drink any other liquids yesterday, during the day or night?",
            " बिगत २४ घण्टा भित्रमा $$ लाई कुनै तरल पदार्थ पिलाउनु भयो?"
        ]
    },
    drink_yogurt: {
        label: [
            "Did $$ drink or eat yogurt yesterday, during the day or night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई दहि खुवाउनु भयो?"
        ]
    },
    yogurt_times: {
        label: [
            "How many times did $$ drink or eat yogurt? ",
            " बिगत २४ घण्टा भित्रमा  $$ ले कति पटक दहि खानुभयो?"
        ]
    },
    eat_porridge: {
        label: [
            "Did $$ eat thin porridge yesterday, during the day or night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई हलुवा खुवाउनु भयो?"
        ]
    },
    eat_solid: {
        label: [
            "Did $$ eat solid or semi-solid (soft, mushy) food yesterday, during the day or night? ",
            " बिगत २४ घण्टा भित्रमा $$ लाई ठोस तथा अर्ध ठोस खाना  (solid or semi-solid)  खुवाउनु भयो?"
        ]
    },
    solid_times: {
        label: [
            "How many times did $$ eat solid or semi-solid (soft, mushy) food?",
            " बिगत २४ घण्टा भित्रमा $$ लाई कति पटक  ठोस तथा अर्ध ठोस खाना  (solid or semi-solid)  खुवाउनु भयो?"
        ]
    },
    health_outcome8: {
        label: [
            "Proportion of children receiving minimum dietary diversity  (C1A.0022)",
            "Proportion of children receiving minimum dietary diversity  (C1A.0022)"
        ]
    },
    grain_root: {
        label: [
            "Since this time yesterday (during the day or the night), did $$ eat any grains, white coloured roots, or cereals, including porridge, ugali, rice, white potatoes, etc? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई कुनै अन्न, भात, हलुवा, आलु खुवाउनु भयो?"
        ]
    },
    grain_fortified: {
        label: [
            "Was this grain, root or tuber commercially fortified?",
            "व्यवसायिक रुपमा प्रसोधन गरियक अन्न र कन्दमुल जन्य खाने कुराहरु खुवाउनु भयो? "
        ]
    },
    fruits_yellow: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any orange or yellow coloured fruits or vegetables for example, carrots, orange sweet potatoes, mango? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई कुनै पहेलो फलफुल वा तरकारी जस्तै गाजर, आप,  सखरखण्ड आदि खुवाउनु भयो ?"
        ]
    },
    fruits_other: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any other fruits or vegetables? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई कुनै फलफुल वा तरकारी खुवाउनु भयो?"
        ]
    },
    meat: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any meats, fish or poultry, including organs (i.e. Chicken liver, fish meal)? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई माछा मासु खुवाउनु भयो ?"
        ]
    },
    egg: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any eggs? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई अन्डा खुवाउनु भयो?"
        ]
    },
    legum_nut: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any legumes or nuts? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई कोसे बल्ली ( केरौ, छाना, भटमास आदि) वा काजु, बदम, किसमिस आदि (nuts)  खुवाउनु भयो?"
        ]
    },
    dairy: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any dairy products for example, milk, yogurt or cheese? ",
            "बिगत २४ घण्टा भित्रमा $$ लाई दुध वा दुध जन्य उत्पादन जस्तै दहि, चिज खुवाउनु भयो?"
        ]
    },
    health_outcome15: {
        label: [
            "Proportion of parents or caregivers with children 0–23 months who report that their child\'s stools are safely disposed off (C1B.0131)",
            "Proportion of parents or caregivers with children 0–23 months who report that their child\'s stools are safely disposed off (C1B.0131)"
        ]
    },
    stool_dispose: {
        label: [
            "The last time $$ passed stools, what was done to dispose of the stools?",
            "अन्तिम पटक $$ ले गरेको दिसा के कसरी निपट्नु / व्यवस्थापन गर्नु भयो?"
        ]
    },
    stool_dispose_o: {
        label: [
            "Where was that other place?",
            "अरु स्थान कहाँ होला?"
        ]
    },
    Under_24end: {
        label: [
            "",
            ""
        ]
    },
    child6_59month: {
        label: [
            "",
            ""
        ]
    },
    health_outcome9: {
        label: [
            "% of children 6-59 months who consume iron rich foods  in past 24 hours (C1A.0020)",
            "% of children 6-59 months who consume iron rich foods  in past 24 hours (C1A.0020)"
        ]
    },
    insects: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any Insects (e.g. spiders, grasshoppers, caterpillars)? ",
            "बिगत २४ घण्टा भित्रमा $$ ले कुनै किरा खुवाउनु भएको छ? (जस्तै: माकुरा, किरा)"
        ]
    },
    cerelac: {
        label: [
            "Since this time yesterday (during the day or night) did $$ eat any [BRAND NAME OF COMMERCIALLY FORTIFIED BABY FOOD, E.G., Cerelac]?",
            "बिगत २४ घण्टा भित्रमा $$ ले कुनै ब्रान्डको खाना जस्तै सेर्लैक, लक्तोजीन जस्ता बजार मा पाईने खाने कुरा खुवाउनु भएको छ?"
        ]
    },
    Iron_pil2child_24hr: {
        label: [
            "Was $$ given Iron Pills since this time yesterday (during the day or night)?",
            "बिगत २४ घण्टा भित्रमा $$ लाई आईरन चक्की खुवाउनु भएको छ?"
        ]
    },
    Iron_pil2child_7dy: {
        label: [
            "Was $$ given Iron Pills in the the last seven days?",
            " $$लाई बिगत ७ दिनमा आईरन चक्की खुवाउनु भएको छ?"
        ]
    },
    Iron_syr2child_24hr: {
        label: [
            "Was $$ given Iron Syrup since this time yesterday (during the day or night)?",
            "$$ लाई बिगत २४ घण्टा भित्रमा आईरन सिरप खुवाउनु भएको छ?"
        ]
    },
    Iron_syr2child_7dy: {
        label: [
            "Was $$ given Iron Syrup  in the the last seven days?",
            "$$लाई बिगत ७ दिनमा आईरन सिरप खुवाउनु भएको छ?"
        ]
    },
    end6_59month: {
        label: [
            "",
            ""
        ]
    },
    child12_59month: {
        label: [
            "",
            ""
        ]
    },
    health_outcome11: {
        label: [
            "% of children aged 12-59 months who have completed 3rd DPT dose plus measles vaccination, verified by vaccination card and mother\'s recall. (C1B.0065)",
            "% of children aged 12-59 months who have completed 3rd DPT dose plus measles vaccination, verified by vaccination card and mother\'s recall. (C1B.0065)"
        ]
    },
    healthcard: {
        label: [
            "Does $$ have a health card? May I see it? ",
            " $$ को स्वस्थ कार्ड छ? के म हेर्न सक्छु ?"
        ]
    },
    dpt_card: {
        label: [
            "How many DPT vaccinations are recorded on the card? ",
            "कार्डमा कति पटक DPT सुइ लगाएको रेकर्ड छ ?"
        ]
    },
    measles_card: {
        label: [
            "Is there a Measles or MMR vaccination recorded on the card? ",
            "कार्डमा दादुरा (Measles) वा MMR को सुइ लगाएको रेकर्ड छ ?"
        ]
    },
    dpt_nocard: {
        label: [
            "Has $$ ever received a DPT vaccination- that is, an injection in the thigh or buttocks- to prevent him/ her from getting tetanus, whooping cough, and diphtheria? ",
            "$$ ले DPT को सुइ लगाउनु भएको छ- जुन तिघ्रा वा पछाडी पट्टिको भागमा लगाईन्छ- टिटानस, लहरे खोकी र भ्यागुतेरोगबाट बच्नको लागि लगाईन्छ /"
        ]
    },
    dpt_recall: {
        label: [
            "How many times was a DPT vaccine received? ",
            " DPT सुइ कति पटक लगाएको छ?"
        ]
    },
    measles_recall: {
        label: [
            "Has $$ ever received a Measles injection or an MMR injection - that is, an injection in the upper arm at the age of 9 months or older- to prevent him/ her from getting measles? ",
            "$$ ले दादुरा (Measles) वा MMRको सुइ लगाउनु भएको छ- जुन पाखुरामा ९ महिनाको उमेरमा लगाईन्छ र Measles बाट बच्नको लागि लगाईन्छ "
        ]
    },
    end12_59: {
        label: [
            "",
            ""
        ]
    },
    care_giver: {
        label: [
            "",
            ""
        ]
    },
    health_outcome14: {
        label: [
            "% of households with a designated place for handwashing where water and soap are present   (C1B.0130)",
            "% of households with a designated place for handwashing where water and soap are present   (C1B.0130)"
        ]
    },
    water_health: {
        label: [
            "Does the household have sufficient water? (both for drinking and other purposes)",
            "घरमा प्रसस्त पानी छ? (पिउन र अरु प्रयोजन दुवैको लागि)"
        ]
    },
    handwash_point_health: {
        label: [
            "Is there designated hand washing point with water facility? (observe)",
            "तपाईको घरमा पानीको सुबिधा सहितको  हात धुनको लागि तोकियको स्थान छ? (अवलोकन गर्नुहोस)"
        ]
    },
    handwash_soap_health: {
        label: [
            "Is there soap available for handwashing at the hand washing point? (observe)",
            "त्यस्तो हात धुने स्थानमा साबुनको व्यवस्था छ? (अवलोकन गर्नुहोस)"
        ]
    },
    health_outcome16: {
        label: [
            "% of households  with protected eating and playing space for children",
            "% of households  with protected eating and playing space for children"
        ]
    },
    eating_place: {
        label: [
            "Does your household have a designated protected eating place for children?",
            "के तपाइको घरमा बालबालिकाका लागि खान खान सुरक्षित स्थान तोकिएको छ?"
        ]
    },
    playing_space: {
        label: [
            "Does your household have space for children to play?",
            "के तपाइको घरमा बालबालिका लाई खेल्न स्थान छ?"
        ]
    },
    space_protection: {
        label: [
            "Is that playing space protected?",
            "के त्यो खेल्ने स्थान सुरक्षित छ?"
        ]
    },
    health_outcome17: {
        label: [
            "Proportion of parents or caregivers with appropriate hand-washing behaviour (C1B.0128)",
            "Proportion of parents or caregivers with appropriate hand-washing behaviour (C1B.0128)"
        ]
    },
    soap_use: {
        label: [
            "Do the members of your family use soap to wash their hands?",
            "के तपाईको परिवारका सदस्यहरुले हात धुन साबुन प्रयोग गर्नुहुन्छ?"
        ]
    },
    handwash_time: {
        label: [
            "If yes, when did you wash your hands within these 24 hours? ",
            "यदि धुनु हुन्छ भने, बिगत २४ घण्टामा तपाईले कहिले कहिले हात धुनु भयो?"
        ]
    },
    care_giverend: {
        label: [
            "",
            ""
        ]
    },
    health: {
        label: [
            "",
            ""
        ]
    },
    youth: {
        label: [
            "",
            ""
        ]
    },
    respondent: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    respondent_youth: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    respondent_health: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    respondent_cesp: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    respondent_dap: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    respondent_education: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    respondent_aed: {
        label: [
            "Select respondent",
            "Select respondent"
        ]
    },
    youth_start: {
        label: [
            "",
            ""
        ]
    },
    goal_indicator1youth: {
        label: [
            "Goal  Indicators 1 Proportion of youth who are  not in education, employment, or training (NEET) among participating youths",
            "Respondents: NEET_SKYE Club Members"
        ]
    },
    go_school: {
        label: [
            "1. Do you go to school?",
            "तपाई विद्यालय जानु हुन्छ ?"
        ]
    },
    vocational_skill: {
        label: [
            "2. Have you taken any vocational skill training recently?",
            "तपाईले हालसालै कुनै प्रकारको ब्यवसायिक तालिम लिनु भएको छ ?"
        ]
    },
    employed: {
        label: [
            "3. Are you employed?",
            "तपाई रोजगार हो कि होइन होला ?"
        ]
    },
    achieve_goal: {
        label: [
            "1. I will be able to achieve most of the goals that I have set for myself.",
            "मैले आफ्नो जीवनको लागी तय गरेको सबैजसो उद्येश्यहरु पुरा गर्न सक्ने छु "
        ]
    },
    accomplish_task: {
        label: [
            "2. When facing difficult tasks, I am certain that I will accomplish them.",
            "कठिन कार्यको सामना गर्दा, त्यो कार्य पुरा गरेरै छोड्छु भन्ने कुरामा म ढुक्क छु "
        ]
    },
    important_outcome: {
        label: [
            "3. In general, I think that I can obtain outcomes that are important to me.",
            "सामान्यता, मेरो लागि महत्वपूर्ण उद्येश्यहरु प्राप्त गर्न म सक्षम छु भन्ने लाग्छ "
        ]
    },
    believe_succeed: {
        label: [
            "4. I believe I can succeed at most any endeavor to which I set my mind.",
            "म बिस्वस्थ छु कि मैले सोचेका कुनै पनि प्रयास हरु पुरा गर्न मा सक्षम छु "
        ]
    },
    overcome_challenges: {
        label: [
            "5. I will be able to successfully overcome many challenges.",
            "म धेरै खालका चुनौतिहरुको  सफलतापूर्बक सामना  गर्न सक्नेछु  "
        ]
    },
    confident_errform: {
        label: [
            "6. I am confident that I can perform effectively on many different tasks.",
            "विभिन्न खालका कामहरु प्रभावकारी रुपमा गर्न सक्छु भन्ने कुरामा म बिस्वस्त छु "
        ]
    },
    do_well: {
        label: [
            "7. Compared to other people, I can do most tasks very well.",
            "अरु मानिसहरुको तुलनामा, धेरै जसो काम म धेरै राम्रो संग गर्न सक्छु "
        ]
    },
    tough_perform: {
        label: [
            "8. Even when things are tough, I can perform quite well.",
            "कठिन परिस्थितिमा पनि, मैले कुशलतापूर्वक कार्य सम्पादन गर्न सक्छु "
        ]
    },
    outcome_indicator2: {
        label: [
            "outcome 2 (C3C.22869) Proportion of female and male youth that report improved attitudes towards civic engagement.",
            "Respondents: Youth 15-24 years "
        ]
    },
    responsible_community: {
        label: [
            "1. I feel responsible for my community.",
            "म मेरो समुदायको लागि जिम्मेवार भएको महसुस गर्छु "
        ]
    },
    make_difference: {
        label: [
            "2. I believe I should make a difference in my community.",
            "मलाइ आफ्नो समुदायमा मैले केहि परिवर्तन ल्याउनु पर्छ भन्ने मलाई लाग्छ "
        ]
    },
    responsible_poor: {
        label: [
            "3. I believe that I have a responsibility to help the poor and the hungry.",
            "मलाई गरिब र भोकालाई प्रती मेरो पनि जिम्मेवारी छ भन्ने लाग्छ "
        ]
    },
    committed_serve: {
        label: [
            "4. I am committed to serve in my community.",
            "म आफ्नो समुदायका लागि सेवा गर्न प्रतिबद्ध छु ."
        ]
    },
    citizens_responsibility: {
        label: [
            "5. I believe that all citizens have a responsibility to their community.",
            "सबै नागरिकको आफ्नो समुदाय प्रति जिम्मेवारि छ भन्ने मलाई लाग्छ "
        ]
    },
    infromed_issue: {
        label: [
            "6. I believe that it is important to be informed of community issues.",
            "मेरो बिचारमा आफ्नो समुदायका बिषयहरुमा आफु जानकार रहनु महत्वपूर्ण हुन्छ "
        ]
    },
    believe_volunteer: {
        label: [
            "7. I believe that it is important to volunteer.",
            "मेरो बिचारमा स्वयंसेवा गर्नु महत्वपुर्ण छ  "
        ]
    },
    believe_support: {
        label: [
            "8. I believe that it is important to financially support charitable organizations. ",
            "मेरो बिचारमा परोपकारी संघ संस्था लाई आर्थिक सहयोग गर्नु महत्वपूर्ण हुन्छ "
        ]
    },
    structured_volunteer: {
        label: [
            "9. I am involved in structured volunteer position(s. in the community. ",
            "म (यस समुदायमा) एक प्रकारको संरचनाको स्वयंसेवकको रुपमा काम गर्छु . "
        ]
    },
    positive_changes: {
        label: [
            "10. When working with others, I make positive changes in the community. ",
            "अरु मानिसहरु संग काम गर्दा, म समुदायमा सकारात्मक परिवर्तन दिन सक्छु "
        ]
    },
    help_community: {
        label: [
            "11. I help members of my community. ",
            "म मेरो समुदायका सदस्यहरुलाई सहयोग गर्छु "
        ]
    },
    stay_informed: {
        label: [
            "12. I stay informed of events in my community. ",
            "म मेरो समुदायमा हुने विभिन्न कार्यक्रमहरुको बारेमा जानकार रहन्छु"
        ]
    },
    discussions_social: {
        label: [
            "13. I participate in discussions that raise issues of social responsibility. ",
            "मा सामाजिक जिम्बेवारीको बिषयमा हुने छलफलमा सहभागी हुने गर्छु"
        ]
    },
    contribute_charitable: {
        label: [
            "14. I contribute to charitable organizations within the community.",
            "म समुदायमा भएका परोपकारी संस्थाहरुलाई आर्थिक सहयोग गर्छु . "
        ]
    },
    outcome_indicator3: {
        label: [
            "outcome 3 (C2C.0293) Proportion of youth and adolescents who have a learning opportunity that leads to a productive life",
            "Respondents: Youth 15-24 years "
        ]
    },
    resume_write: {
        label: [
            "1. I have written a resume or CV (for me). ",
            "मैले मेरो लागि एउटा बायोडाटा (CV) तयार गरेको छु "
        ]
    },
    saving_plan: {
        label: [
            "2. I have created a saving plan to meet my personal financial goals.",
            "मेरो व्यक्तिगत आर्थिक उद्येश्य प्राप्तिको लागि मैले एउटा बचत योजना तयार पारेको छु "
        ]
    },
    application_letter: {
        label: [
            "3. I have completed an application or letter of inquiry to apply for an opportunity (training, event, leadership role etc.) ",
            "मैले कुनै अवसर (तालिम, कार्यक्रम, नेतृत्वदायी भूमीका)  लागि एउटा निबेदन तयार पारेर राखेको छु. "
        ]
    },
    job_interview: {
        label: [
            "4. I have participated in a job interview (real or practice) and answered questions appropriately ",
            "म कुनै रोजगारीको कामको लागि अन्तर्वार्ता (वास्तविक वा अभ्यासको) मा सहभागी भएको छु र उपयुक्त जवाफहरू दिएको छु "
        ]
    },
    personal_mentor: {
        label: [
            "5. I have sought out a personal mentor or coach or have found an apprenticeship where I can learn skills for my future.",
            "मेरो भबिस्यको लागि आवस्यक पर्ने सीप सिक्न तथा जानकारी लिनको लागि मैले एक जना सिकाउने मानिस संग सम्पर्क गरेको छु अथवा प्रशिक्षक पाएको छु "
        ]
    },
    youth_end: {
        label: [
            "",
            ""
        ]
    },
    youth: {
        label: [
            "",
            ""
        ]
    },
    cp_group: {
        label: [
            "CP Project ",
            "बाल संरक्षण परियोगना "
        ]
    },
    respondent_cp: {
        label: [
            "select respondents ",
            ""
        ]
    },
    adole_start: {
        label: [
            "",
            ""
        ]
    },
    cp_goal_indicator1: {
        label: [
            " Goal Indicators 1 (C4A.0014) Proportion of children under 18 years, married  ",
            ""
        ]
    },
    ever_married: {
        label: [
            "CP1. Are you or have you ever been married?",
            "CP1. के तपाई ले बिवाह गर्नु भएको छ ?"
        ]
    },
    age_married: {
        label: [
            "CP2.At what age did you first get married? ",
            "CP2. बिवाह हुँदा तपाई कति बर्षको हुनुहुन्थ्यो?"
        ]
    },
    have_children: {
        label: [
            "CP3. Do you have any children ?",
            "CP3. के तपाइका छोराछोरी  छन् ?"
        ]
    },
    cp_goal_indicator2: {
        label: [
            "Goal Indicator 2 ( C4A.0221) Proportion of adolescents who report having experienced any physical violence in the past 12 months",
            ""
        ]
    },
    note1: {
        label: [
            "In the last 12 months, how often has anyone hurt you in any of the following ways?",
            "के तपाइलाई कसैले गएको १२ महिनामा तल दिएको मध्ये एक वा अर्को प्रकारले  हाँनी (दुख/चोट)  पुर्यायो?"
        ]
    },
    standing: {
        label: [
            "CP4. Made you uncomfortable by standing too close or touching you (opposite gender)?",
            "CP4. के तपाईलाई कसैले नजिक आएर उभियर वा खराब नियतले छोएर असजिलो महसुस भएको छ ?"
        ]
    },
    shouted: {
        label: [
            "CP5. Shouted or screamed at you?",
            "CP5. तपाइलाई कसैले करायो वा चिचायो?"
        ]
    },
    swore: {
        label: [
            "CP6. Called your names or swore at you?",
            "CP6. कसैले तपाइको नाम बिगारेर बोलायो वा नराम्ररी गाली गरयो ?"
        ]
    },
    slapped: {
        label: [
            "CP7. Hit or slapped you with bare hands?",
            "CP7. कसैले तपाइलाई हातले हिर्कायो वा थप्पड हान्यो ?"
        ]
    },
    hit_object: {
        label: [
            "CP8. Hit you with a belt/stick/hard object?",
            "CP8. कसैले तपाइलाई बेल्ट/ लट्ठी/ सारो बस्तुले हिर्कायो?"
        ]
    },
    punched: {
        label: [
            "CP9. Punched, kicked or beat you up?",
            "CP9. कसैले तपाइलाई मुक्काले,लात्तिले हिर्कायो?"
        ]
    },
    hurt_physically: {
        label: [
            "CP10. Hurt you physically in some other way?",
            "CP10.के तपाइलाई शारीरिक रुपमा एक वा अन्य प्रकारले चोट पुर्यायो?"
        ]
    },
    beaten_group: {
        label: [
            "In the last 12 months, have you been beaten up or physically hurt in other ways by any of the following people?",
            "बिगत १२ महिना भरिमा कहिल्यै तपाईलाई (तलका मध्येका व्यक्तिले) ले पिटेको अथवा शारीरिक रुपमा कुनै प्रकारको पिडा दिएको थियो ?"
        ]
    },
    beaten_family: {
        label: [
            "CP11. Someone from your family?",
            "CP11. तपाईको परिवारको कसैले पिटेको अथवा शारीरिक रुपमा कुनै प्रकारको पिडा दिएको थियो ?"
        ]
    },
    beaten_adult: {
        label: [
            "CP12. Another adult you know?",
            "CP12. तपाईंले चिनेको कुनै (बयस्क) व्यक्तिले पिटेको अथवा शारीरिक रुपमा कुनै प्रकारको पिडा दिएको थियो ?"
        ]
    },
    beaten_bgfriend: {
        label: [
            "CP13. Boyfriend or girlfriend?",
            "CP13. केटा साथी (Boyfriend) अथवा केटी साथी (girlfriend) ले पिटेको अथवा शारीरिक रुपमा कुनै प्रकारको पिडा दिएको थियो ?"
        ]
    },
    beaten_friends: {
        label: [
            "CP14. Friends?",
            "CP14. साथीले पिटेको अथवा शारीरिक रुपमा कुनै प्रकारको पिडा दिएको थियो ?"
        ]
    },
    beaten_dnk: {
        label: [
            "CP15. Someone you don’t know?",
            "CP15. तपाईंले नचिनेको कुनै व्यक्तिले पिटेको अथवा शारीरिक रुपमा कुनै प्रकारको पिडा दिएको थियो ?"
        ]
    },
    cp_goal_indicator3: {
        label: [
            "Goal Inicator 2 (C4A.23441) Proportion of adolescent females and males (direct participants) who have an increase in gender equitable attitudes ",
            ""
        ]
    },
    women_imrole: {
        label: [
            "CP16. Woman\'s most important role is to take care of her home and cook",
            "CP16. महिलाको सबै भन्दा महत्वपुर्ण काम भनेको उनको घरपरिवारको हेरचाह गर्नु र खाना बनाउनु हो |"
        ]
    },
    woman_beateb: {
        label: [
            "CP17.There are times when a woman deserves to be beat",
            "CP17. कहिले कहिँ महिलालाई पिट्नु नै पर्ने अवस्था हुन्छ |"
        ]
    },
    mother_responsibility: {
        label: [
            "CP18. Changing diapers, giving kids a bath & feeding kids are mothers responsibility",
            "CP18. छोराछोरीको थांगना (डाइपर) फेर्ने, छोरा छोरीलाई नुहाईदिने र उनीहरुलाई खुवाउने काम महिलाकै जिम्मेवारी हो |"
        ]
    },
    avoid_pregnant: {
        label: [
            "CP19. It is a womans responsibility to avoid getting pregnant",
            "CP19. गर्भवती हुनबाट जोगिने कुराको जिम्मेवारी महिलाको नै हो "
        ]
    },
    man_decision: {
        label: [
            "CP20. A man should have the final word about decision in his home",
            "CP20. आफ्नो घरपरिवारको निर्णय गर्दा अन्तिम कुरा पुरुषको नै हुनु पर्छ "
        ]
    },
    violence_tolerate: {
        label: [
            "CP21. A woman should tolerate violence in order to keep her family together",
            "CP21. आफ्नो परिवारलाई संगै कायम राख्न को लागी महिलाले हिंसा सहनु पर्ने  हुन्छ आफ्नो परिवारलाई राम्रो वा बलियो अथवा एकीकृत बनाउन महिलाले हिंसा सहनु पर्ने पनि हुन्छ "
        ]
    },
    man_tough: {
        label: [
            "CP22. To be a man, you need to be tough ",
            "CP22. पुरुष भएपछि  कडा स्वभाव को  नै हुनु पर्छ |"
        ]
    },
    contraceptive_use: {
        label: [
            "CP23. A man and a woman should decide together what type of contraceptive to use",
            "CP23. गर्भ निरोधका लागि कुन  साधन प्रयोग गर्ने बिषयमा  श्रीमान श्रीमतीले  संगै निर्णय  गर्नु पर्छ |"
        ]
    },
    child_responsibility: {
        label: [
            "CP24. If a guy gets women pregnant, child is responsibility of both",
            "CP24. बच्चा हेरचाह र पालन पोषणको जिम्मेवारी महिला पुरुष दुबैको हो |"
        ]
    },
    raising_children: {
        label: [
            "CP25. The participation of the father is important in raising children",
            "CP25. छोरा छोरी हुर्काउने क्रममा बाबुको सहभागिता महत्वपुर्ण हुन्छ |"
        ]
    },
    men_friend: {
        label: [
            "CP26. Its important for men to have friends to talk about his problems",
            "CP26. पुरुषलाई आफ्नो समस्याको बारेमा कुरा गर्न कोहि साथी हुनु महत्वपूर्ण हुन्छ |"
        ]
    },
    couple_decide: {
        label: [
            "CP27. Couple should decide together if they want to have children",
            "CP27. छोराछोरी जन्माउने बिषयमा  श्रीमान श्रीमती दुबैको निर्णय  हुनुपर्छ | "
        ]
    },
    cp_outcome_indicator2a: {
        label: [
            "Outcome2a (C4A.0219) Proportion of adolescents who know of the presence of services and mechanisms to receive and respond to reports of abuse, neglect, exploitation or violence against children",
            ""
        ]
    },
    bad_happening: {
        label: [
            "CP32.If something bad happened to you or was happening to you, such as if you were being abused (physically or sexually) or if you were being neglected or exploited by someone, do you know about services that can help you safely? ",
            "CP32. यदि तपाईलाई कुनै नराम्रो घटना भएको वा भइरहेको अवस्थामा  जस्तै तपाई दुर्व्यवहारमा परेको (शारीरिक रुपमा वा यौनजन्यरुप) अथवा कसैले तपाईलाई बेवास्ता वा शोषण गरेको बेलामा , कहाँबाट सुरक्षित् रुपमा सेवा लिन सकिन्छ भन्ने तपाईंलाई थाहा छ?  "
        ]
    },
    prevent_reasons: {
        label: [
            "CP33. What would prevent you from being able to use these services if you needed them?",
            "CP33. आफुलाई आवश्यक परेको बेलामा तपाईलाई त्यस्ता सेवाहरु लिन के ले रोक्न सक्छ अथवा अप्ठ्यारो  पर्न सक्छ जस्तो लाग्छ ?"
        ]
    },
    prevent_reasons_other: {
        label: [
            "CP33a.Reason other",
            "CP33a. अन्य कारण खुलाउनुहोस् "
        ]
    },
    cp_outcome_indicator5b: {
        label: [
            "Outcome 5b. (C4D.23075 ) Proportion of female and male adolescents (aged 12 to 18) who report improved quality of child protection services",
            ""
        ]
    },
    weight_child: {
        label: [
            "What is $$ weight in kilograms? Record weight to the nearest 0.01 kg.",
            "$$को किलोग्राममा कति तौल छ?"
        ]
    },
    used_service: {
        label: [
            "CP49. Have you used any services (police or social welfare services) in the last 12 months?          ",
            "CP49. गत १२ महिना भित्रमा तपाईंले (प्रहरी अथवा सामाजिक welfare सेवा सम्बन्धि कुनै सरकारी कार्यालयको) सेवा प्रयोग गर्नुभएको छ ?"
        ]
    },
    quality_service: {
        label: [
            " CP50. If yes, how would you assess the quality of the services that you received?",
            "CP50. यदि भएमा, तपाईले पाउनु भएको त्यस्तो सेवाको स्तर अथवा गुणस्तर के-कस्तो लाग्यो? "
        ]
    },
    access: {
        label: [
            "CP50a. Access? ",
            "CP50a.तपाईले पाउनु भएको त्यस्तो सेवाको पहुँच कस्तो लाग्यो ?"
        ]
    },
    affordability: {
        label: [
            "CP50b. Affordability?",
            "CP50b.तपाईले पाउनु भएको त्यस्तो सेवा बापत लागेको खर्च बेहोर्न  सक्ने हैसियत कस्तो छ ?"
        ]
    },
    availability: {
        label: [
            "CP50c. Availability?",
            "CP50c. तपाईले पाएको सेवाको उपलब्धता कस्तो लाग्यो ?"
        ]
    },
    accommodation: {
        label: [
            "CP50d. Accommodation? ",
            "CP50d. तपाईले सेवा लिन जाँदा  बस्ने ठाउको अवस्था कस्तो लाग्यो ?"
        ]
    },
    cp_outcome_indicator1a: {
        label: [
            "Outcome1a (C4A.22920) Proportion of child protection service users who report increased responsiveness of child protection service providers towards communities ",
            ""
        ]
    },
    protection_service: {
        label: [
            "CP28. Did you receive any child protection services from service provider (DCWB, WCO, local child protection committee, Women and Children cell) with in last 12 months (a year)?  ",
            "CP28. तपाईंले बिगत १२ महीना (एक वर्ष) भित्रमा कुनै सेवा प्रदायक (DCWB, WCO, local child protection committee, Women and Children cell) बाट कुनै पनि बाल संरक्षण सेवा लिनुभयो?"
        ]
    },
    satisfied_responsiveness: {
        label: [
            "CP29. Are you satisfied with the responsiveness of child protection service provider ?",
            "CP29. तपाईं  बाल संरक्षण सेवा प्रदायकहरुको  सेवा दिने तत्परताबाट  सन्तुष्ट हुनुहुन्छ  ?"
        ]
    },
    cp_outcome_indicator1b: {
        label: [
            "Outcome1b (C4A.22917) Proportion of users who are satisfied with the child protection services they have received ",
            ""
        ]
    },
    reported_case: {
        label: [
            "CP30. Did you make complaints or reported cases, our  have attended trainings or other events or received any services by service provider ?",
            "CP30. के तपाइँले सेवा प्रदायकमा कुनै घटनाको  उजुरी गर्नु भएको छ  वा तालिममा वा अन्य कार्यकममा सहभागी हुनु भएको छ  वा बाल संरक्षणको कुनै प्रकारको सेवा  लिनु भएको छ ? "
        ]
    },
    satisfied_service: {
        label: [
            "CP31. Are you satisfied from  the child protection services that you received ?",
            "CP31. तपाई त्यो बाल संरक्षण सेवा प्रदायकबाट पाउनु भएको सहयोग/सेवाबाट  सन्तुष्ट हुनुहुन्छ ?"
        ]
    },
    cp_outcome_indicator2b: {
        label: [
            "Outcome 2b.(C4A.0196)Proportion of parents or caregivers who feel that their community is a safe place for children",
            ""
        ]
    },
    feel_children: {
        label: [
            "CP34. How safe do you feel your children are from danger and violence in your neighbourhood?",
            "CP34. तपाइको बालबालिकाहरु आफ्नो छरछिमेकमा कुनै खतरा अथवा हिंसात्मक क्रियाकलापबाट  कत्तिको सुरक्षित रहेको अनुभूति गर्नु हुन्छ ?"
        ]
    },
    reason_notsafe: {
        label: [
            "CP35.If you do not feel your children are safe most of the time, What is the main reason?",
            "CP35. असुरक्षित महसुस हुनुको  मुख्य कारण के होला ? "
        ]
    },
    reason_notsafe_other: {
        label: [
            "CP35a. Pls specify other ",
            "CP35a. अन्य (खुलाउनुहोस् )"
        ]
    },
    cp_outcome_indicator3b: {
        label: [
            "Outcome 3b. (C4A.0190) Proportion of children engaged in child labour",
            ""
        ]
    },
    do_work: {
        label: [
            "CP36. During the past week, did your child any do any kind of work for someone who is not a member of this household?",
            "CP36. गएको हप्ताभरिमा कुनै बालबालिकाले  तपाइको आफ्नो परिवार बाहेक कसैको लागि कुनै काम गर्नु भएको थियो ? "
        ]
    },
    pay_cash: {
        label: [
            "CP37. If yes: For pay in cash or kind?",
            "CP37. उहाँले  काम गरे बापत कुनै नगद वा जिन्सी पाउनको लागि काम गर्नु भएको हो ?"
        ]
    },
    work_hours_other: {
        label: [
            "CP38. Since last (day of the week), about how many hours did he/she do this work for someone who is not a member of this household? (If more than one job, include all hours at all jobs)",
            "CP38. बिगत अन्तिम काम गरेको  दिनमा (बालक/बालिका)उहाँले आफ्नो परिवार बाहिर को कोहि मानिसको लागि कति घण्टा जति त्यस्तो  काम गर्नु भएको थियो होला? (यदि एक भन्दा बढी खालको काम गरेको भएमा सबै काम गर्दाको समयलाई जोड्ने)"
        ]
    },
    family_work: {
        label: [
            "CP39.During the past week, did your child do any paid or unpaid work on a family farm or in a family business or selling goods in the street?(Include work for a business run by the child, alone or with one or more partners.)",
            "CP39. गत हप्ता भरिमा (हप्ताको बार), उहाले (नाम) आफ्नो परिवारको खेतीपाती/घर गोठमा वा पारिवारिक ब्यापारमा अथवा कुनै सरसामान बेच्ने जस्तो पैसा पाउने वा  पैसा नपाउने कुनै काम गर्नुभयो ?  "
        ]
    },
    family_work_hours: {
        label: [
            "CP40.Since last (day of the week), about how many hours did he/she do this work for his/her family or himself/herself?",
            "CP40 गत हप्ता भरिमा (हप्ताको बार), उहाले (नाम) आफ्नो परिवारको लागि अथवा आफ्नै लागि त्यस्तो काम कति घण्टा जति गर्नुभयो होला?   "
        ]
    },
    house_work: {
        label: [
            "CP41.During the past week, did your child fetch water or collect firewood for household use?",
            "CP41.गत हप्ता भरिमा (हप्ताको बार), उहाले (नाम) आफ्नो परिवारको लागि आवस्यक पर्ने दाउरा अथवा पानी बोक्ने काम गर्नु भयो?   "
        ]
    },
    house_work_hours: {
        label: [
            "CP42. Since last (day of the week), about how many hours did he/she fetch water or collect firewood for household use?",
            "CP42. गत हप्ता भरिमा (हप्ताको बार), उहाले (नाम) आफ्नो परिवारको लागि अथवा आफ्नै लागि  दाउरा अथवा पानी बोक्ने काम कति घण्टा जति गर्नुभयो होला?   "
        ]
    },
    house_chores: {
        label: [
            "CP43. During the past week, did   help with household chores such as shopping, cleaning, washing clothes, cooking; or caring for children, old or sick people?",
            "CP43. गत हप्ता भरिमा (हप्ताको बार), तपाइको त्यो बालबालिकाले  किनमेल गर्ने, सरसफाई गर्ने, लुगा धुने, खान पकाउने, भदा माझ्ने, अथवा बालबालिका/बुढाबूढी/बिरामीको हेरचाह गर्ने जस्तो  घर धान्दाको काममा सहयोग गर्नु भयो ?"
        ]
    },
    house_chores_hours: {
        label: [
            "CP44. Since last (day of the week), about how many hours did he/she spend doing these chores?",
            "CP44.गत हप्ता भरिमा (हप्ताको बार), तपाईको बालबालिकाले  आफ्नो परिवारको घरधन्दाको त्यस्तो काम कति घण्टा जति गर्नुभयो होला?   "
        ]
    },
    cp_outcome_indicator4a: {
        label: [
            "Outcome 4a. (C4A.23443 ) Proportion of most vulnerable HHs who are confident that they can access CP services available without external support ",
            ""
        ]
    },
    available_services: {
        label: [
            "CP45. Do you know about the available CP services  ?",
            "CP45. तपाईलाई बाल संरक्षण सेवाको उपलाब्धाको बारेमा थाहा छ ?"
        ]
    },
    condident_access: {
        label: [
            "CP46. If a child in your family needed assistance, do you feel confidennt that your family could access child protection services without external support ?",
            "CP46. यदि तपाइको परिवारको कुनै बालबालिकालाई केहि सरसहयोग आवश्यकता परेको खण्डमा, तपाईहरुले अरु कसैको बाहिरी सहयोग बिना नै त्यस्तो बाल संरक्षण सेवा लिन सकिन्छ भन्ने कुरामा तपाई कत्तिको ढुक्क हुनुहुन्छ ?"
        ]
    },
    cp_outcome_indicator5a: {
        label: [
            "Outcome 5a. Proportion of household  having access to Government prescribed Social protection schemes ",
            ""
        ]
    },
    protection_schemes: {
        label: [
            "CP47. Does you family have access to social protection schemes provided by local government ?",
            "CP47.तपाईको परिवारको यहाको स्थानीय तह (गाउँ/नगरपालिका)ले प्रदान गरेको सामाजिक सुरक्षा सेवाको पहुच छ ? (पाउनु परेमा लिन सक्नुहुन्छ? )"
        ]
    },
    received_schemes: {
        label: [
            "CP48. What type of social protection schemes you and your familiy received from local government?",
            "CP48.तपाईले वा तपाईंको परिवारको कसैले स्थानीय तहबाट कस्तो प्रकारको सामाजिक सुरक्षा सेवा लिनु भएको छ ?"
        ]
    },
    received_schemes_other: {
        label: [
            "CP48a.Please Specify",
            "CP48a. कृपया अन्यलाई खुलाएर लेख्नुहोस "
        ]
    },
    care_giverend: {
        label: [
            "",
            ""
        ]
    },
    cp_end: {
        label: [
            "",
            ""
        ]
    },
    CESP_start: {
        label: [
            "CESP",
            ""
        ]
    },
    respondent_CESP: {
        label: [
            "select respondents ",
            ""
        ]
    },
    vision_start: {
        label: [
            "vision ",
            ""
        ]
    },
    cesp_outcome_indicator1: {
        label: [
            "Outcome Indicators 1 (C4A.21415) Proportion of households able to recall the community vision for child well-being",
            ""
        ]
    },
    vision_cwb: {
        label: [
            "CESP 2.  Have the community has vision for child well being ?",
            "CESP 2. यो समुदायले बालबालिकाहरुको सम्मुनतिको लागि कुनै दृष्टिकोण बनाएको छ ?"
        ]
    },
    share_cwbvision: {
        label: [
            "CESP 3. Could you please share the vision statment of the child well being ?",
            "CESP 3. यदि भएमा, तपाईं मलाई त्यो दृष्टिकोण वाक्याम्स (vision statement) बताई दिन सक्नुहुन्छ ?"
        ]
    },
    vision_end: {
        label: [
            "",
            ""
        ]
    },
    drr_start: {
        label: [
            "",
            ""
        ]
    },
    cesp_outcome_indicator2: {
        label: [
            "Percentage of communities that implement their disaster management/preparedness plans according to the guidelines",
            ""
        ]
    },
    happen: {
        label: [
            'How often did this happen?',
            'त्यस्तो कति पटक भयो?'
        ]
    },
    ldrm_exist: {
        label: [
            "CESP 4.Does  LDRM committee exist in your community? ",
            "CESP 4. तपाईको समुदायमा स्थानीय प्रकोप जोखिम व्यवस्थापन समिति (LDRM) छ?"
        ]
    },
    ldrmp_developed: {
        label: [
            "CESP 5. Does LDRM Plan has been developed ?",
            " CESP 5. स्थानीय प्रकोप जोखिम व्यवस्थापन योजना (LDRM plan) छ?"
        ]
    },
    ldrmp_guideline: {
        label: [
            "CESP 6. Has LDRM Plan  been developed as per the government guildline ?",
            "CESP 6. त्यस्तो स्थानीय प्रकोप जोखिम व्यवस्थापन योजना (LDRM plan) सरकारको निर्देशिका अनुसार नै बनाईएको हो ?"
        ]
    },
    ldrmp_implemented: {
        label: [
            "CESP 7. Does the LDRMP is being implemented  ?",
            "CESP 7.त्यस्तो स्थानीय प्रकोप जोखिम व्यवस्थापन योजना (LDRM plan) कार्यन्वयन भएको छ ?"
        ]
    },
    cesp_goal_indicator1: {
        label: [
            " Goal Indicators 1 (C4A.0165) The strengths of the assets and the contexts in which adolescents live, learn and work as reported by adolescents 12-18 years of age.",
            ""
        ]
    },
    d1: {
        label: [
            "D1. Stand up for what I believe in.",
            "D१. म आफूले विश्वास गरेको कुरामा अडिग वा स्थिर रहन्छु ।"
        ]
    },
    d2: {
        label: [
            "D2.  Feel in control of my life and future.",
            "D२. मेरो भविष्य र जीवन मेरो हातमा वा अधिनमा छ ।"
        ]
    },
    d3: {
        label: [
            "D3.  Feel good about myself.",
            "D३. मलाइ आफु/आफ्नो बारेमा राम्रो लाग्छ ।"
        ]
    },
    d4: {
        label: [
            "D4.  Avoid things that are dangerous or unhealthy.",
            "D४. खतरा हुने र अस्वस्थ कुराबाट म टाढा बस्छु ।"
        ]
    },
    d5: {
        label: [
            "D5. Enjoy reading or being read to.",
            "D५. म आफै पढ्न वा अरुले मलाइ पढेर सुनाएको मन पराउछु ।"
        ]
    },
    d6: {
        label: [
            "D6.  Build friendships with other people.",
            "D६. म अरु मानिसहरुसंग मित्रता कायम गर्छु ।"
        ]
    },
    d7: {
        label: [
            "D7. Care about school.",
            "D७. म विद्यालयको ख्याल राख्छु ।"
        ]
    },
    d8: {
        label: [
            "D8.  Do my homework.",
            "D८. म आफ्नो गृहकार्य गर्दछु ।"
        ]
    },
    d9: {
        label: [
            "D9.  Stay away from tobacco, alcohol, and other drugs.",
            "D९. म सुर्ति, रक्सी र अन्य लागु पदार्थबाट टाढा बस्छु ।"
        ]
    },
    d10: {
        label: [
            "D10. Enjoy learning.",
            "D१०. म सिक्न पाउदा रमाउछु ।"
        ]
    },
    d11: {
        label: [
            "D11. Express my feelings in proper ways.",
            "D११. म आफ्नो भावनाहरू सहि तरिकाबाट व्यक्त गर्दछु ।"
        ]
    },
    d12: {
        label: [
            "D12. Feel good about my future.",
            "D१२. मलाइ आफ्नो भविष्य राम्रो हुन्छ जस्तो लाग्छ ।"
        ]
    },
    d13: {
        label: [
            "D13. Seek advice from my parents.",
            "D१३. म मेरो अभिभावकहरुबाट सल्लाह खोज्दछु ।"
        ]
    },
    d14: {
        label: [
            "D14. Deal with frustration in positive ways.",
            "D१४. म निरासालाई सकारात्मक तरिकाले लिन्छु ।"
        ]
    },
    d15: {
        label: [
            "D15. Overcome challenges in positive ways.",
            "D१५. म चुनौतीहरू वा वाधा अड्चनलाई सकारात्मक तरिकाले समाधान गर्छु |"
        ]
    },
    d16: {
        label: [
            "D16. Think it is important to help other people.",
            "D१६. म अरूलाई मद्दत गर्नु महत्वपूर्ण छ भनी सोच्दछु ।"
        ]
    },
    d17: {
        label: [
            "D17. Feel safe and secure at home.",
            "D१७. म घरमा सुरक्षित र ढुक्क महसुस गर्दछु ।"
        ]
    },
    d18: {
        label: [
            "D18. Plan ahead and make good choices.",
            "D१८. म आफ्नो योजना पहिल्यै बनाउछु र सही निर्णय लिन्छु |"
        ]
    },
    d19: {
        label: [
            "D19. Resist bad influences.",
            "D१९. म नराम्रो प्रभावहरुबाट टाढै बस्छु ।"
        ]
    },
    d20: {
        label: [
            "D20. Resolve conflicts without anyone getting hurt.",
            "D२०. म कसैलाई चोट नपुर्याईकन झैझगडा मिलाउछु ।"
        ]
    },
    d21: {
        label: [
            "D21. Feel valued and appreciated by others.",
            "D२१. मलाई अरुले महत्व दिएको र राम्रो ठानेको जस्तो लाग्छ |"
        ]
    },
    d22: {
        label: [
            "D22. Take responsibility for what I do.",
            "D२२. म जे गर्छु त्यसको जिम्मेवारीका साथ गर्छु ।"
        ]
    },
    d23: {
        label: [
            "D23. Tell the truth even when it is not easy.",
            "D२३. सत्य कुरा भए भन्नलाई गाह्रै भएपनि म भन्छु ।"
        ]
    },
    d24: {
        label: [
            "D24. Accept people who are different from me.",
            "D२४. म आफूभन्दा फरक व्यक्तिहरुलाई पनि स्वीकार गर्छु/मान्छु ।"
        ]
    },
    d25: {
        label: [
            "D25. Feel safe at school.",
            "D२५. म विद्यालयमा सुरक्षित महसुस गर्छु |"
        ]
    },
    d26: {
        label: [
            "D26. Actively engaged in learning new things.",
            "D२६. म नया कुराहरू सिक्न सक्रिय रुपमा लाग्छु |"
        ]
    },
    d27: {
        label: [
            "D27. Developing a sense of purpose in my life.",
            "D२७. म आऋनो जीवनको उद्धेश्य के हो पाउदैछु |"
        ]
    },
    d28: {
        label: [
            "D28. Encouraged to try things that might be good for me.",
            "D२८. मेरो लागी राम्रो हुन सक्ने चिज गरी हे दिइएको छ"
        ]
    },
    d29: {
        label: [
            "D29. Included in family tasks and decisions.",
            "D२९. मलाई मेरो परिवारको काम र निर्णयमा साम"
        ]
    },
    d30: {
        label: [
            "D30. Helping to make my community a better place.",
            "D३०. आफ्नो समुदायलाई राम्रो मदत गरिरहेको"
        ]
    },
    d31: {
        label: [
            "D31. Involved in a religious group or activity.",
            "D३१. म धार्मिक कार्य वा धार्मिक समुहमा सहभागी गराइन्छु"
        ]
    },
    d32: {
        label: [
            "D32. Developing good health habits.",
            "D३२. म राम्रो स्वास्थ्य र बानीहरूको विकास"
        ]
    },
    d33: {
        label: [
            "D33. Encouraged to help others.",
            "D३३. म अरूलाई मद्दत गर्न उत्साहित छु"
        ]
    },
    d34: {
        label: [
            "D34. Involved in a sport, club, or other group.",
            "D३४. म खेलकुदमा, वा अन्य समूहमा सहभागी छ"
        ]
    },
    d35: {
        label: [
            "D35. Trying to help solve social problems.",
            "D३५. म आऋनो समाज भएका समस्याहरू सुल्झाउनलाई गर्ने पगर्दैछु ।"
        ]
    },
    d36: {
        label: [
            "D36. Given useful roles and responsibilities.",
            "D३६. मलाई महत्वपूर्ण भुमिका र जिम्मेवारीहरु "
        ]
    },
    d37: {
        label: [
            "D37. Developing respect for other people.",
            "D३७. म अरूलाइ आदर गर्न सिक्दै छु "
        ]
    },
    d38: {
        label: [
            "D38. Eager to do well in school and other activities.",
            "३८. म विद्यालयमा र अतिरिक्त क्रियाकलापहरुमा एकदमै राम्रो गर्न मन परौछु "
        ]
    },
    d39: {
        label: [
            "D39. Sensitive to the needs and feelings of others.",
            "D३९. म अरूको आवश्यक र भावनाहरुप्रति संवेदनशि छु"
        ]
    },
    d40: {
        label: [
            "D40. Involved in creative things such as music, theater, or art.",
            "D४०. म सङगीत, नाटक, कला आदिजस्ता सृजनात्मक कुराहरूमा संलग्न हुन्छु "
        ]
    },
    d41: {
        label: [
            "D41. Serving others in my community.",
            "D४१. म समुदाय मानिसहरूको सहयोग गर्दैछु |"
        ]
    },
    d42: {
        label: [
            "D42. Spending quality time at home with my parent(s).",
            "D४२. म घरमा आऋनो अभिभाव (हरू) सग एकदम राम्रो समय बितौदै छु |"
        ]
    },
    d43: {
        label: [
            "D43. Friends who set good examples for me.",
            "D४३. मेरो साथीहरूले मलाइ उदाहरणको रुपमा लिन्छन ।"
        ]
    },
    d44: {
        label: [
            "D44. A school that gives students clear rules.",
            "D४४. बिद्यार्थीहरुलाई स्पस्ट रुपमा नियमहरु सिकाउने बिद्यालय |"
        ]
    },
    d45: {
        label: [
            "D45. Adults who are good role models for me.",
            "D४५. मलाइ राम्रो लाग्ने अथवा उदाहरणीय (ठुलो) व्यक्ति ।"
        ]
    },
    d46: {
        label: [
            "D46. A safe neighborhood.",
            "D४६. सुरक्क्षित् मेरो छरछिमेकि छन्"
        ]
    },
    d47: {
        label: [
            "D47. Parent(s) who try to help me succeed.",
            "D४७. मेरो अभिभावकले सफल बन्न मद्दत गर्नुहुन्छ "
        ]
    },
    d48: {
        label: [
            "D48. Good neighbors who care about me.",
            "D४८. मेरो वास्ता गर्ने राम्रो छिमेकि छन् ।"
        ]
    },
    d49: {
        label: [
            "D49. A school that cares about kids and encourages them.",
            "D४९. मेरो विद्यालयले बालबालिकाहरूलाई हेरविचार र पुर्याउछ "
        ]
    },
    d50: {
        label: [
            "D50. Teachers who urge me to develop and achieve.",
            "D५०. मेरोगुरुहरूले मलाई प्रगति र सफलता सहयो गनहुन्छ "
        ]
    },
    d51: {
        label: [
            "D51. Support from adults other than my parents.",
            "D५१. मेरो अभिभाव बाहेक अरु वयष्कहरुब पनि मैले सहयोग गर्नु हुन्छ "
        ]
    },
    d52: {
        label: [
            "D52. A family that provides me with clear rules.",
            "D५२. मेरो परिवारले मलाई स्पष्ट नियमहरू सिकाउनु हुन्छ ।"
        ]
    },
    d53: {
        label: [
            "D53. Parent(s) who urge me to do well in school.",
            "D५३. मेरो अभिभावकले विद्यालयमा राम्रो गर्न लगाउनु हुन्छ |"
        ]
    },
    d54: {
        label: [
            "D54. A family that gives me love and support.",
            "D५४. मेरो परिवारले मलाई माया र सहयोग गर्छ "
        ]
    },
    d55: {
        label: [
            "D55. Neighbors who help watch out for me.",
            "D५५. मेरो छिमेकीहरूले मेरो हेरचाह गर्छन "
        ]
    },
    d56: {
        label: [
            "D56. Parent(s) who are good at talking with me about things.",
            "D५६. मेरो अभिभावकहरु मा संग विभिन्न विषयमा राम्रोसगं कुराकानी गर्नुहुन्छ "
        ]
    },
    d57: {
        label: [
            "D57. A school that enforces rules fairly.",
            "D५७. मेरो विद्यालयले समान रूपमा नियमहरू लागु गर्छ "
        ]
    },
    d58: {
        label: [
            "D58. A family that knows where I am and what I am doing.",
            "D५८. मेरो परिवारलाई म कहाँ छु र के गर्दैछु’ भन्ने थाहा छ ।"
        ]
    },
    respondent_AED: {
        label: [
            "What type of respondent is going to be interviewed?",
            "उत्तरदाता"
        ]
    },
    provide_clothes: {
        label: [
            "In the past year, were you able to provide two sets of clothes for all the children (5-18 years) living in your household, without assistance from family, the government or NGO?",
            "१२. के तपाईले गएको बर्ष परिवारमा भएको ५-१८ बर्षका सम्पूर्ण बालबालिकाहरुलाई दुइ जोड लुगा  कुनै सरकारी निकाय वा संस्थाको सहयोग  बिना  किन्न सक्नु भयो? "
        ]
    },
    difficult_cloths: {
        label: [
            "Which of your children age (5-18) are not able to get two sets of clothes?",
            "१२अ.  तपाइको कुन उमेरको  बच्चाले २ सेट कपडा कुनै सरकारी निकाय वा संस्थाको सहयोग  बिना  किन्न सक्नु भयो? "
        ]
    },
    provide_shoes: {
        label: [
            "In the past year, were you able to provide a pair of shoes for all the children (5-18 years) living in your household, without assistance from family, the government or NGO? ",
            "१३. के तपाईले गएको बर्ष परिवारमा भएको ५-१८ बर्षका सम्पूर्ण बालबालिकाहरुलाई एक जोड जुत्ता कुनै सरकारी निकाय वा संस्थाको सहयोग  बिना  किन्न सक्नु भयो? "
        ]
    },
    difficult_shoes: {
        label: [
            "Which of your children age (5-18) are not able to get a pair of shoes?",
            "१२अ.  तपाइको कुन उमेरको  बच्चाले एक जोड जुत्ता  कुनै सरकारी निकाय वा संस्थाको सहयोग  बिना  किन्न सक्नु भयो? "
        ]
    },
    provide_blanket: {
        label: [
            "In the past year, were you able to provide a blanket for sleeping for all the children (5-18 years) living in your household, without assistance from family, the government or NGO? ",
            "१४. के तपाईले गएको बर्ष परिवारमा भएको ५-१८ बर्षका सम्पूर्ण बालबालिकाहरुलाई ओढ्ने कम्बल कुनै सरकारी निकाय वा संस्थाको सहयोग  बिना  किन्न सक्नु भयो? "
        ]
    },
    difficult_blanket: {
        label: [
            "Which of your children age (5-18) are not able to get two sets of clothes?",
            "१२अ.  तपाइको कुन उमेरको  बच्चाले २ सेट कपडा कुनै सरकारी निकाय वा संस्थाको सहयोग  बिना  किन्न सक्नु भयो? "
        ]
    },
    check_orphan: {
        label: [
            "Does house have any disable or orphan children?",
            "के घरमा कुनै अपाङ्ग वा अनाथ बालबालिका छन? "
        ]
    },
    orphan_discrimination: {
        label: [
            "Did orphans or disabled children receive following commodity?",
            "के घरमा भएका अपाङ्ग वा अनाथ बालबालिकाले तपसिलकासमान हरु पाए? "
        ]
    },
    add_outcome_indicator1: {
        label: [
            "Outcome indicator1 (C4B.21068) Proportion of participating producer groups with an increased annual net profit",
            ""
        ]
    },
    part1: {
        label: [
            "PART A.1- GROUP DESCRIPTION - To be filled in by Market Facilitator before meeting with a producer group",
            "भाग- १ "
        ]
    },
    market_facilitator: {
        label: [
            "Are you market facilitator? ",
            "तपाई बजार सहजकर्ता हो ? "
        ]
    },
    group_information: {
        label: [
            "Group information",
            ""
        ]
    },
    group_leader: {
        label: [
            "Name of Group Leader",
            "१5. समुहको अगुवाको नाम"
        ]
    },
    location: {
        label: [
            "Gorup location",
            "१६. समुहको स्थान"
        ]
    },
    date: {
        label: [
            "Date of data collection",
            "१७. तथ्याड़क संकलन गरेको मिति"
        ]
    },
    product: {
        label: [
            "What are the type of product that are produced by your group ?",
            "1८. तपाइको  उत्पादन समुहले उत्पादन के के हो ?"
        ]
    },
    product_other: {
        label: [
            "pls specify others",
            "१८.अ अन्य भए खुलाउनुहोस "
        ]
    },
    measurement: {
        label: [
            "Measurements",
            "उत्पादन मापन"
        ]
    },
    volumn: {
        label: [
            "Volumn measurements ",
            "१९. उत्पादनको मात्राको  मापन के मा गरिन्छ ?"
        ]
    },
    volumn_other: {
        label: [
            "pls specify others",
            "१९.अ  अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    land: {
        label: [
            "Used Land by Produer Group",
            "२०. तपाइको समुहले उत्पादनको लागि प्रयोग गरेको जग्गा को क्षेत्रफल  कति छ ?"
        ]
    },
    area: {
        label: [
            "Area measured in ",
            "२१. समुहले प्रयोग गरेको जग्गाको  नाप के मा गरिएको हो "
        ]
    },
    part2: {
        label: [
            "PART A.2 - To be filled in by Market Facilitator WITH the group",
            "भाग- २ ,- सामुहिक प्रश्न- बजार सहजकर्ताले समुहमा भर्ने "
        ]
    },
    production_cycle: {
        label: [
            "In the last production cycle, did the group in group basis:",
            " समुहले गत उत्पादन चक्रमा  केहि  सामुहिक रुपमा :"
        ]
    },
    access_financial: {
        label: [
            "2.1 Access formal financial services?",
            "औपचारिक वित्तीय सेवामा पहुँच रह्यो? "
        ]
    },
    technical_service: {
        label: [
            "2.2 Access technical services?",
            "तपाई को समुहले प्राबिधिक सेवामा पहुच पाउनु भयो ?"
        ]
    },
    purchase_inputs: {
        label: [
            "2.3 Purchase inputs together?",
            "सामुहिक बस्तुको खरिद गर्नुभयको छ ?"
        ]
    },
    collected_together: {
        label: [
            "2.4 How much product was collected by the group to be sold collectively or processed? ",
            "समुहले सामुहिक रुपमा बिक्री वा प्रशोधन गर्न कति उताप्दन गर्नुभयो?"
        ]
    },
    processed_product: {
        label: [
            "2.5 Of this, how much product was processed?",
            "यसमा कति उत्पादन प्रशोधन गरियो ?"
        ]
    },
    processed_cost: {
        label: [
            "2.6 How much money did the group receive for processed products?",
            "समूले प्रशोधन गरे वापत कति रकम प्राप्त गर्यो ?"
        ]
    },
    unprocessed_cost: {
        label: [
            "2.7 What was the sale price for product that was sold by group as unprocessed product:",
            "समुहले प्रशोधन नगरी बिक्री गरेको उत्पादनको बिक्री मूल्य कति थियो ?"
        ]
    },
    men_member: {
        label: [
            "2.8 Number of men",
            "पुरुषको संख्या कति छ?"
        ]
    },
    women_member: {
        label: [
            "2.9 Number of women",
            "महिलाको संख्या कति छ?"
        ]
    },
    total_group_production: {
        label: [
            "2.10 Total group production",
            "जम्मा सामुहिक उत्पादन कति छ?"
        ]
    },
    total_cost_production: {
        label: [
            "2.11 Total costs of production",
            "उत्पादनको जम्मा लागत कति थियो?"
        ]
    },
    total_growing_area: {
        label: [
            "2.12 Total growing area (group total)",
            "जम्मा उत्पादन क्षेत्रफ़ल्  कति थियो?"
        ]
    },
    quantity_individually: {
        label: [
            "2.13 Quantity sold individually",
            "एकल बिक्री परिमाण कति थियो"
        ]
    },
    goods: {
        label: [
            "2.14 Total goods",
            "जम्मा बस्तुहरु कति थियो?"
        ]
    },
    average_price: {
        label: [
            "2.15 Existing average price in local market",
            "स्थानीय बजारमा  औसत मुल्य कति छ?"
        ]
    },
    aed_outcome_indicator2: {
        label: [
            "Outcome 2 (C4B.21071) Percent of participating producers who report utilising formal financial services.  Formal financial services include credit, savings and insurance products offered by a registered financial institution (i.e. not local money lenders and not savings groups).",
            "नतिजा-२ औपचारिक आर्थिक सेवाहरु प्रयोग गरिरहेका सहभागी उत्पादकहरुको प्रतिशत | कुनै सरकारी निकायमा दर्ता भएका संस्था जसले ऋण, बचत र बीमा जस्ता  उपलब्ध गराउने सेवाहरुलाई  औपचारिक  आर्थिक सेवाहरु भनिन्छ  ( स्थानीय कर्जादिने ब्यक्तिहरु वा बचत समूहरु होइन ) | "
        ]
    },
    individual_beneficiary: {
        label: [
            "Questions for individual beneficiary",
            "व्यक्तिगत लाभग्राहीकोलागि प्रश्नहरु"
        ]
    },
    grow_food: {
        label: [
            "Did you grow crops in this season ?",
            "यो मौसममा बाली उत्पादन गर्नुभयो ?"
        ]
    },
    crops: {
        label: [
            "1. What are the crops that are grown during this season? ( quantity and unit)",
            "१. यो मौसममा उत्पादन गर्नु भएको बालीहरु के के हुन् ? ( मात्रा र एकाई )"
        ]
    },
    crops_other: {
        label: [
            "pls specify others",
            "अरु उत्पादनहरुको जानकारी दिनुहोस "
        ]
    },
    crop_details: {
        label: [
            "crop_details",
            "बालीहरुको बिवरण"
        ]
    },
    crops_grown: {
        label: [
            "",
            ""
        ]
    },
    crops_grown_name: {
        label: [
            "",
            ""
        ]
    },
    quantity_grown: {
        label: [
            "1b. What is the quantity of grown $$ during this seasion ?",
            "यो समयमा कति मात्रामा  $$ उत्पादन भयो?"
        ]
    },
    unit_grown: {
        label: [
            "1c. What is the unit of grown $$  during this seasion ?",
            "यो समयमा के इकाइमा $$ उत्पादन भयो? "
        ]
    },
    crop_end: {
        label: [
            "crop_end",
            ""
        ]
    },
    food_sale: {
        label: [
            "Did you sell crops in this season ?",
            "के यो मौसममा तपाईंले फसल बेच्नुभयो?"
        ]
    },
    crops_sale: {
        label: [
            "What crops did you sale in this season?",
            "यो मौसममा के फसल बेच्नुभयो?"
        ]
    },
    crops_sale_other: {
        label: [
            "pls specify others",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    sale_details: {
        label: [
            "sale_details",
            ""
        ]
    },
    crops_sale_item: {
        label: [
            "",
            ""
        ]
    },
    crops_sell_name: {
        label: [
            "",
            ""
        ]
    },
    crops_sale_details: {
        label: [
            "2a. Details about the crops sale this season?",
            "यो मौसममा बिक्री भएको बालीको बिबरण?"
        ]
    },
    crops_sale_details_other: {
        label: [
            "pls specify others",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    quantity_sale: {
        label: [
            "2b. What is the quantity of $$ sale in this season ?",
            "बिक्रीहुने  मौसम(सिजन)मा कति  परिमाण $$  बिक्री गर्नु  भयो ?"
        ]
    },
    unit_sale: {
        label: [
            "2c. What is the unit of $$ sale during this seasion ?",
            "यो  मौसम(सिजन)मा कति  इकाई $$  बिक्री गर्नु  भयो ?"
        ]
    },
    saving_money: {
        label: [
            "Do you, or any member of your household, have a formal means of saving money in cash form?",
            "तपाई वा तपाइको परिवारमा कसैले नियमित रुपमा नगत बचत गर्ने गर्नु भएको छ?"
        ]
    },
    price_sale: {
        label: [
            "2d. What is the total price $$ sale during this seasion ?",
            "मौसम(सिजन)मा जम्मा कति  मूल्यको $$ बिक्रि गर्नु भयो ?"
        ]
    },

    sale_end: {
        label: [
            "sale_end",
            ""
        ]
    },
    sale_location: {
        label: [
            "3. Where did you sale?",
            "कहाँ बेच्नुहुन्छ./बेच्नुभयो?"
        ]
    },
    sale_location_other: {
        label: [
            "pls specify others",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    other_saving: {
        label: [
            "4.Do you have other saving except group saving such as in cooperative and bank?  ",
            "के तपाइको समूह  बाहेक अरुमा बचत छ जस्तै सहकारी वा बैंक?"
        ]
    },
    group_saving: {
        label: [
            "Group saving this month (Family)",
            "यो महिनाको समूहमा बचत"
        ]
    },
    saving: {
        label: [
            "Saving this month (Family) ",
            "यो महिना बचत "
        ]
    },
    recent_loan: {
        label: [
            "5. Where did you get the recent loan from?",
            "तपाइले कहाँ बाट ऋण लिनु भयो?"
        ]
    },
    recent_loan_other: {
        label: [
            "pls specify others",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    financing_require: {
        label: [
            "6.Referring to most recent loan, did the financing require any collateral?                                       ",
            "हालै  लिईएको ऋणको लागि कुनै जमानत राख्नु भयो?"
        ]
    },
    collateral_type: {
        label: [
            "6a.  If yes What type of collateral did you use ?   ",
            "यदि छ भने , कस्तो प्रकारको  धितो राखियो?"
        ]
    },
    collateral_type_other: {
        label: [
            "pls specify others",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    loan: {
        label: [
            "7. Recently did you apply for any loans for your farm?",
            "तपाईले गखेतीको लागि ऋण लिनुभएको हो? "
        ]
    },
    loan_apply: {
        label: [
            "7a. If no why?",
            "यदि लिनु भएको छैन भने, किन?"
        ]
    },
    loan_apply_other: {
        label: [
            "other specify",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    crop_insurence: {
        label: [
            "8. In the last year did you apply for any crop insurance for your farm? ",
            "गएको बर्ष के तपाइले बालीको बिमाको लागि निबेदन दिनुभयो?"
        ]
    },
    crop_insurence_why: {
        label: [
            "8a. Why didnt you apply for insurence ? ",
            "तपाइले किन बिमाको लागि निबेदन दिनुभएन?"
        ]
    },
    insurence_why_other: {
        label: [
            "other specify",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    major_source: {
        label: [
            "9. Major income sources?",
            "आयको मुख्य स्रोत?"
        ]
    },
    major_source_other: {
        label: [
            "9a.other specify",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    alternative_source: {
        label: [
            "10. Types of alternative income sources?",
            "बैकल्पिक आम्दानीको प्रकारहरु?"
        ]
    },
    alternative_source_other: {
        label: [
            "10a. other specify",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    estimated_earning: {
        label: [
            "11. Estimated earning per year (in Thousands, NRS) from all sources of income?",
            "सबै स्रोतबाट अनुमानीत आम्दानी प्रति वर्ष (हजार,ने रु.)?"
        ]
    },
    expenses: {
        label: [
            "12. What are the area of your household expenses ?",
            "तपाइको घर परिवारको  खर्च हुने  छेत्रहरु?"
        ]
    },
    expenditure: {
        label: [
            "Expenditure this month (Excluding agri expenditure) ",
            "Expenditure this month (Excluding agri expenditure) "
        ]
    },
    expenses_other: {
        label: [
            "12c.other specify",
            "अन्यको लागि बिस्तृत लेख्नुहोस"
        ]
    },
    estimated_expenses: {
        label: [
            "13. Estimated expenses per YEAR  in the household?",
            "तपाइको घरको वार्षिक औसत खर्च कति हुन्छ ?"
        ]
    },
    land_area: {
        label: [
            "14. How much land do you have for production?",
            "उत्पादन गर्नको लागि  तपाईसंग कती जग्गा-जमिन छ?"
        ]
    },
    area_production: {
        label: [
            "15. What is the production area measured in ?",
            "उत्पादन क्षेत्रलाई के मा  मापन गरिन्छ?"
        ]
    },
    production_consumed: {
        label: [
            "17. Production consumed at HH?",
            "उत्पादन मध्ये  घरमा उपभोगकति  भयो ?"
        ]
    },
    production_wastage: {
        label: [
            "18. Production Wastage on Stock?",
            "उत्पादन मध्ये भण्डारमा कति नाश भएको उत्पादन? "
        ]
    },
    production_sold: {
        label: [
            "19. Production Sold",
            "उत्पादन मध्ये  कति बिक्री गर्नु भयो ?"
        ]
    },
    production_investment: {
        label: [
            "20. Cash Investment for Production ?",
            "उत्पादनको लागि नगद लगानी कति गर्नु भयो  ?"
        ]
    },
    production_saving: {
        label: [
            "21. Annual Saving from production ?",
            "उत्पादनबाट वार्षिक बचत गर्नु भयो ?"
        ]
    },
    food_worry: {
        label: [
            "In the past twelve months, did you worry that your household would not have enough food?",
            "बिगत १२ महिनामा, के तपाइलाई आफ्नो घरमा प्रशस्त खाना नहोला भन्ने चिन्ता लागेको थियो?"
        ]
    },
    lack_food: {
        label: [
            "In the past twelve months, were you or any household member not able to eat the kinds of foods you preferred because of a lack of resources?",
            "बिगत १२ महिनामा, तपाई वा तपाइको परिवारको कुनै सदस्यले श्रोतको अभावका कारण आफुले खोजेको जस्तो खाना खान नपाएको  अवस्था थियो?"
        ]
    },
    variety_food: {
        label: [
            "In the past twelve months, did you or any household member have to eat a limited variety of foods due to a lack of resources?",
            "बिगत १२ महिनामा, तपाई वा तपाइको परिवारको कुनै सदस्यले श्रोतको अभावका कारण सिमित प्रकारका खान खानु परेको थियो?"
        ]
    },
    want_food: {
        label: [
            "In the past twelve months, , did you or any household member have to eat some foods that you really did not want to eat because of a lack of resources to obtain any other of food?",
            "	बिगत १२ महिनामा, तपाई वा तपाइको परिवारको कुनै सदस्यले श्रोतको अभावका कारण अथवा चाहेको खाना प्राप्त गर्न नसकेको कारण आफुले नचाहेको खाना खानु परेको थियो?"
        ]
    },
    small_meal: {
        label: [
            "In the past 12 months, did you or any household member have to eat a smaller meal than you felt you needed because there was not enough food?",
            "बिगत १२ महिनामा, प्रशस्त खाना नभएका कारण तपाई वा तपाइको परिवारको कुनै सदस्यले चाहिने भन्दा कम खाना खानु परेको ?"
        ]
    },
    few_meal: {
        label: [
            "In the past 12 months, did you or any other household member have to eat fewer meals in a day because there was not enough food?",
            "बिगत १२ महिनामा, प्रशस्त खाना नभएका कारण तपाई वा तपाइको परिवारको कुनै सदस्यले दिनमा सामान्यतया खाने भन्दा कम चोटी खाना खानु परेको ?"
        ]
    },
    no_food: {
        label: [
            "In the past 12 months, was there ever no food to eat of any kind in your household because of lack of resources to get food?",
            "बिगत १२ महिनामा, श्रोत अभावका कारण तपाइको परिवारमा खानको निम्ति कुनै पनि किसिमको खाना नभएको अवस्था थियो?"
        ]
    },
    hungry_night: {
        label: [
            "In the past 12 months, did you or any household member go to sleep at night hungry because there was not enough food?",
            "बिगत १२ महिनामा, प्रशस्त खाना नभएका कारण तपाई वा तपाइको परिवारको कुनै सदस्य भोकै सुत्नु परेको थियो?"
        ]
    },
    hungry_day: {
        label: [
            "In the past 12 months, did you or any household member go a whole day and night without eating anything because there was not enough food?",
            "बिगत १२ महिनामा, प्रशस्त खाना नभएका कारण तपाई वा तपाइको परिवारको कुनै सदस्य पुरै एक दिन र रात केहि पनि नखाई बस्नु परेको थियो? "
        ]
    },
    lease_land: {
        label: [
            "Lease Land",
            "कमाएको जमीन"
        ]
    },
    own_land: {
        label: [
            "Own Land",
            "आफ्नै जमीन"
        ]
    },
    vegetable_production_land: {
        label: [
            "Vegetable Production Land ",
            "सागसब्जी उत्पादन जमीन"
        ]
    },
    unit: {
        label: [
            "Unit? ",
            "एकाइ? "
        ]
    },
    returning_from_healthpost:{
        label:[
            "Is this child returning from healthpost?",
            "विगत केहि दिनहरुमा तपाई अस्पताल गएको छ?",
        ]
    }
}

export default (label, lang = 0, replace = false, search = '$$') => {
    let main = labels[label] || [];
    if (main.length == 0) {
        return "Label not found (Skip this question).";
    }
    let lab = main.label[lang];
    if (replace) {
        lab = lab.replace(search, replace)
    }
    return lab;
}