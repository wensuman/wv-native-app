import {
    StyleSheet
} from "react-native";

export const paddingH = {
    paddingLeft: 20,
    paddingRight: 20
}
export default styles = StyleSheet.create({
    paddingH
})