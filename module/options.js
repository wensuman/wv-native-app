import _ from 'lodash';
export const options = {
    phase: [{
        label: ['phase I', 'phase I'],
        value: 'phase I'
    },
    {
        label: ['phase II', 'phase II'],
        value: 'phase II'
    },
    {
        label: ['phase III', 'phase III'],
        value: 'phase III'
    }],
    phase2: [{
        label: ['phase I', 'phase I'],
        value: 'phase I'
    },
    {
        label: ['phase II', 'phase II'],
        value: 'phase II'
    }],
    pdherth_taken_at: [{
        label: ['First day', 'First day'],
        value: 1
    }, {
        label: ['12 days', '12 days'],
        value: 12
    }, {
        label: ['1 month', '1 month'],
        value: 30
    }, {
        label: ['Three month', 'Three month'],
        value: 90
    }, {
        label: ['Six month', '६ महिना'],
        value: 180
    }, {
        label: ['1 year', '१ वर्ष'],
        value: 360
    }],
    building: [{
        label: ['One', 'एक'],
        value: 1
    },
    {
        label: ['Two', 'दुई'],
        value: 2
    }],
    u5_number: [{
        label: ['One', 'एक'],
        value: [1]
    },
    {
        label: ['Two', 'दुइ'],
        value: [1, 2]
    },
    {
        label: ['Three', 'तिन'],
        value: [1, 2, 3]
    },
    {
        label: ['Four', 'चार'],
        value: [1, 2, 3, 4]
    },
    {
        label: ['Five', 'पाँच'],
        value: [1, 2, 3, 4, 5]
    }],
    project: [{
        label: ['CESP', 'सामुदायिक संलग्नता तथा स्पोंसोर्शिप परियोजना'],
        value: 1
    },
    {
        label: ['DAP', 'विकासमुलक सम्पतिहरु'],
        value: 2
    },
    {
        label: ['YOUTH', 'युवा'],
        value: 3
    },
    {
        label: ['AED', 'कृषि तथा आर्थिक विकास'],
        value: 5
    },
    {
        label: ['Education', 'शिक्षा'],
        value: 6
    },
    {
        label: ['Health', 'स्वास्थ्य'],
        value: 7
    },
    {
        label: ['Child Protection', 'बाल संरक्षण'],
        value: 8
    }],
    all: [{
        label: ['C1a_0008 : Prevalence of Stunting among children 0-5 years', 'c1a_0008: 0-५ वर्षको बालबालिकाहरुमा हुने पुड्कोपन '],
        value: 'c1a_0008'
    },
    {
        label: ["C2A.24274", "C2A.24274"],
        value: "c2a_24274"
    }, {
        label: ["C2D.24263 : Proportion schools meeting DRR criteria for safe learning environments", "C2D.24263 : सुरक्षित शिक्षण वातावरणको लागि DRR मापदण्डको बैठकमा विद्यालयहरू"],
        value: "c2d_24263"
    },{
        label: ["C2A.24023 : % reading clubs achieving minimum standards", "C2A.24023 : न्यूनतम स्तरहरू प्राप्त गर्न पढ्ने क्लबहरू % "],
        value: "c2a_24023"
    }, {
        label: ["C2A.23988", "C2A.23988"],
        value: "c2a_23988"
    }, {
        label: ["C2D.24275", "C2D.24275"],
        value: "c2d_24275"
    }, {
        label: ["C2D.24276 : % schools with basic handwashing facilities ", "C2D.24276 : % आधारभूत हस्तलेखन सुविधाहरू संग स्कूलहरू"],
        value: "c2d_24276"
    }, {
        label: ["C2D. 24264: % of schools with improved prioritized minimum enabling conditions (PMEC) results. ", "C2D.24264 : सुधारिएको प्राथमिकता न्यूनतम सक्षम स्थितिहरू (पीईईसीईसी) संग विद्यालयहरूको% परिणाम"],
        value: "c2d_24264"
    }, {
        label: ["C2D.23018 parent: Proportion of users who are satisfied with the education services they have received", " C2D.23018: प्राप्त गरेको शिक्षा सेवाबाट सन्तुस्ट भएका प्रयोगकर्ताको अनुपात"],
        value: "c2d_23018p"
    },{
        label: ["C2D.23018 student: Proportion of users who are satisfied with the education services they have received", " C2D.23018: प्राप्त गरेको शिक्षा सेवाबाट सन्तुस्ट भएका प्रयोगकर्ताको अनुपात"],
        value: "c2d_23018s"
    },
    {
        label: ['C1a_0013: Prevalence of Underweight among children 0-5 years ', 'c1a_0013: 0-५ बर्षको बालबालिकाहरुमा कम वजन हुनु '],
        value: 'c1a_0013'
    },
    {
        label: ['C1a_0015 : % of children aged 0-59 months who were sick in the  previous 2 weeks (diarrhoea, fever, cough) and who had increased breastfeeding and/or fluids and/or continued foods, as appropriate. ', 'c1a_0015: 0-59 महिनाको बालबालिका जो गत २ हप्तामा बिरामी भएका थिए( झाडापखाला, जोरो, खोकी) र जसले धेरै स्तनपान र झोल पदार्थ खाएका थिए'],
        value: 'c1a_0015'
    },
    {
        label: ['C1a_0018: Prevalence of Wasting among children 0-5 years', 'c1a_0018: 0-५ बर्षको बालबालिकाहरुमा खेर फाल्ने बानि'],
        value: 'c1a_0018'
    },
    {
        label: ['C1a_0020: % of children 6-59 months who consume iron rich foods  in past 24 hours ', 'c1a_0020: गत २४ घन्टामा ६-५९ महिनाको बालबालिकाहरुको प्रतिशत जसले आइरनयुक्त खानाहरु खाए '],
        value: 'c1a_0020'
    },
    {
        label: ['C1a_0022: Proportion of children receiving minimum dietary diversity ', 'c1a_0022: न्यूनतम आहार विविधता प्राप्त गर्ने बालबालिकाहरूको अनुपात'],
        value: 'c1a_0022'
    },
    {
        label: ['C1a_0032: Proportion of pregnant women who consume iron and folic acid tablet during pregnancy', 'c1a_0032: गर्भवती महिलाहरुको अनुपात जसले गर्भावस्थामा आइरन र फोलिक अचिद चक्की उपभोग गरे'],
        value: 'c1a_0032'
    },
    {
        label: ['C1a_0033: Proportion of children under 2 years receiving early initiation of breastfeeding (early initiation of BF) ', 'c1a_0033: स्तनपानको प्रारम्भिक सुरुवात गर्ने २ बर्ष तलका बालबालिकाहरुको अनुपात'],
        value: 'c1a_0033'
    },
    {
        label: ['C1a_0048: Proportion of women who increased food consumption during most', 'c1a_0048: गर्भवती समयमा खाना खपत बढाउनु भएका महिलाहरूको अनुपात'],
        value: 'c1a_0048'
    },
    {
        label: ['C1a_21338: Prevalence of Low Birth Weight Baby (Birth Weight less than 2500 grams) ', 'c1a_21338: कम वजन भएको बालबालिकाको जन्म (जन्मिदा २५००ग्राम् भन्दा कम वजन)'],
        value: 'c1a_21338'
    },
    {
        label: ['C1A.24267', 'C1A.24267'],
        value: 'c1a_24267'
    },
    {
        label: ['C1b_0065: % of children aged 12-59 months who have completed 3rd DPT dose plus measles vaccination, verified by vaccination card and mother\'s recall','c1b_0065: १२-५९ महिनाको बालबालिकाहरुको प्रतिशत जसले तेस्रो तहको डिपी टि डोज साथै दादुरा खोप लगाईसकेका छन् जुन खोप पत्र र आमाको कुराबाट पुस्टि हुन्छ'],
        value: 'c1b_0065'
    },
    {
        label: ['C1b_0067: Proportion of children under 5 with diarrhoea who received correct management of diarrhoea ', 'c1b_0067: ५ बर्ष मुनिका बालबालिकाहरुको प्रतिशत जसलाई झाडापखाला लागेको र त्यसको ठिक उपचार पाएको '],
        value: 'c1b_0067'
    },
    {
        label: ['C1b_0072: % of children aged 0–59 months with a ‘presumed pneumonia’ (ARI) episode in the past two weeks that were taken to an appropriate health-care provider ', 'c1b_0072: 0-५९ महिनाको बालबालिकाहरुको प्रतिशत जसलाई गत २ हप्तामा निमोनिया अनुमानित गरिएको थियो र उपयुक्त स्वाथ्य हेरविचार केन्द्रमा लागिएको थियो '],
        value: 'c1b_0072'
    },
    {
        label: ['C1b_0087: Prevalance of disease and infection among under five children especially Diarrhoea', 'c1b_0087: ५ बर्ष मुनिका बालबालिकाहरुमा रोग र संक्रमण , बिशेष गरि झाडापखाला'],
        value: 'c1b_0087'
    },
    {
        label: ['C1b_0091: Prevalance of disease and infection among under five children especially pneumonia ', 'c1b_0091: ५ बर्ष मुनिका बालबालिकाहरुमा रोग र संक्रमण , बिशेष गरि निमोनिया'],
        value: 'c1b_0091'
    },
    {
        label: ['C1b_0128: Proportion of parents or caregivers with appropriate hand-washing behaviour ', 'c1b_0128: अभिभवक वा हेरचाह गर्ने व्यक्तिको प्रतिशत जो राम्रो संग हात धुने गर्छन'],
        value: 'c1b_0128'
    },
    {
        label: ['C1b_0130: % of households with a designated place for handwashing where water and soap are present ', 'c1b_0130: घरको प्रतिशत जहाँ हात धुनको लागि साबुन र पानी भएको ठाउँ छ'],
        value: 'c1b_0130'
    },
    {
        label: ['C1b_0131: Proportion of parents or caregivers with children 0–23 months who report that their child\'s stools are safely disposed off', 'c1b_0131: 0-२३ महिनाको बच्चा भएको आमाबाबु वा हेरचाह गर्ने व्यक्तिको प्रतिशत जसले आफ्नो बच्चाको दिशापिसाब सुरछित ढंगले बिसर्जन गरेको रिपोर्ट दिन्छन्'],
        value: 'c1b_0131'
    },
    {
        label: ['C1BSAV', 'C1BSAV'],
        value: 'c1bsav'
    },
    {
        label: ['C1c_0152: % of mothers of children aged 0-23 months who delivered with Skilled Birth Attendant and to be disaggregated by age (C) (S)', 'c1c_0152: 0-२३ महिनाको बच्चाहरुको आमाको प्रतिशत जसले दछ जन्म परिचरबाट बच्चा जन्म दिए र उमेरद्वारा अलग हुन '],
        value: 'c1c_0152'
    },
    {
        label: ['C1c_0156: Proportion of mothers who report that they had four or more antenatal visits while they were pregnant  and to be disaggregated by age ', 'c1c_0156: आमाहरुको प्रतिशत जसले आफु गर्भवती हुदा ४ अथवा बढी स्वाथ्य भेटघाट भएको'],
        value: 'c1c_0156'
    },
    {
        label: ['C1c_0160: Proportion of mothers of children aged 0–23 months who received at least 2 post-natal visit from a trained health care worker during the first week after birth (C)(S)', 'c1c_0160: 0-२३ महिनाको बच्चाहरुको आमाको प्रतिशत जसले जन्म दिए पश्चात कमसेकम २ चोटी दछ स्वाथ्य सेवा कर्मिबाट जाच गराएको '],
        value: 'c1c_0160'
    },
    {
        label: ['C2a_21686: Strengthened teaching and learning practices enhance children\'s literacy ', 'c2a_21686: बलियो शिक्षा र सिकाउने अभ्यासहरूले बच्चाहरूको साक्षरता बढाउँछ'],
        value: 'c2a_21686'
    },
    {
        label: ['C2b_22844: Proportion of Youths and adolescents that report improved self-efficacy', 'c2b_22844: युवा र किशोरहरूको अनुपात जसले आत्म-प्रभावकारिता सुधार भएको बताए '],
        value: 'c2b_22844'
    },
    {
        label: ['C2c_0293: Proportion of youth and adolescents who have a learning opportunity that leads to a productive life', 'c2c_0293: जवान र किशोरीहरूको अनुपात जसले सिक्ने अवसर पाएको छन् जसले उत्पादक जीवन यापन गर्न मदत गर्छ'],
        value: 'c2c_0293'
    },
    {
        label: ['C2d_23018: Proportion of users who are satisfied with the education services they have received ', 'c2d_23018: प्रयोगकर्ताहरुको अनुपात जो पाएको शिक्षाको सेवाबाट सन्तुस्ट छन्'],
        value: 'c2d_23018'
    },

    {
        label: ['C3c_22869: Proportion of female and male youth that report improved attitudes towards civic engagement', 'c3c_22869: युवा महिला र पुरुषहरुको अनुपात जसले नागरिक संलग्नतामा राम्रो अथवा सुधार गरिएको मनोवृतिको बारेमा बताएका छन्'],
        value: 'c3c_22869'
    },
    {
        label: ['C4a_0014:  Proportion of children under 18 years, married  ', 'c4a_0014: १८ बर्ष भन्दा मुनिको विवाहित बालबालिकाहरुको अनुपात'],
        value: 'c4a_0014'
    },
    {
        label: ['C4a_0165: The strengths of the assets and the contexts in which adolescents live,learn and work as reported by adolescents 12-18 years of age', 'c4a_0165: सम्पति र सन्दर्भको शक्ति जसद्वारा जवानहरु जिउन, सिक्न र काम गर्छन'],
        value: 'c4a_0165'
    },
    {
        label: ['C4a_0190: Proportion of children engaged in child labour', 'c4a_0190: बाल श्रममा संलग्न बालबालिकाहरुको अनुपात'],
        value: 'c4a_0190'
    },
    {
        label: ['C4a_0196: Proportion of parents or caregivers who feel that their community is a safe place for children', 'c4a_0196: अभिभावक र हेरचाह गर्ने व्यक्तिको अनुपात जसले आफ्नो समुदाय बालबालिकाहरुको लागि सुरक्षित भएको महसुस गर्छन'],
        value: 'c4a_0196'
    },
    {
        label: ['C4a_0219: Proportion of adolescents who know of the presence of services and mechanisms to receive and respond to reports of abuse, neglect, exploitation or violence against children', 'c4a_0219: जवानहरुको अनुपात जसले बालबालिकाहरुको बिरुद्ध हुने दुरुपयोग,उपेक्षा, शोषण र हिंसाको बारेमा रिपोर्ट गर्ने सेवा अथवा प्रणालीको बारेमा जान्दछन'],
        value: 'c4a_0219'
    },
    {
        label: ['C4a_0221: Proportion of adolescents who report having experienced any physical violence in the past 12 months', 'c4a_0221: गत 12 महिनामा कुनै पनि शारीरिक हिंसाको अनुभव गर्ने किशोरहरुको अनुपात'],
        value: 'c4a_0221'
    },
    {
        label: ['C4a_21415: Proportion of households able to recall the community vision for child well-being', 'c4a_21415: घरको सदस्यहरुको अनुपात जसले बालबालिकाहरुको भलोको लागि सामुदायिक द्दृस्टीको रुपमा सम्झाउने काम गरेका छन् '],
        value: 'c4a_21415'
    },
    {
        label: ["C4B.24262", "C4B.24262"],
        value: "c4b_24262"
    },
    {
        label: ['C4a_22917: Proportion of users who are satisfied with the child protection services they have received', 'c4a_22917: प्रयोगकर्ताहरुको अनुपात जो पाएको बालबालिकाहरुको सुरक्षा सम्बन्धि सेवाहरुप्रति संतुस्ता छन '],
        value: 'c4a_22917'
    },
    {
        label: ['C4a_22920: Proportion of child protection service users who report increased responsiveness of child protection service providers towards communities ', 'c4a_22920: बाल सुरक्षा सेवाको प्रयोगकर्ताहरुको अनुपात जसले बाल सुरक्षा सेवाकर्मीहरुकोबढ्दो प्रतिक्रियाको बारेमा बताएका छन् '],
        value: 'c4a_22920'
    },
    {
        label: ['C4a_23441: Proportion of adolescent females and males (direct participants) who have an increase in gender equitable attitudes ', 'c4a_23441: किशोर महिला र पुरुषहरुको अनुपात जसको समान लिंग प्रति बढ्दो दृष्टिकोण रहेको छ '],
        value: 'c4a_23441'
    },
    {
        label: ['C4a_23443: Proportion of most vulnerable HHs who are confident that they can access CP services available without external support ', 'c4a_23443: कम्जोर परिवारको अनुपात जो बाल सुरक्षा सम्बन्धि सेवाहरु कुनै बाहिरी सहायता बिना पहुच गर्न सक्ने कुरामा बिस्वस्त छन्'],
        value: 'c4a_23443'
    },
    {
        label: ['C4A.24265', 'C4A.24265'],
        value: 'c4a_24265'
    },
    {
        label: ['C4b_0044: Percent of parents or caregivers who are able to provide all the children in the household, aged 5-18 years, with three important items, through their own means (assets/production/income), without external assistance (from outside the family, NGO or government) in the past 12 months.', 'c4b_0044: आमाबुबा र हेरचाह गर्ने व्यक्तिको प्रतिशत जसले आफ्नो ५-१८ बर्षको बालबालिकाहरुलाई गत १२ महिनामा सबै कुरा पुर्याउन सकेका छन्, बिशेष गरि ३ मुख्य कुरा हरु आफ्नै माध्यमबाट (सम्पति/उत्पादन/आय), कुनै बाह्य सहयोग नलिकन (परिवारको भन्दा बाहिर/गैर सरकारी संगठन/सरकार)'],
        value: 'c4b_0044'
    },
    {
        label: ['C4b_0069: Percent of households who report being able to save money in liquid form. For example in a bank or credit union', 'c4b_0069:: घरपरिवारको प्रतिशत जसले पैसा जोगाउन सके जस्तै: बैंक अथवा सहकारी संस्थामा '],
        value: 'c4b_0069'
    },

    {
        label: ['C4b_21068 : Proportion of participating producer groups with an increased annual net profit', 'c4b_21068: वार्षिक कुल मुनाफा वृद्धि भएको सहभागी उत्पादन समुहको अनुपात'],
        value: 'c4b_21068'
    },
    {
        label: ['C4b_21071: Percent of participating producers who report utilising formal financial services.  Formal financial services include credit, savings and insurance products offered by a registered financial institution (i.e. not local money lenders and not savings groups).', 'c4b_21071: सहभागी उत्पादनकर्ताहरूको प्रतिशत जसले औपचारिक वित्तीय सेवाहरू प्रयोग गरेको रिपोर्ट गर्दछ। औपचारिक वित्तीय सेवा भित्र क्रेडिट, बचत र बिमाको उत्पादनहरु पर्दछ जुन दर्ता गरिएको वित्तीय संस्थाद्वारा प्रस्तावित गरिन्छ|'],
        value: 'c4b_21071'
    },
    {
        label: ["C4B.24266", "C4B.24266"],
        value: "c4b_24266"
    },
    {
        label: ['C4C.23075', 'C4C.23075'],
        value: 'c4c_23075'
    },
    ],
    tp_1: [{
        label: ["C4a_21415: Proportion of households able to recall the community vision for child well-being", "c4a_21415: बालबालिकाको भलाइको लागि सामुदायिक दृष्टिलाई सम्झना गर्ने परिवारहरूको अनुपात"],
        value: "c4a_21415"
    },
    {
        label: ["C4B.24262", "C4B.24262"],
        value: "c4b_24262"
    }
    ],
    tp_2: [{
        label: ["C4a_0165: The strengths of the assets and the contexts in which adolescents live, learn and work as reported by adolescents 12-18 years of age.", "c4a_0165: सम्पति र सन्दर्भको शक्ति जसद्वारा जवानहरु जिउन, सिक्न र काम गर्छन'"],
        value: "c4a_0165"
    }],
    tp_3: [{
        label: ["C4B.24266", "C4B.24266"],
        value: "c4b_24266"
    },
    {
        label: ["C2b_22844: Proportion of Youths and adolescents that report improved self-efficacy", "c2b_22844: युवा र किशोरहरूको अनुपात जसले आत्म-प्रभावकारिता सुधार गरेको रिपोर्ट गरे"],
        value: "c2b_22844"
    },
    {
        label: ["C3c_22869: Proportion of female and male youth that report improved attitudes towards civic engagement.", "c3c_22869: युवा महिला र पुरुषहरुको अनुपात जसले नागरिक संलग्नतामा राम्रो अथवा सुधार गरिएको मनोवृतिको बारेमा बताएका छन्"],
        value: "c3c_22869"
    },
    {
        label: ["C2c_0293: Proportion of youth and adolescents who have a learning opportunity that leads to a productive life", "c2c_0293: जवान र किशोरीहरूको अनुपात जसले सिक्ने अवसर पाएको छन् जसले उत्पादक जीवन यापन गर्न मदत गर्छ"],
        value: "c2c_0293"
    }
    ],
    tp_5: [{
        label: ["C4b_0069: Percent of households who report being able to save money in liquid form. For example in a bank or credit union. ", "c4b_0069: घरपरिवारको प्रतिशत जसले पैसा जोगाउन सके जस्तै: बैंक अथवा सहकारी संस्थामा"],

        value: "c4b_0069"
    },
    {
        label: ["C4b_0044: Percent of parents or caregivers who are able to provide all the children in the household, aged 5-18 years, with three important items, through their own means (assets/production/income), without external assistance (from outside the family, NGO or government) in the past 12 months.", "c4b_0044: सहभागी उत्पादनकर्ताहरूको प्रतिशत जसले औपचारिक वित्तीय सेवाहरू प्रयोग गरेको रिपोर्ट गर्दछ। औपचारिक वित्तीय सेवा भित्र क्रेडिट, बचत र बिमाको उत्पादनहरु पर्दछ जुन दर्ता गरिएको वित्तीय संस्थाद्वारा प्रस्तावित गरिन्छ"],
        value: "c4b_0044"
    },
    {
        label: ["C4b_21068: Proportion of participating producer groups with an increased annual net profit", "c4b_21068: बार्षिक कुल मुनाफा वृद्धि भएको सहभागी उत्पादन समुहको अनुपात"],
        value: "c4b_21068"
    },
    {
        label: ["C4b_21071: Percent of participating producers who report utilising formal financial services.  Formal financial services include credit, savings and insurance products offered by a registered financial institution (i.e. not local money lenders and not savings groups).", "c4b_21071: सहभागी उत्पादनकर्ताहरूको प्रतिशत जसले औपचारिक वित्तीय सेवाहरू प्रयोग गरेको रिपोर्ट गर्दछ। औपचारिक वित्तीय सेवा भित्र क्रेडिट, बचत र बिमाको उत्पादनहरु पर्दछ जुन दर्ता गरिएको वित्तीय संस्थाद्वारा प्रस्तावित गरिन्छ"],
        value: "c4b_21071"
    },
    ],
    tp_6: [{
        label: ["C2A.24274", "C2A.24274"],
        value: "c2a_24274"
    }, {
        label: ["C2D.24263", "C2D.24263"],
        value: "c2d_24263"
    }, {
        label: ["C2A.21686: % of trained teachers using the skills acquired to teach reading", "C2A.21686 प्रशिक्षित सिकाउने कौशल प्रयोग गरी प्रशिक्षित शिक्षकहरूको%"],
        value: "c2a_21686"
    }, {
        label: ["C2A.24023", "C2A.24023"],
        value: "c2a_24023"
    }, {
        label: ["C2A.23988", "C2A.23988"],
        value: "c2a_23988"
    }, {
        label: ["C2D.24275", "C2D.24275"],
        value: "c2d_24275"
    }, {
        label: ["C2d_23018: Proportion of users who are satisfied with the education services they have received ", "c2d_23018: प्रयोगकर्ताहरुको अनुपात जो पाएको शिक्षाको सेवाबाट सन्तुस्ट छन्"],
        value: "c2d_23018"
    }],
    tp_7: [{
        label: ["C1a_0008: Prevalence of Stunting among children 0-5 years", "c1a_0008: 0-५ बर्षको बालबालिकाहरुमा हुने पुद्कोपन"],
        value: "c1a_0008"
    },
    {
        label: ["C1a_0018: Prevalence of Wasting among children 0-5 years", "c1a_0018: 0-५ बर्षको बालबालिकाहरुमा खेर फाल्ने बानि"],
        value: "c1a_0018"
    },
    {
        label: ["C1a_0013: Prevalence of Underweight among children 0-5 years ", "c1a_0013: 0-५ बर्षको बालबालिकाहरुमा कम वजन हुनु"],
        value: "c1a_0013"
    },
    {
        label: ["C1b_0091: Prevalance of disease and infection among under five children especially pneumonia ", "c1b_0091: ५ बर्ष मुनिका बालबालिकाहरुमा रोग र संक्रमण , बिशेष गरि निमोनिया"],
        value: "c1b_0091"
    },
    {
        label: ["C1a_0015: % of children aged 0-59 months who were sick in the  previous 2 weeks (diarrhoea, fever, cough) and who had increased breastfeeding and/or fluids and/or continued foods, as appropriate. ", "c1a_0015: 0-59 महिनाको बालबालिका जो गत २ हप्तामा बिरामी भएका थिए( झाडापखाला, जोरो, खोकी) र जसले धेरै स्तनपान र झोल पदार्थ खाएका थिए"],
        value: "c1a_0015"
    },
    {
        label: ["C1b_0072: % of children aged 0–59 months with a ‘presumed pneumonia’ (ARI) episode in the past two weeks that were taken to an appropriate health-care provider   ", "c1b_0072: 0-५९ महिनाको बालबालिकाहरुको प्रतिशत जसलाई गत २ हप्तामा निमोनिया अनुमानित गरिएको थियो र उपयुक्त स्वाथ्य हेरविचार केन्द्रमा लागिएको थियो "],
        value: "c1b_0072"
    },
    {
        label: ["C1b_0087: Prevalance of disease and infection among under five children especially Diarrhoea", "c1b_0087: ५ बर्ष मुनिका बालबालिकाहरुमा रोग र संक्रमण , बिशेष गरि झाडापखाला"],
        value: "c1b_0087"
    },
    {
        label: ["C1b_0067: Proportion of children under 5 with diarrhoea who received correct management of diarrhoea ", "c1b_0067: ५ बर्ष मुनिका बालबालिकाहरुको प्रतिशत जसलाई झाडापखाला लागेको र त्यसको ठिक उपचार पाएको "],
        value: "c1b_0067"
    },
    {
        label: ["C1a_21338: Prevalence of Low Birth Weight Baby (Birth Weight less than 2500 grams)  ", "c1a_21338: कम वजन भएको बालबालिकाको जन्म (जन्मिदा २५००ग्राम् भन्दा कम वजन"],
        value: "c1a_21338"
    },
    {
        label: ["C1A.24267", "C1A.24267"],
        value: "c1a_24267"
    },
    {
        label: ["C1c_0156: Proportion of mothers who report that they had four or more antenatal visits while they were pregnant  and to be disaggregated by age ", "c1c_0156: आमाहरुको प्रतिशत जसले आफु गर्भवती हुदा ४ अथवा बढी स्वाथ्य भेटघाट भएको"],
        value: "c1c_0156"
    },
    {
        label: ["C1a_0032: Proportion of pregnant women who consume iron and folic acid tablet during pregnancy", "c1a_0032: गर्भवती महिलाहरुको अनुपात जसले गर्भावस्थामा आइरन र फोलिक अचिद चक्की उपभोग गर"],
        value: "c1a_0032"
    },
    {
        label: ["C1c_0152: % of mothers of children aged 0-23 months who delivered with Skilled Birth Attendant and to be disaggregated by age (C) (S)", "c1c_0152: 0-२३ महिनाको बच्चाहरुको आमाको प्रतिशत जसले दछ जन्म परिचरबाट बच्चा जन्म दिए र उमेरद्वारा अलग हुन"],
        value: "c1c_0152"
    },
    {
        label: ["C1c_0160: Proportion of mothers of children aged 0–23 months who received at least 2 post-natal visit from a trained health care worker during the first week after birth (C) (S)", "c1c_0160: 0-२३ महिनाको बच्चाहरुको आमाको प्रतिशत जसले जन्म दिए पश्चात कमसेकम २ चोटी दछ स्वाथ्य सेवा कर्मिबाट जाच गराएको "],
        value: "c1c_0160"
    },
    {
        label: ["C1a_0048: Proportion of women who increased food consumption during pregnancy", "c1a_0048: गर्भवती समयमा खाना खपत बढाउनु भएका महिलाहरूको अनुपात"],
        value: "c1a_0048"
    },
    {
        label: ["C1a_0033: Proportion of children under 2 years receiving early initiation of breastfeeding (early initiation of BF) ", "c1a_0033: स्तनपानको प्रारम्भिक सुरुवात गर्ने २ बर्ष तलका बालबालिकाहरुको अनुपात"],
        value: "c1a_0033"
    },
    {
        label: ["C1a_0022: Proportion of children receiving minimum dietary diversity ", "c1a_0022: न्युनतम आहार विविधता प्राप्त गर्ने बालबालिकाहरूको अनुपात"],
        value: "c1a_0022"
    },
    {
        label: ["C1b_0131: Proportion of parents or caregivers with children 0–23 months who report that their child's stools are safely disposed off ", " c1b_0131: 0-२३ महिनाको बच्चा भएको आमाबाबु वा हेरचाह गर्ने व्यक्तिको प्रतिशत जसले आफ्नो बच्चाको दिशापिसाब सुरछित ढंगले बिसर्जन गरेको रिपोर्ट दिन्छन्"],
        value: "c1b_0131"
    },
    {
        label: ["C1a_0020: % of children 6-59 months who consume iron rich foods  in past 24 hours ", "c1a_0020: गत २४ घन्टामा ६-५९ महिनाको बालबालिकाहरुको प्रतिशत जसले आइरनयुक्त खानाहरु खाए"],
        value: "c1a_0020"
    },
    {
        label: ["C1b_0065: % of children aged 12-59 months who have completed 3rd DPT dose plus measles vaccination, verified by vaccination card and mother's recall. ", "c1b_0065: १२-५९ महिनाको बालबालिकाहरुको प्रतिशत जसले तेस्रो तहको डिपी टि डोज साथै दादुरा खोप लगाईसकेका छन् जुन खोप पत्र र आमाको कुराबाट पुस्टि हुन्छ"],
        value: "c1b_0065"
    },
    {
        label: ["C1b_0130: % of households with a designated place for handwashing where water and soap are present ", "c1b_0130: घरको प्रतिशत जहाँ हात धुनको लागि साबुन र पानी भएको ठाउँ छ"],
        value: "c1b_0130"
    },
    {
        label: ["C1b_0128: Proportion of parents or caregivers with appropriate hand-washing behaviour ", "c1b_0128: अभिभवक वा हेरचाह गर्ने व्यक्तिको प्रतिशत जो राम्रो संग हात धुने गर्छन"],
        value: "c1b_0128"
    },
    ],
    tp_8: [{
        label: ["C4a_0014: Proportion of children under 18 years, married ", "c4a_0014: १८ बर्ष भन्दा मुनिको विवाहित बालबालिकाहरुको अनुपात"],
        value: "c4a_0014"
    },
    {
        label: ["C4a_0190: Proportion of children engaged in child labour", "c4a_0190: बाल श्रममा संलग्न बालबालिकाहरुको अनुपात"],
        value: "c4a_0190"
    },
    {
        label: ["C4a_0196: Proportion of parents or caregivers who feel that their community is a safe place for children", "c4a_0196: अभिभावक र हेरचाह गर्ने व्यक्तिको अनुपात जसले आफ्नो समुदाय बालबालिकाहरुको लागि सुरक्षित भएको महसुस गर्छन"],
        value: "c4a_0196"
    },
    {
        label: ["C4a_0219: Proportion of adolescents who know of the presence of services and mechanisms to receive and respond to reports of abuse, neglect, exploitation or violence against children", "c4a_0219: जवानहरुको अनुपात जसले बालबालिकाहरुको बिरुद्ध हुने दुरुपयोग,उपेक्षा, शोषण र हिंसाको बारेमा रिपोर्ट गर्ने सेवा अथवा प्रणालीको बारेमा जान्दछन"],
        value: "c4a_0219"
    },
    {
        label: ["C4a_0221: Proportion of adolescents who report having experienced any physical violence in the past 12 months", "c4a_0221: गत 12 महिनामा कुनै पनि शारीरिक हिंसाको अनुभव गर्ने किशोरहरुको अनुपात"],
        value: "c4a_0221"
    },
    {
        label: ["C4a_22917: Proportion of users who are satisfied with the child protection services they have received ", "c4a_22917: प्रयोगकर्ताहरुको अनुपात जो पाएको बालबालिकाहरुको सुरक्षा सम्बन्धि सेवाहरुप्रति संतुस्ता छन "],
        value: "c4a_22917"
    },
    {
        label: ["C4a_22920: Proportion of child protection service users who report increased responsiveness of child protection service providers towards communities ", "c4a_22920: बाल सुरक्षा सेवाको प्रयोगकर्ताहरुको अनुपात जसले बाल सुरक्षा सेवाकर्मीहरुकोबढ्दो प्रतिक्रियाको बारेमा बताएका छन्"],
        value: "c4a_22920"
    },
    
    {
        label: ["C4a_23443: Proportion of most vulnerable HHs who are confident that they can access CP services available without external support ", "c4a_23443:कम्जोर परिवारको अनुपात जो बाल सुरक्षा सम्बन्धि सेवाहरु कुनै बाहिरी सहायता बिना पहुच गर्न सक्ने कुरामा बिस्वस्त छन्"],
        value: "c4a_23443"
    },
    {
        label: ["C4C.23075", "C4C.23075"],
        value: "c4c_23075"
    },
    {
        label: ['C4A.24265', 'C4A.24265'],
        value: 'c4a_24265'
    },
    ],
    respondent: [{
        label: ['HH Head ', 'घरमुली'],
        value: 1
    },
    {
        label: ['Children 12-18 years ', '१२ बर्ष देखि १८ बर्षको बालबालिका'],
        value: 2
    },
    {
        label: ['Youth 16-25 Years', '१६ बर्ष देखि २५ बर्षको युवा'],
        value: 3
    },
    {
        label: ['Pregnant Woman', 'गर्भवती महिला'],
        value: 4
    },
    {
        label: ['Mother with  below 2 years child', '२ बर्ष भन्दा कम उमेरको बच्चाको आमा'],
        value: 5
    },
    {
        label: ['Mother with  below 5 years child', '५ बर्ष मुनिको बच्चाको आमा'],
        value: 6
    },
    {
        label: ['Producer/farmer', 'कृषक/उत्पादक'],
        value: 7
    },
    {
        label: ['School teacher', 'बिद्यालय शिक्षक'],
        value: 8
    },
    {
        label: ['Students', 'बिद्यार्थी'],
        value: 9
    },
    {
        label: ['Students Parent', 'बिध्यर्थिको अविभावक'],
        value: 10
    },
    {
        label: ['Reading Club facilitator', 'Reading Club सहजकर्ता'],
        value: 11
    },
    {
        label: ['VCPPC Members', 'बाल संरक्षण समिति सदस्य'],
        value: 12
    },
    {
        label: ['LDRMP Members ', 'स्थानीय विपद जोखिम ब्यबस्थापन समिति सदस्य हरु'],
        value: 13
    },
    {
        label: ['Skype Club members', 'SKY club  सदस्य'],
        value: 14
    },
    {
        label: ['SMC', ' बिद्यालय ब्यबस्थापन समिति'],
        value: 15
    },
    {
        label: ['Head teacher', 'प्रधानाध्यापक'],
        value: 16
    },
    {
        label: ['Care givers with 0-18 year children', '०-१८ बर्षको बालबालिकाको अविभावक/हेरचाह कर्ता'],
        value: 17
    },
    {
        label: ['CP service user (community members)', 'बाल संरक्षण सेवाग्राही /समुदायका सदस्य'],
        value: 18
    },
    {
        label: ['Mother with 6-59 months child', '६ देखि ५९ महिनाको बच्चाको आमा'],
        value: 19
    },
    {
        label: ['Mother with 12-59 months child', '१२ देखि ५९ महिनाको बाचाको आमा'],
        value: 20
    },
    {
        label: ['Care giver with 5-18 year children', '५-१८ बर्षको बच्चाको आमा'],
        value: 21
    },
    {
        label: ['Producer group', 'उत्पादन समूह'],
        value: 22
    },
    ],
    yes_no: [{
        label: ['Yes', 'छ/ हो'],
        value: 1
    },
    {
        label: ['No', 'छैन/होइन'],
        value: 2
    },
    ],
    yes_no_dk: [{
        label: ['Yes', 'छ/ हो'],
        value: 1
    },
    {
        label: ['No', 'छैन/होइन'],
        value: 2
    },
    {
        label: ['Don’t know/cant say', 'थाहा छैन/भन्न सकेन'],
        value: 99
    },
    ],
    playgroundfance: [{
        label: ['Barbed wire fencing', 'काँडे तारबार'],
        value: 1
    },
    {
        label: ['Gabion wire mesh fencing', 'तारजाली'],
        value: 2
    },
    {
        label: ['Brick/stone masonry fencing', 'पर्खाल (पक्कि)'],
        value: 3
    },
    {
        label: ['Temporary fencing (with wooden materials)', 'अस्थाई बार'],
        value: 4
    },
    {
        label: ['No fence', 'खुला'],
        value: 5
    },
    ],
    meeting_freq: [{
        label: ['More than once a month', 'महिनामा एक पटक भन्दा बढी'],
        value: 1
    },
    {
        label: ['monthly', 'महिनामा एक पटक'],
        value: 2
    },
    {
        label: [' bi-monthly', 'दुई महिनामा एक पटक'],
        value: 3
    },
    {
        label: ['less than once every two months', 'दुई महिनामा एक पटकपनि नभएको'],
        value: 4
    },
    ],
    skills_used: [{
        label: ['a. letter/alphabet/sound', 'अक्षर'],
        value: 1
    },
    {
        label: ['b. words', 'शब्द'],
        value: 2
    },
    {
        label: ['c. vocabulary', 'शब्द भण्डार (vocabulary)'],
        value: 3
    },
    {
        label: ['d. story/passage reading', 'कथा/अनुच्छेद'],
        value: 4
    },
    {
        label: ['e. reading with comprehension', 'बोध पठन (बुझाई सहितको पढाई)'],
        value: 5
    },
    {
        label: ['f. none of them', 'माथिको कुनै पनि होइन'],
        value: 6
    },
    ],
    reasons_nonskills: [{
        label: ['a. Dont know how to use.', 'कसरि प्रयोग गर्ने थाहा छैन'],
        value: 1
    },
    {
        label: ['b. Students do not understand.', 'बिद्यार्थीले बुझ्दैनन्'],
        value: 2
    },
    {
        label: ['c. Its time consuming.', 'बढी समय लाग्ने'],
        value: 3
    },
    {
        label: ['d. Do not have adequate materials to use.', 'पर्याप्त सामग्री छैन'],
        value: 4
    },
    {
        label: ['e. Not appropriate in our context.', 'हाम्रो सन्दर्भमा उपयुक्त छैना'],
        value: 5
    },
    {
        label: ['f. others, please specify', 'अन्य'],
        value: 77
    },
    ],
    reasons_teachread: [{
        label: ['a. need more skill/training', 'अझ बढी शिप चाहिन्छ'],
        value: 1
    },
    {
        label: ['b. need more materials', 'अझ बढी सामग्री चाहिन्छ'],
        value: 2
    },
    {
        label: ['c. need parents support', 'अभिभावकको सहयोग चाहिन्छ'],
        value: 3
    },
    {
        label: ['d. need support from school management ', 'स्कुल ब्यबथापनको सहयोग चाहिन्छ'],
        value: 4
    },
    {
        label: ['e. no idea/dont know', 'थाहा छैन'],
        value: 99
    },
    ],
    Likert_5_satisfy: [{
        label: ['strongly satisfied', 'पूर्ण सन्तुष्ट'],
        value: 5
    },
    {
        label: ['satisfied', 'सन्तुष्ट'],
        value: 4
    },
    {
        label: ['neutral', 'न सन्तुष्ट, न असन्तुष्ट'],
        value: 3
    },
    {
        label: ['dissatisfied', 'असन्तुष्ट'],
        value: 2
    },
    {
        label: ['strongly dissatisfied', 'पूर्ण असन्तुष्ट'],
        value: 1
    },
    ],
    sex_child: [{
        label: ['Male', 'पुरुष'],
        value: 1
    },
    {
        label: ['Female', 'महिला'],
        value: 2
    },
    ],
    sex: [{
        label: ['Male', 'पुरुष'],
        value: 1
    },
    {
        label: ['Female', 'महिला'],
        value: 2
    },
    ],
    weight_record: [{
        label: ['Health card', 'स्वास्थ्य कार्ड'],
        value: 1
    },
    {
        label: ['Mothers recall', 'आमाले सम्झेको'],
        value: 2
    },
    ],
    health_provider: [{
        label: ['a) Doctor', 'डाक्टर'],
        value: 1
    },
    {
        label: ['b) Nurse', 'स्टाफ नर्स'],
        value: 2
    },
    {
        label: ['c) Midwife/ ANM', 'अ. न. मि.'],
        value: 3
    },
    {
        label: ['d) Not sure', 'सम्झना नभएको'],
        value: 4
    },
    {
        label: ['e) Don’t know/No response (if don’t know, skip to section on TT and Helminths)', 'थाहा छैन/उत्तर नदिएको'],
        value: 99
    },
    {
        label: ['f) Otherspecify (if other, skip to next section)', 'अन्य'],
        value: 77
    },
    ],
    completed_education_level: [
        { label: ['ECD', 'ECD'], value: 'ECD' },
        { label: ['Nursery', 'Nursery'], value: 'Nursery' },
        { label: ['Lower Kinder Garden', 'Lower Kinder Garden'], value: 'Lower Kinder Garden' },
        { label: ['Upper Kinder Garden', 'Upper Kinder Garden'], value: 'Upper Kinder Garden' },
        { label: ['Class 1', 'Class 1'], value: 'Class 1' },
        { label: ['Class 2', 'Class 2'], value: 'Class 2' },
        { label: ['Class 3', 'Class 3'], value: 'Class 3' },
        { label: ['Class 4', 'Class 4'], value: 'Class 4' },
        { label: ['Class 5', 'Class 5'], value: 'Class 5' },
        { label: ['Class 6', 'Class 6'], value: 'Class 6' },
        { label: ['Class 7', 'Class 7'], value: 'Class 7' },
        { label: ['Class 8', 'Class 8'], value: 'Class 8' },
        { label: ['Class 9', 'Class 9'], value: 'Class 9' },
        { label: ['Class 10', 'Class 10'], value: 'Class 10' },
        { label: ['Class 11', 'Class 11'], value: 'Class 11' },
        { label: ['Class 12', 'Class 12'], value: 'Class 12' },
        { label: ['Bachelor First year', 'Bachelor First year'], value: 'Bachelor First year' },
        { label: ['Bachelor Second year', 'Bachelor Second year'], value: 'Bachelor Second year' },
        { label: ['Bachelor Third year', 'Bachelor Third year'], value: 'Bachelor Third year' },
        { label: ['Bachelor Fourth year', 'Bachelor Fourth year'], value: 'Bachelor Fourth year' },
        { label: ['Bachelor Fifth year', 'Bachelor Fifth year'], value: 'Bachelor Fifth year' },
        { label: ['Master First year', 'Master First year'], value: 'Master First year' },
        { label: ['Master Second year', 'Master Second year'], value: 'Master Second year' },
        { label: ['P.H.D.', 'P.H.D.'], value: 'P.H.D.' },
    ],
    type_of_disabilities:[
        { label: ['Physical', 'सरिरिक'], value: 'Physical' },
        { label: ['Blindness / low vision', 'आधोपन/ कम दृष्टि'], value: 'Blindness / low vision' },
        { label: ['Deaf / hard to hearing', 'बहिरोपन/सुनाइमा समस्या'], value: 'Deaf / hard to hearing' },
        { label: ['Deaf - blind', 'बहिरोपन-आन्धोपन'], value: 'Deaf - blind' },
        { label: ['Speech problem', 'बोलाईमा समस्या'], value: 'Speech problem' },
        { label: ['Mental disable', 'मानसिक समस्या'], value: 'Mental disable' },
        { label: ['Intellectual disable', 'बौद्धिक असक्षम'], value: 'Intellectual disable' },
        { label: ['Multiple disable', 'Iबहु आपन्गता'], value: 'Multiple disable' },
    ],
    delivery_type: [{
        label: ['Normal delivery', 'सामान्य प्रसुती'],
        value: 1
    },
    {
        label: ['Operation (CS)', 'अप्रेशन गरेर'],
        value: 2
    },
    ],
    delivery_place: [{
        label: ['Home in presence of skill  birth attendants', 'घरमै तालिमप्राप्त व्यक्तिबाट'],
        value: 1
    },
    {
        label: ['Home without presence of Skill birth attendants', 'घरमै तालिमप्राप्त व्यक्तिको अनुपस्थितिमा'],
        value: 2
    },
    {
        label: ['Hospital/health center/nursing home ', 'हस्पिटल/स्वास्थ्य केन्द्र मा'],
        value: 3
    },
    ],
    delivery_helper: [{
        label: ['a) Doctor', 'डाक्टर'],
        value: 1
    },
    {
        label: ['b) Nurse', 'नर्स'],
        value: 2
    },
    {
        label: ['c) Midwife/ ANM', 'अ. न. मि.'],
        value: 3
    },
    {
        label: ['d) Other health service provider', 'अन्य सेवा प्रदायक'],
        value: 4
    },
    {
        label: ['e) FCHV', 'ग्रामीण स्वास्थ्य स्वयम्सेविका'],
        value: 5
    },
    {
        label: ['f) Other non health provider (relatives etc)', 'स्वास्थ्य सेवा प्रदायक बाहेक अन्य (नातेदार वा छिमेकी)'],
        value: 6
    },
    {
        label: ['g) Nobody', 'कोहीपनि होइन'],
        value: 7
    },
    {
        label: ['h) (Others : to specify)', 'अन्य'],
        value: 77
    },
    ],
    pnc_visit: [{
        label: ['Yes, mother only', 'आमा मात्र'],
        value: 1
    },
    {
        label: ['Yes, mother and baby both', 'आमा र बच्चा दुवै'],
        value: 2
    },
    {
        label: ['Yes, baby only', 'बच्चा मात्र'],
        value: 3
    },
    {
        label: ['No. (if no module is finished)', 'केहि होइन'],
        value: 4
    },
    ],
    pnc_visittime: [{
        label: ['0-12 hours', '०-१२ घण्टा'],
        value: 1
    },
    {
        label: ['13-24 hours', '१३-२४ घण्टा'],
        value: 2
    },
    {
        label: ['25-48 hours', '२५-४८ घण्टा'],
        value: 3
    },
    {
        label: ['More than 48 hours (more than 2 days)', '४८ घण्टा भन्दा बढी'],
        value: 4
    },
    {
        label: ['No visit (finish module)', 'कुनै पनि होइन'],
        value: 5
    },
    ],
    food_pregnancy: [{
        label: ['Less  than usual', 'नियमित/संधै भन्दा कम'],
        value: 1
    },
    {
        label: ['Same as usual', 'नियमित/संधै जस्तै'],
        value: 2
    },
    {
        label: ['More than usual', 'नियमित/संधै भन्दा बढी'],
        value: 3
    },
    ],
    iron_compliance: [{
        label: [' Less than 90 ', '९० भन्दा कम'],
        value: 1
    },
    {
        label: ['90-180 ', '९०-१८० सम्म'],
        value: 2
    },
    {
        label: ['181 to 225 ', '१८१ देखि २२५ सम्म'],
        value: 3
    },
    {
        label: [' 225 or more than 225 ', '२२५ र यो भन्दा बढी'],
        value: 4
    },
    {
        label: ['Dont know/no response', 'थाहा छैन/उत्तर छैन'],
        value: 99
    },
    ],
    first_breastfeed: [{
        label: ['Immediately/Within the first hour after delivery', 'तुरुन्तै/ जन्मेको १ घण्टा भित्र'],
        value: 1
    },
    {
        label: ['After the first hour', '१ घण्टा बढी'],
        value: 2
    },
    ],
    food_sickness: [{
        label: ['Never given so far', 'कहिलेपनि दिएको छैन'],
        value: 1
    },
    {
        label: ['Much less', 'धेरै कम'],
        value: 2
    },
    {
        label: ['Somewhat less', 'अलि कम'],
        value: 3
    },
    {
        label: ['About the same', 'लगभग उस्तै'],
        value: 4
    },
    {
        label: ['More', 'बढी'],
        value: 5
    },
    {
        label: ['Don’t Know/No response', 'थाहा छैन/उत्तर छैन'],
        value: 99
    },
    ],
    healthcard: [{
        label: ['Child has health card, interviewer able to review', 'स्वास्थ्य कार्ड छ, (अन्तर्वार्ताकारले हेर्न पाइयो)'],
        value: 1
    },
    {
        label: ['Child has health card, card unavailable during interview', 'स्वास्थ्य कार्ड छ, (अन्तर्वार्ताकारले हेर्न भने पाइएन)'],
        value: 2
    },
    {
        label: ['Child has health card, interviewer was not given permission to see card', 'स्वास्थ्य कार्ड छ, (अन्तर्वार्ताकारलाई हेर्ने अनुमति दिइएन)'],
        value: 3
    },
    {
        label: ['Child does not have health card ', 'स्वास्थ्य कार्ड छैन'],
        value: 4
    },
    ],
    treatment_center: [{
        label: ['Homemade treatment', 'घरेलु उपचार'],
        value: 1
    },
    {
        label: ['Traditional or religious healer', 'परम्परागत/धार्मिक उपचार'],
        value: 2
    },
    {
        label: ['Ayurvedic treatment', 'आयुर्बेदिक उपचार'],
        value: 3
    },
    {
        label: ['Gov’t health facility/outreach clinic', 'सरकारी स्वास्थ्य केन्द्र/ गाउघर क्लिनिक'],
        value: 4
    },
    {
        label: ['FCHV', 'ग्रामीण स्वास्थ्य स्वयम्सेविका'],
        value: 5
    },
    {
        label: ['Pharmacy or medicine seller', 'औषधि पसल'],
        value: 6
    },
    {
        label: ['Other: (specify) ______________', 'अन्य'],
        value: 77
    },
    ],
    stool_dispose: [{
        label: ['Child used toilet or latrine', 'बालबालिकाले शौचालय प्रयोग गर्छन'],
        value: 1
    },
    {
        label: ['Put/rinsed into toilet or latrine', 'दिशालाई शौचालयमा हालिन्छ'],
        value: 2
    },
    {
        label: ['Put/rinsed into drain or ditch', 'दिशालाई ढल/नालामा फलिन्छ'],
        value: 3
    },
    {
        label: ['Thrown into garbage', 'दिशालाई फोहोरफाल्ने ठाउमा फालिन्छ'],
        value: 4
    },
    {
        label: ['Buried', 'दिशालाई खाल्डोमा गाडिन्छ'],
        value: 5
    },
    {
        label: ['Left in the open', 'दिशालाई त्यसै खुल्ला स्थानमा छाडीन्छ'],
        value: 6
    },
    {
        label: ['Other (Specify)', 'अन्य'],
        value: 77
    },
    ],
    handwash_time: [{
        label: ['After defecation', 'दिशा गरेपछि'],
        value: 1
    },
    {
        label: ['After cleaning the bottom of child', 'केटाकेटीको दिशा सफागरेपछि'],
        value: 2
    },
    {
        label: ['After caring animals dung', 'बस्तुभाउको गोबर सफागरेपछि'],
        value: 3
    },
    {
        label: ['Before feeding children', 'केटाकेटीलाई खानखुवाउनु अघि'],
        value: 4
    },
    {
        label: ['Before eating', 'खाना खानु अघि'],
        value: 5
    },
    {
        label: ['Before making food', 'खाना पकाउनु अघि'],
        value: 6
    },
    ],
    agreement: [{
        label: ['Strongly disagree', 'एकदमै असहमत'],
        value: 1
    },
    {
        label: ['Disagree', 'असहमत'],
        value: 2
    },
    {
        label: ['Agree', 'सहमत'],
        value: 3
    },
    {
        label: ['Strongly agree  ', 'एकदमै सहमत'],
        value: 4
    },
    ],
    agreement_real: [{
        label: ['Strongly disagree', 'एकदमै असहमत'],
        value: 'Strongly disagree'
    },
    {
        label: ['Disagree', 'असहमत'],
        value: 'Disagree'
    },
    {
        label: ['Neither agree nor disagree', 'न त सहमत न त असहमत'],
        value: 'Neither agree nor disagree'
    },
    {
        label: ['Agree', 'सहमत'],
        value: 'Agree'
    },
    {
        label: ['Strongly agree', 'एकदमै सहमत'],
        value: 'Strongly agree'
    }],
    reason_for_child_working:[
        { label: ['Poor family economic condition', 'Poor family economic condition'], value: 'Poor family economic condition' },
        { label: ['Pressure from parents', 'Pressure from parents'], value: 'Pressure from parents' },
        { label: ['Peer pressure', 'Peer pressure'], value: 'Peer pressure' },
        { label: ['Interested on working', 'Interested on working'], value: 'Interested on working' },
        { label: ['Tailoring', 'Tailoring'], value: 'Tailoring' },
    ],
    marital_status:[
        { label: ['Unmarried', 'अबिबाहित'], value: 'unmarried' },
        { label: ['Married', 'बिबाहित'], value: 'married' },
        { label: ['Widow', 'बिधुवा'], value: 'widow' },
        { label: ['Divorced', 'सम्बन्ध बिच्छेद/छुट्टीएको'], value: 'divorced' },
    ],
    married: [{
        label: ['1. Yes I am married', 'अ. म विवाहित हो'],
        value: 1
    },
    {
        label: ['3. I was once married, but not now', 'आ. मैले पहिले बिवाह गरेको थिए तर अहिले छैन '],
        value: 3
    },
    {
        label: ['2. No I have never been married', 'इ.  म अहिले सम्म अविवाहित छु'],
        value: 2
    },
    ],
    hurt: [{
        label: ['1. Never', 'अ. कहिले पनि छैन'],
        value: 1
    },
    {
        label: ['2. Once', 'आ. एकपटक'],
        value: 2
    },
    {
        label: ['2. 2 or 3 times', 'इ. दुइ-तीनपटक'],
        value: 3
    },
    {
        label: ['4. 4 or more times', 'ई. चार वा चार भन्दा बढी पटक'],
        value: 4
    },
    ],
    gems_response: [{
        label: ['1. Strongly Agree   ', '1. पूर्ण सहमत'],
        value: 1
    },
    {
        label: ['2. Somewhat agree', '2. आंशिक सहमत'],
        value: 2
    },
    {
        label: ['3. do  not know agree ', '3. असहमत'],
        value: 3
    },
    ],
    gems_response_reverse: [{
        label: ['3. Strongly Agree   ', '3. पूर्ण सहमत'],
        value: 3
    },
    {
        label: ['2. Somewhat agree', '2. आंशिक सहमत'],
        value: 2
    },
    {
        label: ['1. do  not know agree ', '1. असहमत'],
        value: 1
    },
    ],
    beaten: [{
        label: ['1. Yes', 'अ.छ'],
        value: 1
    },
    {
        label: ['2. No', 'आ. छैन'],
        value: 2
    },
    {
        label: ['3. Dont Know', 'इ. थाहा छैन'],
        value: 99
    },
    {
        label: ['4. I dont want to respond', 'ई.म यो प्रश्नको उत्तर  दिन चाहन्न'],
        value: 88
    },
    ],
    prevent_reasons: [{
        label: ['1. I am scared of what my family or friends would say or do if they found out', 'अ. मलाई भेटे को खण्डमा  मेरो साथीहरु वा परिवारले के भन्लान वा के गर्लान  भन्ने डर लाग्छ |'],
        value: 1
    },
    {
        label: ['2. I do not trust the services or the people who work there', 'आ.मलाई उनीहरुले दिने सेवा र त्यहाँ काम गर्ने व्यक्तिहरुमा मेरो बिश्वास छैन |'],
        value: 2
    },
    {
        label: ['3. I do not think the people who work there will listen to me or believe me', 'इ. त्यहाँ काम गर्ने कर्मचारीले मेरो कुरा सुन्छन वा मलाई बिश्वास गर्छन भन्ने कुरामा मलाइ बिश्वास लाग्दैन'],
        value: 3
    },
    {
        label: ['4. It is against my religion or my cultural practices', 'ई. यो मेरो धर्म वा मेरो संस्कृतिक अभ्यास बिरुद्द छ |'],
        value: 4
    },
    {
        label: ['5. They do not speak the same language as me', 'उ. मैले बोल्ने भाषा उनीहरुले बोल्न सक्दैनन्'],
        value: 5
    },
    {
        label: ['6. I do not have enough money to pay for transportation or the services', 'ऊ मसंग यातायात र अन्य सेवाको लागि तिर्ने  प्रयाप्त पैसा छैन'],
        value: 6
    },
    {
        label: ['7. Another reason', 'ए. अन्य कारण'],
        value: 77
    },
    {
        label: ['8. I don’t know about services that can help me if I am being abused or neglected.', 'ए.  दुर्ब्यबहार वा तिरस्कार भोग्न परेको खण्डमा कहाँ सेवा पाउछु भन्ने मलाई थाहा छैन '],
        value: 99
    },
    ],
    feel_children: [{
        label: ['1.They are safe most of the time ', 'उनीहरु प्राय जसो समय सुरक्षित छन्'],
        value: 1
    },
    {
        label: ['2. They are safe some of the time, ', 'उनीहरु केहि समय  सुरक्षित छन्'],
        value: 2
    },
    {
        label: ['3. I don’t feel that they are safe.', 'मलाई उनीहरु सुरक्षित छन् भन्ने महसुस हुदैन'],
        value: 3
    },
    ],
    reason_notsafe: [{
        label: ['1  Traffic related danger', 'अ. यातायात समन्धी खतरा'],
        value: 1
    },
    {
        label: ['2  Lack of safe places to play', 'आ. सुरक्षित खल्ने ठाउको अभाव'],
        value: 2
    },
    {
        label: ['3  Gang related problems', 'इ. समूह समन्धी समस्या'],
        value: 3
    },
    {
        label: ['4  Drugs and alcohol related problems', 'ई. लागुपदार्थ र मध्यपान सम्बन्धी समस्या'],
        value: 4
    },
    {
        label: ['5  Other violence ', 'उ.अन्य हिँसा'],
        value: 5
    },
    {
        label: ['6  Other crime such as robbery or vandalism', 'ऊ. अन्य अपराध जस्तो चोरी वा गुण्डागर्दी'],
        value: 6
    },
    {
        label: ['7  Sexual harassment and rape', 'ए. यौन हैरानी र बलात्कार'],
        value: 7
    },
    {
        label: ['8  Pollution and environmental hazards', 'ओ. प्रदुषण र वातावारनिय जोखिम'],
        value: 8
    },
    {
        label: ['9  Other (specify) ____________', 'औ. अन्य'],
        value: 77
    },
    ],
    pay_cash: [{
        label: ['1 = Yes, for pay (cash or kind)', 'अ. नगद वा जिन्सी सामान पाउने गरि काम गरेको'],
        value: 1
    },
    {
        label: ['3 = Yes, unpaid', 'आ. केहि नपाउने गरि काम गरेको'],
        value: 3
    },
    {
        label: ['2 = No ', 'इ. काम नै नगरेको'],
        value: 2
    },
    ],
    quality_service: [{
        label: ['Very poor', 'धेरै न्यून'],
        value: 1
    },
    {
        label: ['Poor', 'न्यून'],
        value: 2
    },
    {
        label: ['Neutral', 'ठीकै'],
        value: 3
    },
    {
        label: ['good', 'राम्रो'],
        value: 4
    },
    {
        label: ['very good', 'धेरै राम्रो'],
        value: 5
    },
    ],
    received_schemes: [{
        label: ['Educational Allownces', 'अ. शिक्षा सहयोग भता'],
        value: 1
    },
    {
        label: ['Ultra Poor Household Allowances', 'आ. अति बिपन्न भत्ता'],
        value: 2
    },
    {
        label: ['Senior Citizens Allownce', 'इ वृध भत्ता'],
        value: 3
    },
    {
        label: ['Single Women/Widow allowance', 'ई. विधवा/अकल महिला  भत्ता'],
        value: 4
    },
    {
        label: ['Lower Cast Allowance ', 'उ. दलित भत्ता'],
        value: 5
    },
    {
        label: ['Dissability Allownce', 'ऊ. अपाङ्ग भत्ता'],
        value: 6
    },
    {
        label: [' Special Health Grant ( Nutritional grant, Delivery grant)', 'ए. बिशेष सास्था अनुदान ( पोषण, सुत्केरी भत्ता)'],
        value: 7
    },
    {
        label: ['Special Cases Support Grant ', 'ऐ. बिशेष सहयोग'],
        value: 8
    },
    {
        label: ['Girls Grant', 'ओ. बालिका अनुदान'],
        value: 9
    },
    {
        label: ['Minority group allowance', 'औ. अल्पसंख्यक समूह सहयोग'],
        value: 10
    },
    {
        label: [' Other........ ', 'अँ. अन्य  ( खुलाउनुहोस )'],
        value: 77
    },
    ],
    dap_response: [{
        label: ['Not at all (rarely)', 'कहिल्यै पनि होइन'],
        value: 1
    },
    {
        label: ['Somewhat (sometimes)', 'कहिले काही मात्र'],
        value: 2
    },
    {
        label: ['Very (often)', 'प्राय'],
        value: 3
    },
    {
        label: ['Extremely (almost always)', 'सधै जसो'],
        value: 4
    },
    ],
    agriculture_land: [{
        label: ['A. No', 'छैन'],
        value: 1
    },
    {
        label: ['B. Yes, but non irrigated', 'छ तर सिंचाई छैन'],
        value: 2
    },
    {
        label: ['C. Yes, and some irrigated', 'छ केहि सिचाई भएको'],
        value: 3
    },
    ],
    provide: [{
        label: ['Yes (with no assistance) ', 'किन्न सके (सहयोग बिना)'],
        value: 1
    },
    {
        label: ['Yes (only with assistance) ', 'किन्न सके (सहयोग पाएर)'],
        value: 3
    },
    {
        label: ['No unable to provide for all the children  ', 'किन्न सकिन'],
        value: 2
    },
    {
        label: ['Don’t know', 'थाहा छैन'],
        value: 99
    },
    ],
    crops: [{
        label: ['Vegetable', 'सागसब्जी'],
        value: 1
    },
    {
        label: ['Sorghum', 'जुनेलो'],
        value: 2
    },
    {
        label: ['Sunflower', 'सूर्यमुखी'],
        value: 3
    },
    {
        label: ['Finger Millet  ', 'कोदो'],
        value: 4
    },
    {
        label: ['Groundnuts ', 'बदाम/मुङफली'],
        value: 5
    },
    {
        label: ['Sugar beans', 'सिमि'],
        value: 6
    },
    {
        label: ['Rice', 'धान'],
        value: 7
    },
    {
        label: ['Wheat', 'गहु'],
        value: 8
    },
    {
        label: ['Paprika', 'भेडे खुर्सानी'],
        value: 9
    },
    {
        label: ['Cotton ', 'कपास'],
        value: 10
    },
    {
        label: ['Tobacco', 'सुर्ती'],
        value: 11
    },
    {
        label: ['Soya beans', 'भटमास'],
        value: 12
    },
    {
        label: ['other', 'अन्य'],
        value: 77
    },
    ],
    sale_location: [{
        label: ['Traders', 'ब्यापारी'],
        value: 1
    },
    {
        label: ['Local market', 'स्थानीय बजार'],
        value: 2
    },
    {
        label: ['Farmer to farmer', 'अन्य किसानहरु'],
        value: 3
    },
    {
        label: ['Others', 'अन्य'],
        value: 77
    },
    ],
    recent_loan: [{
        label: ['Not taken any loan ', 'कुनै ऋण लिएको छैन'],
        value: 1
    },
    {
        label: ['Bank', 'बैंक'],
        value: 2
    },
    {
        label: ['Money lenders', 'साहु (ऋणदाता)'],
        value: 3
    },
    {
        label: ['Micro credit institutions ', 'साना ऋणप्रदायक संस्थाहरु'],
        value: 4
    },
    {
        label: ['Input supplier', 'आपूर्तिकर्ता'],
        value: 5
    },
    {
        label: ['Customer', 'ग्राहक'],
        value: 6
    },
    {
        label: ['Others specify', 'अन्य (उल्लेख गर्नुहोस)'],
        value: 77
    },
    ],
    collateral_type: [{
        label: ['Land building and other establishments, ', 'जग्गा, भवन तथा अन्य अचल सम्पत्ति'],
        value: 1
    },
    {
        label: ['machinery equipment and movables, ', 'यान्त्रिक उपकरण तथा अन्य चल सम्पत्ति'],
        value: 2
    },
    {
        label: ['account receivable and inventories', 'आसामी तथा मालसामान'],
        value: 3
    },
    {
        label: ['Personal assets', 'व्यक्तिगत सम्पत्ति'],
        value: 4
    },
    {
        label: ['other specify', 'अन्य (उल्लेख गर्नुहोस)'],
        value: 77
    },
    ],
    apply: [{
        label: ['No need', 'नचाहिएको भएर'],
        value: 1
    },
    {
        label: ['complex application processes, ', 'जटिल प्रक्रिया'],
        value: 2
    },
    {
        label: ['High interest rates, ', 'महँगो ब्याजदर'],
        value: 3
    },
    {
        label: ['Lack of required guarantees, ', 'चाहिने धितो नभएर'],
        value: 4
    },
    {
        label: ['Did not think it could be obtained', 'पाइन्छ जस्तो नलागेर'],
        value: 5
    },
    {
        label: ['other specify', 'अन्य (उल्लेख गर्नुहोस)'],
        value: 77
    },
    ],
    source: [{
        label: ['Traditional/subsistence type of agriculture', 'परम्परागत/जीवन निर्वाहका लागि गरिने खेति'],
        value: 1
    },
    {
        label: ['Livestock & Poultry', 'पशुपालन तथा कुखुरापालन'],
        value: 2
    },
    {
        label: ['Daily wage: Unskilled Labourer', 'दैनिक ज्यालादारी : सिपबिहिन मजदुरी'],
        value: 3
    },
    {
        label: ['Agriculture labour', 'कृषि श्रमिक'],
        value: 4
    },
    {
        label: ['Remittance/foreign income', 'बैदेशिक रोजगार बाट भएको आय'],
        value: 5
    },
    {
        label: ['Skilled labour', 'सिपमुलक श्रमिक'],
        value: 6
    },
    {
        label: ['Employee (Government Job, Teacher, I/NGO sector, private company)', 'जागिरे (सरकारी जागिर, शिक्षक, अ/गैसस क्षेत्र, निजि क्षेत्र)'],
        value: 7
    },
    {
        label: ['Own business/retailer/enterprenuer', 'आफ्नै व्यवसाय/खुद्रा ब्यापारी/उद्ध्यमी)'],
        value: 8
    },
    {
        label: [' rental of property', 'सम्पत्ति भाडामा लगाएर'],
        value: 9
    },
    {
        label: ['Government allowance(pension/allownance )', 'सरकारी भत्ता (निवृत्तिभरण/भत्ता)'],
        value: 10
    },
    ],
    expenses: [{
        label: ['House rent', 'घरभाडा'],
        value: 1
    },
    {
        label: ['Home maintenance', 'घर मर्मत खर्च'],
        value: 2
    },
    {
        label: ['Water', 'पानी'],
        value: 3
    },
    {
        label: ['Children education', 'बच्चाको पढाइ'],
        value: 4
    },
    {
        label: ['Electricity', 'बिजुली'],
        value: 5
    },
    {
        label: ['Transpotation', 'यातायात'],
        value: 6
    },
    {
        label: ['phone/internet', 'फोन/इन्टरनेट'],
        value: 7
    },
    {
        label: ['Fuel (Gas, fire, petrol)', 'इन्धन (ग्यास, दाउरा, पेट्रोल)'],
        value: 8
    },
    {
        label: ['Alcohol/gamble', 'जाडरक्सी/जुवातास'],
        value: 9
    },
    {
        label: ['hygiene/sanitation', 'सरसफाई'],
        value: 10
    },
    {
        label: ['health treatment', 'स्वास्थ्य उपचार'],
        value: 11
    },
    {
        label: ['Food', 'खाना'],
        value: 12
    },
    {
        label: ['entertainment', 'मनोरन्जन'],
        value: 13
    },
    {
        label: ['other', 'अन्य'],
        value: 77
    },
    ],
    volumn: [{
        label: ['KG', 'किलो'],
        value: 1
    },
    {
        label: ['Litres', 'लिटर'],
        value: 2
    },
    {
        label: ['Others', 'अन्य'],
        value: 77
    },
    ],
    area: [{
        label: ['Hectares', 'हेक्टर'],
        value: 1
    },
    {
        label: ['Ropani', 'रोपनी'],
        value: 2
    },
    {
        label: ['Katha', 'कठ्ठा'],
        value: 3
    },
    {
        label: ['Bigha', 'बिघा'],
        value: 4
    },
    ],
    food_security: [{
        label: ['0-3 months', '०-३ महिना'],
        value: 1
    },
    {
        label: ['4-6 months', '४-६ महिना'],
        value: 2
    },
    {
        label: ['7-9 months', '७-९ महिना'],
        value: 3
    },
    {
        label: ['10-12 months', '१०-१२ महिना'],
        value: 4
    },
    {
        label: ['Above 12 months ', '१२ महिना भन्दा बढी'],
        value: 5
    },
    ],
    food_insecure: [{
        label: ['By reducing number of meals eaten in a day?', 'दिनमा खाने छाक घटाएर'],
        value: 1
    },
    {
        label: ['By taking less quality or less- preferred foods?', 'कम गुणस्तरीय वा मन नपरेको खाना खाएर'],
        value: 2
    },
    {
        label: ['By engaging children to work to earn money for food?', 'खानाका लागि पैसा कमाउन बालबच्चालाइ काममा लगाएर'],
        value: 3
    },
    {
        label: ['Adults worked longer hours than normal to earn money for food?', 'खानाका लागि पैसा कमाउन वयस्क/युवाहरुले सामान्य भन्दा बढी घण्टा काम गरेर'],
        value: 4
    },
    {
        label: ['By selling more of the small livestock than usual (e.g. goats, sheep, chicken, duck, pigs)?', 'सामान्यतया भन्दा धेरै पशुपन्छी बेचेर (जस्तै बाख्रा, भेडा, कुखुरा, हाँस, बंगुर आदि)'],
        value: 5
    },
    {
        label: ['By migrating to another area to work to earn money for food?', 'खानाका लागि पैसा कमाउन अर्को ठाउँमा बसाइ सरेर'],
        value: 6
    },
    {
        label: ['By selling/ pledging productive assets (such as land) to acquire food?', 'खाना प्राप्त गर्न उत्पादनशील सम्पत्ति (जस्तै जग्गा) बेचेर वा धितो राखेर'],
        value: 7
    },
    {
        label: ['By selling/ pledging productive house to acquire food?', 'खाना प्राप्त गर्न उत्पादनशील घर बेचेर वा धितो राखेर'],
        value: 8
    },
    {
        label: ['By borrowing food or money from relatives, friends, or neighbours?', 'नातेदार, साथि वा छिमेकी संग खाना वा पैसा सापटी लिएर'],
        value: 9
    },
    {
        label: ['Got relief assistance from the INGO/NGO/ government?', 'अ/गैसस वा सरकार संग राहत पाएर'],
        value: 10
    },
    {
        label: ['By borrowing money from financial institutions?', 'वित्तीय संस्थाहरु संग पैसा सापटी लिएर'],
        value: 11
    },
    {
        label: ['By borrowing money from local money lenders?', 'साहु/आसामीकर्ता संग पैसा सापटी मागेर'],
        value: 12
    },
    {
        label: ['By selling off some household non-productive possessions (such as ornament, TV, mobile)?', 'अनुत्पादक सम्पत्ति (जस्तै गहना, टेलिभिजन, मोबाइल आदि) बेचेर'],
        value: 13
    },
    {
        label: ['Others', 'अन्य'],
        value: 77
    },
    ],

    food_worry: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    lack_food: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    variety_food: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    want_food: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    small_meal: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    few_meal: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    no_food: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    hungry_night: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    hungry_day: [{
        label: ['Rarely', 'बिरलै'],
        value: 'Rarely'
    },
    {
        label: ['Sometimes', 'कहिलेकाही'],
        value: 'Sometimes'
    },
    {
        label: ['Often', 'प्राय'],
        value: 'Often'
    },
    {
        label: ['Never', 'कहिल्यै पनि थिएन'],
        value: 'Never'
    },
    ],
    orphan_discrimination: [{
        label: ['Two set of clothes', 'दुइ जोड कपडा'],
        value: 1
    },
    {
        label: ['One pair of shoes', 'एक जोड जुत्ता'],
        value: 2
    },
    {
        label: ['One blanket', 'एउटा कम्बल'],
        value: 3
    },
    ],
    food_deficiency_cause: [{
        label: ['1. Drought', 'खडेरी'],
        value: 1
    },
    {
        label: ['2. Landslide', 'पहिरो'],
        value: 2
    },
    {
        label: ['3. Crop Failure', 'बालि नोक्सानी'],
        value: 3
    },
    {
        label: ['4. Flood', 'बाढी'],
        value: 4
    },
    {
        label: ['5. Financial Problem', 'आर्थिक समस्या'],
        value: 5
    },
    {
        label: ['6. Not available in market', 'बजारमा उपलब्धता नहुनु'],
        value: 6
    },
    {
        label: ['Other (specify)', 'अन्य (उल्लेख गर्नुहोस)'],
        value: 77
    },
    ],
    children_group: [{
        label: ['6-11 years children', '६ देखि ११ बर्षको बालबालिका'],
        value: 1
    },
    {
        label: ['12-18 years children ', '12-१८ बर्षको बालबालिका'],
        value: 2
    },
    {
        label: ['All under 18 children', '१८ बर्षमुनिको सबै बालबालिका'],
        value: 3
    },
    ],
    product: [{
        label: ['cash crops', 'नगदेबाली'],
        value: 1
    },
    {
        label: ['Cereal crops', 'खाद्य बालि'],
        value: 2
    },
    {
        label: ['livestock', 'पशुपालन'],
        value: 3
    },
    {
        label: ['poultry', 'पन्क्षीपालन'],
        value: 4
    },
    {
        label: ['fishery', 'माछा पालन'],
        value: 5
    },
    {
        label: ['medicinal horbs', 'औषधिमुलक जडिबुटी'],
        value: 6
    },
    {
        label: ['fruits farming ', 'फलफुल खेति'],
        value: 7
    },
    {
        label: ['other', 'अन्य'],
        value: 77
    },
    ],
    quality_product: [{
        label: ['Poor', 'अति कम गुणस्तरीय'],
        value: 1
    },
    {
        label: ['Average', 'सामान्य गुणस्तारिय'],
        value: 2
    },
    {
        label: ['Good', 'राम्रो गुणस्तरको'],
        value: 3
    },
    ],
    tablets_count: [{
        value: "Less than 90",
        label: ["Less than 90", "९० भन्दा कम "]
    },
    {
        value: "90 to 180",
        label: ["90 to 180", "९० देखि १८० "]
    },
    {
        value: "180 to 225",
        label: ["180 to 225", "१८० देखि २२५ "]
    },
    {
        value: "More than 225",
        label: ["More than 225", "२२५ देखि माथि "]
    },
    ],
    usual_consent: [{
        value: "No as usual",
        label: ["No as usual", "सामान्य हो "]
    },
    {
        value: "More than usual",
        label: ["More than usual", "सामान्य भन्दा बढी "]
    },
    {
        value: "Less than usual",
        label: ["Less than usual", "सामान्य भन्दा कम "]
    },
    ],
    rest_consent: [{
        value: "No rest",
        label: ["No rest", "आराम नै नगर्ने "]
    },
    {
        value: "Less than one hour",
        label: ["Less than one hour", "१ घण्टा भन्दा कम आराम "]
    },
    {
        value: "1-2 Hour",
        label: ["1-2 Hour", "१ देखि २ घण्टा आराम "]
    },
    {
        value: "More than 2 Hour",
        label: ["More than 2 Hour", "२ घण्टा भन्दा बढी आराम "]
    }
    ],
    delivery_place: [{
        value: "Home without presence of skill birth attendants",
        label: ["Home without presence of skill birth attendants", "घरमै तालिमप्राप्त व्यक्तिको अनुपस्थितिमा "]
    },
    {
        value: "Home in presence of Skill birth attendants",
        label: ["Home in presence of Skill birth attendants", "घरमै तालिमप्राप्त व्यक्तिबाट"]
    },
    {
        value: "Hospital / Health Center / Nursing Home",
        label: ["Hospital / Health Center / Nursing Home", "हस्पिटल/स्वास्थ्य केन्द्र/नर्सिङ्ग होम "]
    }
    ],
    malam: [{
        value: "Navi Malam",
        label: ["Navi Malam", "नाभी मलम "]
    },
    {
        value: "Local oil",
        label: ["Local oil", "स्थानीय/घरेलु  तेल "]
    },
    {
        value: "Nothing",
        label: ["Nothing", "केही पनि होइन "]
    }
    ],
    visits: [{
        value: "First Visit (just after the birth)",
        label: ["First Visit (just after the birth)", "पहिलो भेट बच्चा जन्मिनासाथ  "]
    },
    {
        value: "Third day of the delivery",
        label: ["Third day of the delivery", "सुत्केरी भएको तेश्रो दिन "]
    },
    {
        value: "Seventh day of the delivery",
        label: ["Seventh day of the delivery", "सुत्केरी भएको सातौ दिन "]
    }
    ],
    gender:[
        { label: ['Male', 'पुरुष'], value: 'male' },
        { label: ['Female', 'महिला'], value: 'female' },
        { label: ['Third Gender', 'Third Gender'], value: 'third gender' },
    ],
    weight: [{
        value: "Less than 2500 gram",
        label: ["Less than 2500 gram", "२५०० ग्राम भन्दा कम तौल "]
    },
    {
        value: "Approx. 2500 gram",
        label: ["Approx. 2500 gram", "करिब २५०० ग्राम तौल "]
    },
    {
        value: "More than 2500 gram",
        label: ["More than 2500 gram", "२५०० ग्राम भन्दा बढी तौल "]
    }
    ],
    feed: [{
        value: "VitaminGrant, root, tuber",
        label: ["Grant, root, tuber", "तरुल तथा कन्दमुल "]
    },
    {
        value: "Legumes and nuts",
        label: ["Legumes and nuts", "गेडागुडी "]
    },
    {
        value: "Dairy products",
        label: ["Dairy products", "दुध र दुध बाट बनेको परिकार "]
    },
    {
        value: "Eggs",
        label: ["Eggs", "अन्डा "]
    },
    {
        value: "Flesh foods",
        label: ["Flesh foods", "मासु "]
    },
    {
        value: "Vitamin A rich foods",
        label: ["Vitamin A rich foods", "भिटामिन ए पाउने खानेकुरा "]
    },
    {
        value: "Other fruits and vegetablets",
        label: ["Other fruits and vegetablets", "फलफुल तथा तरकारी "]
    },
    ],
    zinc: [{
        value: "none (no Zinc no ORS was given",
        label: ["none (no Zinc no ORS was given", "केहि पनि नाखुवाएको (पुर्नार्जलीय झोल वा झिंक केहि नखुवाएको  )"]
    },
    {
        value: "ORS only ( Punarjaliya jhol )",
        label: ["ORS only ( Punarjaliya jhol )", "पुनर्जलीय झोल(जीवनजल)"]
    },
    {
        value: "ORS and Zinc tablets",
        label: ["ORS and Zinc tablets", "जिंक चक्की र पुनर्जलीय झोल खुवाएको "]
    },
    {
        value: "ORS and Zinx X10 tablets",
        label: ["ORS and Zinx X10 tablet", "पुनर्जलीय झोल र १० चक्की जिंक ट्याब्लेट "]
    },
    {
        value: "ORS, zinc &10 tablets and additional foods",
        label: ["ORS, zinc &10 tablets and additional foods", "पुनर्जलीय झोल, १० वटा जिंक चक्की र अन्य खाना "]
    },
    ],
    treatment_seeked: [{
        value: "Homemade treatment",
        label: ["Homemade treatment", "घरेलु उपचार "]
    },
    {
        value: "Traditional or religious healer",
        label: ["Traditional or religious healer", "परम्परागत/धार्मिक उपचार"]
    },
    {
        value: "Ayurveda treatment",
        label: ["Ayurveda treatment", "आयुर्बेदिक उपचार"]
    },
    {
        value: "Government health facility / outreach clinic",
        label: ["Government health facility / outreach clinic", "सरकारी स्वास्थ्य संस्था/ गाउघर क्लिनिक"]
    },
    {
        value: "FCHV",
        label: ["FCHV", "ग्रामिण स्वास्थ्य स्वयम्सेविका"]
    },
    {
        value: "Pharmacy or medicine seller",
        label: ["Pharmacy or medicine seller", "औषधि पसल/क्लिनिक"]
    },
    ],
    timespan: [{
        value: "Immediately / within first hour after delivery",
        label: ["Immediately / within first hour after delivery", "जन्मन बित्थिकै वा सुत्केरी भएको पहिलो घण्टा भित्र"]
    },
    {
        value: "After the first hour",
        label: ["After the first hour", "सुत्केरी भएको पहिलो घण्टा भित्र "]
    },
    ],
    toiletPlace: [{
        value: "Toilet",
        label: ["Toilet", "सौचालय "]
    },
    {
        value: "Open area",
        label: ["Open area", "खुला क्षेत्र"]
    }
    ],
    wash_hands: [{
        value: "After defecation",
        label: ["After defecation", "दिशा गरेपछि"]
    },
    {
        value: "After cleaning the bottom of child",
        label: ["After cleaning the bottom of child", "केटाकेटीको दिशा सफा गरेपछि"]
    },
    {
        value: "After caring animal dung",
        label: ["After caring animal dung", "बस्तुभाउको गोबर सफागरेपछि"]
    },
    {
        value: "Before Feeding children",
        label: ["Before Feeding children", "केटाकेटीलाई खानखुवाउनु अघि"]
    },
    {
        value: "Before eating",
        label: ["Before eating", "खाना खानु अघि"]
    },
    {
        value: "Before making food",
        label: ["Before making food", "खाना पकाउनु अघि"]
    },
    ],
    immunization_received: [{
        value: "BCG",
        label: ["BCG", "बि सी जी "]
    },
    {
        value: "DPT-HepB-HiB",
        label: ["DPT-HepB-HiB", "डी पी ति हेपातईटिस बि "]
    },
    {
        value: "OPT ( Oral Polio Vaccine)",
        label: ["OPT ( Oral Polio Vaccine)", " पोलियो ठो-थोपा ( Oral Polio Vaccine)"]
    },
    {
        value: "PCV ( Pneumococcal conjugate vaccine )",
        label: ["PCV ( Pneumococcal conjugate vaccine )", " पी सी भी ( Pneumococcal conjugate vaccine )"]
    },
    {
        value: "IPV ( Injectable Polio Vaccine )",
        label: ["IPV ( Injectable Polio Vaccine )", "आइ पी भी  ( Injectable Polio Vaccine )"]
    },
    {
        value: "MR ( Measles Rubella )",
        label: ["MR ( Measles Rubella )", "दादुरा/रुबेला  ( Measles Rubella )"]
    },
    {
        value: "JE ( Japanese Encephlitis )",
        label: ["JE ( Japanese Encephlitis )", "जापानिज इन्सेफलाईटिस  ( Japanese Encephlitis )"]
    },
    {
        value: "Td ( Tetanus diphitheria )",
        label: ["Td ( Tetanus diphitheria )", "टीडी  ( Tetanus diphitheria )"]
    },
    ],
    participated_before: [{
        value: "Never",
        label: ["Never", "कहिले पनि छैन "]
    },
    {
        value: "Before 6 months",
        label: ["At least once", "कम से कम एक पल्ट"]
    },
    ],
    day_of_age: [{
        value: "179",
        'label': ['Less than 6 months', '६ महिना भन्दा पछी']
    },
    {
        value: "181",
        'label': ['More than 6 months', '६ महिना भन्दा पछी']
    },
    ]
}

export default (label, lang = 0, item = false) => {
    if (item) {
        return _.find(options[label], {
            value: item
        }).label[lang]
    }
    let top = options[label] || [];
    return top.map(o => ({
        label: o.label[lang],
        value: o.value
    }))
}
