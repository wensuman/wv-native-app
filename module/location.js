import {
  Container,
  Content,
  Form,
  Input as Inp,
  Item,
  Label,
  List,
  ListItem,
  Picker
} from 'native-base';
import {
  TextInput as Input,
  StyleSheet,
  Text,
  ToastAndroid,
  View
} from 'react-native';
import {
  marginLR,
  paddingLR
} from '../styles';
import {
  NavigationActions
} from 'react-navigation'
import React from 'react';
import _ from 'lodash';
import {file} from './store'

const state = {
  state_id: '',
  municipality_id: '',
  district_id: '',
  ward_id: '',
  tole: '',
  locations: [],
  changed: false
}

export default class Location extends React.Component {

  constructor(props) {
    super(props);
    this.state = this.props.locations || state
    this.componentDidMount = this.componentDidMount.bind(this)
    this.componentDidUpdate = this.componentDidUpdate.bind(this)
    this.childOf = this.childOf.bind(this)
    this.pickerItem = this.pickerItem.bind(this);
    this.load = this.load.bind(this);
  }

  componentDidMount() {
    this.load();
  }

  async load() {
    let parse = await file('place');

    !parse && ToastAndroid.show("Location has not been loaded.", ToastAndroid.SHORT);

    try {
      this.setState({
        is_ready: true,
        locations: _.groupBy(parse.data || parse || [], 'Parent')
      });
    } catch (error) {
      ToastAndroid.show("Location has not been loaded.", ToastAndroid.SHORT);
      this.props.navigation.dispatch(NavigationActions.goBack());
    }
  }

  childOf(parent) {
    let defaults = [{ 'Location_Id': '', 'Location_name': 'Select location' }];

    return this.state.locations ? defaults.concat(this.state.locations[Number(parent)] || []) : defaults;
  }

  pickerItem(item) {
    return <Picker.Item key={item.Location_Id} label={_.capitalize(item.Location_name)} value={item.Location_Id} />;
  }

  componentDidUpdate() {

    this.state.changed && this.props.onChange(_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole']));

    this.state.changed && this.setState({
      changed: false
    });

  }

  render() {

    if (!this.state.is_ready) {
      return <View></View>;
    }

    if (this.props.display) {
      return <View style={{width:'100%'}}>
        <List>
          <ListItem style={{marginLeft:0,paddingLeft:10}}><Text>State : {_.capitalize(_.find(this.state.locations[0], { Location_Id: parseInt(this.props.data.state_id) }).Location_name)}</Text></ListItem>
          <ListItem style={{marginLeft:0,paddingLeft:10}}><Text>District : {_.capitalize(_.find(this.state.locations[parseInt(this.props.data.state_id)], { Location_Id: parseInt(this.props.data.district_id) }).Location_name)}</Text></ListItem>
          <ListItem style={{marginLeft:0,paddingLeft:10}}><Text>Municipality : {_.capitalize(_.find(this.state.locations[parseInt(this.props.data.district_id)], { Location_Id: parseInt(this.props.data.municipality_id) }).Location_name)}</Text></ListItem>
          <ListItem style={{marginLeft:0,paddingLeft:10}}><Text>Ward No. : {_.capitalize(_.find(this.state.locations[parseInt(this.props.data.municipality_id)], { Location_Id: parseInt(this.props.data.ward_id) }).Location_name)}</Text></ListItem>
          {this.props.tole !== false && <ListItem style={{marginLeft:0,paddingLeft:10}}><Text>Tole : {_.capitalize(this.props.data.tole)}</Text></ListItem>}
        </List>
      </View>
    }

    return (
      <View>
        <View style={{ borderWidth: 1, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }}>
          <Label>Select State *</Label>
          <Picker selectedValue={this.state.state_id} onValueChange={value => {
            this.setState({ state_id: value, district_id: '', municipality_id: '', ward_id: '', changed: true });
          }}>
            {this.childOf(0).map(this.pickerItem)}
          </Picker>
        </View>
        <View style={{ borderWidth: 1, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }}>
          <Label>Select District *</Label>
          <Picker enabled={Boolean(this.state.state_id)} selectedValue={this.state.district_id} onValueChange={value => {
            this.setState({ district_id: value, municipality_id: '', ward_id: '', changed: true });
          }}>
            {this.state.state_id ? this.childOf(this.state.state_id).map(this.pickerItem) : <Picker.Item value={''} label='Select State first' />}
          </Picker>
        </View>
        <View style={{ borderWidth: 1, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }}>
          <Label>Select Municipality *</Label>
          <Picker enabled={Boolean(this.state.district_id)} selectedValue={this.state.municipality_id} onValueChange={value => {
            this.setState({ municipality_id: value, ward_id: '', changed: true });
          }}>
            {this.state.district_id ? this.childOf(this.state.district_id).map(this.pickerItem) : <Picker.Item value={''} label='Select District first' />}
          </Picker>
        </View>
        <View style={{ borderWidth: 1, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }}>
          <Label>Select Ward *</Label>
          <Picker enabled={Boolean(this.state.municipality_id)} selectedValue={this.state.ward_id} onValueChange={value => {
            this.setState({ ward_id: value, changed: true });
          }}>
            {this.state.municipality_id ? this.childOf(this.state.municipality_id)
              .sort((first, second) => first.Location_name - second.Location_name)
              .map(this.pickerItem) : <Picker value={''} label='Select Municipality first' />}
          </Picker>
        </View>
        {this.props.tole !== false && <Item style={{ borderWidth: 2, borderColor: 'transparent', borderBottomColor: 'black', borderStyle: 'solid' }} stackedLabel>
          <Label>Tole name</Label>
          <Inp placeholder='Enter tole name' value={this.state.tole} onChangeText={text => this.setState({ changed: true, tole: text })} />
        </Item>}
      </View>
    );
  }
}