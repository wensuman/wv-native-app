import {
    AsyncStorage,
    ToastAndroid
} from "react-native";

import {
    Constants,
    Location,
    FileSystem
} from 'expo';

import _ from 'lodash';

export const file = async (i)=>{
    let token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let fileUri = FileSystem.documentDirectory + i + token.id;
    return JSON.parse(await FileSystem.readAsStringAsync(fileUri));
}

export const fileExists = async () => {

}

export const get = async () =>{
    let token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    return JSON.parse(await AsyncStorage.getItem('@wv:data-'+token.id));
}

export const clear = async () =>{

    let token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    return AsyncStorage.removeItem('@wv:data-'+token.id);
}

export const set = async (data)=>{
    let token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    return await AsyncStorage.setItem('@wv:data-'+token.id,JSON.stringify(data));
}

export const RemoveUploaded = async (i) => {
    let store = await get();
    _.remove(store, obj => obj.uploaded && obj.belongsTo == i)
    i == 'childsurvey' && _.remove(store, obj=>obj.belongsTo = 'childsurvey')
    i == 'survey' && _.remove(store, obj=>obj.belongsTo == 'survey')
    await set(store);
    return true;
}

export default async(state, callback) => {
    state.groups = undefined;
    state.detail = undefined;
    state.details = undefined;
    state.members = undefined;
    state.meta = undefined;
    state.u5_count = undefined;
    state.lang = undefined;

    let store = await get();

    if (state.index) _.remove(store, obj => obj.index == state.index)
    else state.index = Math.random().toString(36).substr(2, 15);

    state.exists = true;

    if (state.id && state.belongsTo == 'family') {
        store = store.map((item) => {
            if (item.belongsTo == 'member' && item.family_index == state.index) {
                item.family_id = state.id
            }

            return item;
        })
    }

    if (state.id && state.belongsTo == 'group') {
        store = store.map((item) => {
            if (item.belongsTo == 'member') {
                if (item.group_index == state.index) {
                    if (item.groups_involved) {
                        item.groups_involved.push(state.id);
                    } else {
                        item.groups_involved = [state.id]
                    }
                }
                if (item['group_' + state.index + '_designation']) {
                    item['group_' + state.id + '_designation'] = item['group_' + state.index + '_designation'];
                }
            }
            return item;
        })
    }
    if (state.id && state.belongsTo == 'institute') {
        store = store.map((item) => {
            if (item.belongsTo == 'group' && (item.institute_index == state.index || item.institute_id == state.index)) {
                item.institute_id = state.id;
            }
            return item;
        });
    }

    await set([state].concat(store || []));

    callback && callback();

    return true;
}

export const bulkStore = async(states, callback) => {

    let store = await get();

    let indexes = store.map(f => f.index)

    _.remove(store, obj => indexes.includes(obj.index));

    let ob = states.map(m => {
        m.exists = true;
        return m;
    });

    await set(ob.concat(store || []));

    callback && callback();

    return true;
}

export const remove = async(index, toast = true) => {

    let data = await get();

    let removed = _.remove(data, obj => obj.index == index)

    if (removed && removed.length > 0) {
        removed = removed[0];
        if (removed.belongsTo == 'family') {
            _.remove(data, obj => obj.family_index == index)
        } else if (removed.belongsTo == 'group') {
            _.remove(data, obj => obj.group_index == index)
        } else if (removed.belongsTo == 'institute') {
            _.remove(data, obj => obj.institute_id == index || obj.institute_index == index);
        }
    }

    await set(data);

    toast && ToastAndroid.show('Deleted successfully.', ToastAndroid.SHORT);

    return;
}
