import { Input, Number, Select } from '../../../forms/input';
import { ScrollView, ToastAndroid } from 'react-native';

import Create from '../../../designs/create';
import Header from '../../../designs/header';
import Label from '../../../module/questions'
import { NavigationActions } from "react-navigation";
import Options from '../../../module/options'
import React from "react";
import _ from 'lodash';
import store from '../../../module/store';
import Diff from 'date-fns/difference_in_days';
import ShouldBePdherth from './shouldbepdherth';

export default class Question extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    state = {
        belongsTo: 'child survey',
        lang: 0,
    }

    constructor(props) {
        super(props);
        this.state = { day_of_age: Diff(new Date(), this.props.navigation.state.params.member.date_of_birth) };
        this.getQuestions = this.getQuestions.bind(this);
    }

    validation(data) {
        this.criteria = data;
        return true;
    }

    required(checkIn, message) {
        if (checkIn.filter((state) => !this.state[state]).length > 0) {
            ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
            return false;
        }
        return true;
    }

    render() {
        return <ScrollView>
            <Header icon={'md-book'} title="Child Survey" alertOnBack {...this.props} rightText={this.state.lang == 1 ?  'NP' : 'EN'} onRight={() => { this.setState({ lang: this.state.lang == 0 ? 1: 0 }) }} />
            <Create exists={this.state.exists} onSave={() => {
                var values = { ...this.state, lang: undefined, age: undefined, day_of_age: undefined, belongsTo: undefined, participated_before: undefined };
                store({ belongsTo: 'child survey', full_name: this.props.navigation.state.params.member.full_name, id: this.props.navigation.state.params.member.id, meta: this.props.navigation.state.params.member, response: values, day_of_age: undefined }, () => {
                    this.props.navigation.dispatch(NavigationActions.reset({
                        index: 1,
                        actions: [
                            NavigationActions.navigate({ routeName: 'MainMonitor' }),
                            NavigationActions.navigate({ routeName: 'ChildSurvey' })
                        ]
                    }))
                });
            }} {...this.props} questions={this.getQuestions(this.state.status)} />
        </ScrollView>;
    }

    getQuestions(name) {

        let question = [
            { component: () => <Select key='participated_before' selected={this.state.participated_before} onChange={participated_before => this.setState({ participated_before })} label={Label('participated_before', this.state.lang)} data={Options('participated_before', this.state.lang)} />, validation: () => this.required(['participated_before']) }

        ];

        return question.concat([
            this.state.participated_before == "Never" && { component: () => <Select label={Label(12, this.state.lang)} data={Options('weight', this.state.lang)} key='b_weight' selected={this.state.b_weight} onChange={b_weight => this.setState({ b_weight })} /> , validation: ()=>this.required(['b_weight'])},
            !(this.state.day_of_age > 180) && { component: () => <Select label={Label(18, this.state.lang)} key='infant_formula' selected={this.state.infant_formula} onChange={infant_formula => this.setState({ infant_formula })} /> , validation: ()=>this.required(['infant_formula'])},
            this.state.day_of_age > 180 && { component: () => <Select multiple label={Label(19, this.state.lang)} data={Options('feed', this.state.lang)} key='day_feed' selected={this.state.day_feed} onChange={day_feed => this.setState({ day_feed })} /> , validation: ()=>this.required(['day_feed'])},
            { component: () => <Number label={Label(20, this.state.lang)} key='day_feed1' value={this.state.feed_in_24_hours} onChange={feed_in_24_hours => this.setState({ feed_in_24_hours })} /> , validation: ()=>this.required(['feed_in_24_hours'])},
            { component: () => <Select label={Label(21, this.state.lang)} key='loose_stool' selected={this.state.loose_stool} onChange={loose_stool => this.setState({ loose_stool,medicine:undefined })} /> , validation: ()=>this.required(['loose_stool'])},
            this.state.loose_stool == 'yes' && { component: () => <Select label={Label(22, this.state.lang)} data={Options('zinc', this.state.lang)} key='medicine' selected={this.state.medicine} onChange={medicine => this.setState({ medicine })} /> , validation: ()=>this.required(['medicine'])},
            { component: () => <Select label={Label(23, this.state.lang)} key='cough' selected={this.state.cough} onChange={cough => this.setState({ cough,breathing_problem:undefined,treatment:undefined  })} /> , validation: ()=>this.required(['cough'])},
            this.state.cough == 'yes' && { component: () => <Select label={Label(24, this.state.lang)} key='breathing_problem' selected={this.state.breathing_problem} onChange={breathing_problem => this.setState({ breathing_problem })} /> , validation: ()=>this.required(['breathing_problem'])},
            this.state.cough == 'yes' && this.state.breathing_problem == 'yes' && { component: () => <Select label={Label(25, this.state.lang)} data={Options('treatment_seeked', this.state.lang)} key='treatment' selected={this.state.treatment} onChange={treatment => this.setState({ treatment })} /> , validation: ()=>this.required(['treatment'])},
            this.state.day_of_age > 180 && { component: () => <Select label={Label(30, this.state.lang)} key='deworming' selected={this.state.deworming} onChange={deworming => this.setState({ deworming })} /> , validation: ()=>this.required(['deworming'])},
            { component: () => <Select label={Label('vitamin', this.state.lang)} key='vitamin' selected={this.state.vitamin} onChange={vitamin => this.setState({ vitamin })} /> , validation: ()=>this.required(['vitamin'])},
            { component: () => <Number label={Label(31, this.state.lang)} key='weight' value={this.state.weight} onChange={weight => this.setState({ weight,pdherth_applicable: undefined })} />, validation: ()=>this.required(['weight']) , validation: ()=>this.required(['weight'])},
            { component: () => <Number label={Label(32, this.state.lang)} key='height' value={this.state.height} onChange={height => this.setState({ height })} />, validation: ()=>this.required(['height'])  , validation: ()=>this.required(['height'])},
            { component: () => <Input label={Label('muac', this.state.lang)} key='muac' value={this.state.muac} onChange={muac => this.setState({ muac })} /> , validation: ()=>this.required(['muac'])},
            { component: () => <Select label={Label(35, this.state.lang)} key='bcg' selected={this.state.bcg} onChange={bcg => this.setState({ bcg })} />},
            { component: () => <Select multiple data={Options('phase',this.state.lang)} label={Label(36, this.state.lang)} key='dpt' selected={this.state.dpt} onChange={dpt => this.setState({ dpt })} />},
            { component: () => <Select multiple data={Options('phase',this.state.lang)} label={Label(37, this.state.lang)} key='opv' selected={this.state.opv} onChange={opv => this.setState({ opv })} />},
            { component: () => <Select multiple data={Options('phase',this.state.lang)} label={Label(38, this.state.lang)} key='pcv' selected={this.state.pcv} onChange={pcv => this.setState({ pcv })} />},
            { component: () => <Select label={Label(39, this.state.lang)} key='ipv' selected={this.state.ipv} onChange={ipv => this.setState({ ipv })} />},
            this.state.day_of_age > 270 && { component: () => <Select multiple data={Options('phase2',this.state.lang)} label={Label(40, this.state.lang)} key='mr' selected={this.state.mr} onChange={mr => this.setState({ mr })} />},
            { component: () => <Select label={Label(42, this.state.lang)} key='je' selected={this.state.je} onChange={je => this.setState({ je })} />},
            this.state.day_of_age > 180 && (this.state.muac < 12.5 || ShouldBePdherth(Diff( new Date(),this.props.navigation.state.params.member.date_of_birth), this.state.weight,this.props.navigation.state.params.member.sex[0].toLowerCase())) && { component: () => <Select label={Label('pdherth_applicable', this.state.lang)} key='pdherth_applicable' selected={this.state.pdherth_applicable} onChange={pdherth_applicable => this.setState({ pdherth_applicable })} /> , validation: ()=>this.required(['pdherth_applicable'])},
        ]);
    }
}