import { Alert, Platform, ScrollView, Text, ToastAndroid } from 'react-native';
import { Body, Button, Container, Content, Header, Icon, Input, Item, Left, List, ListItem, Radio, Right, View } from 'native-base';
import React from 'react';
import _ from 'lodash';
import { NavigationActions } from 'react-navigation';
import { remove } from '../../../module/store';
import {get,file} from '../../../module/store';
import Age from '../../../module/age';

export default class Monitor extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    };

    state = {

    };

    collection = [];

    sureys = [];

    constructor(props) {
        super(props);
        this.searches = this.searches.bind(this);
        this.load = this.load.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async searches() {
        
        if (this.collection && this.collection.length == 0) {
            this.collection = await file('member');
        }
        
        let result = _.find(this.collection, { id: parseInt(this.state.search) });
        
        result && this.setState({ result: result, age: Age(result.date_of_birth) });
        
        !result && this.setState({ result: undefined, age: undefined });
        
        !result && ToastAndroid.show('Member not found.', ToastAndroid.SHORT);
    }

    async load() {
        
        ToastAndroid.show('Loading...', ToastAndroid.SHORT);
        
        let models = await get() || [];
        
        models = _.filter(models, { belongsTo: 'child survey' });

        this.setState({
            models
        });
        
    }

    to(member) {
        const routes = [
            NavigationActions.navigate({ routeName: 'MainMonitor' }),
            NavigationActions.navigate({ routeName: 'ChildSurvey' }),
            NavigationActions.navigate({ routeName: 'ChildSurveyQuestionnaire', params: { member } })
        ];

        this.props.navigation.dispatch(NavigationActions.reset({
            index: 2,
            actions: routes
        }));
    }

    render() {
        return <Container>
            <Header style={{ backgroundColor: 'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded>
                <Item>
                    <Icon name="ios-search" />
                    <Input onChangeText={search => this.setState({ search })} placeholder="Enter ID" />
                    <Button style={{ paddingHorizontal: 10 }} onPress={this.searches} success>
                        <Text>Find</Text>
                    </Button>
                </Item>
            </Header>
            <Content>
                {this.state.result && <ScrollView>
                    <List>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>ID : {this.state.result.id}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Name : {this.state.result.full_name}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Gender : {_.capitalize(this.state.result.sex)}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Date of Birth : {this.state.result.date_of_birth}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Age: {this.state.age} Approximately.</Text></ListItem>
                    </List>
                    {this.state.age < 5 && <Button full block onPress={this.to.bind(this, this.state.result).debounce()}><Text>Proceed</Text></Button>}
                </ScrollView>}
                {!this.state.result && !this.state.search && <ScrollView>
                    <List>
                        {this.state.models && this.state.models.map(m => <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} key={m.index}><Body><Text>{m.id} - {m.full_name}</Text></Body><Right><Button onPress={() => Alert.alert(
                            'Are you sure?',
                            'It will delete this entry from this device.',
                            [
                                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                { text: 'OK', onPress: () => remove(m.index) && this.setState({ models: _.reject(this.state.models, { index: m.index }) }) },
                            ],
                            { cancelable: false }
                        )} danger><Icon name='trash' /></Button></Right></ListItem>)}
                    </List>
                </ScrollView>}
            </Content>
        </Container >;
    }
}
