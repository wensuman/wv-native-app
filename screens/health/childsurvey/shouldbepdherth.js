import LMS from './lms';
import _ from 'lodash';
export default (day, weight, gender, ret = false)=>{
    try {

        let lms = _.find(LMS, {day, gender});
        let {l,m,s} = lms;
        let mat = (Math.pow(weight/m,l)-1)/(l*s);
        
        return ret ? mat : mat <= -1;
        
    } catch (error) {

        return false;
    }
}