import { Alert, Platform, ScrollView, Text, ToastAndroid } from 'react-native';
import { Body, Button, Container, Content, Header, Icon, Input, Item, Left, List, ListItem, Radio, Right, View } from 'native-base';
import React from 'react';
import _ from 'lodash';
import { NavigationActions } from 'react-navigation';
import { remove } from '../../../module/store';
import { get, file } from '../../../module/store';

export default class Monitor extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    };

    state = {

    };

    collection = [];

    sureys = [];
    members = [];

    constructor(props) {
        super(props);
        this.searches = this.searches.bind(this);
        this.load = this.load.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async searches() {
        if (this.collection && this.collection.length == 0) {
            this.collection = await file('pdherth');
            this.members = await file('member');
        }
        let result = _.find(this.collection, { member_id: parseInt(this.state.search) });
        if(result){
            let result =  _.find(this.members, { id: parseInt(this.state.search) });
            this.setState({ result: result });
            return;
        }
        this.setState({result: undefined});
        ToastAndroid.show('Not found in PD Hearth.', ToastAndroid.SHORT);
    }

    async load() {
        ToastAndroid.show('Loading...', ToastAndroid.SHORT);
        let models = await get() || [];
        models = _.filter(models, { belongsTo: 'pdherth' });
        this.setState({
            models
        });
    }

    to(member){
        const actions = [
            NavigationActions.navigate({ routeName: 'MainMonitor' }),
            NavigationActions.navigate({ routeName: 'Pdherth' }),
            NavigationActions.navigate({ routeName: 'PdherthQuestionnaire', params: { member } })
        ];

        this.props.navigation.dispatch(NavigationActions.reset({
            index:2,
            actions:actions
        }));
    }

    render() {
        return <Container>
            <Header style={{ backgroundColor: 'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded>
                <Item>
                    <Icon name="ios-search" />
                    <Input keyboardType="numeric" onChangeText={search => this.setState({ search })} placeholder="Enter ID" />
                    <Button style={{ paddingHorizontal: 10 }} onPress={this.searches} success>
                        <Text>Find</Text>
                    </Button>
                </Item>
            </Header>
            <Content>
                {this.state.result && <ScrollView>
                    <List>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>ID : {this.state.result.id}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Name : {this.state.result.full_name}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Gender : {_.capitalize(this.state.result.sex)}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Date of Birth : {this.state.result.date_of_birth}</Text></ListItem>
                    </List>
                    <Button full block onPress={this.to.bind(this, this.state.result).debounce()}><Text>Proceed</Text></Button>
                </ScrollView>}
                {!this.state.result && !this.state.search && <ScrollView>
                    <List>
                        {this.state.models && this.state.models.map(m => <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} key={m.index}><Body><Text>{m.id} - {m.full_name}</Text></Body><Right><Button onPress={() => Alert.alert(
                            'Are you sure?',
                            'It will delete this entry from this device.',
                            [
                                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                { text: 'OK', onPress: () => remove(m.index) && this.setState({ models: _.reject(this.state.models, { index: m.index }) }) },
                            ],
                            { cancelable: false }
                        )} danger><Icon name='trash' /></Button></Right></ListItem>)}
                    </List>
                </ScrollView>}
            </Content>
        </Container>;
    }
}
