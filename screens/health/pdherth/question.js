import { Input, Number, Select } from '../../../forms/input';
import { ScrollView, ToastAndroid } from 'react-native';

import Create from '../../../designs/create';
import Header from '../../../designs/header';
import Labels from '../../../module/questions'
import { NavigationActions } from "react-navigation";
import Options from '../../../module/options'
import React from "react";
import _ from 'lodash';
import ShouldBePdherth from '../childsurvey/shouldbepdherth';
import store from '../../../module/store';
import Diff from 'date-fns/difference_in_days';

export default class Question extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    state = {
        belongsTo:'child survey',
        lang: 0,
    }

    constructor(props) {
        super(props);
        this.getQuestions = this.getQuestions.bind(this);
    }

    validation(data) {
        this.criteria = data;
        return true;
    }

    required(checkIn, message) {
        if (checkIn.filter((state) => !this.state[state]).length > 0) {
            ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
            return false;
        }
        return true;
    }

    render() {
        return <ScrollView>
            <Header icon={'md-book'} title="PD Hearth" alertOnBack {...this.props} rightText={this.state.lang ?  'NP' : 'EN'} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
            <Create exists={this.state.exists} onSave={() => {
                var values = _.pick(this.state, ['muac','height','weight','status','waz','center_name','round_session','returning_from_healthpost']);
                store({ belongsTo: 'pdherth', full_name: this.props.navigation.state.params.member.full_name, id: this.props.navigation.state.params.member.id, meta: this.props.navigation.state.params.member, response: values }, () => {
                    this.props.navigation.dispatch(NavigationActions.reset({
                        index: 1,
                        actions: [
                            NavigationActions.navigate({routeName:'MainMonitor'}),
                            NavigationActions.navigate({routeName:'Pdherth'}),
                        ]
                    }))
                });
            }} {...this.props} questions={this.getQuestions(this.state.status)} />
        </ScrollView>;
    }

    getQuestions(name) {
  
        let question = [
            
        ];
        return question.concat([
            { component: () => <Input label="Round / Session # " value={this.state.round_session} onChange={round_session =>  this.setState({ round_session  }) } />, validation:  () => this.required(['round_session']) },
            { component: () => <Input label="PD Hearth center name" value={this.state.center_name} onChange={center_name =>  this.setState({ center_name  }) } />, validation:  () => this.required(['center_name']) },
            { component: () => <Select label={Labels('returning_from_healthpost', this.state.lang)} key='returning_from_healthpost' selected={this.state.returning_from_healthpost} onChange={returning_from_healthpost => this.setState({ returning_from_healthpost })} /> , validation: ()=>this.required(['returning_from_healthpost'])},
            { component: () => <Number label={Labels(31,this.state.lang)} value={this.state.weight} onChange={weight =>  this.setState({ weight, waz: ShouldBePdherth(Diff( new Date(),this.props.navigation.state.params.member.date_of_birth), weight,this.props.navigation.state.params.member.sex[0].toLowerCase(), true)  }) } />, validation: () => () => this.required(['weight']) },
            { component: () => <Number label={Labels(32,this.state.lang)} value={this.state.height} onChange={height =>  this.setState({ height  }) } /> },
            { component: () => <Number label={Labels('muac',this.state.lang)} value={this.state.muac} onChange={muac =>  this.setState({ muac  }) } />, validation: () => this.required(['muac']) },
            { component: () => <Select data={Options('pdherth_taken_at',this.state.lang)} label={Labels('status',this.state.lang)} selected={this.state.status} onChange={status => this.setState({ status })} />, validation: () => this.required(['status']) },
        ]);
    }
}