import React from 'react';
import { Container, Content, Button, Text } from 'native-base';
import Header from '../../designs/header';
import { NavigationActions } from 'react-navigation';

export default class Main extends React.Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
    }

    to(where = 'nowhere'){
        const actions = [
            NavigationActions.navigate({ routeName: 'MainMonitor' }),
            NavigationActions.navigate({ routeName: where })
        ];

        this.props.navigation.dispatch(NavigationActions.reset({
            index:1,
            actions:actions
        }))
    }

    render() {
        return <Container>
            <Header title="Choose monitoring" menu {...this.props} />
            <Content>
                <Button onPress={this.to.bind(this,'Monitor').debounce()} style={{margin: 20, marginTop: 40}} block>
                    <Text>Pregnant or Lactating</Text>
                </Button>
                <Button  onPress={this.to.bind(this,'ChildSurvey').debounce()} style={{margin: 20}} block success>
                    <Text>Child Survey</Text>
                </Button>
                <Button  onPress={this.to.bind(this,'Pdherth').debounce()} style={{margin: 20}} block warning>
                    <Text>PD Hearth</Text>
                </Button>
                <Button  onPress={this.to.bind(this,'ProducerGroup').debounce()} style={{margin: 20}} block info>
                    <Text>Producer Group</Text>
                </Button>
            </Content>
        </Container>;
    }
}