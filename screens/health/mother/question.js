import { Input, Number, Select } from '../../../forms/input';
import { ScrollView, ToastAndroid } from 'react-native';

import Create from '../../../designs/create';
import Header from '../../../designs/header';
import Label from '../../../module/questions'
import { NavigationActions } from "react-navigation";
import Options from '../../../module/options'
import React from "react";
import _ from 'lodash';
import store from '../../../module/store';

export default class Survey extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    constructor(props) {
        super(props);

        this.getQuestions = this.getQuestions.bind(this);

        let state = {};

        state.belongsTo = 'health';
        state.lang = 0;
        state.status = 'pregnant';

        if (this.props.navigation.state.params.member.is_lactating == 'yes') {
            state.status = 'lactating';
        }

        this.state = state;
    }

    validation(data) {
        this.criteria = data;
        return true;
    }

    required(checkIn, message) {
        if (checkIn.filter((state) => !this.state[state]).length > 0) {
            ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
            return false;
        }
        return true;
    }

    render() {
        console.log(this.state, this.props);
        return <ScrollView>
            <Header icon={'md-book'} title="Health Survey" alertOnBack {...this.props} rightText={this.state.lang ? 'NP' : 'EN'} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
            <Create exists={this.state.exists} onSave={() => {
                var values = {};
                _.filter(this.state, (f, k) => {
                    k.startsWith('id_$_') ? values[k.replace('id_$_', '')] = f : [];
                });
                store({ belongsTo: 'health', status: this.state.status, full_name: this.props.navigation.state.params.member.full_name, respondent_id: this.props.navigation.state.params.member.id, meta: this.props.navigation.state.params.member, response: values }, () => {
                    this.props.navigation.dispatch(NavigationActions.reset({
                        index: 1,
                        actions: [
                            NavigationActions.navigate({ routeName: 'MainMonitor' }),
                            NavigationActions.navigate({ routeName: 'Monitor' })
                        ]
                    }))
                });
            }} {...this.props} questions={this.getQuestions(this.state.status)} />
        </ScrollView>;
    }

    getQuestions(name) {

        let pregnant = [
            { component: () => <Select label={Label(1, this.state.lang)} key='id_$_1' selected={this.state.id_$_1} onChange={id_$_1 => this.setState({ id_$_1 })} /> },
            { component: () => <Select label={Label(2, this.state.lang)} key='id_$_2' selected={this.state.id_$_2} onChange={id_$_2 => this.setState({ id_$_2 })} /> },
            this.state.id_$_2 == 'yes' && { component: () => <Select label={Label(3, this.state.lang)} data={Options('tablets_count', this.state.lang)} key='id_$_3' selected={this.state.id_$_3} onChange={id_$_3 => this.setState({ id_$_3 })} /> },
            { component: () => <Select label={Label(4, this.state.lang)} data={Options('usual_consent', this.state.lang)} key='id_$_4' selected={this.state.id_$_4} onChange={id_$_4 => this.setState({ id_$_4 })} /> },
            { component: () => <Number label={Label(5, this.state.lang)} value={this.state.id_$_5} onChange={id_$_5 => this.setState({ id_$_5 })} /> },
            { component: () => <Input label={Label(6, this.state.lang)} value={this.state.id_$_6} onChange={id_$_6 => this.setState({ id_$_6 })} /> },
            { component: () => <Select label={Label(7, this.state.lang)} data={Options('rest_consent', this.state.lang)} key='id_$_7' selected={this.state.id_$_7} onChange={id_$_7 => this.setState({ id_$_7 })} /> },
        ];

        let lactating = [
            { component: () => <Select selected={this.state.participated_before} onChange={participated_before => this.setState({ participated_before })} label={Label('participated_before', this.state.lang)} data={Options('participated_before', this.state.lang)} /> },
            this.state.participated_before == 'Never' && { component: () => <Select label={Label(8, this.state.lang)} data={Options('rest_consent', this.state.lang)} key='id_$_8' selected={this.state.id_$_8} onChange={id_$_8 => this.setState({ id_$_8 })} /> },
            this.state.participated_before == 'Never' && { component: () => <Select label={Label(11, this.state.lang)} data={Options('malam', this.state.lang)} key='id_$_11' selected={this.state.id_$_11} onChange={id_$_11 => this.setState({ id_$_11 })} /> },
            this.state.participated_before == 'Never' && { component: () => <Select label={Label(13, this.state.lang)} data={Options('visits', this.state.lang)} key='id_$_12' selected={this.state.id_$_13} onChange={id_$_13 => this.setState({ id_$_13 })} /> },
            this.state.participated_before == 'Never' && { component: () => <Select label={Label(14, this.state.lang)} key='id_$_1' selected={this.state.id_$_14} onChange={id_$_14 => this.setState({ id_$_14, id_$_15: undefined, id_$_16: undefined, id_$_17: undefined })} /> },
            this.state.id_$_14 == 'yes' && this.state.participated_before == 'Never' && { component: () => <Select label={Label(15, this.state.lang)} data={Options('timespan', this.state.lang)} key='id_$_321' selected={this.state.id_$_15} onChange={id_$_15 => this.setState({ id_$_15 })} /> },
            this.state.id_$_14 == 'yes' && this.state.participated_before == 'Never' && { component: () => <Select label={Label(16, this.state.lang)} key='id_$_16' selected={this.state.id_$_16} onChange={id_$_16 => this.setState({ id_$_16 })} /> },
            ((this.state.id_$_14 == 'yes' && this.state.participated_before == 'Never') || this.state.participated_before !== 'Never') && { component: () => <Select label={Label(17, this.state.lang)} key='id_$_17' selected={this.state.id_$_17} onChange={id_$_17 => this.setState({ id_$_17 })} /> },
            this.state.id_$_17 == 'yes' && { component: () => <Number label={Label(20, this.state.lang)} value={this.state.id_$_20} onChange={id_$_20 => this.setState({ id_$_20 })} /> },
            { component: () => <Select label={Label(26, this.state.lang)} key='id_$_233' selected={this.state.id_$_26} onChange={id_$_26 => this.setState({ id_$_26 })} /> },
            { component: () => <Select label={Label(27, this.state.lang)} data={Options('toiletPlace', this.state.lang)} key='id_$_23122' selected={this.state.id_$_27} onChange={id_$_27 => this.setState({ id_$_27 })} /> },
            { component: () => <Select label={Label(28, this.state.lang)} key='id_$_23w2' selected={this.state.id_$_28} onChange={id_$_28 => this.setState({ id_$_28 })} /> },
            { component: () => <Select multiple label={Label(29, this.state.lang)} data={Options('wash_hands', this.state.lang)} key='id_$32_2' selected={this.state.id_$_29} onChange={id_$_29 => this.setState({ id_$_29 })} /> },
        ];

        let list = { pregnant, lactating };

        let question = [
        ];

        return question.concat(list[name] || []);
    }
}