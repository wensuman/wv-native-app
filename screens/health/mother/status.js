import { Body, Button, Container, Content, Icon, Input, Item, Left, List, ListItem, Radio, Right, View } from 'native-base';

import Header from '../../../designs/header';
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import _ from 'lodash';

export default class Status extends React.Component {
    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }
    constructor(props) {
        super(props)
        this.state = this.props.navigation.state.params.member || {};
        this.toSurvey = this.toSurvey.bind(this)
    }

    toSurvey() {
        this.props.navigation.navigate('Questionnaire', { member: _.pick(this.state, 'full_name', 'id', 'marital_status', 'sex', 'is_pregnant', 'is_lactating'), backKey: this.props.navigation.state.params.backKey })
    }

    render() {
        return <Container>
            <Header {...this.props} title={this.state.full_name} />
            <Content>
                <List>
                    <ListItem style={{ paddingLeft: 10, marginLeft: 0 }} onPress={() => this.setState({ is_pregnant: 'yes', is_lactating: 'no' })}>
                        <Body><Text>Is Pregnant ? </Text></Body>
                        <Right>
                            {this.state.is_pregnant == 'yes' && this.state.is_lactating == 'no' && <Icon style={{ color: 'orange' }} name="md-checkmark" />}
                        </Right>
                    </ListItem>
                    <ListItem onPress={() => this.setState({ is_lactating: 'yes', is_pregnant: 'no' })} style={{ paddingLeft: 10, marginLeft: 0 }}>
                        <Body><Text>Is Lactating ? </Text></Body>
                        <Right>
                            {this.state.is_lactating == 'yes' && this.state.is_pregnant == 'no' && <Icon style={{ color: 'orange' }} name="md-checkmark" />}
                        </Right>
                    </ListItem>
                </List>
                {(this.state.is_pregnant == 'yes' || this.state.is_lactating == 'yes') && <Button full block onPress={this.toSurvey.debounce()}><Text>Proceed to questionnaire</Text></Button>}
            </Content>
        </Container>
    }
}