import { Alert, Platform, ScrollView, Text, ToastAndroid } from 'react-native';
import { Body, Button, Container, Content, Header, Icon, Input, Item, Left, List, ListItem, Radio, Right, View } from 'native-base';

import React from 'react';
import _ from 'lodash';
import { remove } from '../../../module/store';
import {get,file} from '../../../module/store';

export default class Monitor extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    };

    state = {

    };

    collection = [];

    sureys = [];

    constructor(props) {
        super(props);
        this.searches = this.searches.bind(this);
        this.load = this.load.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.statusField = this.statusField.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async searches() {
        if (this.collection && this.collection.length == 0) {
            this.collection  = await file('member');
        }
        let result = _.find(this.collection, { id: parseInt(this.state.search) });
        this.setState({ result: result });
        !result && ToastAndroid.show('Member not found.', ToastAndroid.SHORT);
    }

    async load() {
        ToastAndroid.show('Loading...', ToastAndroid.SHORT);
        let models = await get() || [];
        models = _.filter(models, { belongsTo: 'health' });
        this.setState({
            models
        });
    }

    statusField() {
        this.props.navigation.navigate('StatusCheck', { member: this.state.result, backKey: this.props.navigation.state.key })
    }

    render() {
        return <Container>
            <Header style={{ backgroundColor: 'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded>
                <Item>
                    <Icon name="ios-search" />
                    <Input onChangeText={search => this.setState({ search })} placeholder="Enter ID" />
                    <Button style={{ paddingHorizontal: 10 }} onPress={this.searches} success>
                        <Text>Find</Text>
                    </Button>
                </Item>
            </Header>
            <Content>
                {this.state.result && <ScrollView>
                    <List>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>ID : {this.state.result.id}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Name : {this.state.result.full_name}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Gender : {_.capitalize(this.state.result.sex)}</Text></ListItem>
                        {this.state.result.sex == 'female' && this.state.result.detail && this.state.result.detail.is_pregnant && <ListItem key="pregnant" style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Is Pregnant ? : {_.capitalize(this.state.result.detail.is_pregnant)}</Text></ListItem>}
                        {this.state.result.sex == 'female' && this.state.result.detail && this.state.result.detail.is_lactating && <ListItem key="lactating" style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Is Lactating ? : {_.capitalize(this.state.result.detail.is_lactating)}</Text></ListItem>}
                    </List>
                    {this.state.result.sex == 'female' && <Button full block onPress={this.statusField.debounce()}><Text>Proceed</Text></Button>}
                </ScrollView>}
                {!this.state.result && !this.state.search && <ScrollView>
                    <List>
                        {this.state.models && this.state.models.map(m => <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} key={m.index}><Body><Text>{m.respondent_id} - {m.full_name}</Text></Body><Right><Button onPress={() => Alert.alert(
                            'Are you sure?',
                            'It will delete this entry from this device.',
                            [
                                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                { text: 'OK', onPress: () => remove(m.index) && this.setState({ models: _.reject(this.state.models, { index: m.index }) }) },
                            ],
                            { cancelable: false }
                        )} danger><Icon name='trash' /></Button></Right></ListItem>)}
                    </List>
                </ScrollView>}
            </Content>
        </Container>;
    }
}
