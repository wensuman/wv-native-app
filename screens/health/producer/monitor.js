import { Alert, Platform, ScrollView, Text, ToastAndroid } from 'react-native';
import { Body, Button, Container, Content, Header, Icon, Input, Item, Left, List, ListItem, Radio, Right, View } from 'native-base';

import React from 'react';
import _ from 'lodash';
import { get, file, remove } from '../../../module/store';
import { NavigationActions } from 'react-navigation';

export default class Monitor extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    };

    state = {

    };

    collection = [];

    surveys = [];

    members = [];

    constructor(props) {
        super(props);
        this.searches = this.searches.bind(this);
        this.load = this.load.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async searches() {
        
        if (this.collection && this.collection.length == 0) {
            this.collection = await file('group');
            this.members = await file('member');
        }

        let result = _.find(this.collection, {type_of_group: 'Producer group', id: parseInt(this.state.search) });

        if(result){
            result = {...result, ...result.details};
            this.setState({ result });
            this.setState({ members: this.members.filter(f=>result.memberids.includes(f.id))})
            return;
        }

        this.setState({result: undefined});

        ToastAndroid.show('Not found in producer group.', ToastAndroid.SHORT);

    }

    async load() {

        ToastAndroid.show('Loading...', ToastAndroid.SHORT);

        let models = await get() || [];

        models = _.filter(models, { belongsTo: 'Producer Group' });
        this.setState({
            models
        });
        
    }

    go(member){
        Alert.alert('Proceed to survey?','',[
            {text: 'OK', onPress: () => this.props.navigation.dispatch(NavigationActions.reset({
                index: 2,
                actions:this.routeActions(member)
            }))},
        ])
        
    }

    routeActions(member) {
        return [
            NavigationActions.navigate({ routeName: 'MainMonitor' }),
            NavigationActions.navigate({ routeName: 'ProducerGroup' }),
            NavigationActions.navigate({ routeName: 'ProducerQuestionnaire', params: { member_id: member.id, full_name: member.full_name, group_id: this.state.result.id } })
        ];
    }

    render() {
        return <Container>
            <Header style={{ backgroundColor: 'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded>
                <Item>
                    <Icon name="ios-search" />
                    <Input onChangeText={search => this.setState({ search })} placeholder="Enter ID" />
                    <Button style={{ paddingHorizontal: 10 }} onPress={this.searches} success>
                        <Text>Find</Text>
                    </Button>
                </Item>
            </Header>
            <Content>
                {this.state.result && <ScrollView>
                    <List>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>ID : {this.state.result.id}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Name : {this.state.result.name_of_group}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Year of establishment : {this.state.result.year_of_establishment}</Text></ListItem>
                        <ListItem style={{ paddingLeft: 10, marginLeft: 0 }}><Text>Group Type : {this.state.result.group_type}</Text></ListItem>
                        <ListItem itemDivider >
                            <Text>Members</Text>
                        </ListItem>
                        {this.state.members && this.state.members.length > 0 && this.state.members.map(m=><ListItem onPress={this.go.bind(this, m).debounce()} key={m.id} style={{marginLeft:0, paddingLeft: 10}}><Text>{m.full_name}, ID: {m.id}</Text></ListItem>)}
                        {this.state.members && this.state.members.length == 0 &&  <ListItem style={{marginLeft:0, paddingLeft: 10}}><Text>There are no members in this group.</Text></ListItem>}
                    </List>

                </ScrollView>}
                {!this.state.result && !this.state.search && <ScrollView>
                    <List>
                        {this.state.models && this.state.models.map(m => <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} key={m.index}><Body><Text>{m.id} - {m.full_name}</Text></Body><Right><Button onPress={() => Alert.alert(
                            'Are you sure?',
                            'It will delete this entry from this device.',
                            [
                                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                { text: 'OK', onPress: () => remove(m.index) && this.setState({ models: _.reject(this.state.models, { index: m.index }) }) },
                            ],
                            { cancelable: false }
                        )} danger><Icon name='trash' /></Button></Right></ListItem>)}
                    </List>
                </ScrollView>}
            </Content>
        </Container>;
    }
}
