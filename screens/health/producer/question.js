import { Input, Number, Select } from '../../../forms/input';
import { ScrollView, ToastAndroid } from 'react-native';

import Create from '../../../designs/create';
import Header from '../../../designs/header';
import Label from '../../../module/questions'
import { NavigationActions } from "react-navigation";
import Options from '../../../module/options'
import React from "react";
import _ from 'lodash';
import store from '../../../module/store';

export default class Question extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    state = {
        belongsTo:'child survey',
        lang: 0,
    }

    constructor(props) {
        super(props);
        this.getQuestions = this.getQuestions.bind(this);
    }

    validation(data) {
        this.criteria = data;
        return true;
    }

    required(checkIn, message) {
        if (checkIn.filter((state) => !this.state[state]).length > 0) {
            ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
            return false;
        }
        return true;
    }

    render() {
        return <ScrollView>
            <Header icon={'md-book'} title="Producer Group" alertOnBack {...this.props} rightText={this.state.lang ?  'NP' : 'EN'} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
            <Create exists={this.state.exists} onSave={() => {
                let values = _.pick(this.state,_.without(_.keys(this.state),'belongsTo', 'lang'));
                store({ belongsTo: 'Producer Group', group_id: this.props.navigation.state.params.group_id, full_name: this.props.navigation.state.params.full_name, id: this.props.navigation.state.params.member_id, response: values }, () => {
                    this.props.navigation.dispatch(NavigationActions.reset({
                        index: 1,
                        actions: [
                            NavigationActions.navigate({routeName:'MainMonitor'}),
                            NavigationActions.navigate({routeName:'ProducerGroup'}),
                        ]
                    }))
                });
            }} {...this.props} questions={this.getQuestions(this.state.status)} />
        </ScrollView>;
    }

    getQuestions(name) {
  
        let question = [
            
        ];

        return question.concat([
            { component: () => <Number key='lease_land' value={this.state.lease_land} onChange={lease_land => this.setState({ lease_land })} label={Label('lease_land', this.state.lang)} /> },
            { component: () => <Number key='own_land' value={this.state.own_land} onChange={own_land => this.setState({ own_land })} label={Label('own_land', this.state.lang)} /> },
            { component: () => <Number key='land_area' value={this.state.land_area} onChange={land_area => this.setState({ land_area })} label={Label('land_area', this.state.lang)} /> },
            { component: () => <Number key='vegetable_production_land' value={this.state.vegetable_production_land} onChange={vegetable_production_land => this.setState({ vegetable_production_land })} label={Label('vegetable_production_land', this.state.lang)} /> },
            
            { component: () => <Select key='grow_food' selected={this.state.grow_food} onChange={grow_food => this.setState({ grow_food, crops:[] })} label={Label('grow_food', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.grow_food == 1 && { component: () => <Select multiple key='crops' selected={this.state.crops || []} onChange={crops => this.setState({ crops })} label={Label('crops', this.state.lang)} data={Options('crops', this.state.lang)} /> },
            ...(this.state.crops || []).map((crop) => (
                {
                    component: () => [
                        <Number key="1" value={this.state['crop_details_quantity_grown_' + crop]} onChange={quantity_grown => this.setState({ ['crop_details_quantity_grown_' + crop]: quantity_grown })} label={Label('quantity_grown', this.state.lang, Options('crops', this.state.lang, crop))} />,
                    ]
                }
            )),
            { component: () => <Select key='food_sale' selected={this.state.food_sale} onChange={food_sale => this.setState({ food_sale, crop_sale: [] })} label={Label('food_sale', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.food_sale == 1 && { component: () => <Select multiple key='crops_sale' selected={this.state.crops_sale || []} onChange={crops_sale => this.setState({ crops_sale })} label={Label('crops_sale', this.state.lang)} data={Options('crops', this.state.lang)} /> },
            ...(this.state.crops_sale || []).map((crop) => (
                {
                    component: () => [
                        <Number key="1" value={this.state['sale_details_quantity_sale_' + crop]} onChange={quantity_sale => this.setState({ ['sale_details_quantity_sale_' + crop]: quantity_sale })} label={Label('quantity_sale', this.state.lang, Options('crops', this.state.lang, crop))} />,
                        <Number key="2" value={this.state['sale_details_price_sale_' + crop]} onChange={price_sale => this.setState({ ['sale_details_price_sale_' + crop]: price_sale })} label={Label('price_sale', this.state.lang, Options('crops', this.state.lang, crop))} />,
                    ]
                }
            )),
            (this.state.crop_sale || []).length > 0 && { component: () => <Select key='sale_location' selected={this.state.sale_location} onChange={sale_location => this.setState({ sale_location})} label={Label('sale_location', this.state.lang)} data={Options('sale_location', this.state.lang)} /> },
            { component: () => <Number key='group_saving' value={this.state.group_saving} onChange={group_saving => this.setState({ group_saving })} label={Label('group_saving', this.state.lang)} /> },
            { component: () => <Number key='saving' value={this.state.saving} onChange={saving => this.setState({ saving })} label={Label('saving', this.state.lang)} /> },
            { component: () => <Select key='recent_loan' selected={this.state.recent_loan} onChange={recent_loan => this.setState({ recent_loan,loan: undefined,financing_require:undefined  })} label={Label('recent_loan', this.state.lang)} data={Options('recent_loan', this.state.lang)} /> },
            this.state.recent_loan != 1 && { component: () => <Select key='financing_require' selected={this.state.financing_require} onChange={financing_require => this.setState({ financing_require, collateral_type: undefined })} label={Label('financing_require', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.financing_require == 1 && { component: () => <Select key='collateral_type' selected={this.state.collateral_type} onChange={collateral_type => this.setState({ collateral_type })} label={Label('collateral_type', this.state.lang)} data={Options('collateral_type', this.state.lang)} /> },
            this.state.recent_loan != 1 && { component: () => <Select key='loan' selected={this.state.loan} onChange={loan => this.setState({ loan, load_apply:undefined })} label={Label('loan', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.loan == 2 && { component: () => <Select key='loan_apply' selected={this.state.loan_apply} onChange={loan_apply => this.setState({ loan_apply })} label={Label('loan_apply', this.state.lang)} data={Options('apply', this.state.lang)} /> },
            { component: () => <Select key='crop_insurence' selected={this.state.crop_insurence} onChange={crop_insurence => this.setState({ crop_insurence,crop_insurence_why:undefined})} label={Label('crop_insurence', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.crop_insurence == 2 && { component: () => <Select key='crop_insurence_why' selected={this.state.crop_insurence_why} onChange={crop_insurence_why => this.setState({ crop_insurence_why })} label={Label('crop_insurence_why', this.state.lang)} data={Options('apply', this.state.lang)} /> },
            { component: () => <Select key='major_source' selected={this.state.major_source} onChange={major_source => this.setState({ major_source })} label={Label('major_source', this.state.lang)} data={Options('source', this.state.lang)} /> },
            { component: () => <Select multiple key='alternative_source' selected={this.state.alternative_source} onChange={alternative_source => this.setState({ alternative_source })} label={Label('alternative_source', this.state.lang)} data={Options('source', this.state.lang)} /> },
            { component: () => <Number key='estimated_earning' value={this.state.estimated_earning} onChange={estimated_earning => this.setState({ estimated_earning })} label={Label('estimated_earning', this.state.lang)} /> },
            { component: () => <Select multiple key='expenses' selected={this.state.expenses} onChange={expenses => this.setState({ expenses})} label={Label('expenses', this.state.lang)} data={Options('expenses', this.state.lang)} /> },
            { component: () => <Number key='estimated_expenses' value={this.state.estimated_expenses} onChange={estimated_expenses => this.setState({ estimated_expenses })} label={Label('estimated_expenses', this.state.lang)} /> },
            { component: () => <Number key='production_consumed' value={this.state.production_consumed} onChange={production_consumed => this.setState({ production_consumed })} label={Label('production_consumed', this.state.lang)} /> },
            { component: () => <Number key='production_wastage' value={this.state.production_wastage} onChange={production_wastage => this.setState({ production_wastage })} label={Label('production_wastage', this.state.lang)} /> },
            { component: () => <Number key='production_investment' value={this.state.production_investment} onChange={production_investment => this.setState({ production_investment })} label={Label('production_investment', this.state.lang)} /> },
            ]);
    }
}