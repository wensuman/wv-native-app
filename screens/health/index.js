import Monitor from './mother/monitor';
import Questionnaire from './mother/question';
import { StackNavigator } from 'react-navigation';
import StatusCheck from './mother/status';
import ChildSurvey from "./childsurvey/monitor";
import ChildSurveyQuestionnaire from "./childsurvey/question";
import PdherthSurvey from "./pdherth/monitor";
import PdherthSurveyQuestionnaire from "./pdherth/question";
import ProducerSurvey from "./producer/monitor";
import ProducerSurveyQuestionnaire from "./producer/question";
import Main from './main';
export default StackNavigator({
    MainMonitor:{
        screen: Main
    },
    Monitor:{
        screen: Monitor
    },
    ChildSurvey:{
        screen: ChildSurvey
    },
    ChildSurveyQuestionnaire:{
        screen: ChildSurveyQuestionnaire
    },
    Pdherth:{
        screen: PdherthSurvey
    },
    PdherthQuestionnaire:{
        screen: PdherthSurveyQuestionnaire
    },
    ProducerGroup:{
        screen: ProducerSurvey
    },
    ProducerQuestionnaire:{
        screen: ProducerSurveyQuestionnaire
    },
    StatusCheck:{
        screen: StatusCheck
    },
    Questionnaire:{
        screen: Questionnaire
    },
});