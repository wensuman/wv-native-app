export const institution_type = [
    { value: "Health Post", label: ["Health Post", "स्वास्थ्य चौकी"] },
    { value: "PHC ORC", label: ["PHC ORC", "PHC ORC"] },
    { label: ["School", "बिद्यालय"], value: "School" },
    { label: ["Reading Camp", "पढाइ शिविर"], value: "Reading Camp" },
    { label: ["Child Protection and Promotion Committee", "बाल संरक्षण र संवर्धन समिति"], value: "Child Protection and Promotion Committee" },
    { label: ["Livestock Service Center", "पशुधन सेवा केन्द्र"], value: "Livestock Service Center" },
    { label: ["Agriculture Service Center", "सङ्कलन केन्द्र"], value: "Agriculture Service Center" },
]

export const type_of_school = [
    { label: ['Basic', 'Basic'], value: 'Basic' },
    { label: ['Secondary', 'Secondary'], value: 'Secondary' },
]

export const have_software_or_hardware_components = [
    { label: ['Software', 'Software'], value: 'software' },
    { label: ['Hardware', 'Hardware'], value: 'software' },
    { label: ['Both', 'Both'], value: 'both' },
]

export default {
    institution_type,
    type_of_school,
    have_software_or_hardware_components
}