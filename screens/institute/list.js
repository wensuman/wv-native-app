import { ActivityIndicator, Platform, ScrollView, Text, ToastAndroid, TouchableOpacity } from "react-native";
import { Body, Button, Container, Content, Fab, Header, Icon, Input, Item, List, ListItem } from 'native-base';

import { NavigationActions } from "react-navigation";
import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import {get, file} from '../../module/store';
import _ from 'lodash';

export default class Lists extends React.Component {

  static navigationOptions = {
    title: "Institutes",
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      models: [],
      loading: 'Loading...',
      chunk:10
    };
    this.componentDidMount = this.componentDidMount.bind(this);
    this.load = this.load.bind(this);
    this.single = this.single.bind(this);
    this.newBing = this.newBing.bind(this);
    this.searches = this.searches.bind(this);
  }

  componentDidMount() {
    this.load();
  }

  async load() {

    let models = await get() || [];
    ToastAndroid.show('Refreshing...', ToastAndroid.SHORT);

    let downloadedModels = await file('institute') || [];
    downloadedModels = downloadedModels.map(m => ({ ...m, ...m.details, uploaded: true, ...m.location, index: 'index_' + m.id, id: m.id })).reverse()
    models = _.filter(models, { belongsTo: 'institute' }).concat(downloadedModels || []);

    this.setState({
      models, original: models || [], institute_loaded: true, loading: ''
    });

  }

  newBing() {
    this.props.navigation.navigate("NewInstitute")
  }

  searches() {
    !this.state.search && this.setState({ models: this.state.original });
    this.state.search && this.setState({ models: this.state.original.filter(f => f.id == parseInt(this.state.search) || f.name_of_institution.includes(this.state.search)) })
  }

  single(Institute) {
    
  }

  render() {
    return <Container>
      <Header style={{ backgroundColor: 'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded >
        <Item>
          <Icon name="ios-search" />
          <Input onChangeText={search => this.setState({ search })} placeholder="Search ID, name" />
          <Button style={{paddingHorizontal: 10}} onPress={this.searches} success>
            <Text  style={{color:'white'}}>Search</Text>
          </Button>
        </Item>
      </Header>
      <Content>
        <ScrollView>
          {this.state.models && <List>
            {this.state.models.map((Institute, index) => (
              <ListItem key={index} onPress={()=>{
                this.props.navigation.dispatch(NavigationActions.reset({
                  index: 1,
                  actions: [
                    NavigationActions.navigate({ routeName: 'InstituteList' }),
                    NavigationActions.navigate({
                      routeName: 'SingleInstitute', params: {
                        name: 'View institute of ',
                        data: Institute
                      }
                    })
                  ]
                }))
              }} style={{ marginLeft: 0, paddingLeft: 10 }}>
                <Body>
                  <Text>ID: {Institute.id}</Text>
                  <Text>Institute Name: {Institute.name_of_institution}</Text>
                  <Text>Type: {Institute.institution_type}</Text>
                  <Text>Established year: {Institute.established_year}</Text>
                </Body>
              </ListItem>
            ))}
          </List>}
      </ScrollView>
      </Content>
      <Fab active={true}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: 'orange' }}
        position="bottomRight"
        onPress={this.newBing.debounce()}>
        <Icon name="md-add" />
        <Button onPress={this.load}><Icon name="refresh" /></Button>
      </Fab>
    </Container>;
  }
}
