import { ScrollView, Text, ToastAndroid } from 'react-native'
import { Body, Button, Container, Fab, Icon, Item, List, ListItem, Right, ScrollableTab, Tab, Tabs, View } from "native-base";

import GestureRecognizer from 'react-native-swipe-gestures';
import Header from '../../designs/header';
import Label from "../../module/label";
import Location from '../../module/location'
import {
  NavigationActions
} from "react-navigation";
import React from "react";
import _ from "lodash";
import { remove } from '../../module/store';
import {get, file} from '../../module/store';

export default class Single extends React.Component {

  static navigationOptions = {
    drawerLockMode: "locked-closed",
    header: null
  }

  constructor(props) {
    super(props);
    this.state = { ...this.props.navigation.state.params.data };
    this.state.lang = 0;
    this.load = this.load.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.edits = this.edits.bind(this);
    this.adds = this.adds.bind(this);
  }

  componentDidMount() {
    this.load();
  }

  async load() {
    let data = await get() || [];
    ToastAndroid.show('Refreshing...', ToastAndroid.SHORT);
    let institute = _.find(data, { belongsTo: 'institute', index: this.state.index });
    let groups = _.filter(data,{belongsTo:'group',institute_id: this.state.id || this.state.index});
    let downloadedGroups =await file('group');
    let specifics = (this.state.id || this.state.index )? downloadedGroups.filter(g=>(this.state.id && g.institute_id == this.state.id) || g.institute_id == this.state.index).map(g=>({...g,...g.details})):[];
    groups = groups.concat(specifics);
    this.setState({...institute, groups});
  }

  edits() {
    this.props.navigation.navigate("NewInstitute", { state: this.state })
  }

  adds(){
    this.props.navigation.navigate("NewGroupOfInstitute",{state:{institute_index: this.state.index, institute_id: this.state.id}})
  }

  render() {
    return <Container>
      <Header hasTabs title={'Institute Detail'} alertOnRight onRight={this.state.uploaded ? undefined : () => {
        remove(this.state.index);
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'InstituteList' }),
          ]
        }));
      }} {...this.props} />
      <Tabs renderTabBar={() => <ScrollableTab style={{ backgroundColor: "orange" }} />}>
        <Tab heading="Description">
          <ScrollView>
            <List>
              <ListItem><Text>ID : {this.state.id}</Text></ListItem>
              <ListItem><Text>{Label.institution_type[this.state.lang]} : {_.capitalize(this.state.institution_type)}</Text></ListItem>
              <ListItem><Text>{Label.name_of_institution[this.state.lang]} : {_.capitalize(this.state.name_of_institution)}</Text></ListItem>
              <ListItem><Text>{Label.established_year[this.state.lang]} : {this.state.established_year}</Text></ListItem>
              {this.state.institution_type == 'School' && <List>
                <ListItem><Text>{Label.name_of_resource_center[this.state.lang]} : {this.state.name_of_resource_center}</Text></ListItem>
                <ListItem><Text>{Label.does_school_have_non_structural_mitigation_measures[this.state.lang]} : {this.state.does_school_have_non_structural_mitigation_measures}</Text></ListItem>
              </List>}
              {this.state.institution_type == 'Reading Camp' && <List>
                <ListItem><Text>{Label.settlement_name_for_reading_camp[this.state.lang]} : {this.state.settlement_name_for_reading_camp}</Text></ListItem>
                <ListItem><Text>{Label.name_of_the_facilitator[this.state.lang]} : {this.state.name_of_the_facilitator}</Text></ListItem>
                <ListItem><Text>{Label.contact_detail_of_the_facilitator[this.state.lang]} : {this.state.contact_detail_of_the_facilitator}</Text></ListItem>
                <ListItem><Text>{Label.institute_id[this.state.lang]} : {this.state.institute_id}</Text></ListItem>
              </List>}
              {this.state.institution_type == 'Child Protection and Promotion Committee' && <List>
                <ListItem><Text>{Label.chair_person_of_vcppc[this.state.lang]} : {this.state.chair_person_of_vcppc}</Text></ListItem>
                <ListItem><Text>{Label.contact_of_chair_person[this.state.lang]} : {this.state.contact_of_chair_person}</Text></ListItem>
              </List>}
              {this.state.institution_type == 'Local Disaster Management Committee' && <List>
                <ListItem><Text>{Label.chair_person_of_committee[this.state.lang]} : {this.state.chair_person_of_committee}</Text></ListItem>
                <ListItem><Text>{Label.contact_of_chair_person[this.state.lang]} : {this.state.contact_of_chair_person}</Text></ListItem>
              </List>}
            {this.state.institution_type == 'Health Post Management Committee' && <List>
              <ListItem key="number_of_health_staff_on_health_post"><Text>{Label.number_of_health_staff_on_health_post[this.state.lang]}: {this.state.number_of_health_staff_on_health_post}</Text></ListItem>
              <ListItem key="do_health_post_have_its_own_building"><Text>{Label.do_health_post_have_its_own_building[this.state.lang]}: {this.state.do_health_post_have_its_own_building}</Text></ListItem>
              <ListItem key="does_health_post_have_birthing_service"><Text>{Label.does_health_post_have_birthing_service[this.state.lang]}: {this.state.does_health_post_have_birthing_service}</Text></ListItem>
              <ListItem key="are_essential_birthing_equipment_is_in_place_for_birthing_service"><Text>{Label.are_essential_birthing_equipment_is_in_place_for_birthing_service[this.state.lang]}: {this.state.are_essential_birthing_equipment_is_in_place_for_birthing_service}</Text></ListItem>
              <ListItem key="is_sba_available_24_hour_in_health_facility"><Text>{Label.is_sba_available_24_hour_in_health_facility[this.state.lang]}: {this.state.is_sba_available_24_hour_in_health_facility}</Text></ListItem>
              <ListItem key="do_health_facility_have_toilet_and_water_supply"><Text>{Label.do_health_facility_have_toilet_and_water_supply[this.state.lang]}: {this.state.do_health_facility_have_toilet_and_water_supply}</Text></ListItem>
              <ListItem key="do_health_facility_have_all_medicine_as_per_the_government_provision"><Text>{Label.do_health_facility_have_all_medicine_as_per_the_government_provision[this.state.lang]}: {this.state.do_health_facility_have_all_medicine_as_per_the_government_provision}</Text></ListItem>
              <ListItem key="do_health_facility_have_health_facility_operation_management_committee"><Text>{Label.do_health_facility_have_health_facility_operation_management_committee[this.state.lang]}: {this.state.do_health_facility_have_health_facility_operation_management_committee}</Text></ListItem>
              <ListItem key="is_it_functional"><Text>{Label.is_it_functional[this.state.lang]}: {this.state.is_it_functional}</Text></ListItem>
              <ListItem key="how_frequently_meeting_is_conducted"><Text>{Label.how_frequently_meeting_is_conducted[this.state.lang]}: {this.state.how_frequently_meeting_is_conducted}</Text></ListItem>
              <ListItem key="what_is_the_population_size_of_its_catchment_area"><Text>{Label.what_is_the_population_size_of_its_catchment_area[this.state.lang]}: {this.state.what_is_the_population_size_of_its_catchment_area}</Text></ListItem>
              <ListItem key="what_is_the_average_number_of_opd_visit_per_day"><Text>{Label.what_is_the_average_number_of_opd_visit_per_day[this.state.lang]}: {this.state.what_is_the_average_number_of_opd_visit_per_day}</Text></ListItem>
            </List>}
            {this.state.institution_type == 'PHC OCR' && <List>
              <ListItem key="date_when_it_runs"><Text>{Label.date_when_it_runs[this.state.lang]} : {this.state.date_when_it_runs}</Text></ListItem>
              <ListItem key="does_it_have_it_owns_building"><Text>{Label.does_it_have_it_owns_building[this.state.lang]} : {this.state.does_it_have_it_owns_building}</Text></ListItem>
              <ListItem key="does_it_have_essential_furniture_and_equipment_in_place"><Text>{Label.does_it_have_essential_furniture_and_equipment_in_place[this.state.lang]} : {this.state.does_it_have_essential_furniture_and_equipment_in_place}</Text></ListItem>
              <ListItem key="does_it_have_toilet_and_water_supply"><Text>{Label.does_it_have_toilet_and_water_supply[this.state.lang]} : {this.state.does_it_have_toilet_and_water_supply}</Text></ListItem>
              <ListItem key="does_have_health_facility_operation_management_committee"><Text>{Label.does_have_health_facility_operation_management_committee[this.state.lang]} : {this.state.does_have_health_facility_operation_management_committee}</Text></ListItem>
              <ListItem key="is_it_functional"><Text>{Label.is_it_functional[this.state.lang]} : {this.state.is_it_functional}</Text></ListItem>
              <ListItem key="how_frequently_meeting_is_conducted"><Text>{Label.how_frequently_meeting_is_conducted[this.state.lang]} : {this.state.how_frequently_meeting_is_conducted}</Text></ListItem>
              <ListItem key="what_is_the_number_of_population_served_by_it_in_an_month_average"><Text>{Label.what_is_the_number_of_population_served_by_it_in_an_month_average[this.state.lang]} : {this.state.what_is_the_number_of_population_served_by_it_in_an_month_average}</Text></ListItem>
              <ListItem key="does_have_all_medicine_as_per_the_government_provision"><Text>{Label.does_have_all_medicine_as_per_the_government_provision[this.state.lang]} : {this.state.does_have_all_medicine_as_per_the_government_provision}</Text></ListItem>
            </List>}
            </List>
          </ScrollView>
        </Tab>
        {this.state.institution_type == 'School' && <Tab heading="General">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.head_teacher_name[this.state.lang]} : {this.state.head_teacher_name}</Text></ListItem>
              <ListItem><Text>{Label.head_teacher_contact[this.state.lang]} : {this.state.head_teacher_contact}</Text></ListItem>
              <ListItem><Text>{Label.type_of_school[this.state.lang]} : {this.state.type_of_school}</Text></ListItem>
              <ListItem><Text>{Label.maximum_grade_available[this.state.lang]} : {this.state.maximum_grade_available}</Text></ListItem>
              <ListItem><Text>{Label.are_there_any_organizations_working_directly[this.state.lang]} : {this.state.are_there_any_organizations_working_directly}</Text></ListItem>
              <ListItem><Text>{Label.major_ingo_name[this.state.lang]} : {this.state.major_ingo_name}</Text></ListItem>
              <ListItem><Text>{Label.software_or_hardware_components[this.state.lang]} : {this.state.software_or_hardware_components}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Child Protection and Promotion Committee' && <Tab heading="General">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.have_organizations_working_directly[this.state.lang]} : {this.state.have_organizations_working_directly}</Text></ListItem>
              <ListItem><Text>{Label.have_software_or_hardware_components[this.state.lang]} : {this.state.have_software_or_hardware_components}</Text></ListItem>
              <ListItem><Text>{Label.what_are_the_trainings_received[this.state.lang]} : {this.state.what_are_the_trainings_received}</Text></ListItem>
              <ListItem><Text>{Label.other_gvb_issues_prevailing_issues_in_that_area[this.state.lang]} : {this.state.other_gvb_issues_prevailing_issues_in_that_area}</Text></ListItem>
            </List>
          </ScrollView></Tab>}
        {this.state.institution_type == 'Local Disaster Management Committee' && <Tab heading="General">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.have_organizations_working_directly[this.state.lang]} : {this.state.have_organizations_working_directly}</Text></ListItem>
              <ListItem><Text>{Label.have_software_or_hardware_components[this.state.lang]} : {this.state.have_software_or_hardware_components}</Text></ListItem>
              <ListItem><Text>{Label.what_are_the_trainings_received[this.state.lang]} : {this.state.what_are_the_trainings_received}</Text></ListItem>
              <ListItem><Text>{Label.other_gvb_issues_prevailing_issues_in_that_area[this.state.lang]} : {this.state.other_gvb_issues_prevailing_issues_in_that_area}</Text></ListItem>
              <ListItem><Text>{Label.do_the_committee_have_yearly_plan[this.state.lang]} : {this.state.do_the_committee_have_yearly_plan}</Text></ListItem>
              <ListItem><Text>{Label.have_a_separate_office_for_committee[this.state.lang]} : {this.state.have_a_separate_office_for_committee}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        <Tab heading="Location">
          <ScrollView>
            <Location display data={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} />
          </ScrollView>
        </Tab>
        {this.state.institution_type == 'Child Protection and Promotion Committee' && <Tab heading="Number">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.number_of_children_girls[this.state.lang]} : {this.state.number_of_children_girls}</Text></ListItem>
              <ListItem><Text>{Label.number_of_children_boys[this.state.lang]} : {this.state.number_of_children_boys}</Text></ListItem>
              <ListItem><Text>{Label.number_vcppc_members_trained_in_creating_protective_environment_for_children_and_women[this.state.lang]} : {this.state.number_vcppc_members_trained_in_creating_protective_environment_for_children_and_women}</Text></ListItem>
              <ListItem><Text>{Label.number_of_training_received[this.state.lang]} : {this.state.number_of_training_received}</Text></ListItem>
              <ListItem><Text>{Label.number_of_meetings_coordinated_in_a_year[this.state.lang]} : {this.state.number_of_meetings_coordinated_in_a_year}</Text></ListItem>
            </List>
          </ScrollView></Tab>}
        {this.state.institution_type == 'Child Protection and Promotion Committee' && <Tab heading="Rate">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.child_marriage_rate[this.state.lang]} : {this.state.child_marriage_rate}</Text></ListItem>
              <ListItem><Text>{Label.child_labour_rate[this.state.lang]} : {this.state.child_labour_rate}</Text></ListItem>
              <ListItem><Text>{Label.percentage_out_of_school_children[this.state.lang]} : {this.state.percentage_out_of_school_children}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_children_with_disability[this.state.lang]} : {this.state.percentage_of_children_with_disability}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_linguistically_deprived_students[this.state.lang]} : {this.state.percentage_of_linguistically_deprived_students}</Text></ListItem>
            </List>
          </ScrollView></Tab>}
        {this.state.institution_type == 'Local Disaster Management Committee' && <Tab heading="Number">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.number_of_children_girls[this.state.lang]} : {this.state.number_of_children_girls}</Text></ListItem>
              <ListItem><Text>{Label.number_of_children_boys[this.state.lang]} : {this.state.number_of_children_boys}</Text></ListItem>
              <ListItem><Text>{Label.number_of_training_received[this.state.lang]} : {this.state.number_of_training_received}</Text></ListItem>
              <ListItem><Text>{Label.number_committee_members_trained_in_creating_protective_environment_for_children_and_women[this.state.lang]} : {this.state.number_committee_members_trained_in_creating_protective_environment_for_children_and_women}</Text></ListItem>
              <ListItem><Text>{Label.number_of_meetings_coordinated_in_a_year[this.state.lang]} : {this.state.number_of_meetings_coordinated_in_a_year}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Local Disaster Management Committee' && <Tab heading="Rate">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.child_marriage_rate[this.state.lang]} : {this.state.child_marriage_rate}</Text></ListItem>
              <ListItem><Text>{Label.child_labour_rate[this.state.lang]} : {this.state.child_labour_rate}</Text></ListItem>
              <ListItem><Text>{Label.percentage_out_of_school_children[this.state.lang]} : {this.state.percentage_out_of_school_children}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_children_with_disability[this.state.lang]} : {this.state.percentage_of_children_with_disability}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_linguistically_deprived_students[this.state.lang]} : {this.state.percentage_of_linguistically_deprived_students}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading='Rate'>
          <ScrollView>
            <List>
              <ListItem><Text>{Label.gross_intake_rate_in_grade_1[this.state.lang]} : {this.state.gross_intake_rate_in_grade_1}</Text></ListItem>
              <ListItem><Text>{Label.net_intake_rate_in_grade_1[this.state.lang]} : {this.state.net_intake_rate_in_grade_1}</Text></ListItem>
              <ListItem><Text>{Label.gross_enrollment_rate_in_grade_1_to_3[this.state.lang]} : {this.state.gross_enrollment_rate_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.gross_enrollment_rate_in_basic_4_to_8[this.state.lang]} : {this.state.gross_enrollment_rate_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.gross_enrollment_rate_in_secondary_9_to_12[this.state.lang]} : {this.state.gross_enrollment_rate_in_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.net_enrollment_rate_in_grade_1_to_3[this.state.lang]} : {this.state.net_enrollment_rate_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.net_enrollment_rate_in_basic_4_to_8[this.state.lang]} : {this.state.net_enrollment_rate_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.net_enrollment_rate_in_secondary_9_to_12[this.state.lang]} : {this.state.net_enrollment_rate_in_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.drop_out_rate_in_grade_1_to_3[this.state.lang]} : {this.state.drop_out_rate_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.drop_out_rate_in_basic_4_to_8[this.state.lang]} : {this.state.drop_out_rate_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.drop_out_rate_in_secondary_9_to_12[this.state.lang]} : {this.state.drop_out_rate_in_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.promotion_rate_in_grade_1_to_3[this.state.lang]} : {this.state.promotion_rate_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.promotion_rate_in_basic_4_to_8[this.state.lang]} : {this.state.promotion_rate_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.promotion_rate_in_secondary_9_to_12[this.state.lang]} : {this.state.promotion_rate_in_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.repetition_rate_in_grade_1_to_3[this.state.lang]} : {this.state.repetition_rate_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.repetition_rate_in_basic_4_to_8[this.state.lang]} : {this.state.repetition_rate_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.repetition_rate_in_secondary_9_to_12[this.state.lang]} : {this.state.repetition_rate_in_secondary_9_to_12}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading='Percentage'>
          <ScrollView>
            <List>
              <ListItem><Text>{Label.percentage_out_of_school_children_in_grade_1_to_3[this.state.lang]} : {this.state.percentage_out_of_school_children_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.percentage_out_of_school_children_in_basic_4_to_8[this.state.lang]} : {this.state.percentage_out_of_school_children_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.percentage_out_of_school_children_in_secondary_9_to_12[this.state.lang]} : {this.state.percentage_out_of_school_children_in_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.percentage_children_with_disability_in_school_in_grade_1_to_3[this.state.lang]} : {this.state.percentage_children_with_disability_in_school_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.percentage_children_with_disability_in_school_in_basic_4_to_8[this.state.lang]} : {this.state.percentage_children_with_disability_in_school_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.percentage_children_with_disability_in_school_in_secondary_9_to_12[this.state.lang]} : {this.state.percentage_children_with_disability_in_school_in_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_linguistically_deprived_students_in_grade_1_to_3[this.state.lang]} : {this.state.percentage_of_linguistically_deprived_students_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_linguistically_deprived_students_in_basic_4_to_8[this.state.lang]} : {this.state.percentage_of_linguistically_deprived_students_in_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.percentage_of_linguistically_deprived_students_in_secondary_9_to_12[this.state.lang]} : {this.state.percentage_of_linguistically_deprived_students_in_secondary_9_to_12}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Count">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.permanent_teachers_male[this.state.lang]} : {this.state.permanent_teachers_male}</Text></ListItem>
              <ListItem><Text>{Label.permanent_teachers_female[this.state.lang]} : {this.state.permanent_teachers_female}</Text></ListItem>
              <ListItem><Text>{Label.temporary_teachers_male[this.state.lang]} : {this.state.temporary_teachers_male}</Text></ListItem>
              <ListItem><Text>{Label.temporary_teachers_female[this.state.lang]} : {this.state.temporary_teachers_female}</Text></ListItem>
              <ListItem><Text>{Label.teachers_hired_through_private_funding_male[this.state.lang]} : {this.state.teachers_hired_through_private_funding_male}</Text></ListItem>
              <ListItem><Text>{Label.teachers_hired_through_private_funding_female[this.state.lang]} : {this.state.teachers_hired_through_private_funding_female}</Text></ListItem>
              <ListItem><Text>{Label.students_teacher_ratio_grade_1_to_3[this.state.lang]} : {this.state.students_teacher_ratio_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.students_teacher_ratio_basic_4_to_8[this.state.lang]} : {this.state.students_teacher_ratio_basic_4_to_8}</Text></ListItem>
              <ListItem><Text>{Label.students_teacher_ratio_secondary_9_to_12[this.state.lang]} : {this.state.students_teacher_ratio_secondary_9_to_12}</Text></ListItem>
              <ListItem><Text>{Label.number_of_teacher_trained_in_lb_or_early_grade_reading_methods[this.state.lang]} : {this.state.number_of_teacher_trained_in_lb_or_early_grade_reading_methods}</Text></ListItem>
              <ListItem><Text>{Label.number_of_teacher_trained_in_creating_print_rich_environment[this.state.lang]} : {this.state.number_of_teacher_trained_in_creating_print_rich_environment}</Text></ListItem>
              <ListItem><Text>{Label.number_of_teacher_trained_on_tpd[this.state.lang]} : {this.state.number_of_teacher_trained_on_tpd}</Text></ListItem>
              <ListItem><Text>{Label.learning_achievement_rate_of_nepali_subject_in_grade_3[this.state.lang]} : {this.state.learning_achievement_rate_of_nepali_subject_in_grade_3}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Info">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.is_there_reading_assessment_system_to_measure_the_reading_level[this.state.lang]} : {this.state.is_there_reading_assessment_system_to_measure_the_reading_level}</Text></ListItem>
              <ListItem><Text>{Label.have_implemented_egrp_in_coordination_with_guidance_of_deo[this.state.lang]} : {this.state.have_implemented_egrp_in_coordination_with_guidance_of_deo}</Text></ListItem>
              <ListItem><Text>{Label.have_followed_continuous_assessment_system[this.state.lang]} : {this.state.have_followed_continuous_assessment_system}</Text></ListItem>
              <ListItem><Text>{Label.have_updated_school_improvement_plan[this.state.lang]} : {this.state.have_updated_school_improvement_plan}</Text></ListItem>
              <ListItem><Text>{Label.have_book_cornors_in_grade_1_to_3[this.state.lang]} : {this.state.have_book_cornors_in_grade_1_to_3}</Text></ListItem>
              <ListItem><Text>{Label.have_separate_library[this.state.lang]} : {this.state.have_separate_library}</Text></ListItem>
              <ListItem><Text>{Label.is_there_enough_space_for_the_children_grade_1_to_3_in_class[this.state.lang]} : {this.state.is_there_enough_space_for_the_children_grade_1_to_3_in_class}</Text></ListItem>
              <ListItem><Text>{Label.is_there_sufficient_furniture_in_classroom_for_students[this.state.lang]} : {this.state.is_there_sufficient_furniture_in_classroom_for_students}</Text></ListItem>
              <ListItem><Text>{Label.medium_of_instruction[this.state.lang]} : {this.state.medium_of_instruction}</Text></ListItem>
              <ListItem><Text>{Label.number_of_classrooms_with_child_friendly_sitting_arrangement[this.state.lang]} : {this.state.number_of_classrooms_with_child_friendly_sitting_arrangement}</Text></ListItem>
              <ListItem><Text>{Label.number_of_reading_clubs_or_camps_established_in_the_school_catchment_area[this.state.lang]} : {this.state.number_of_reading_clubs_or_camps_established_in_the_school_catchment_area}</Text></ListItem>
              <ListItem><Text>{Label.have_organized_parental_education_or_parental_meeting_frequently[this.state.lang]} : {this.state.have_organized_parental_education_or_parental_meeting_frequently}</Text></ListItem>
              <ListItem><Text>{Label.have_received_text_book_by_3rd_week_of_baisakh[this.state.lang]} : {this.state.have_received_text_book_by_3rd_week_of_baisakh}</Text></ListItem>
              <ListItem><Text>{Label.is_there_provision_of_ict_based_education[this.state.lang]} : {this.state.is_there_provision_of_ict_based_education}</Text></ListItem>
              <ListItem><Text>{Label.is_there_power_supply_in_school[this.state.lang]} : {this.state.is_there_power_supply_in_school}</Text></ListItem>
              <ListItem><Text>{Label.electricity[this.state.lang]} : {this.state.electricity}</Text></ListItem>
              <ListItem><Text>{Label.solar[this.state.lang]} : {this.state.solar}</Text></ListItem>
              <ListItem><Text>{Label.rcc[this.state.lang]} : {this.state.rcc}</Text></ListItem>
              <ListItem><Text>{Label.steel_frame[this.state.lang]} : {this.state.steel_frame}</Text></ListItem>
              <ListItem><Text>{Label.load_bearing_wall[this.state.lang]} : {this.state.load_bearing_wall}</Text></ListItem>
              <ListItem><Text>{Label.temporary_structures[this.state.lang]} : {this.state.temporary_structures}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Number">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.total_number_of_block_in_school[this.state.lang]} : {this.state.total_number_of_block_in_school}</Text></ListItem>
              <ListItem><Text>{Label.total_number_of_block_used_for_classroom[this.state.lang]} : {this.state.total_number_of_block_used_for_classroom}</Text></ListItem>
              <ListItem><Text>{Label.number_of_earthquake_resilient_block[this.state.lang]} : {this.state.number_of_earthquake_resilient_block}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_with_child_friendly_features[this.state.lang]} : {this.state.number_of_blocks_with_child_friendly_features}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_with_disable_friendly_features[this.state.lang]} : {this.state.number_of_blocks_with_disable_friendly_features}</Text></ListItem>
              <ListItem><Text>{Label.is_school_safe_from_multiple_hazards[this.state.lang]} : {this.state.is_school_safe_from_multiple_hazards}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_flood[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_flood}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_earthquake[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_earthquake}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_fire[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_fire}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Have/Have not">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.have_early_warning_system_in_the_place[this.state.lang]} : {this.state.have_early_warning_system_in_the_place}</Text></ListItem>
              <ListItem><Text>{Label.have_availability_of_safe_and_sufficient_drinking_water_facilities[this.state.lang]} : {this.state.have_availability_of_safe_and_sufficient_drinking_water_facilities}</Text></ListItem>
              <ListItem><Text>{Label.have_separate_toilet_for_girls[this.state.lang]} : {this.state.have_separate_toilet_for_girls}</Text></ListItem>
              <ListItem><Text>{Label.have_ramp_in_toilet_for_disable_children[this.state.lang]} : {this.state.have_ramp_in_toilet_for_disable_children}</Text></ListItem>
              <ListItem><Text>{Label.have_safe_fencing_around_school_premises[this.state.lang]} : {this.state.have_safe_fencing_around_school_premises}</Text></ListItem>
              <ListItem><Text>{Label.have_safe_play_ground_available[this.state.lang]} : {this.state.have_safe_play_ground_available}</Text></ListItem>
              <ListItem><Text>{Label.have_functional_school_based_disaster_risk_management_committee[this.state.lang]} : {this.state.have_functional_school_based_disaster_risk_management_committee}</Text></ListItem>
              <ListItem><Text>{Label.have_updated_school_based_disaster_risk_management_plan[this.state.lang]} : {this.state.have_updated_school_based_disaster_risk_management_plan}</Text></ListItem>
              <ListItem><Text>{Label.have_drr_education_materials[this.state.lang]} : {this.state.have_drr_education_materials}</Text></ListItem>
              <ListItem><Text>{Label.have_organized_health_education_on_menstrual_hygiene_practices[this.state.lang]} : {this.state.have_organized_health_education_on_menstrual_hygiene_practices}</Text></ListItem>
              <ListItem><Text>{Label.have_provided_first_aid_box_to_student[this.state.lang]} : {this.state.have_provided_first_aid_box_to_student}</Text></ListItem>
              <ListItem><Text>{Label.have_waste_bin_in_identified_place_and_used_properly[this.state.lang]} : {this.state.have_waste_bin_in_identified_place_and_used_properly}</Text></ListItem>
              <ListItem><Text>{Label.have_functional_SMC[this.state.lang]} : {this.state.have_functional_SMC}</Text></ListItem>
              <ListItem><Text>{Label.have_organized_social_audit_annually[this.state.lang]} : {this.state.have_organized_social_audit_annually}</Text></ListItem>
              <ListItem><Text>{Label.have_annual_work_plan_or_have_developed_the_academic_calendar[this.state.lang]} : {this.state.have_annual_work_plan_or_have_developed_the_academic_calendar}</Text></ListItem>
              <ListItem><Text>{Label.have_annual_budget_management_plan_in_place_and_updated_regularly[this.state.lang]} : {this.state.have_annual_budget_management_plan_in_place_and_updated_regularly}</Text></ListItem>
              <ListItem><Text>{Label.does_school_have_internal_revenue[this.state.lang]} : {this.state.does_school_have_internal_revenue}</Text></ListItem>
              <ListItem><Text>{Label.does_school_have_child_club[this.state.lang]} : {this.state.does_school_have_child_club}</Text></ListItem>
              <ListItem><Text>{Label.is_this_school_in_merger_strategy[this.state.lang]} : {this.state.is_this_school_in_merger_strategy}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Reading Camp' && <Tab heading="Have/Have not">
          <ScrollView><List>
            <ListItem><Text>{Label.have_book_bank[this.state.lang]} : {this.state.have_book_bank}</Text></ListItem>
            <ListItem><Text>{Label.is_there_enough_space_for_the_children[this.state.lang]} : {this.state.is_there_enough_space_for_the_children}</Text></ListItem>
            <ListItem><Text>{Label.does_organize_parental_education_or_parental_meeting_frequently[this.state.lang]} : {this.state.does_organize_parental_education_or_parental_meeting_frequently}</Text></ListItem>
          </List>
          </ScrollView>
        </Tab>}
        {(this.state.institution_type == 'Reading Camp' || this.state.institution_type == 'School') && <Tab heading="Enrolled">
          <ScrollView>
            <List>
              {this.state.institution_type == 'School' && <List>
                <ListItem><Text>{Label.number_of_child_club_members_girls[this.state.lang]} : {this.state.number_of_child_club_members_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_child_club_members_boys[this.state.lang]} : {this.state.number_of_child_club_members_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_ecd_girls[this.state.lang]} : {this.state.number_of_children_enrolled_ecd_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_ecd_boys[this.state.lang]} : {this.state.number_of_children_enrolled_ecd_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_1_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_1_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_1_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_1_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_2_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_2_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_2_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_2_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_3_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_3_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_3_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_3_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_4_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_4_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_4_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_4_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_5_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_5_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_5_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_5_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_6_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_6_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_6_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_6_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_7_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_7_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_7_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_7_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_8_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_8_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_8_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_8_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_9_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_9_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_9_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_9_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_10_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_10_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_10_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_10_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_11_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_11_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_11_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_11_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_12_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_12_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_12_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_12_boys}</Text></ListItem>
              </List>}
              {this.state.institution_type == 'Reading Camp' && <List>
                <ListItem><Text>{Label.number_of_children_enrolled_ecd_girls[this.state.lang]} : {this.state.number_of_children_enrolled_ecd_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_ecd_boys[this.state.lang]} : {this.state.number_of_children_enrolled_ecd_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_1_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_1_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_1_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_1_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_2_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_2_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_2_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_2_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_3_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_3_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_3_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_3_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_4_girls[this.state.lang]} : {this.state.number_of_children_enrolled_grade_4_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_children_enrolled_grade_4_boys[this.state.lang]} : {this.state.number_of_children_enrolled_grade_4_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_ecd_girls[this.state.lang]} : {this.state.number_of_registered_children_ecd_girls}</Text></ListItem>
              </List>}
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Minority">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.children_from_minority_ecd_girls[this.state.lang]} : {this.state.children_from_minority_ecd_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_ecd_boys[this.state.lang]} : {this.state.children_from_minority_ecd_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_1_girls[this.state.lang]} : {this.state.children_from_minority_grade_1_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_1_boys[this.state.lang]} : {this.state.children_from_minority_grade_1_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_2_girls[this.state.lang]} : {this.state.children_from_minority_grade_2_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_2_boys[this.state.lang]} : {this.state.children_from_minority_grade_2_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_3_girls[this.state.lang]} : {this.state.children_from_minority_grade_3_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_3_boys[this.state.lang]} : {this.state.children_from_minority_grade_3_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_4_girls[this.state.lang]} : {this.state.children_from_minority_grade_4_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_4_boys[this.state.lang]} : {this.state.children_from_minority_grade_4_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_5_girls[this.state.lang]} : {this.state.children_from_minority_grade_5_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_5_boys[this.state.lang]} : {this.state.children_from_minority_grade_5_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_6_girls[this.state.lang]} : {this.state.children_from_minority_grade_6_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_6_boys[this.state.lang]} : {this.state.children_from_minority_grade_6_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_7_girls[this.state.lang]} : {this.state.children_from_minority_grade_7_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_7_boys[this.state.lang]} : {this.state.children_from_minority_grade_7_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_8_girls[this.state.lang]} : {this.state.children_from_minority_grade_8_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_8_boys[this.state.lang]} : {this.state.children_from_minority_grade_8_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_9_girls[this.state.lang]} : {this.state.children_from_minority_grade_9_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_9_boys[this.state.lang]} : {this.state.children_from_minority_grade_9_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_10_girls[this.state.lang]} : {this.state.children_from_minority_grade_10_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_10_boys[this.state.lang]} : {this.state.children_from_minority_grade_10_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_11_girls[this.state.lang]} : {this.state.children_from_minority_grade_11_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_11_boys[this.state.lang]} : {this.state.children_from_minority_grade_11_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_12_girls[this.state.lang]} : {this.state.children_from_minority_grade_12_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_from_minority_grade_12_boys[this.state.lang]} : {this.state.children_from_minority_grade_12_boys}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Disability">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.children_with_disability_ecd_girls[this.state.lang]} : {this.state.children_with_disability_ecd_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_ecd_boys[this.state.lang]} : {this.state.children_with_disability_ecd_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_1_girls[this.state.lang]} : {this.state.children_with_disability_grade_1_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_1_boys[this.state.lang]} : {this.state.children_with_disability_grade_1_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_2_girls[this.state.lang]} : {this.state.children_with_disability_grade_2_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_2_boys[this.state.lang]} : {this.state.children_with_disability_grade_2_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_3_girls[this.state.lang]} : {this.state.children_with_disability_grade_3_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_3_boys[this.state.lang]} : {this.state.children_with_disability_grade_3_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_4_girls[this.state.lang]} : {this.state.children_with_disability_grade_4_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_4_boys[this.state.lang]} : {this.state.children_with_disability_grade_4_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_5_girls[this.state.lang]} : {this.state.children_with_disability_grade_5_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_5_boys[this.state.lang]} : {this.state.children_with_disability_grade_5_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_6_girls[this.state.lang]} : {this.state.children_with_disability_grade_6_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_6_boys[this.state.lang]} : {this.state.children_with_disability_grade_6_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_7_girls[this.state.lang]} : {this.state.children_with_disability_grade_7_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_7_boys[this.state.lang]} : {this.state.children_with_disability_grade_7_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_8_girls[this.state.lang]} : {this.state.children_with_disability_grade_8_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_8_boys[this.state.lang]} : {this.state.children_with_disability_grade_8_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_9_girls[this.state.lang]} : {this.state.children_with_disability_grade_9_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_9_boys[this.state.lang]} : {this.state.children_with_disability_grade_9_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_10_girls[this.state.lang]} : {this.state.children_with_disability_grade_10_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_10_boys[this.state.lang]} : {this.state.children_with_disability_grade_10_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_11_girls[this.state.lang]} : {this.state.children_with_disability_grade_11_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_11_boys[this.state.lang]} : {this.state.children_with_disability_grade_11_boys}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_12_girls[this.state.lang]} : {this.state.children_with_disability_grade_12_girls}</Text></ListItem>
              <ListItem><Text>{Label.children_with_disability_grade_12_boys[this.state.lang]} : {this.state.children_with_disability_grade_12_boys}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {(this.state.institution_type == 'School' || this.state.institution_type == 'Reading Camp') && <Tab heading="Registered">
          <ScrollView>
            <List>
              {this.state.institution_type == 'School' && <List>
                <ListItem><Text>{Label.number_of_registered_children_ecd_girls[this.state.lang]} : {this.state.number_of_registered_children_ecd_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_ecd_boys[this.state.lang]} : {this.state.number_of_registered_children_ecd_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_1_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_1_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_1_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_1_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_2_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_2_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_2_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_2_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_3_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_3_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_3_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_3_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_4_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_4_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_4_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_4_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_5_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_5_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_5_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_5_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_6_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_6_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_6_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_6_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_7_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_7_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_7_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_7_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_8_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_8_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_8_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_8_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_9_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_9_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_9_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_9_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_10_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_10_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_10_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_10_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_11_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_11_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_11_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_11_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_12_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_12_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_12_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_12_boys}</Text></ListItem>
              </List>}
              {this.state.institution_type == 'Reading Camp' && <List>
                <ListItem><Text>{Label.number_of_registered_children_ecd_boys[this.state.lang]} : {this.state.number_of_registered_children_ecd_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_1_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_1_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_1_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_1_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_2_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_2_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_2_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_2_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_3_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_3_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_3_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_3_boys}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_4_girls[this.state.lang]} : {this.state.number_of_registered_children_grade_4_girls}</Text></ListItem>
                <ListItem><Text>{Label.number_of_registered_children_grade_4_boys[this.state.lang]} : {this.state.number_of_registered_children_grade_4_boys}</Text></ListItem>
              </List>}
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Reading Camp' && <Tab heading="Vulnerable">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.vulnerable_disabled_girls[this.state.lang]} : {this.state.vulnerable_disabled_girls}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_disabled_boys[this.state.lang]} : {this.state.vulnerable_disabled_boys}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_ultra_poor_girls[this.state.lang]} : {this.state.vulnerable_ultra_poor_girls}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_ultra_poor_boys[this.state.lang]} : {this.state.vulnerable_ultra_poor_boys}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_orphan_girls[this.state.lang]} : {this.state.vulnerable_orphan_girls}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_orphan_boys[this.state.lang]} : {this.state.vulnerable_orphan_boys}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_out_of_school_children_girls[this.state.lang]} : {this.state.vulnerable_out_of_school_children_girls}</Text></ListItem>
              <ListItem><Text>{Label.vulnerable_out_of_school_children_boys[this.state.lang]} : {this.state.vulnerable_out_of_school_children_boys}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Reading Camp' && <Tab heading="Caste">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.caste_brahmanchettri_sanyasi_girls[this.state.lang]} : {this.state.caste_brahmanchettri_sanyasi_girls}</Text></ListItem>
              <ListItem><Text>{Label.caste_brahmanchettri_sanyasi_boys[this.state.lang]} : {this.state.caste_brahmanchettri_sanyasi_boys}</Text></ListItem>
              <ListItem><Text>{Label.caste_janajati_girls[this.state.lang]} : {this.state.caste_janajati_girls}</Text></ListItem>
              <ListItem><Text>{Label.caste_janajati_boys[this.state.lang]} : {this.state.caste_janajati_boys}</Text></ListItem>
              <ListItem><Text>{Label.caste_dalit_girls[this.state.lang]} : {this.state.caste_dalit_girls}</Text></ListItem>
              <ListItem><Text>{Label.caste_dalit_boys[this.state.lang]} : {this.state.caste_dalit_boys}</Text></ListItem>
              <ListItem><Text>{Label.caste_muslim_girls[this.state.lang]} : {this.state.caste_muslim_girls}</Text></ListItem>
              <ListItem><Text>{Label.caste_muslim_boys[this.state.lang]} : {this.state.caste_muslim_boys}</Text></ListItem>
              <ListItem><Text>{Label.caste_madhesi_girls[this.state.lang]} : {this.state.caste_madhesi_girls}</Text></ListItem>
              <ListItem><Text>{Label.caste_madhesi_boys[this.state.lang]} : {this.state.caste_madhesi_boys}</Text></ListItem>
              <ListItem><Text>{Label.caste_others_girls[this.state.lang]} : {this.state.caste_others_girls}</Text></ListItem>
              <ListItem><Text>{Label.caste_others_boys[this.state.lang]} : {this.state.caste_others_boys}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'School' && <Tab heading="Safety">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.is_reading_camp_safe_from_multiple_hazards[this.state.lang]} : {this.state.is_reading_camp_safe_from_multiple_hazards}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_flood[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_flood}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_landslides[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_landslides}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_earthquake[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_earthquake}</Text></ListItem>
              <ListItem><Text>{Label.number_of_blocks_vulnerable_from_fire[this.state.lang]} : {this.state.number_of_blocks_vulnerable_from_fire}</Text></ListItem>
              <ListItem><Text>{Label.have_safe_and_sufficient_drinking_water_facilities[this.state.lang]} : {this.state.have_safe_and_sufficient_drinking_water_facilities}</Text></ListItem>
              <ListItem><Text>{Label.have_hand_washing_facilities[this.state.lang]} : {this.state.have_hand_washing_facilities}</Text></ListItem>
              <ListItem><Text>{Label.number_of_toilets_in_proportion_to_number_of_students[this.state.lang]} : {this.state.number_of_toilets_in_proportion_to_number_of_students}</Text></ListItem>
              <ListItem><Text>{Label.have_ramp_in_toilet_for_disable_children[this.state.lang]} : {this.state.have_ramp_in_toilet_for_disable_children}</Text></ListItem>
              <ListItem><Text>{Label.have_safe_fencing_around_reading_camp_premises[this.state.lang]} : {this.state.have_safe_fencing_around_reading_camp_premises}</Text></ListItem>
              <ListItem><Text>{Label.have_safe_play_ground_available[this.state.lang]} : {this.state.have_safe_play_ground_available}</Text></ListItem>
              <ListItem><Text>{Label.have_functional_reading_camp_management_committee[this.state.lang]} : {this.state.have_functional_reading_camp_management_committee}</Text></ListItem>
              <ListItem><Text>{Label.maximum_walking_distance_for_the_children_to_reading_camp[this.state.lang]} : {this.state.maximum_walking_distance_for_the_children_to_reading_camp}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Local Disaster Management Committee' && <Tab heading="Case">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.do_the_committee_have_the_case_registration_system[this.state.lang]} : {this.state.do_the_committee_have_the_case_registration_system}</Text></ListItem>
              <ListItem><Text>{Label.number_of_cases_referred_by_committee[this.state.lang]} : {this.state.number_of_cases_referred_by_committee}</Text></ListItem>
              <ListItem><Text>{Label.who_are_the_informers_on_the_cases_to_the_committee[this.state.lang]} : {this.state.who_are_the_informers_on_the_cases_to_the_committee}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Local Disaster Management Committee' && <Tab heading="Funding">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.do_they_have_the_emergency_fund[this.state.lang]} : {this.state.do_they_have_the_emergency_fund}</Text></ListItem>
              <ListItem><Text>{Label.do_the_committee_get_support_from_dcwb[this.state.lang]} : {this.state.do_the_committee_get_support_from_dcwb}</Text></ListItem>
              <ListItem><Text>{Label.do_the_committee_get_support_from_vdc[this.state.lang]} : {this.state.do_the_committee_get_support_from_vdc}</Text></ListItem>
              <ListItem><Text>{Label.do_the_committee_get_support_from_committee[this.state.lang]} : {this.state.do_the_committee_get_support_from_committee}</Text></ListItem>
              <ListItem><Text>{Label.do_the_committee_have_the_funding[this.state.lang]} : {this.state.do_the_committee_have_the_funding}</Text></ListItem>
              <ListItem><Text>{Label.how_was_the_funding_received_and_from_whom[this.state.lang]} : {this.state.how_was_the_funding_received_and_from_whom}</Text></ListItem>
              <ListItem><Text>{Label.how_much_funding_was_received[this.state.lang]} : {this.state.how_much_funding_was_received}</Text></ListItem>
              <ListItem><Text>{Label.what_do_the_committee_do_with_the_funding_they_receive[this.state.lang]} : {this.state.what_do_the_committee_do_with_the_funding_they_receive}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Child Protection and Promotion Committee' && <Tab heading="Case">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.do_the_vcppc_have_the_case_registration_system[this.state.lang]} : {this.state.do_the_vcppc_have_the_case_registration_system}</Text></ListItem>
              <ListItem><Text>{Label.number_of_cases_referred_by_vcppc[this.state.lang]} : {this.state.number_of_cases_referred_by_vcppc}</Text></ListItem>
              <ListItem><Text>{Label.who_are_the_informers_on_the_cases_to_the_vcppc[this.state.lang]} : {this.state.who_are_the_informers_on_the_cases_to_the_vcppc}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        {this.state.institution_type == 'Child Protection and Promotion Committee' && <Tab heading="Funding">
          <ScrollView>
            <List>
              <ListItem><Text>{Label.do_the_vcppc_have_yearly_plan[this.state.lang]} : {this.state.do_the_vcppc_have_yearly_plan}</Text></ListItem>
              <ListItem><Text>{Label.have_a_separate_office_for_VCPPC[this.state.lang]} : {this.state.have_a_separate_office_for_VCPPC}</Text></ListItem>
              <ListItem><Text>{Label.do_they_have_the_emergency_fund[this.state.lang]} : {this.state.do_they_have_the_emergency_fund}</Text></ListItem>
              <ListItem><Text>{Label.do_the_vcppc_get_support_from_dcwb[this.state.lang]} : {this.state.do_the_vcppc_get_support_from_dcwb}</Text></ListItem>
              <ListItem><Text>{Label.do_the_vcppc_get_support_from_vdc[this.state.lang]} : {this.state.do_the_vcppc_get_support_from_vdc}</Text></ListItem>
              <ListItem><Text>{Label.do_the_vcppc_get_support_from_committee[this.state.lang]} : {this.state.do_the_vcppc_get_support_from_committee}</Text></ListItem>
              <ListItem><Text>{Label.do_the_vcppc_have_the_funding[this.state.lang]} : {this.state.do_the_vcppc_have_the_funding}</Text></ListItem>
              <ListItem><Text>{Label.how_was_the_funding_received_and_from_whom[this.state.lang]} : {this.state.how_was_the_funding_received_and_from_whom}</Text></ListItem>
              <ListItem><Text>{Label.how_much_funding_was_received[this.state.lang]} : {this.state.how_much_funding_was_received}</Text></ListItem>
              <ListItem><Text>{Label.what_do_the_vcppc_do_with_the_funding_they_receive[this.state.lang]} : {this.state.what_do_the_vcppc_do_with_the_funding_they_receive}</Text></ListItem>
            </List>
          </ScrollView>
        </Tab>}
        <Tab heading='Groups'>
        <ScrollView>
          <List>
            {(this.state.groups||[]).map(g=><ListItem onPress={()=>this.props.navigation.navigate('SingleInstituteGroup',{data:g,newGroupMemberRoute:'NewInstituteGroupMember',
            NewInstituteGroupMember:'NewInstituteGroupMember',
            SingleInstituteGroupMember:'SingleInstituteGroupMember',
            editGroupRoute: 'NewInstituteGroup'})} key={g.index || g.id}><Text>{g.name_of_group}</Text></ListItem>)}
            <Button block warning onPress={()=>{
              this.props.navigation.navigate('NewInstituteGroup',{state:{institute_id: this.state.id || this.state.index, location: _.pick(this.state, ['state_id', 'district_id', 'municipality_id'])}})
            }}><Text>Add new group</Text></Button>
          </List>
        </ScrollView>
        </Tab>
      </Tabs>
      {(!this.state.uploaded && !this.state.id) && <Fab active={true}
        direction="up"
        style={{ backgroundColor: 'orange' }}
        position="bottomRight"
        onPress={this.edits.debounce()}>
        <Icon name="md-color-filter" />
        <Button onPress={this.load}>
          <Icon name='refresh' />
        </Button>
      </Fab>}
      {!(!this.state.uploaded && !this.state.id) && <Fab active={true}
        direction="up"
        style={{ backgroundColor: 'blue' }}
        position="bottomRight"
        onPress={this.load}>
        <Icon name="refresh" />
      </Fab>}
    </Container>
  }
};
