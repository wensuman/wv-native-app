import List from "./list";
import New from "./new";
import Single from './single';
import SingleInstituteGroup from '../group/single';
import NewInstituteGroup from '../group/new';
import SingleInstituteGroupMember from '../family/member/single';
import NewInstituteGroupMember from '../family/member/new';

import {
  StackNavigator
} from "react-navigation";

export default StackNavigator({
  InstituteList: {
    screen: List
  },
  NewInstitute: {
    screen: New
  },
  SingleInstitute: {
    screen: Single
  },
  SingleInstituteGroup:{
    screen:SingleInstituteGroup
  },
  NewInstituteGroup:{
    screen:NewInstituteGroup
  },
  SingleInstituteGroupMember:{
    screen:SingleInstituteGroupMember
  },
  NewInstituteGroupMember:{
    screen:NewInstituteGroupMember
  }
}, {
    headerMode: 'none',
    initialRouteName: 'InstituteList',
});