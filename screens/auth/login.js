import { AsyncStorage, Image, NetInfo, Text } from 'react-native';
import { Button, Container, Content, Form, Input, Item, Label, List, ListItem, View } from 'native-base';
import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';

import Axios from 'axios';
import Header from '../../designs/header';
import Spinner from 'react-native-loading-spinner-overlay';
import Url from '../../module/url';
import _ from 'lodash';

export default class Login extends Component {

    static navigationOptions = {
        header: null
    };

    state = {};

    dispatcher = {};

    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.forgot = this.forgot.bind(this);
        this.dispatcher = this.login;
    }

    async login() {
        this.setState({ logging: 'Loggin In' })
        Axios.post(Url.server_url + 'login', _.pick(this.state, ['email', 'password'])).then((response) => {
            let tok = {
                email: this.state.email,
                token: response.data.data.token,
                id: response.data.data.id
            };
            AsyncStorage.setItem('@wv-data:token', JSON.stringify(tok)), this.setState({ logging: '', ...tok })
            this.props.onSignIn();
        })
        .catch(error => { 
            alert('Login Failed.'),
            this.setState({ logging: '' })
        })
    }

    forgot(){
        Axios.post(Url.server_url + 'forgot-password', _.pick(this.state, ['email'])).then(function(){
            alert('Please check your email for new password');
        })
      
    }

    render() {
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Spinner visible={Boolean(this.state.logging)} />
                <View style={{ backgroundColor:'#fff',alignContent:'center', height: '70%',padding:8 }}>
                    <Image style={{resizeMode:'contain',width:'100%'}} source={require('../../icon.png')} />
                    <Form>
                        <Item style={{ marginLeft: 0, paddingLeft: 10 }} floatingLabel>
                            <Label>Email</Label>
                            <Input keyboardType="email-address" onChangeText={email => this.setState({ email })} />
                        </Item>
                        {!this.state.forgot && <Item style={{ marginLeft: 0, paddingLeft: 10 }} floatingLabel>
                            <Label>Password</Label>
                            <Input secureTextEntry onChangeText={password => this.setState({ password })} />
                        </Item>}
                        <Button onPress={this.dispatcher}  disabled={!this.state.email} style={{ marginTop: 4 }} full >
                            <Text style={{color:'white'}}>{this.state.forgot ? 'Request password reset' : 'Log In' }</Text>
                        </Button>
                    </Form>
                    <View style={{flex:1,flexDirection:'row', paddingTop:8, justifyContent:'center', alignContent:'flex-end'}}>
                        <Button onPress={()=>{this.dispatcher = !this.state.forgot ? this.forgot : this.login, this.setState({forgot: !this.state.forgot})}} transparent >
                            <Text style={{color:'orange'}}>{this.state.forgot ? 'Log In' : 'Forgot password' }</Text>
                        </Button>
                    </View>
                </View>
               
            </Container>
        );
    }
}