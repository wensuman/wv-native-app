import { Container, Content, Form, Input, Button, Text, Image, View, Item, Label } from 'native-base';
import React, { Component } from 'react';

import Header from '../../designs/header';
export default class Login extends Component {
    static navigationOptions = {
        header: null
    };
    
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Container style={{backgroundColor:'#fff'}}>
            <View style={{ backgroundColor:'#fff', justifyContent: 'center', height:'75%',padding:4 }}>
                <Image style={{resizeMode:'contain',width:'100%'}} source={require('../../icon.png')} />
                <Form>
                    <Item style={{ marginLeft: 0, paddingLeft: 10 }} floatingLabel>
                        <Label>Email</Label>
                        <Input keyboardType="email-address" onChangeText={email => this.setState({ email })} />
                    </Item>
                    <Button onPress={this.login} disabled={!(this.state.password && this.state.email)} style={{ marginTop: 4 }} full>
                        <Text>Reset</Text>
                    </Button>
                </Form>
            </View>
        </Container>
        );
    }
}