import { AsyncStorage, DeviceEventEmitter, Image, NetInfo, Text, Alert, BackHandler, ToastAndroid } from 'react-native';
import { Button, Container, Content, Form, Input, Item, Label, List, ListItem, View} from 'native-base';
import React, { Component } from 'react';

import Axios from 'axios';
import Header from '../../designs/header';
import Spinner from 'react-native-loading-spinner-overlay';
import Url from '../../module/url';
import _ from 'lodash';

export default class Logout extends Component {

    static navigationOptions = {
        header: null
    };

    state = {};

    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }
    
    async componentDidMount() {
        let token = await AsyncStorage.getItem('@wv-data:token')
        if (token) {
            this.setState(JSON.parse(token));
        }
    }

    async logout() {
        await NetInfo.isConnected.fetch().then(async (is_connected) => {
            is_connected && (await AsyncStorage.removeItem('@wv-data:token'),DeviceEventEmitter.emit('logged-out'));
            !is_connected && ToastAndroid.show('No internet connection available.', ToastAndroid.SHORT);
        });
        
    }

    render() {
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Spinner visible={Boolean(this.state.logging)} textContent={this.state.logging} />
                <View style={{ backgroundColor:'#fff', justifyContent: 'center', height:'75%',padding:4 }}>
                    {this.state.token && <List>
                        <ListItem style={{marginLeft:0, paddingLeft: 10}} header>
                            <Text>You are logged in.</Text>
                        </ListItem>
                        <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Text>Email: {this.state.email}</Text>
                        </ListItem>
                        <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Button onPress={this.logout} style={{ marginTop: 4, padding:10 }} warning full>
                                <Text>Log out</Text>
                            </Button>
                        </ListItem>
                    </List>}
                </View>
            </Container>
        );
    }
}