import Forgot from './forgot';
import Login from './login'
import {
    StackNavigator
} from "react-navigation";
export default StackNavigator({
    Login: {
        name: 'Login',
        screen: Login
    },
    Forgot: {
        name: 'Forgot',
        screen: Forgot
    }
})