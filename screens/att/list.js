import { Body, Button, Container, Content, Icon, List, ListItem, Right, View } from 'native-base';
import { ScrollView, Text, ToastAndroid, TouchableOpacity } from 'react-native';
import Header from '../../designs/header';
import {file} from '../../module/store';
import React from 'react';
import { Select } from '../../forms/input'
import _ from 'lodash'

export default class ATT extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            activities: [],
            projects: [],
            project_id: '',
            activity_id: ''
        }
        this.load = this.load.bind(this);
        this.loadProjects = this.loadProjects.bind(this);
    }

    componentDidMount() {
        this.load();
        this.loadProjects();
    }

    async load() {
        ToastAndroid.show('Refreshing...', ToastAndroid.SHORT);
        this.setState({ activities: _.values(await file('activities')).filter(a=>a.activities && a.activities.length > 0) });
    }

    async loadProjects() {
        this.setState({ projects: await file('project') });
    }

    activities(activity) {
        this.props.navigation.navigate('Activity', { state: activity, project_id: this.state.project_id });
    }

    render() {
        let content = <Text>It seems that there are no enrollments made in any activities.</Text>;
        if (this.state.activities.length > 0) {
            let project = _.find(this.state.activities, { project_id: this.state.project_id })
            content = <View>
                {this.state.projects && <Select placeholder="Select a project" selected={this.state.project_id} onChange={project_id => this.setState({ project_id })} data={this.state.projects.filter(p=>_.map(this.state.activities,'project_id').includes(p.id)).map((project) => ({ label: project.name, value: project.id }))} />}
                {project && <List>
                    {project.activities.map(activity => <ListItem onPress={_.debounce(this.activities.bind(this, activity), 500)} style={{ marginLeft: 0, paddingLeft: 10 }} key={activity.id}>
                        <Body>
                            <Text>{activity.name} ({activity.code_name || 'Code not available'})</Text>
                        </Body>
                        <Right>
                            <Button transparent  onPress={_.debounce(this.activities.bind(this, activity), 500)}>
                                <Icon  name='arrow-forward' />
                            </Button>
                        </Right>
                    </ListItem>)}
                </List>}
                {Boolean(this.state.project_id) && !Boolean(project) && <List><ListItem><Text>There are no activities with enrollments in this project yet.</Text></ListItem></List>}
            </View>;
        }
        return <Container style={{ backgroundColor: 'white' }}>
            <Header {...this.props} menu title="Activity tracking" />
            <Content>
                <ScrollView>
                    {content}
                </ScrollView>
            </Content>
        </Container>;
    }
}