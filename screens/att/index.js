import {
    StackNavigator
} from "react-navigation";
import ListATT from './list'
import Activity from './activity';
export default StackNavigator({
    ListATT: {
        name: 'ATT',
        screen: ListATT
    },
    Activity: {
        name: 'Activity',
        screen: Activity
    }
});