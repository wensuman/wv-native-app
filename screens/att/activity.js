import {
    Text,
    ToastAndroid,
    View,
    TouchableOpacity
} from 'react-native';
import {
    Body,
    Button,
    Card,
    CardItem,
    CheckBox,
    Container,
    Content,
    Icon,
    Left,
    List,
    ListItem,
    Right,
    H3
} from 'native-base';

import {get,file} from '../../module/store';

import Header from '../../designs/header';
import {
    Input
} from '../../forms/input'
import React from 'react';
import {
    Select
} from '../../forms/input';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import store from '../../module/store';

export default class Activity extends React.Component {

    static navigationOptions = {
        header: null,
        drawerLockMode: "locked-closed",
    }

    constructor(props) {
        super(props)
        this.state = {
            ...this.props.navigation.state.params.state,
            rolled: []
        };
        this.load = this.load.bind(this)
        this.dispatch = this.dispatch.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
        this.individual = this.individual.bind(this)
        if (this.state.activity_type) {
            if (this.state.activity_type == 'Individual') {
                this.state.activity_type = 'Member';
            }
            if (this.state.activity_type == 'Institutional') {
                this.state.activity_type = 'Institute';
            }
            this.state.activity_type = this.state.activity_type.toLowerCase();
        }
    }

    componentDidMount() {
        this.load()
    }

    async load() {
        this.setState({
            locates: await file('place')
        });
        try{
            let models = await file(this.state.activity_type);
            this.setState({
                activityloaded: true,
                models: models.filter(m => {
                    return this.state.enrollments.includes(m.id);
                })
            });
        } catch ( error) {
            this.setState({activityloaded: true})
        }
    }

    async componentDidUpdate(prev, next) {
        if (this.state.location_id !== next.location_id) {
            let data = await get();
            let belongs = {
                belongsTo: 'ATT',
                index: 'att:' + this.state.location_id
            };
            let reals = _.find(data, belongs) || belongs;
            this.setState({
                rolled: reals.rolled || []
            })
        }
    }

    individual(l) {
        if (!l) {
            return <Card>
                <CardItem header>
                    <Text>Select activity location.</Text>
                </CardItem>
            </Card>;
        }

        let location = _.find(this.state.locations, { id: l });
        let ward = _.find(this.state.locates, { Location_Id: location.location_id });
        
        if(!ward){
            return ;
        }

        let id = ward.Location_Id;
        let models = this.state.models || [];

        if (this.state.search) {
            models = models.filter(f => f.name.toLowerCase().replace(/ /g, '').includes(this.state.search) || f.id == parseInt(this.state.search));
        }

        if(!ward){
            return;
        }

        let municipality = _.find(this.state.locates, { Location_Id: ward.Parent })
        let loc = [municipality.Location_name, ward.Location_name].join('-');

        return (<Card key={location.id}>
            {loc && <CardItem header>
                <Text>Location: {_.capitalize(loc)} ({location.start_date} - {location.end_date}) - {_.capitalize(this.state.activity_type)}</Text>
            </CardItem>}
            <Input placeholder="Search by ID or name" value={this.state.search} onChange={(search) => this.setState({ search })} />
            {models && models.map(m =><ListItem key={m.id} onPress={this.batchUpdate.bind(this, l, m.id)} style={{ marginLeft: 0, paddingLeft: 5, width: '100%', height:50 }}>
                <Body>
                    <Text>{m.name} (ID: {m.id})</Text>
                </Body>
                <Right>
                    <Icon style={{color:'black'}} name={(this.state.rolled && this.state.rolled.includes(m.id)) ? 'md-radio-button-on' : 'md-radio-button-off'} />
                </Right>
            </ListItem>)}
        </Card>);
    }

    batchUpdate(l, id) {
        this.dispatch(l, id, () => { });
    }

    async dispatch(l, m, callback) {
        this.setState({
            updating: 'Updating...'
        });

        let data = await get();

        let belongs = {
            belongsTo: 'ATT',
            index: 'att:' + l
        };
        
        let reals = _.find(data, belongs) || belongs;

        if (reals.rolled) {
            if (reals.rolled.includes(m))
                reals.rolled = reals.rolled.filter(f => f != m)
            else
                reals.rolled.push(m)
        } else {
            reals.rolled = [];
            reals.rolled.push(m);
        }

        reals.uploaded = undefined;

        reals.location_id = l;

        await store(reals);

        callback && callback();

        this.setState({
            updating: '',
            rolled: reals.rolled
        });

    }

    label(w) {
        let ward = _.find(this.state.locates, {
            Location_Id: w.location_id
        });
	console.log(ward);
        // Error will be here if activity is assigned to locations where project is not assigned
        if(!ward){
            return false;
        }
        let municipality = _.find(this.state.locates, {
            Location_Id: ward.Parent
        })

	if(!municipality){
		return;
	}
        let loc = [municipality.Location_name, ward.Location_name].join('-');

        return _.capitalize(loc + '(' + w.start_date + '-' + w.end_date + ')');

    }

    render() {
        if (!this.state.activityloaded) {
            return <View><Text>Loading...</Text></View>;
        }
        const locations = this.state.locations.map(w => ({ value: w.id, label: this.label(w) })).filter(w=>w && w.label);

        return <Container>
            <Header {...this.props} title="ATT" />
            <Spinner visible={Boolean(this.state.updating)} textContent={this.state.updating} />
            <Content>
                <Card>
                    <CardItem>
                        <Left>
                            <Body>
                                <H3>{this.state.name} ({this.state.code_name || 'Code not available'})</H3>
                                <Text note>{_.capitalize(this.state.activity_type)}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
                <Card>
                    <CardItem>
                        <Left>
                            <Body>
                                {locations.length > 0 ?  <Select placeholder="Select a location" data={locations} selected={this.state.location_id || ''} onChange={location_id => this.setState({ location_id })} />: <Text>No locations has been created yet.</Text>}
                            </Body>
                        </Left>
                    </CardItem>
                </Card> 
                {locations.length > 0 && this.individual(this.state.location_id)}
            </Content>
        </Container>;
    }
}
