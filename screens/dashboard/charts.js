/*global setInterval*/
/* demo.js is loaded by both index.ios.js and index.android.js */

import {
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import React, { Component } from "react";
import {
    VictoryArea,
    VictoryBar,
    VictoryLine,
    VictoryPie
} from "victory-native";
