import { Asset, FileSystem, Font } from 'expo';
import {DatePick} from '../forms/input';
import {
  Button,
  Card,
  CardItem,
  Col,
  Container,
  Content,
  Grid,
  H3,
  ListItem
} from 'native-base';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
  AsyncStorage,
  NetInfo
} from "react-native";
import { VictoryArea, VictoryBar, VictoryChart, VictoryContainer, VictoryLabel, VictoryLine, VictoryPie, VictoryTheme } from 'victory-native';

import Spinner from 'react-native-loading-spinner-overlay';

import Axios from 'axios';
import Chart from './dashboard/charts'
import Header from '../designs/header';
import React from "react";
import { StackNavigator } from 'react-navigation';
import Svg from 'react-native-svg';
import Url from '../module/url';

class Home extends React.Component {

  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      member_loaded: false,
      family_loaded: false,
      institute_loaded: false,
      group_loaded: false,
      place_loaded: false,
      project_loaded: false,
      activities_loaded: false
    }
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  async componentDidMount() {
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));

    try {
        await FileSystem.downloadAsync(Url.server_url + "dashboard?X-ID="+token.id, FileSystem.documentDirectory + 'stats');
    } catch (error) {
        ToastAndroid.show('Could not download latest dashboard data', ToastAndroid.SHORT);
    }
    let stats = JSON.parse(await FileSystem.readAsStringAsync(FileSystem.documentDirectory + 'stats'));
    stats && this.setState({ stats, is_ready: true });
  }

  render() {
    if (this.state.is_ready)
      if (this.state.stats) {
        const gender = this.state.stats.gender;
        const locations = this.state.stats.locations;
        return <Container>
          <Header menu {...this.props} title="Dashboard" />
          <Content style={{alignContent:'center'}}>
            {this.state.stats && <ScrollView>
              <H3 style={{paddingLeft:20, paddingTop:20}}>{'Project Count'}</H3>
              {this.state.stats && this.state.stats.project && <Svg style={{alignContent:'center'}} width={400} height={400} viewBox="0 0 400 400" style={{ width: "100%", height: "auto" }}>
                <VictoryPie polar
                  containerComponent={<VictoryContainer responsive={true} />}
                  colorScale={["tomato", "orange", "gold", "cyan", "navy"]}
                  data={this.state.stats.project}
                  standalone={false}
                  labelRadius={90}
                  x="label"
                  y="y"
                  />
              </Svg>}
              <H3 style={{ paddingLeft: 20 }}>{"Registered members by gender"}</H3>
              {this.state.stats && this.state.stats.gender && <Svg width={400} height={400}  style={{ width: "100%", height: "auto" }}>
                <VictoryPie
                  standalone={false}
                  containerComponent={<VictoryContainer responsive={true} />}
                  colorScale={["tomato", "orange", "gold", "cyan", "navy"]}
                  data={this.state.stats.beneficiaries && this.state.stats.beneficiaries.slice(0,4)}
                  labelRadius={90}
                  x="label"
                  y="y"
                />
              </Svg>}
              {this.state.stats && this.state.stats.locations && this.state.stats.locations.map(l => <ListItem style={{marginLeft:0, paddingLeft: 10}} key={l.name}><Text>{l.name}:{l.count}</Text></ListItem>)}
            </ScrollView>}
          </Content>
        </Container>
      }
      return <Spinner visible={true} textContent="Loading..." />;
  }
}

export default StackNavigator({
  Dashboard:{
    screen:Home
  }
})