import { ToastAndroid } from "react-native";
import { Body, Button, Container, Content, Fab, Icon, List, ListItem, Text, View } from 'native-base';
import { Input, Number, Select } from '../../forms/input';

import Header from '../../designs/header';
import Label from '../../module/questions';
import Location from '../../module/location';
import { NavigationActions } from 'react-navigation';
import Options from '../../module/options';
import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import list from './list';
import { file } from '../../module/store';
import type from './type';

export default class Lists extends React.Component {
    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    };

    institute = [];

    family = [];

    member = [];

    group = [];

    project = [];

    tp = [];

    all = [];

    constructor(props) {
        super(props);
        this.state = {
            models: [],
            is_ready: false,
            ...this.props.navigation.state.params
        };
        this.componentDidMount = this.componentDidMount.bind(this);
        this.load = this.load.bind(this);
        this.asks = this.asks.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    load() {
        ['family', 'group', 'member', 'institute', 'project'].map(async f => {
            this[f] = await file(f);

            'project' == f && this.setState({
                is_ready: true
            });
        });

    }

    _new() {
        this.props.navigation.navigate("Survey");
    }

    asks() {
        this.props.navigation.dispatch(NavigationActions.reset({
            index: 2,
            actions: [
                NavigationActions.navigate({ routeName: 'Survey' }),
                NavigationActions.navigate({ routeName: 'Perform', params: this.state }),
                NavigationActions.navigate({ routeName: 'Ask', params: { state: _.pick(this.state, ['tp', 'question', 'indicators', 'respondent_id', 'respondent_type', 'project_id']) } }),
            ]
        }));
    }

    setBy(tp) {
        if (tp && tp.includes('Agriculture')) {
            return Options('tp_5').map(f => f.value);
        }
        if (tp && tp.includes('Youth')) {
            return Options('tp_3').map(f => f.value);
        }
        if (tp && tp.includes('Health')) {
            return Options('tp_7').map(f => f.value);
        }
        if (tp && tp.includes('Protection')) {
            return Options('tp_8').map(f => f.value);
        }
        if (tp && tp.includes('Education')) {
            return Options('tp_6').map(f => f.value);
        }
    }
    getIndicators(){
        let options = Options('all', this.state.lang);
        if(this.state.respondent_list && this.state.respondent_specific){
            if(this.state.respondent_specific == 'School Head Teacher and SMC PTA'){
                options = options.filter(o=>['c2d_24276','c2d_24264','c2d_24263'].includes(o.value));
            }
            if(this.state.respondent_specific == 'Grade teachers'){
                options = options.filter(o=>['c2a_21686'].includes(o.value));
            }
            if(this.state.respondent_specific == 'Reading club facilitators'){
                options = options.filter(o=>['c2a_24023'].includes(o.value));
            }
            
            if(this.state.respondent_specific == 'Parent'){
                options = options.filter(o=>['c2d_23018p'].includes(o.value));
            }
            if(this.state.respondent_specific == 'Student'){
                options = options.filter(o=>['c2d_23018s'].includes(o.value));
            }
        }
        return options;
    }
    render() {
        let content = (<Spinner visible={Boolean(!this.state.is_ready)} textContent="Loading" />);
        if(!this.state.project_type){
            return <View>
                <Header title="Select Project Type" {...this.props} />
                <Button style={{marginTop:20}} block success onPress={()=>this.setState({project_type: 'education',respondent_list:['School Head Teacher and SMC PTA','Grade teachers','Reading club facilitators','Parent','Student']})}><Text>Education project</Text></Button>
                <Button style={{marginTop:20}} block info onPress={()=>this.setState({project_type: 'aed',respondent_list:false})}><Text>AED project</Text></Button>
                <Button style={{marginTop:20}} block warning onPress={()=>this.setState({project_type: 'mchn',respondent_list:false})}><Text>MCHN project</Text></Button>
                <Button style={{marginTop:20}} block danger onPress={()=>this.setState({project_type: 'protection',respondent_list:false})}><Text>Protection project</Text></Button>
            </View>;
        }
// , indicators: this.setBy(f.tp_name) 
        if (this.state.is_ready) {
            content = (<View>
                <Input value={this.state.question} onChange={question => this.setState({ question })} label={"Enter the survey ID"} />
                <Select label="Select project" data={this.project.filter(p=>{
                    return p.name && p.name.toLowerCase().includes(this.state.project_type.toLowerCase());
                }).map(f => ({ label: f.name, value: f }))} selected={this.state.selectedProject || undefined} onChange={f => {
                    this.setState({ selectedProject: f, project_id: f.id})
                }} />
                {this.state.respondent_list && <Select label="Select respondent type" selected={this.state.respondent_specific} data={(this.state.respondent_list).map(id => ({ label:id, value: id }))} onChange={respondent_specific => this.setState({ respondent_specific })} />}
                <Select searchable style={{ marginLeft: 10 }} multiple label="Select indicators" selected={this.state.indicators} data={this.getIndicators()} onChange={indicators => this.setState({ indicators })} />
                <Location tole={false} lang={this.state.lang} locations={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} onChange={state => this.setState({ ...state, respondent_type: undefined })} />
                <Select label="Select respondent type" selected={this.state.respondent_type} data={type} onChange={respondent_type => {
                    let needed = [];
                    needed = ['state_id', 'district_id', 'municipality_id', 'ward_id'].filter(f => Boolean(this.state[f]));
                    let ids = [];
                    if (this.state.state_id && this.state.district_id && this.state.municipality_id && this[respondent_type]) {
                        ids = this[respondent_type].filter(i => {
                            return i.location && needed.filter(n => {
                                return i.location[n] == this.state[n]
                            }).length == needed.length;
                        });
                    }
                    this.setState({ respondent_type, ids })
                }} />
                <Select label="Select respondent" selected={this.state.respondent_id} data={(this.state.ids || []).map(id => ({ label: id.id + ' - ' + _.capitalize(id.name || id.name_of_group || id.name_of_institution || 'name not available'), value: id.id }))} onChange={respondent_id => this.setState({ respondent_id })} />
                <Button block color="blue" disabled={!this.state.respondent_id || !this.state.question} onPress={this.asks.debounce(100)}><Text>Proceed to question</Text></Button>
            </View>);
        }

        try {
            content = (<Container style={{ backgroundColor: "white" }}>
                <Header title="Survey" {...this.props} />
                <Content>
                    {content}
                </Content>
            </Container>);
        } catch (error) {
        }
        return content;
    }
}
