import { DatePick, Input, Number, Select } from '../../forms/input';
import { ScrollView, ToastAndroid } from 'react-native'

import Create from '../../designs/create'
import Header from '../../designs/header';
import Label from '../../module/questions';
import { NavigationActions } from "react-navigation";
import Options from '../../module/options';
import React from "react";
import _ from 'lodash';
import {file} from '../../module/store';
import store from '../../module/store';

export default class Survey extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    constructor(props) {
        super(props);
        this.getQuestions = this.getQuestions.bind(this);
        let state = {}
        if (this.props.navigation && this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.state) {
            state = this.props.navigation.state.params.state;
        }
        state.belongsTo = 'survey';
        this.componentDidMount = this.componentDidMount.bind(this);
        state.lang = 0;
        this.state = state;

    }

    async componentDidMount() {
        data = await file(this.state.respondent_type);
        let find = _.find(data, { id: parseInt(this.state.respondent_id) });
        find && this.setState({ respondent_name: find.name });
    }

    required(checkIn, message = 'Fill up the field correctly') {
        if (checkIn.filter((state) => {
          return (!this.state[state] || (this.state[state] || []).length == 0)
        }).length > 0) {
          ToastAndroid.show(message , ToastAndroid.SHORT);
          return false;
        }
        return true;
      }

    render() {
        return <ScrollView>
            <Header icon={'md-book'} rightText={this.state.lang ?  'NP' : 'EN' } title="Survey" alertOnBack {...this.props} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
            <Create exists={this.state.exists} onSave={() => {
                store(this.state, () => {
                    this.props.navigation.dispatch(NavigationActions.reset({
                        index: 0,
                        actions: [
                            NavigationActions.navigate({ routeName: 'Survey' }),
                        ]
                    }))
                });
            }} {...this.props} questions={this.getQuestions()} />
        </ScrollView>;
    }

    getQuestions() {
        let c4b_24266 = [
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('go_school',this.state.lang)} key='go_school' selected={this.state.go_school} onChange={(go_school) => this.setState({ go_school })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('vocational_skill',this.state.lang)} key='vocational_skill' selected={this.state.vocational_skill} onChange={(vocational_skill) => this.setState({ vocational_skill })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('employed',this.state.lang)} key='employed' selected={this.state.employed} onChange={(employed) => this.setState({ employed })} /> },
      
        ]
        let c1bsav = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('eating_place',this.state.lang)} key='eating_place' selected={this.state.eating_place} onChange={(eating_place) => this.setState({ eating_place })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('playing_space',this.state.lang)} key='playing_space' selected={this.state.playing_space} onChange={(playing_space) => this.setState({ playing_space })} /> },
            this.state.playing_space == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('space_protection',this.state.lang)} key='space_protection' selected={this.state.space_protection} onChange={(space_protection) => this.setState({ space_protection })} /> },
        ];
        
        let c4a_24265 =[
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('protection_schemes',this.state.lang)} key='protection_schemes' selected={this.state.protection_schemes} onChange={(protection_schemes) => this.setState({ protection_schemes })} /> },
            this.state.protection_schemes == 1 && { component: () => <Select multiple data={Options('received_schemes',this.state.lang)} label={Label('received_schemes',this.state.lang)} key='received_schemes' selected={this.state.received_schemes} onChange={(received_schemes) => this.setState({ received_schemes })} /> },
            this.state.received_schemes == 77 && { component: () => <Input label={Label('received_schemes_other',this.state.lang)} key='received_schemes_other' value={this.state.received_schemes_other} onChange={(received_schemes_other) => { this.setState({ received_schemes_other }) }} /> },
        ];

        let c2d_23018p  =[
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('children_school',this.state.lang)} key='children_school' selected={this.state.children_school} onChange={(children_school) => this.setState({ children_school })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_par_teacher',this.state.lang)} key='satis_par_teacher' selected={this.state.satis_par_teacher} onChange={(satis_par_teacher) => this.setState({ satis_par_teacher })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_par_textbook',this.state.lang)} key='satis_par_textbook' selected={this.state.satis_par_textbook} onChange={(satis_par_textbook) => this.setState({ satis_par_textbook })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_par_safety',this.state.lang)} key='satis_par_safety' selected={this.state.satis_par_safety} onChange={(satis_par_safety) => this.setState({ satis_par_safety })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_par_readmater',this.state.lang)} key='satis_par_readmater' selected={this.state.satis_par_readmater} onChange={(satis_par_readmater) => this.setState({ satis_par_readmater })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_par_girltoilet',this.state.lang)} key='satis_par_girltoilet' selected={this.state.satis_par_girltoilet} onChange={(satis_par_girltoilet) => this.setState({ satis_par_girltoilet })} /> },
            { component: () => <Number label={Label('satis_par_overall',this.state.lang)} key='satis_par_overall' value={this.state.satis_par_overall} onChange={(satis_par_overall) => { this.setState({ satis_par_overall }) }} />, validation: () => () => this.required(['satis_par_overall']) },
                ];
        let c2d_23018s =[
             { component: () => <Number label={Label('grade',this.state.lang)} key='grade' value={this.state.grade} onChange={(grade) => { this.setState({ grade }) }} />, validation: () => () => this.required(['grade']) },
            { component: () => <Select data={Options('sex_child',this.state.lang)} label={Label('gender',this.state.lang)} key='gender' selected={this.state.gender} onChange={(gender) => this.setState({ gender })} /> },
            { component: () => <Number label={Label('age',this.state.lang)} key='age' value={this.state.age} onChange={(age) => { this.setState({ age }) }} />, validation: () => () => this.required(['age']) },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_std_teacher',this.state.lang)} key='satis_std_teacher' selected={this.state.satis_std_teacher} onChange={(satis_std_teacher) => this.setState({ satis_std_teacher })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_std_books',this.state.lang)} key='satis_std_books' selected={this.state.satis_std_books} onChange={(satis_std_books) => this.setState({ satis_std_books })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_std_classroom',this.state.lang)} key='satis_std_classroom' selected={this.state.satis_std_classroom} onChange={(satis_std_classroom) => this.setState({ satis_std_classroom })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_std_readmater',this.state.lang)} key='satis_std_readmater' selected={this.state.satis_std_readmater} onChange={(satis_std_readmater) => this.setState({ satis_std_readmater })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_std_girltoilet',this.state.lang)} key='satis_std_girltoilet' selected={this.state.satis_std_girltoilet} onChange={(satis_std_girltoilet) => this.setState({ satis_std_girltoilet })} /> },
            { component: () => <Select data={Options('Likert_5_satisfy',this.state.lang)} label={Label('satis_std_overall',this.state.lang)} key='satis_std_overall' selected={this.state.satis_std_overall} onChange={(satis_std_overall) => this.setState({ satis_std_overall })} /> },
        ];
        let c2a_24023 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('rc_safe',this.state.lang)} key='rc_safe' selected={this.state.rc_safe} onChange={(rc_safe) => this.setState({ rc_safe })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('re_disturb',this.state.lang)} key='re_disturb' selected={this.state.re_disturb} onChange={(re_disturb) => this.setState({ re_disturb })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('rc_attendance',this.state.lang)} key='rc_attendance' selected={this.state.rc_attendance} onChange={(rc_attendance) => this.setState({ rc_attendance })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('re_basicmaterial',this.state.lang)} key='re_basicmaterial' selected={this.state.re_basicmaterial} onChange={(re_basicmaterial) => this.setState({ re_basicmaterial })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('book_accessiblity',this.state.lang)} key='book_accessiblity' selected={this.state.book_accessiblity} onChange={(book_accessiblity) => this.setState({ book_accessiblity })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('materials_display',this.state.lang)} key='materials_display' selected={this.state.materials_display} onChange={(materials_display) => this.setState({ materials_display })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('book_register',this.state.lang)} key='book_register' selected={this.state.book_register} onChange={(book_register) => this.setState({ book_register })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('attendence_percent',this.state.lang)} key='attendence_percent' selected={this.state.attendence_percent} onChange={(attendence_percent) => this.setState({ attendence_percent })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('materials_locallg',this.state.lang)} key='materials_locallg' selected={this.state.materials_locallg} onChange={(materials_locallg) => this.setState({ materials_locallg })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('agenda_display',this.state.lang)} key='agenda_display' selected={this.state.agenda_display} onChange={(agenda_display) => this.setState({ agenda_display })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('threeday_training',this.state.lang)} key='threeday_training' selected={this.state.threeday_training} onChange={(threeday_training) => this.setState({ threeday_training })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('rc_lb_standard',this.state.lang)} key='rc_lb_standard' selected={this.state.rc_lb_standard} onChange={(rc_lb_standard) => this.setState({ rc_lb_standard })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('agenda_prepare',this.state.lang)} key='agenda_prepare' selected={this.state.agenda_prepare} onChange={(agenda_prepare) => this.setState({ agenda_prepare })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('parents_support',this.state.lang)} key='parents_support' selected={this.state.parents_support} onChange={(parents_support) => this.setState({ parents_support })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sustaining_plan',this.state.lang)} key='sustaining_plan' selected={this.state.sustaining_plan} onChange={(sustaining_plan) => this.setState({ sustaining_plan })} /> },
        ];
        
        let c2a_21686 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('training_received',this.state.lang)} key='training_received' selected={this.state.training_received} onChange={(training_received) => this.setState({ training_received })} /> },
            this.state.training_received == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('module_completion',this.state.lang)} key='module_completion' selected={this.state.module_completion} onChange={(module_completion) => this.setState({ module_completion })} /> },
            this.state.training_received == 1 && { component: () => <Select multiple data={Options('skills_used',this.state.lang)} label={Label('skills_used',this.state.lang)} key='skills_used' selected={this.state.skills_used} onChange={(skills_used) => this.setState({ skills_used })} /> },
            this.state.training_received == 1 && { component: () => <Select multiple data={Options('reasons_noskills',this.state.lang)} label={Label('reasons_noskills',this.state.lang)} key='reasons_noskills' selected={this.state.reasons_noskills} onChange={(reasons_noskills) => this.setState({ reasons_noskills })} /> },
            this.state.reasons_noskills == 77 && { component: () => <Input label={Label('other_reasons',this.state.lang)} key='other_reasons' value={this.state.other_reasons} onChange={(other_reasons) => { this.setState({ other_reasons }) }} /> },
            this.state.training_received == 1 && { component: () => <Select multiple data={Options('reasons_teachread',this.state.lang)} label={Label('reasons_teachread',this.state.lang)} key='reasons_teachread' selected={this.state.reasons_teachread} onChange={(reasons_teachread) => this.setState({ reasons_teachread })} /> },
        ];

        let c2d_24264 =[
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('teacher_available',this.state.lang)} key='teacher_available' selected={this.state.teacher_available} onChange={(teacher_available) => this.setState({ teacher_available })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('textbook_available',this.state.lang)} key='textbook_available' selected={this.state.textbook_available} onChange={(textbook_available) => this.setState({ textbook_available })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('classroom_available',this.state.lang)} key='classroom_available' selected={this.state.classroom_available} onChange={(classroom_available) => this.setState({ classroom_available })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('bookcorner_estd',this.state.lang)} key='bookcorner_estd' selected={this.state.bookcorner_estd} onChange={(bookcorner_estd) => this.setState({ bookcorner_estd })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('girls_toilet',this.state.lang)} key='girls_toilet' selected={this.state.girls_toilet} onChange={(girls_toilet) => this.setState({ girls_toilet })} /> },
        ];
                let c2d_24276 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_sufficiency',this.state.lang)} key='water_sufficiency' selected={this.state.water_sufficiency} onChange={(water_sufficiency) => this.setState({ water_sufficiency })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_toilet_running',this.state.lang)} key='water_toilet_running' selected={this.state.water_toilet_running} onChange={(water_toilet_running) => this.setState({ water_toilet_running })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_point',this.state.lang)} key='handwash_point' selected={this.state.handwash_point} onChange={(handwash_point) => this.setState({ handwash_point })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_soap',this.state.lang)} key='handwash_soap' selected={this.state.handwash_soap} onChange={(handwash_soap) => this.setState({ handwash_soap })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_footprint',this.state.lang)} key='handwash_footprint' selected={this.state.handwash_footprint} onChange={(handwash_footprint) => this.setState({ handwash_footprint })} /> },
        ];

        let c2d_24263 = [
            { component: () => <Number label={Label('building',this.state.lang)} key='building' value={this.state.building} onChange={(building) => { this.setState({ building }) }} />, validation: () => () => this.required(['building']) },
            { component: () => <Number label={Label('classroom',this.state.lang)} key='classroom' value={this.state.classroom} onChange={(classroom) => { this.setState({ classroom }) }} />, validation: () => () => this.required(['classroom']) },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('landslide',this.state.lang)} key='landslide' selected={this.state.landslide} onChange={(landslide) => this.setState({ landslide })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('hightension',this.state.lang)} key='hightension' selected={this.state.hightension} onChange={(hightension) => this.setState({ hightension })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('bigtree',this.state.lang)} key='bigtree' selected={this.state.bigtree} onChange={(bigtree) => this.setState({ bigtree })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('hazardous_site',this.state.lang)} key='hazardous_site' selected={this.state.hazardous_site} onChange={(hazardous_site) => this.setState({ hazardous_site })} /> },
            { component: () => <Number label={Label('resilent_building',this.state.lang)} key='resilent_building' value={this.state.resilent_building} onChange={(resilent_building) => { this.setState({ resilent_building }) }} />, validation: () => () => this.required(['resilent_building']) },
            { component: () => <Number label={Label('resilent_toilets',this.state.lang)} key='resilent_toilets' value={this.state.resilent_toilets} onChange={(resilent_toilets) => { this.setState({ resilent_toilets }) }} />, validation: () => () => this.required(['resilent_toilets']) },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('seating_1to3',this.state.lang)} key='seating_1to3' selected={this.state.seating_1to3} onChange={(seating_1to3) => this.setState({ seating_1to3 })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('seating_4plus',this.state.lang)} key='seating_4plus' selected={this.state.seating_4plus} onChange={(seating_4plus) => this.setState({ seating_4plus })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('door_lock',this.state.lang)} key='door_lock' selected={this.state.door_lock} onChange={(door_lock) => this.setState({ door_lock })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('steps_hight',this.state.lang)} key='steps_hight' selected={this.state.steps_hight} onChange={(steps_hight) => this.setState({ steps_hight })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('exit_doors',this.state.lang)} key='exit_doors' selected={this.state.exit_doors} onChange={(exit_doors) => this.setState({ exit_doors })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('doors_open',this.state.lang)} key='doors_open' selected={this.state.doors_open} onChange={(doors_open) => this.setState({ doors_open })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ramps',this.state.lang)} key='ramps' selected={this.state.ramps} onChange={(ramps) => this.setState({ ramps })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handrails',this.state.lang)} key='handrails' selected={this.state.handrails} onChange={(handrails) => this.setState({ handrails })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('spacefor_child',this.state.lang)} key='spacefor_child' selected={this.state.spacefor_child} onChange={(spacefor_child) => this.setState({ spacefor_child })} /> },
            { component: () => <Number label={Label('toilet_no',this.state.lang)} key='toilet_no' value={this.state.toilet_no} onChange={(toilet_no) => { this.setState({ toilet_no }) }} />, validation: () => () => this.required(['toilet_no']) },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('girls_toilet_separate',this.state.lang)} key='girls_toilet_separate' selected={this.state.girls_toilet_separate} onChange={(girls_toilet_separate) => this.setState({ girls_toilet_separate })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('mhm_bin',this.state.lang)} key='mhm_bin' selected={this.state.mhm_bin} onChange={(mhm_bin) => this.setState({ mhm_bin })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('incinerator_mhm',this.state.lang)} key='incinerator_mhm' selected={this.state.incinerator_mhm} onChange={(incinerator_mhm) => this.setState({ incinerator_mhm })} /> },
            { component: () => <Input label={Label('mhm_other',this.state.lang)} key='mhm_other' value={this.state.mhm_other} onChange={(mhm_other) => { this.setState({ mhm_other }) }} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('doorlock_toilet',this.state.lang)} key='doorlock_toilet' selected={this.state.doorlock_toilet} onChange={(doorlock_toilet) => this.setState({ doorlock_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('commode_size',this.state.lang)} key='commode_size' selected={this.state.commode_size} onChange={(commode_size) => this.setState({ commode_size })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ramps_to_toilet',this.state.lang)} key='ramps_to_toilet' selected={this.state.ramps_to_toilet} onChange={(ramps_to_toilet) => this.setState({ ramps_to_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handrails_toilet',this.state.lang)} key='handrails_toilet' selected={this.state.handrails_toilet} onChange={(handrails_toilet) => this.setState({ handrails_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('access_toilet',this.state.lang)} key='access_toilet' selected={this.state.access_toilet} onChange={(access_toilet) => this.setState({ access_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('clean_toilet',this.state.lang)} key='clean_toilet' selected={this.state.clean_toilet} onChange={(clean_toilet) => this.setState({ clean_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_toilet',this.state.lang)} key='water_toilet' selected={this.state.water_toilet} onChange={(water_toilet) => this.setState({ water_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_toilet',this.state.lang)} key='handwash_toilet' selected={this.state.handwash_toilet} onChange={(handwash_toilet) => this.setState({ handwash_toilet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_visual',this.state.lang)} key='handwash_visual' selected={this.state.handwash_visual} onChange={(handwash_visual) => this.setState({ handwash_visual })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_access',this.state.lang)} key='water_access' selected={this.state.water_access} onChange={(water_access) => this.setState({ water_access })} /> },
            this.state.water_access == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_adequate',this.state.lang)} key='water_adequate' selected={this.state.water_adequate} onChange={(water_adequate) => this.setState({ water_adequate })} /> },
            this.state.water_access == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_forall',this.state.lang)} key='water_forall' selected={this.state.water_forall} onChange={(water_forall) => this.setState({ water_forall })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('waste_mgmt',this.state.lang)} key='waste_mgmt' selected={this.state.waste_mgmt} onChange={(waste_mgmt) => this.setState({ waste_mgmt })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('garbage_bin',this.state.lang)} key='garbage_bin' selected={this.state.garbage_bin} onChange={(garbage_bin) => this.setState({ garbage_bin })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('clean_ground',this.state.lang)} key='clean_ground' selected={this.state.clean_ground} onChange={(clean_ground) => this.setState({ clean_ground })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('garbage_point',this.state.lang)} key='garbage_point' selected={this.state.garbage_point} onChange={(garbage_point) => this.setState({ garbage_point })} /> },
            this.state.garbage_point == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('incinerator',this.state.lang)} key='incinerator' selected={this.state.incinerator} onChange={(incinerator) => this.setState({ incinerator })} /> },
            this.state.garbage_point == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('collection',this.state.lang)} key='collection' selected={this.state.collection} onChange={(collection) => this.setState({ collection })} /> },
            this.state.garbage_point == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('landfill',this.state.lang)} key='landfill' selected={this.state.landfill} onChange={(landfill) => this.setState({ landfill })} /> },
            this.state.garbage_point == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('other',this.state.lang)} key='other' selected={this.state.other} onChange={(other) => this.setState({ other })} /> },
            this.state.garbage_point == 1 && this.state.other == 1 && { component: () => <Input label={Label('other_details',this.state.lang)} key='other_details' value={this.state.other_details} onChange={(other_details) => { this.setState({ other_details }) }} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('playground_size',this.state.lang)} key='playground_size' selected={this.state.playground_size} onChange={(playground_size) => this.setState({ playground_size })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('playground_safe',this.state.lang)} key='playground_safe' selected={this.state.playground_safe} onChange={(playground_safe) => this.setState({ playground_safe })} /> },
            { component: () => <Select data={Options('playgroundfance',this.state.lang)} label={Label('playgroundfance',this.state.lang)} key='playgroundfance' selected={this.state.playgroundfance} onChange={(playgroundfance) => this.setState({ playgroundfance })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('assemblyspace',this.state.lang)} key='assemblyspace' selected={this.state.assemblyspace} onChange={(assemblyspace) => this.setState({ assemblyspace })} /> },
            this.state.assemblyspace == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('assembly_adequat',this.state.lang)} key='assembly_adequat' selected={this.state.assembly_adequat} onChange={(assembly_adequat) => this.setState({ assembly_adequat })} /> },
            this.state.assemblyspace == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('assembly_safe',this.state.lang)} key='assembly_safe' selected={this.state.assembly_safe} onChange={(assembly_safe) => this.setState({ assembly_safe })} /> },
            this.state.assemblyspace == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('assembly_access',this.state.lang)} key='assembly_access' selected={this.state.assembly_access} onChange={(assembly_access) => this.setState({ assembly_access })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drr_train',this.state.lang)} key='drr_train' selected={this.state.drr_train} onChange={(drr_train) => this.setState({ drr_train })} /> },
            this.state.drr_train == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drr_drill',this.state.lang)} key='drr_drill' selected={this.state.drr_drill} onChange={(drr_drill) => this.setState({ drr_drill })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fak_teacher_train',this.state.lang)} key='fak_teacher_train' selected={this.state.fak_teacher_train} onChange={(fak_teacher_train) => this.setState({ fak_teacher_train })} /> },
            this.state.fak_teacher_train == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fak_student_train',this.state.lang)} key='fak_student_train' selected={this.state.fak_student_train} onChange={(fak_student_train) => this.setState({ fak_student_train })} /> },
            this.state.fak_teacher_train == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fak_team',this.state.lang)} key='fak_team' selected={this.state.fak_team} onChange={(fak_team) => this.setState({ fak_team })} /> },
            this.state.fak_teacher_train == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fak_eqqiped',this.state.lang)} key='fak_eqqiped' selected={this.state.fak_eqqiped} onChange={(fak_eqqiped) => this.setState({ fak_eqqiped })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fak_accessible',this.state.lang)} key='fak_accessible' selected={this.state.fak_accessible} onChange={(fak_accessible) => this.setState({ fak_accessible })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drr_iec',this.state.lang)} key='drr_iec' selected={this.state.drr_iec} onChange={(drr_iec) => this.setState({ drr_iec })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drr_bcc',this.state.lang)} key='drr_bcc' selected={this.state.drr_bcc} onChange={(drr_bcc) => this.setState({ drr_bcc })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drr_participate',this.state.lang)} key='drr_participate' selected={this.state.drr_participate} onChange={(drr_participate) => this.setState({ drr_participate })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('after_club',this.state.lang)} key='after_club' selected={this.state.after_club} onChange={(after_club) => this.setState({ after_club })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drr_curri_exist',this.state.lang)} key='drr_curri_exist' selected={this.state.drr_curri_exist} onChange={(drr_curri_exist) => this.setState({ drr_curri_exist })} /> },
            this.state.drr_curri_exist == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('toolkits_drr',this.state.lang)} key='toolkits_drr' selected={this.state.toolkits_drr} onChange={(toolkits_drr) => this.setState({ toolkits_drr })} /> },
            this.state.drr_curri_exist == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('eca_drr',this.state.lang)} key='eca_drr' selected={this.state.eca_drr} onChange={(eca_drr) => this.setState({ eca_drr })} /> },
            this.state.drr_curri_exist == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('materials_drr',this.state.lang)} key='materials_drr' selected={this.state.materials_drr} onChange={(materials_drr) => this.setState({ materials_drr })} /> },
            { component: () => <Input label={Label('sdmc',this.state.lang)} key='sdmc' value={this.state.sdmc} onChange={(sdmc) => { this.setState({ sdmc }) }} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_formed',this.state.lang)} key='sdmc_formed' selected={this.state.sdmc_formed} onChange={(sdmc_formed) => this.setState({ sdmc_formed })} /> },
            this.state.sdmc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_female',this.state.lang)} key='sdmc_female' selected={this.state.sdmc_female} onChange={(sdmc_female) => this.setState({ sdmc_female })} /> },
            this.state.sdmc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_dalit',this.state.lang)} key='sdmc_dalit' selected={this.state.sdmc_dalit} onChange={(sdmc_dalit) => this.setState({ sdmc_dalit })} /> },
            this.state.sdmc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_student',this.state.lang)} key='sdmc_student' selected={this.state.sdmc_student} onChange={(sdmc_student) => this.setState({ sdmc_student })} /> },
            this.state.sdmc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_training',this.state.lang)} key='sdmc_training' selected={this.state.sdmc_training} onChange={(sdmc_training) => this.setState({ sdmc_training })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_meet',this.state.lang)} key='sdmc_meet' selected={this.state.sdmc_meet} onChange={(sdmc_meet) => this.setState({ sdmc_meet })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sdmc_minutes',this.state.lang)} key='sdmc_minutes' selected={this.state.sdmc_minutes} onChange={(sdmc_minutes) => this.setState({ sdmc_minutes })} /> },
            { component: () => <Input label={Label('sbdrm',this.state.lang)} key='sbdrm' value={this.state.sbdrm} onChange={(sbdrm) => { this.setState({ sbdrm }) }} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sbdrm_plan',this.state.lang)} key='sbdrm_plan' selected={this.state.sbdrm_plan} onChange={(sbdrm_plan) => this.setState({ sbdrm_plan })} /> },
            this.state.sbdrm_plan == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_assessment',this.state.lang)} key='smc_assessment' selected={this.state.smc_assessment} onChange={(smc_assessment) => this.setState({ smc_assessment })} /> },
            this.state.sbdrm_plan == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sbdrm_sip',this.state.lang)} key='sbdrm_sip' selected={this.state.sbdrm_sip} onChange={(sbdrm_sip) => this.setState({ sbdrm_sip })} /> },
            this.state.sbdrm_plan == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drm_budget',this.state.lang)} key='drm_budget' selected={this.state.drm_budget} onChange={(drm_budget) => this.setState({ drm_budget })} /> },
            this.state.sbdrm_plan == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('sbdrm_support',this.state.lang)} key='sbdrm_support' selected={this.state.sbdrm_support} onChange={(sbdrm_support) => this.setState({ sbdrm_support })} /> },
            this.state.sbdrm_plan == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_drill',this.state.lang)} key='smc_drill' selected={this.state.smc_drill} onChange={(smc_drill) => this.setState({ smc_drill })} /> },
            this.state.sbdrm_plan == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('household_plan',this.state.lang)} key='household_plan' selected={this.state.household_plan} onChange={(household_plan) => this.setState({ household_plan })} /> },
            { component: () => <Input label={Label('sdrm',this.state.lang)} key='sdrm' value={this.state.sdrm} onChange={(sdrm) => { this.setState({ sdrm }) }} /> },
            { component: () => <Input label={Label('smc_form',this.state.lang)} key='smc_form' value={this.state.smc_form} onChange={(smc_form) => { this.setState({ smc_form }) }} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_formed',this.state.lang)} key='smc_formed' selected={this.state.smc_formed} onChange={(smc_formed) => this.setState({ smc_formed })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_female',this.state.lang)} key='smc_female' selected={this.state.smc_female} onChange={(smc_female) => this.setState({ smc_female })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_dalit',this.state.lang)} key='smc_dalit' selected={this.state.smc_dalit} onChange={(smc_dalit) => this.setState({ smc_dalit })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('meeting_freq',this.state.lang)} key='meeting_freq' selected={this.state.meeting_freq} onChange={(meeting_freq) => this.setState({ meeting_freq })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_minutes',this.state.lang)} key='smc_minutes' selected={this.state.smc_minutes} onChange={(smc_minutes) => this.setState({ smc_minutes })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_trainig',this.state.lang)} key='smc_trainig' selected={this.state.smc_trainig} onChange={(smc_trainig) => this.setState({ smc_trainig })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('smc_scpc',this.state.lang)} key='smc_scpc' selected={this.state.smc_scpc} onChange={(smc_scpc) => this.setState({ smc_scpc })} /> },
            this.state.smc_formed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('complain_box',this.state.lang)} key='complain_box' selected={this.state.complain_box} onChange={(complain_box) => this.setState({ complain_box })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('complain_response',this.state.lang)} key='complain_response' selected={this.state.complain_response} onChange={(complain_response) => this.setState({ complain_response })} /> },
        ];
    
        let c1a_24267 = [
            { component: () => <Number label={Label('muac',this.state.lang)} key='muac' value={this.state.muac} onChange={(muac) => { this.setState({ muac }) }} />, validation: () => () => this.required(['muac']) },
            { component: () => <Number label={Label('pregnant_weight',this.state.lang)} key='pregnant_weight' value={this.state.pregnant_weight} onChange={(pregnant_weight) => { this.setState({ pregnant_weight }) }} />, validation: () => () => this.required(['pregnant_weight']) },
            { component: () => <Number label={Label('pregnant_height',this.state.lang)} key='pregnant_height' value={this.state.pregnant_height} onChange={(pregnant_height) => { this.setState({ pregnant_height }) }} />, validation: () => () => this.required(['pregnant_height']) },
        ];

        let c1c_0152 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('under_24months',this.state.lang)} key='under_24months' selected={this.state.under_24months} onChange={(under_24months) => this.setState({ under_24months })} /> },
            { component: () => <Select data={Options('delivery_place',this.state.lang)} label={Label('delivery_place',this.state.lang)} key='delivery_place' selected={this.state.delivery_place} onChange={(delivery_place) => this.setState({ delivery_place })} /> },
            { component: () => <Select data={Options('delivery_type',this.state.lang)} label={Label('delivery_type',this.state.lang)} key='delivery_type' selected={this.state.delivery_type} onChange={(delivery_type) => this.setState({ delivery_type })} /> },
            { component: () => <Select multiple data={Options('delivery_helper',this.state.lang)} label={Label('delivery_helper',this.state.lang)} key='delivery_helper' selected={this.state.delivery_helper} onChange={(delivery_helper) => this.setState({ delivery_helper })} /> },
        ];

        let c1a_0008 = [ { component: () => <Select label={Label('u5_number',this.state.lang)} data={Options('u5_number',this.state.lang)} key='u5_count' selected={this.state.u5_count} onChange={(u5_number) => { this.setState({ u5_number: u5_number.length, u5_count:( u5_number || []).sort() }) }} />, validation: () => () => this.required(['u5_number']) },];
        
        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1a_0008 = c1a_0008.concat([
                { component: () => <Input label={Label('name',this.state.lang)} value={this.state['name_'+index]} onChange={(name) => { this.setState({ ['name_'+index]:name }) }} />, validation:()=>this.required(['name_'+index]) },
                { component: () => <Select data={Options('sex',this.state.lang)} label={Label('sex_child',this.state.lang,this.state['name_'+index])} selected={this.state['sex_child_'+index]} onChange={(sex_child) => this.setState({ ['sex_child_'+index]:sex_child })} /> },
                { component: () => <DatePick label={Label('DOB',this.state.lang,this.state['name_'+index])} value={this.state['DOB_'+index]} onChange={(DOB) => { this.setState({ ['DOB_'+index]:DOB }) }} />},
                { component: () => <Number label={Label('height_child',this.state.lang,this.state['name_'+index])} value={this.state['height_child_'+index]} onChange={(height_child) => { this.setState({ ['height_child_'+index]:height_child }) }} />},
            ]);
        });

        let c1b_0072 =[
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('chest_treatment',this.state.lang)} key='chest_treatment' selected={this.state.chest_treatment} onChange={(chest_treatment) => this.setState({ chest_treatment })} /> },
            this.state.fever_child == 1 && this.state.cough_child == 1 && { component: () => <Select multiple data={Options('treatment_center',this.state.lang)} label={Label('chest_treat_center',this.state.lang)} key='chest_treat_center' selected={this.state.chest_treat_center} onChange={(chest_treat_center) => this.setState({ chest_treat_center })} /> },
        ];
        
        let c1a_0018 = [];

        this.state.u5_count &&  this.state.u5_count.map((f,index)=>{
            c1a_0018 = c1a_0018.concat([{ component: () => <Number label={Label('weight_child',this.state.lang, this.state['name_'+index])} value={this.state['weight_child_'+index]} onChange={(weight_child) => { this.setState({['weight_child_'+index]: weight_child }) }} />, validation: () => () => this.required(['weight_child']) }]);
        });
        
        let c1a_0013 = c1a_0018;
        
        let c1b_0091=[];
        
        this.state.u5_count &&  this.state.u5_count.map((f,index)=>{
            c1b_0091 =c1b_0091.concat([
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('cough_child',this.state.lang,this.state['name_'+index])} selected={this.state['cough_child_'+index]} onChange={(cough_child) => this.setState({ ['cough_child_'+index]:cough_child })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('breath_child',this.state.lang,this.state['name_'+index])} selected={this.state['breath_child_'+index]} onChange={(breath_child) => this.setState({ ['breath_child_'+index]:breath_child })} />},
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('chest_child',this.state.lang,this.state['name_'+index])} selected={this.state['chest_child_'+index]} onChange={(chest_child) => this.setState({ ['chest_child_'+index]:chest_child })} /> },
            ]) ;
        });
        
        let c1a_0015 = [];

        this.state.u5_count &&  this.state.u5_count.map((f,index)=>{
            c1a_0015 = c1a_0015.concat([
                { component: () => <Select data={Options('food_sickness',this.state.lang)} label={Label('cough_drink',this.state.lang,this.state['name_'+index])} selected={this.state['cough_drink_'+index]} onChange={(cough_drink) => this.setState({ ['cough_drink_'+index]:cough_drink })} /> },
                { component: () => <Select data={Options('food_sickness',this.state.lang)} label={Label('cough_eat',this.state.lang,this.state['name_'+index])} selected={this.state['cough_eat_'+index]} onChange={(cough_eat) => this.setState({ ['cough_eat_'+index]:cough_eat })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fever_child',this.state.lang,this.state['name_'+index])} selected={this.state['fever_child_'+index]} onChange={(fever_child) => this.setState({ ['fever_child_'+index]:fever_child })} /> },
                this.state['fever_child_'+index] == 1 && { component: () => <Select data={Options('food_sickness',this.state.lang)} label={Label('fever_child_drink',this.state.lang,this.state['name_'+index])} selected={this.state['fever_child_drink_'+index]} onChange={(fever_child_drink) => this.setState({ ['fever_child_drink_'+index]:fever_child_drink })} /> },
                this.state['fever_child_'+index] == 1 && { component: () => <Select data={Options('food_sickness',this.state.lang)} label={Label('fever_child_eat',this.state.lang,this.state['name_'+index])} selected={this.state['fever_child_eat_'+index]} onChange={(fever_child_eat) => this.setState({ ['fever_child_eat_'+index]:fever_child_eat })} /> },
                this.state['diarrhoea_child_'+index] == 1 && { component: () => <Select data={Options('food_sickness',this.state.lang)} label={Label('diarrhoea_drink',this.state.lang,this.state['name_'+index])} selected={this.state['diarrhoea_drink_'+index]} onChange={(diarrhoea_drink) => this.setState({ ['diarrhoea_drink_'+index]:diarrhoea_drink })} /> },
                this.state['diarrhoea_child_'+index] == 1 && { component: () => <Select data={Options('food_sickness',this.state.lang)} label={Label('diarrhoea_eat',this.state.lang,this.state['name_'+index])} selected={this.state['diarrhoea_eat_'+index]} onChange={(diarrhoea_eat) => this.setState({ ['diarrhoea_eat_'+index]:diarrhoea_eat })} /> },
            ]); 
        });
        
        let c1b_0067 = [];
        this.state.u5_count &&  this.state.u5_count.map((f,index)=>{
            c1b_0091 =c1b_0091.concat([
                this.state['diarrhoea_child_'+index] == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('diarrhoea_fluid_spcl',this.state.lang, this.state['name_'+index])} selected={this.state['diarrhoea_fluid_spcl_'+index]} onChange={(diarrhoea_fluid_spcl) => this.setState({ ['diarrhoea_fluid_spcl_'+index] : diarrhoea_fluid_spcl })} /> },
                this.state['diarrhoea_child_'+index] == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('diarrhoea_fluid_prepak',this.state.lang, this.state['name_'+index])} selected={this.state['diarrhoea_fluid_prepak_'+index]} onChange={(diarrhoea_fluid_prepak) => this.setState({ ['diarrhoea_fluid_prepak_'+index] : diarrhoea_fluid_prepak })} /> },
                this.state['diarrhoea_child_'+index] == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('diarrhoea_fluid_bygovt',this.state.lang, this.state['name_'+index])} selected={this.state['diarrhoea_fluid_bygovt_'+index]} onChange={(diarrhoea_fluid_bygovt) => this.setState({ ['diarrhoea_fluid_bygovt_'+index] : diarrhoea_fluid_bygovt })} /> },
             ]);
        });
        
        let c1b_0087 = [];

        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1b_0087 = c1b_0087.concat([{ component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('diarrhoea_child',this.state.lang, this.state['name_'+index])} selected={this.state['diarrhoea_child_'+index]} onChange={(diarrhoea_child) => this.setState({ ['diarrhoea_child'+index]:diarrhoea_child })} /> }]);
        });
        
        let c1a_21338 = [];

        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1a_21338 = c1a_21338.concat([
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('birthweight_taken',this.state.lang, this.state['name_'+index])} selected={this.state['birthweight_taken_'+index]} onChange={(birthweight_taken) => this.setState({ ['birthweight_taken_'+index]:birthweight_taken })} /> },
                this.state['birthweight_taken_'+index] == 1 && { component: () => <Number label={Label('birth_weight',this.state.lang, this.state['name_'+index])} value={this.state['birth_weight_'+index]} onChange={(birth_weight) => { this.setState({ ['birth_weight_'+index]:birth_weight }) }} />, validation: () => () => this.required(['birth_weight']) },
                this.state['birthweight_taken_'+index] == 1 && { component: () => <Select data={Options('weight_record',this.state.lang)} label={Label('weight_record',this.state.lang, this.state['name_'+index])} selected={this.state['weight_record_'+index]} onChange={(weight_record) => this.setState({ ['weight_record_'+index]:weight_record })} /> },
            ]);
        });
        
        let c1c_0156 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('anc_visit',this.state.lang)} key='anc_visit' selected={this.state.anc_visit} onChange={(anc_visit) => this.setState({ anc_visit })} /> },
            this.state.anc_visit == 1 && { component: () => <Select multiple data={Options('health_provider',this.state.lang)} label={Label('health_provider',this.state.lang)} key='health_provider' selected={this.state.health_provider} onChange={(health_provider) => this.setState({ health_provider })} /> },
            this.state.health_provider == 6 && { component: () => <Input label={Label('health_provider_o',this.state.lang)} key='health_provider_o' value={this.state.health_provider_o} onChange={(health_provider_o) => { this.setState({ health_provider_o }) }} /> },
            this.state.anc_visit == 1 && { component: () => <Number label={Label('anc_times',this.state.lang)} key='anc_times' value={this.state.anc_times} onChange={(anc_times) => { this.setState({ anc_times }) }} />, validation: () => () => this.required(['anc_times']) },
        ];
        
        let c1a_0032 =[
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('iron_taken',this.state.lang)} key='iron_taken' selected={this.state.iron_taken} onChange={(iron_taken) => this.setState({ iron_taken })} /> },
            this.state.iron_taken == 1 && { component: () => <Select data={Options('iron_compliance',this.state.lang)} label={Label('iron_compliance',this.state.lang)} key='iron_compliance' selected={this.state.iron_compliance} onChange={(iron_compliance) => this.setState({ iron_compliance })} /> },
        ];
        
        let c1c_0160 = [];
        
        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1c_0160 = c1c_0160.concat([
                { component: () => <Select data={Options('pnc_visit',this.state.lang)} label={Label('pnc_visit',this.state.lang, this.state['name_'+index])} selected={this.state['pnc_visit_'+index]} onChange={(pnc_visit) => this.setState({ ['pnc_visit_'+index]:pnc_visit })} /> },
                this.state.pnc_visit == 1 && { component: () => <Select data={Options('pnc_visittime',this.state.lang)} label={Label('pnc_visittime',this.state.lang, this.state['name_'+index])} selected={this.state['pnc_visittime_'+index]} onChange={(pnc_visittime) => this.setState({ ['pnc_visittime_'+index]:pnc_visittime })} /> },
                this.state.pnc_visit == 1 && { component: () => <Select data={Options('health_provider',this.state.lang)} label={Label('health_provider',this.state.lang, this.state['name_'+index])} selected={this.state['health_provider_'+index]} onChange={(health_provider) => this.setState({ ['health_provider_'+index]:health_provider })} /> },
                this.state.pnc_visit == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('pnc2_visit',this.state.lang, this.state['name_'+index])} selected={this.state['pnc2_visit_'+index]} onChange={(pnc2_visit) => this.setState({ ['pnc2_visit_'+index]:pnc2_visit })} /> },
                this.state.pnc2_visit == 1 && { component: () => <Select data={Options('pnc_visittime',this.state.lang)} label={Label('pnc2_visittime',this.state.lang, this.state['name_'+index])} selected={this.state['pnc2_visittime_'+index]} onChange={(pnc2_visittime) => this.setState({ ['pnc2_visittime_'+index]:pnc2_visittime })} /> },
                this.state.pnc2_visit == 1 && { component: () => <Select data={Options('health_provider',this.state.lang)} label={Label('pnc2_provider',this.state.lang, this.state['name_'+index])} selected={this.state['pnc2_provider_'+index]} onChange={(pnc2_provider) => this.setState({ ['pnc2_provider_'+index]:pnc2_provider })} /> },
                this.state.pnc2_visit == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('pnc2_babycheck',this.state.lang, this.state['name_'+index])} selected={this.state['pnc2_babycheck_'+index]} onChange={(pnc2_babycheck) => this.setState({ ['pnc2_babycheck_'+index]:pnc2_babycheck })} /> },
            ]);
        });
        let c1a_0048 = [];

        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1a_0048 = c1a_0048.concat([
                { component: () => <Select data={Options('food_pregnancy',this.state.lang)} label={Label('food_pregnancy',this.state.lang,this.state['name_'+index])} selected={this.state['food_pregnancy_'+index]} onChange={(food_pregnancy) => this.setState({ ['food_pregnancy_'+index]:food_pregnancy })} /> },
                this.state.food_pregnancy == 1 && { component: () => <Input label={Label('lessfood_reasons',this.state.lang,this.state['name_'+index])} value={this.state['lessfood_reasons_'+index]} onChange={(lessfood_reasons) => this.setState({ ['lessfood_reasons_'+index]:lessfood_reasons })} /> },
            ]);
        });
        
        let c1a_0033 = [];

        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1a_0033 = c1a_0033.concat([
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ever_breastfeed',this.state.lang, this.state['name_'+index])} selected={this.state['ever_breastfeed_'+index]} onChange={(ever_breastfeed) => this.setState({ ['ever_breastfeed_'+index]:ever_breastfeed })} /> },
                { component: () => <Select data={Options('first_breastfeed',this.state.lang)} label={Label('first_breastfeed',this.state.lang, this.state['name_'+index])} selected={this.state['first_breastfeed_'+index]} onChange={(first_breastfeed) => this.setState({ ['first_breastfeed_'+index]:first_breastfeed })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('pre_lacteal',this.state.lang, this.state['name_'+index])} selected={this.state['pre_lacteal_'+index]} onChange={(pre_lacteal) => this.setState({ ['pre_lacteal_'+index]:pre_lacteal })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('current_breastfed',this.state.lang, this.state['name_'+index])} selected={this.state['current_breastfed_'+index]} onChange={(current_breastfed) => this.setState({ ['current_breastfed_'+index]:current_breastfed })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_water',this.state.lang, this.state['name_'+index])} selected={this.state['drink_water_'+index]} onChange={(drink_water) => this.setState({ ['drink_water_'+index]:drink_water })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_formula',this.state.lang, this.state['name_'+index])} selected={this.state['drink_formula_'+index]} onChange={(drink_formula) => this.setState({ ['drink_formula_'+index]:drink_formula })} /> },
                { component: () => <Number label={Label('formula_times',this.state.lang, this.state['name_'+index])} value={this.state['formula_times_'+index]} onChange={(formula_times) => { this.setState({['formula_times_'+index]: formula_times }) }} />, validation: () => () => this.required(['formula_times']) },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_milk',this.state.lang, this.state['name_'+index])} selected={this.state['drink_milk_'+index]} onChange={(drink_milk) => this.setState({ ['drink_milk_'+index]:drink_milk })} /> },
                { component: () => <Number label={Label('milk_times',this.state.lang, this.state['name_'+index])} value={this.state['milk_times_'+index]} onChange={(milk_times) => { this.setState({['milk_times_'+index]: milk_times }) }} />, validation: () => () => this.required(['milk_times']) },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_juice',this.state.lang, this.state['name_'+index])} selected={this.state['drink_juice_'+index]} onChange={(drink_juice) => this.setState({ ['drink_juice_'+index]:drink_juice })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_vitamin',this.state.lang, this.state['name_'+index])} selected={this.state['drink_vitamin_'+index]} onChange={(drink_vitamin) => this.setState({ ['drink_vitamin_'+index]:drink_vitamin })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_vitamin',this.state.lang, this.state['name_'+index])} selected={this.state['drink_vitamin_'+index]} onChange={(drink_vitamin) => this.setState({ ['drink_vitamin_'+index]:drink_vitamin })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_liquids',this.state.lang, this.state['name_'+index])} selected={this.state['drink_liquids_'+index]} onChange={(drink_liquids) => this.setState({ ['drink_liquids_'+index]:drink_liquids })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('drink_yogurt',this.state.lang, this.state['name_'+index])} selected={this.state['drink_yogurt_'+index]} onChange={(drink_yogurt) => this.setState({ ['drink_yogurt_'+index]:drink_yogurt })} /> },
                this.state.drink_yogurt == 1 && { component: () => <Number label={Label('yogurt_times',this.state.lang, this.state['name_'+index])} value={this.state['yogurt_times_'+index]} onChange={(yogurt_times) => { this.setState({['yogurt_times_'+index]: yogurt_times }) }} />, validation: () => () => this.required(['yogurt_times']) },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('eat_porridge',this.state.lang, this.state['name_'+index])} selected={this.state['eat_porridge_'+index]} onChange={(eat_porridge) => this.setState({ ['eat_porridge_'+index]:eat_porridge })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('eat_solid',this.state.lang, this.state['name_'+index])} selected={this.state['eat_solid_'+index]} onChange={(eat_solid) => this.setState({ ['eat_solid_'+index]:eat_solid })} /> },
                this.state.eat_solid == 1 && { component: () => <Number label={Label('solid_times',this.state.lang, this.state['name_'+index])} value={this.state['solid_times_'+index]} onChange={(solid_times) => { this.setState({['solid_times_'+index]: solid_times }) }} />, validation: () => () => this.required(['solid_times']) },
            ]);
        });

        let c1a_0022 = [];

        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1a_0022 = c1a_0022.concat([
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('grain_root',this.state.lang,this.state['name_'+index])} selected={this.state['grain_root_'+index]} onChange={(grain_root) => this.setState({['grain_root_'+index]:grain_root  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('grain_fortified',this.state.lang,this.state['name_'+index])} selected={this.state['grain_fortified_'+index]} onChange={(grain_fortified) => this.setState({['grain_fortified_'+index]:grain_fortified  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fruits_yellow',this.state.lang,this.state['name_'+index])} selected={this.state['fruits_yellow_'+index]} onChange={(fruits_yellow) => this.setState({['fruits_yellow_'+index]:fruits_yellow  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('fruits_other',this.state.lang,this.state['name_'+index])} selected={this.state['fruits_other_'+index]} onChange={(fruits_other) => this.setState({['fruits_other_'+index]:fruits_other  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('meat',this.state.lang,this.state['name_'+index])} selected={this.state['meat_'+index]} onChange={(meat) => this.setState({['meat_'+index]:meat  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('egg',this.state.lang,this.state['name_'+index])} selected={this.state['egg_'+index]} onChange={(egg) => this.setState({['egg_'+index]:egg  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('legum_nut',this.state.lang,this.state['name_'+index])} selected={this.state['legum_nut_'+index]} onChange={(legum_nut) => this.setState({['legum_nut_'+index]:legum_nut  })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('dairy',this.state.lang,this.state['name_'+index])} selected={this.state['dairy_'+index]} onChange={(dairy) => this.setState({['dairy_'+index]:dairy  })} /> },
            ])
        })
        
        let c1b_0131 = [
              ];

        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1b_0131 = c1b_0131.concat([
                { component: () => <Select data={Options('stool_dispose',this.state.lang)} label={Label('stool_dispose',this.state.lang, this.state['name_'+index])} selected={this.state['stool_dispose_'+index]} onChange={(stool_dispose) => this.setState({ ['stool_dispose_'+index]:stool_dispose })} /> },
                this.state.stool_dispose_o == 77 && { component: () => <Input label={Label('stool_dispose_o',this.state.lang, this.state['name_'+index])} value={this.state['stool_dispose_o_'+index]} onChange={(stool_dispose_o) =>  this.setState({ ['stool_dispose_o_'+index]:stool_dispose_o })} /> },
            ]);
        });
        let c1a_0020 = [];
        this.state.u5_count && this.state.u5_count.map((f,index)=>{
                c1a_0020 = c1a_0020.concat([
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('insects',this.state.lang, this.state['name_'+index])} selected={this.state['insects_'+index]} onChange={(insects) => this.setState({ ['insects'+index]:insects })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('cerelac',this.state.lang, this.state['name_'+index])} selected={this.state['cerelac_'+index]} onChange={(cerelac) => this.setState({ ['cerelac'+index]:cerelac })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('Iron_pil2child_24hr',this.state.lang, this.state['name_'+index])} selected={this.state['Iron_pil2child_24hr_'+index]} onChange={(Iron_pil2child_24hr) => this.setState({ ['Iron_pil2child_24hr'+index]:Iron_pil2child_24hr })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('Iron_pil2child_7dy',this.state.lang, this.state['name_'+index])} selected={this.state['Iron_pil2child_7dy_'+index]} onChange={(Iron_pil2child_7dy) => this.setState({ ['Iron_pil2child_7dy'+index]:Iron_pil2child_7dy })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('Iron_syr2child_24hr',this.state.lang, this.state['name_'+index])} selected={this.state['Iron_syr2child_24hr_'+index]} onChange={(Iron_syr2child_24hr) => this.setState({ ['Iron_syr2child_24hr'+index]:Iron_syr2child_24hr })} /> },
                { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('Iron_syr2child_7dy',this.state.lang, this.state['name_'+index])} selected={this.state['Iron_syr2child_7dy_'+index]} onChange={(Iron_syr2child_7dy) => this.setState({ ['Iron_syr2child_7dy'+index]:Iron_syr2child_7dy })} /> },
            ]);
        });

        let c1b_0065 = []
        this.state.u5_count && this.state.u5_count.map((f,index)=>{
            c1b_0065 = c1b_0065.concat([
            { component: () => <Select data={Options('healthcard',this.state.lang)} label={Label('healthcard',this.state.lang,this.state['name_'+index])} selected={this.state['healthcard_'+index]} onChange={(healthcard) => this.setState({ ['healthcard_'+index]:healthcard })} /> },
            this.state.healthcard == 1 && { component: () => <Number label={Label('dpt_card',this.state.lang,this.state['name_'+index])} value={this.state['dpt_card_'+index]} onChange={(dpt_card) => { this.setState({ ['dpt_card_'+index]:dpt_card }) }} />, validation: () => () => this.required(['dpt_card']) },
            this.state.healthcard == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('measles_card',this.state.lang,this.state['name_'+index])} selected={this.state['measles_card_'+index]} onChange={(measles_card) => this.setState({ ['measles_card_'+index]:measles_card })} /> },
            this.state.healthcard == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('dpt_nocard',this.state.lang,this.state['name_'+index])} selected={this.state['dpt_nocard_'+index]} onChange={(dpt_nocard) => this.setState({ ['dpt_nocard_'+index]:dpt_nocard })} /> },
            { component: () => <Number label={Label('dpt_card',this.state.lang,this.state['name_'+index])} value={this.state['dpt_card_'+index]} onChange={(dpt_card) => { this.setState({ ['dpt_card_'+index]:dpt_card }) }} />, validation: () => () => this.required(['dpt_card']) },
            { component: () => <Number label={Label('measles_recall',this.state.lang,this.state['name_'+index])} value={this.state['measles_recall_'+index]} onChange={(measles_recall) => { this.setState({ ['measles_recall_'+index]:measles_recall }) }} />, validation: () => () => this.required(['measles_recall']) },
        ])});
        
        let c1b_0130 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('water_health',this.state.lang)} key='water_health' selected={this.state.water_health} onChange={(water_health) => this.setState({ water_health })} /> },
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_point_health',this.state.lang)} key='handwash_point_health' selected={this.state.handwash_point_health} onChange={(handwash_point_health) => this.setState({ handwash_point_health })} /> },
            this.state.handwash_point_health == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('handwash_soap_health',this.state.lang)} key='handwash_soap_health' selected={this.state.handwash_soap_health} onChange={(handwash_soap_health) => this.setState({ handwash_soap_health })} /> },
        ];
        
        let c1b_0128 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('soap_use',this.state.lang)} key='soap_use' selected={this.state.soap_use} onChange={(soap_use) => this.setState({ soap_use })} /> },
            this.state.soap_use == 1 && { component: () => <Select multiple data={Options('handwash_time',this.state.lang)} label={Label('handwash_time',this.state.lang)} key='handwash_time' selected={this.state.handwash_time} onChange={(handwash_time) => this.setState({ handwash_time })} /> },
        ];
        
        let c2b_22844 =[
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('achieve_goal',this.state.lang)} key='achieve_goal' selected={this.state.achieve_goal} onChange={(achieve_goal) => this.setState({ achieve_goal })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('accomplish_task',this.state.lang)} key='accomplish_task' selected={this.state.accomplish_task} onChange={(accomplish_task) => this.setState({ accomplish_task })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('important_outcome',this.state.lang)} key='important_outcome' selected={this.state.important_outcome} onChange={(important_outcome) => this.setState({ important_outcome })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('believe_succeed',this.state.lang)} key='believe_succeed' selected={this.state.believe_succeed} onChange={(believe_succeed) => this.setState({ believe_succeed })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('overcome_challenges',this.state.lang)} key='overcome_challenges' selected={this.state.overcome_challenges} onChange={(overcome_challenges) => this.setState({ overcome_challenges })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('confident_errform',this.state.lang)} key='confident_errform' selected={this.state.confident_errform} onChange={(confident_errform) => this.setState({ confident_errform })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('do_well',this.state.lang)} key='do_well' selected={this.state.do_well} onChange={(do_well) => this.setState({ do_well })} /> },
            { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('tough_perform',this.state.lang)} key='tough_perform' selected={this.state.tough_perform} onChange={(tough_perform) => this.setState({ tough_perform })} /> },
        ];
        
        let c3c_22869 = [{ component: () => <Select data={Options('agreement',this.state.lang)} label={Label('responsible_community',this.state.lang)} key='responsible_community' selected={this.state.responsible_community} onChange={(responsible_community) => this.setState({ responsible_community })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('make_difference',this.state.lang)} key='make_difference' selected={this.state.make_difference} onChange={(make_difference) => this.setState({ make_difference })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('responsible_poor',this.state.lang)} key='responsible_poor' selected={this.state.responsible_poor} onChange={(responsible_poor) => this.setState({ responsible_poor })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('committed_serve',this.state.lang)} key='committed_serve' selected={this.state.committed_serve} onChange={(committed_serve) => this.setState({ committed_serve })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('citizens_responsibility',this.state.lang)} key='citizens_responsibility' selected={this.state.citizens_responsibility} onChange={(citizens_responsibility) => this.setState({ citizens_responsibility })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('infromed_issue',this.state.lang)} key='infromed_issue' selected={this.state.infromed_issue} onChange={(infromed_issue) => this.setState({ infromed_issue })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('believe_volunteer',this.state.lang)} key='believe_volunteer' selected={this.state.believe_volunteer} onChange={(believe_volunteer) => this.setState({ believe_volunteer })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('believe_support',this.state.lang)} key='believe_support' selected={this.state.believe_support} onChange={(believe_support) => this.setState({ believe_support })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('structured_volunteer',this.state.lang)} key='structured_volunteer' selected={this.state.structured_volunteer} onChange={(structured_volunteer) => this.setState({ structured_volunteer })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('positive_changes',this.state.lang)} key='positive_changes' selected={this.state.positive_changes} onChange={(positive_changes) => this.setState({ positive_changes })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('help_community',this.state.lang)} key='help_community' selected={this.state.help_community} onChange={(help_community) => this.setState({ help_community })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('stay_informed',this.state.lang)} key='stay_informed' selected={this.state.stay_informed} onChange={(stay_informed) => this.setState({ stay_informed })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('discussions_social',this.state.lang)} key='discussions_social' selected={this.state.discussions_social} onChange={(discussions_social) => this.setState({ discussions_social })} /> },
        { component: () => <Select data={Options('agreement',this.state.lang)} label={Label('contribute_charitable',this.state.lang)} key='contribute_charitable' selected={this.state.contribute_charitable} onChange={(contribute_charitable) => this.setState({ contribute_charitable })} /> }];

        let c2c_0293 = [
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('resume_write',this.state.lang)} key='resume_write' selected={this.state.resume_write} onChange={(resume_write) => this.setState({ resume_write })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('saving_plan',this.state.lang)} key='saving_plan' selected={this.state.saving_plan} onChange={(saving_plan) => this.setState({ saving_plan })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('application_letter',this.state.lang)} key='application_letter' selected={this.state.application_letter} onChange={(application_letter) => this.setState({ application_letter })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('job_interview',this.state.lang)} key='job_interview' selected={this.state.job_interview} onChange={(job_interview) => this.setState({ job_interview })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('personal_mentor',this.state.lang)} key='personal_mentor' selected={this.state.personal_mentor} onChange={(personal_mentor) => this.setState({ personal_mentor })} /> },
        ];
        
        let c4a_0014 = [
            { component: () => <Select data={Options('married',this.state.lang)} label={Label('ever_married',this.state.lang)} key='ever_married' selected={this.state.ever_married} onChange={(ever_married) => this.setState({ ever_married })} /> },
            this.state.ever_married != 2 && { component: () => <Number label={Label('age_married',this.state.lang)} key='age_married' value={this.state.age_married} onChange={(age_married) => { this.setState({ age_married }) }} />, validation: () => () => this.required(['age_married']) },
        ];
        
        let c4a_0221 =[
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('standing',this.state.lang)} key='standing' selected={this.state.standing} onChange={(standing) => this.setState({ standing })} /> },
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('shouted',this.state.lang)} key='shouted' selected={this.state.shouted} onChange={(shouted) => this.setState({ shouted })} /> },
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('swore',this.state.lang)} key='swore' selected={this.state.swore} onChange={(swore) => this.setState({ swore })} /> },
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('slapped',this.state.lang)} key='slapped' selected={this.state.slapped} onChange={(slapped) => this.setState({ slapped })} /> },
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('hit_object',this.state.lang)} key='hit_object' selected={this.state.hit_object} onChange={(hit_object) => this.setState({ hit_object })} /> },
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('punched',this.state.lang)} key='punched' selected={this.state.punched} onChange={(punched) => this.setState({ punched })} /> },
            { component: () => <Select data={Options('hurt',this.state.lang)} label={Label('hurt_physically',this.state.lang)} key='hurt_physically' selected={this.state.hurt_physically} onChange={(hurt_physically) => this.setState({ hurt_physically })} /> },
            { component: () => <Select data={Options('beaten',this.state.lang)} label={Label('beaten_family',this.state.lang)} key='beaten_family' selected={this.state.beaten_family} onChange={(beaten_family) => this.setState({ beaten_family })} /> },
            { component: () => <Select data={Options('beaten',this.state.lang)} label={Label('beaten_adult',this.state.lang)} key='beaten_adult' selected={this.state.beaten_adult} onChange={(beaten_adult) => this.setState({ beaten_adult })} /> },
            { component: () => <Select data={Options('beaten',this.state.lang)} label={Label('beaten_bgfriend',this.state.lang)} key='beaten_bgfriend' selected={this.state.beaten_bgfriend} onChange={(beaten_bgfriend) => this.setState({ beaten_bgfriend })} /> },
            { component: () => <Select data={Options('beaten',this.state.lang)} label={Label('beaten_friends',this.state.lang)} key='beaten_friends' selected={this.state.beaten_friends} onChange={(beaten_friends) => this.setState({ beaten_friends })} /> },
            { component: () => <Select data={Options('beaten',this.state.lang)} label={Label('beaten_dnk',this.state.lang)} key='beaten_dnk' selected={this.state.beaten_dnk} onChange={(beaten_dnk) => this.setState({ beaten_dnk })} /> },
        ];
        
        let c4a_23441 =[
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('women_imrole',this.state.lang)} key='women_imrole' selected={this.state.women_imrole} onChange={(women_imrole) => this.setState({ women_imrole })} /> },
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('woman_beateb',this.state.lang)} key='woman_beateb' selected={this.state.woman_beateb} onChange={(woman_beateb) => this.setState({ woman_beateb })} /> },
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('mother_responsibility',this.state.lang)} key='mother_responsibility' selected={this.state.mother_responsibility} onChange={(mother_responsibility) => this.setState({ mother_responsibility })} /> },
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('avoid_pregnant',this.state.lang)} key='avoid_pregnant' selected={this.state.avoid_pregnant} onChange={(avoid_pregnant) => this.setState({ avoid_pregnant })} /> },
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('man_decision',this.state.lang)} key='man_decision' selected={this.state.man_decision} onChange={(man_decision) => this.setState({ man_decision })} /> },
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('violence_tolerate',this.state.lang)} key='violence_tolerate' selected={this.state.violence_tolerate} onChange={(violence_tolerate) => this.setState({ violence_tolerate })} /> },
            { component: () => <Select data={Options('gems_response',this.state.lang)} label={Label('man_tough',this.state.lang)} key='man_tough' selected={this.state.man_tough} onChange={(man_tough) => this.setState({ man_tough })} /> },
            { component: () => <Select data={Options('gems_response_reverse',this.state.lang)} label={Label('contraceptive_use',this.state.lang)} key='contraceptive_use' selected={this.state.contraceptive_use} onChange={(contraceptive_use) => this.setState({ contraceptive_use })} /> },
            { component: () => <Select data={Options('gems_response_reverse',this.state.lang)} label={Label('child_responsibility',this.state.lang)} key='child_responsibility' selected={this.state.child_responsibility} onChange={(child_responsibility) => this.setState({ child_responsibility })} /> },
            { component: () => <Select data={Options('gems_response_reverse',this.state.lang)} label={Label('raising_children',this.state.lang)} key='raising_children' selected={this.state.raising_children} onChange={(raising_children) => this.setState({ raising_children })} /> },
            { component: () => <Select data={Options('gems_response_reverse',this.state.lang)} label={Label('men_friend',this.state.lang)} key='men_friend' selected={this.state.men_friend} onChange={(men_friend) => this.setState({ men_friend })} /> },
            { component: () => <Select data={Options('gems_response_reverse',this.state.lang)} label={Label('couple_decide',this.state.lang)} key='couple_decide' selected={this.state.couple_decide} onChange={(couple_decide) => this.setState({ couple_decide })} /> },
        ];
        
        let c4a_0219 =[
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('bad_happening',this.state.lang)} key='bad_happening' selected={this.state.bad_happening} onChange={(bad_happening) => this.setState({ bad_happening })} /> },
            this.state.bad_happening == 2 && { component: () => <Select data={Options('prevent_reasons',this.state.lang)} label={Label('prevent_reasons',this.state.lang)} key='prevent_reasons' selected={this.state.prevent_reasons} onChange={(prevent_reasons) => this.setState({ prevent_reasons })} /> },
            this.state.prevent_reasons == 77 && { component: () => <Input label={Label('prevent_reasons_other',this.state.lang)} key='prevent_reasons_other' value={this.state.prevent_reasons_other} onChange={(prevent_reasons_other) => { this.setState({ prevent_reasons_other }) }} /> },
        ];
        
        let c4c_23075 =[
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('used_service',this.state.lang)} key='used_service' selected={this.state.used_service} onChange={(used_service) => this.setState({ used_service })} /> },
            this.state.used_service == 1 && { component: () => <Select data={Options('quality_service',this.state.lang)} label={Label('quality_service',this.state.lang)} key='quality_service' selected={this.state.quality_service} onChange={(quality_service) => this.setState({ quality_service })} /> },
            { component: () => <Select data={Options('quality_service',this.state.lang)} label={Label('access',this.state.lang)} key='access' selected={this.state.access} onChange={(access) => this.setState({ access })} /> },
            { component: () => <Select data={Options('quality_service',this.state.lang)} label={Label('affordability',this.state.lang)} key='affordability' selected={this.state.affordability} onChange={(affordability) => this.setState({ affordability })} /> },
            { component: () => <Select data={Options('quality_service',this.state.lang)} label={Label('availability',this.state.lang)} key='availability' selected={this.state.availability} onChange={(availability) => this.setState({ availability })} /> },
            { component: () => <Select data={Options('quality_service',this.state.lang)} label={Label('accommodation',this.state.lang)} key='accommodation' selected={this.state.accommodation} onChange={(accommodation) => this.setState({ accommodation })} /> },
        ];
        
        let c4a_22920 =[
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('protection_service',this.state.lang)} key='protection_service' selected={this.state.protection_service} onChange={(protection_service) => this.setState({ protection_service })} /> },
            this.state.protection_service && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('satisfied_responsiveness',this.state.lang)} key='satisfied_responsiveness' selected={this.state.satisfied_responsiveness} onChange={(satisfied_responsiveness) => this.setState({ satisfied_responsiveness })} /> },
        ];
        
        let c4a_22917 = [
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('reported_case',this.state.lang)} key='reported_case' selected={this.state.reported_case} onChange={(reported_case) => this.setState({ reported_case })} /> },
            this.state.reported_case == 1 && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('satisfied_service',this.state.lang)} key='satisfied_service' selected={this.state.satisfied_service} onChange={(satisfied_service) => this.setState({ satisfied_service })} /> },
        ];
        
        let c4a_0196 = [
            (this.state.ever_married == 1 || this.state.ever_married == 3) && { component: () => <Select data={Options('feel_children',this.state.lang)} label={Label('feel_children',this.state.lang)} key='feel_children' selected={this.state.feel_children} onChange={(feel_children) => this.setState({ feel_children })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.feel_children == 3 && { component: () => <Select data={Options('reason_notsafe',this.state.lang)} label={Label('reason_notsafe',this.state.lang)} key='reason_notsafe' selected={this.state.reason_notsafe} onChange={(reason_notsafe) => this.setState({ reason_notsafe })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.reason_notsafe == 77 && { component: () => <Input label={Label('reason_notsafe_other',this.state.lang)} key='reason_notsafe_other' value={this.state.reason_notsafe_other} onChange={(reason_notsafe_other) => { this.setState({ reason_notsafe_other }) }} /> },
        ];
        
        let c4a_0190 =  [
            (this.state.ever_married == 1 || this.state.ever_married == 3) && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('do_work',this.state.lang)} key='do_work' selected={this.state.do_work} onChange={(do_work) => this.setState({ do_work })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.do_work == 1 && { component: () => <Select data={Options('pay_cash',this.state.lang)} label={Label('pay_cash',this.state.lang)} key='pay_cash' selected={this.state.pay_cash} onChange={(pay_cash) => this.setState({ pay_cash })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.pay_case != 2 && { component: () => <Number label={Label('work_hours_other',this.state.lang)} key='work_hours_other' value={this.state.work_hours_other} onChange={(work_hours_other) => { this.setState({ work_hours_other }) }} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('family_work',this.state.lang)} key='family_work' selected={this.state.family_work} onChange={(family_work) => this.setState({ family_work })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.family_work == 1 && { component: () => <Number label={Label('family_work_hours',this.state.lang)} key='family_work_hours' value={this.state.family_work_hours} onChange={(family_work_hours) => { this.setState({ family_work_hours }) }} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('house_work',this.state.lang)} key='house_work' selected={this.state.house_work} onChange={(house_work) => this.setState({ house_work })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.house_work == 1 && { component: () => <Number label={Label('house_work_hours',this.state.lang)} key='house_work_hours' value={this.state.house_work_hours} onChange={(house_work_hours) => { this.setState({ house_work_hours }) }} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('house_chores',this.state.lang)} key='house_chores' selected={this.state.house_chores} onChange={(house_chores) => this.setState({ house_chores })} /> },
            (this.state.ever_married == 1 || this.state.ever_married == 3) && this.state.house_chores == 1 && { component: () => <Number label={Label('house_chores_hours',this.state.lang)} key='house_chores_hours' value={this.state.house_chores_hours} onChange={(house_chores_hours) => { this.setState({ house_chores_hours }) }} /> },
        ];
        
        let c4a_23443 =[
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('available_services',this.state.lang)} key='available_services' selected={this.state.available_services} onChange={(available_services) => this.setState({ available_services })} /> },
            this.state.available_services == 1 && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('condident_access',this.state.lang)} key='condident_access' selected={this.state.condident_access} onChange={(condident_access) => this.setState({ condident_access })} /> },
        ];
        
        let c4a_21415 = [
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('vision_cwb',this.state.lang)} key='vision_cwb' selected={this.state.vision_cwb} onChange={(vision_cwb) => this.setState({ vision_cwb })} /> },
            this.state.vision_cwb == 1 && { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('share_cwbvision',this.state.lang)} key='share_cwbvision' selected={this.state.share_cwbvision} onChange={(share_cwbvision) => this.setState({ share_cwbvision })} /> },
        ];

        let c4b_24262 = [
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ldrm_exist',this.state.lang)} key='ldrm_exist' selected={this.state.ldrm_exist} onChange={(ldrm_exist) => this.setState({ ldrm_exist })} /> },
            this.state.ldrm_exist == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ldrmp_developed',this.state.lang)} key='ldrmp_developed' selected={this.state.ldrmp_developed} onChange={(ldrmp_developed) => this.setState({ ldrmp_developed })} /> },
            this.state.ldrmp_developed == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ldrmp_guideline',this.state.lang)} key='ldrmp_guideline' selected={this.state.ldrmp_guideline} onChange={(ldrmp_guideline) => this.setState({ ldrmp_guideline })} /> },
            this.state.ldrmp_guideline == 1 && { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('ldrmp_implemented',this.state.lang)} key='ldrmp_implemented' selected={this.state.ldrmp_implemented} onChange={(ldrmp_implemented) => this.setState({ ldrmp_implemented })} /> },
        ];

        let c4a_0165 = [
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d1',this.state.lang)} key='d1' selected={this.state.d1} onChange={(d1) => this.setState({ d1 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d2',this.state.lang)} key='d2' selected={this.state.d2} onChange={(d2) => this.setState({ d2 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d3',this.state.lang)} key='d3' selected={this.state.d3} onChange={(d3) => this.setState({ d3 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d4',this.state.lang)} key='d4' selected={this.state.d4} onChange={(d4) => this.setState({ d4 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d5',this.state.lang)} key='d5' selected={this.state.d5} onChange={(d5) => this.setState({ d5 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d6',this.state.lang)} key='d6' selected={this.state.d6} onChange={(d6) => this.setState({ d6 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d7',this.state.lang)} key='d7' selected={this.state.d7} onChange={(d7) => this.setState({ d7 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d8',this.state.lang)} key='d8' selected={this.state.d8} onChange={(d8) => this.setState({ d8 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d9',this.state.lang)} key='d9' selected={this.state.d9} onChange={(d9) => this.setState({ d9 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d10',this.state.lang)} key='d10' selected={this.state.d10} onChange={(d10) => this.setState({ d10 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d11',this.state.lang)} key='d11' selected={this.state.d11} onChange={(d11) => this.setState({ d11 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d12',this.state.lang)} key='d12' selected={this.state.d12} onChange={(d12) => this.setState({ d12 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d13',this.state.lang)} key='d13' selected={this.state.d13} onChange={(d13) => this.setState({ d13 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d14',this.state.lang)} key='d14' selected={this.state.d14} onChange={(d14) => this.setState({ d14 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d15',this.state.lang)} key='d15' selected={this.state.d15} onChange={(d15) => this.setState({ d15 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d16',this.state.lang)} key='d16' selected={this.state.d16} onChange={(d16) => this.setState({ d16 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d17',this.state.lang)} key='d17' selected={this.state.d17} onChange={(d17) => this.setState({ d17 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d18',this.state.lang)} key='d18' selected={this.state.d18} onChange={(d18) => this.setState({ d18 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d19',this.state.lang)} key='d19' selected={this.state.d19} onChange={(d19) => this.setState({ d19 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d20',this.state.lang)} key='d20' selected={this.state.d20} onChange={(d20) => this.setState({ d20 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d21',this.state.lang)} key='d21' selected={this.state.d21} onChange={(d21) => this.setState({ d21 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d22',this.state.lang)} key='d22' selected={this.state.d22} onChange={(d22) => this.setState({ d22 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d23',this.state.lang)} key='d23' selected={this.state.d23} onChange={(d23) => this.setState({ d23 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d24',this.state.lang)} key='d24' selected={this.state.d24} onChange={(d24) => this.setState({ d24 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d25',this.state.lang)} key='d25' selected={this.state.d25} onChange={(d25) => this.setState({ d25 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d26',this.state.lang)} key='d26' selected={this.state.d26} onChange={(d26) => this.setState({ d26 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d27',this.state.lang)} key='d27' selected={this.state.d27} onChange={(d27) => this.setState({ d27 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d28',this.state.lang)} key='d28' selected={this.state.d28} onChange={(d28) => this.setState({ d28 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d29',this.state.lang)} key='d29' selected={this.state.d29} onChange={(d29) => this.setState({ d29 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d30',this.state.lang)} key='d30' selected={this.state.d30} onChange={(d30) => this.setState({ d30 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d31',this.state.lang)} key='d31' selected={this.state.d31} onChange={(d31) => this.setState({ d31 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d32',this.state.lang)} key='d32' selected={this.state.d32} onChange={(d32) => this.setState({ d32 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d33',this.state.lang)} key='d33' selected={this.state.d33} onChange={(d33) => this.setState({ d33 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d34',this.state.lang)} key='d34' selected={this.state.d34} onChange={(d34) => this.setState({ d34 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d35',this.state.lang)} key='d35' selected={this.state.d35} onChange={(d35) => this.setState({ d35 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d36',this.state.lang)} key='d36' selected={this.state.d36} onChange={(d36) => this.setState({ d36 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d37',this.state.lang)} key='d37' selected={this.state.d37} onChange={(d37) => this.setState({ d37 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d38',this.state.lang)} key='d38' selected={this.state.d38} onChange={(d38) => this.setState({ d38 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d39',this.state.lang)} key='d39' selected={this.state.d39} onChange={(d39) => this.setState({ d39 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d40',this.state.lang)} key='d40' selected={this.state.d40} onChange={(d40) => this.setState({ d40 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d41',this.state.lang)} key='d41' selected={this.state.d41} onChange={(d41) => this.setState({ d41 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d42',this.state.lang)} key='d42' selected={this.state.d42} onChange={(d42) => this.setState({ d42 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d43',this.state.lang)} key='d43' selected={this.state.d43} onChange={(d43) => this.setState({ d43 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d44',this.state.lang)} key='d44' selected={this.state.d44} onChange={(d44) => this.setState({ d44 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d45',this.state.lang)} key='d45' selected={this.state.d45} onChange={(d45) => this.setState({ d45 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d46',this.state.lang)} key='d46' selected={this.state.d46} onChange={(d46) => this.setState({ d46 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d47',this.state.lang)} key='d47' selected={this.state.d47} onChange={(d47) => this.setState({ d47 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d48',this.state.lang)} key='d48' selected={this.state.d48} onChange={(d48) => this.setState({ d48 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d49',this.state.lang)} key='d49' selected={this.state.d49} onChange={(d49) => this.setState({ d49 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d50',this.state.lang)} key='d50' selected={this.state.d50} onChange={(d50) => this.setState({ d50 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d51',this.state.lang)} key='d51' selected={this.state.d51} onChange={(d51) => this.setState({ d51 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d52',this.state.lang)} key='d52' selected={this.state.d52} onChange={(d52) => this.setState({ d52 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d53',this.state.lang)} key='d53' selected={this.state.d53} onChange={(d53) => this.setState({ d53 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d54',this.state.lang)} key='d54' selected={this.state.d54} onChange={(d54) => this.setState({ d54 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d55',this.state.lang)} key='d55' selected={this.state.d55} onChange={(d55) => this.setState({ d55 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d56',this.state.lang)} key='d56' selected={this.state.d56} onChange={(d56) => this.setState({ d56 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d57',this.state.lang)} key='d57' selected={this.state.d57} onChange={(d57) => this.setState({ d57 })} /> },
            { component: () => <Select data={Options('dap_response',this.state.lang)} label={Label('d58',this.state.lang)} key='d58' selected={this.state.d58} onChange={(d58) => this.setState({ d58 })} /> },
        ];
        
        let c4b_0044 = [
            { component: () => <Select key='provide_clothes' selected={this.state.provide_clothes} onChange={provide_clothes => this.setState({ provide_clothes })} label={Label('provide_clothes', this.state.lang)} data={Options('provide', this.state.lang)} /> },
            this.state.provide_clothes == 2 && { component: () => <Select data={Options('children_group',this.state.lang)} label={Label('difficult_clothes',this.state.lang)} key='difficult_clothes' selected={this.state.difficult_clothes} onChange={(difficult_clothes) => this.setState({ difficult_clothes })} /> },
            { component: () => <Select key='provide_shoes' selected={this.state.provide_shoes} onChange={provide_shoes => this.setState({ provide_shoes })} label={Label('provide_shoes', this.state.lang)} data={Options('provide', this.state.lang)} /> },
            this.state.provide_shoes == 2 && { component: () => <Select data={Options('children_group',this.state.lang)} label={Label('difficult_shoes',this.state.lang)} key='difficult_shoes' selected={this.state.difficult_shoes} onChange={(difficult_shoes) => this.setState({ difficult_shoes })} /> },
            { component: () => <Select key='provide_blanket' selected={this.state.provide_blanket} onChange={provide_blanket => this.setState({ provide_blanket })} label={Label('provide_blanket', this.state.lang)} data={Options('provide', this.state.lang)} /> },
            this.state.provide_blanket == 2 && { component: () => <Select data={Options('children_group',this.state.lang)} label={Label('difficult_blanket',this.state.lang)} key='difficult_blanket' selected={this.state.difficult_blanket} onChange={(difficult_blanket) => this.setState({ difficult_blanket })} /> },
            { component: () => <Select data={Options('yes_no',this.state.lang)} label={Label('check_orphan',this.state.lang)} key='check_orphan' selected={this.state.check_orphan} onChange={(check_orphan) => this.setState({ check_orphan })} /> },
            { component: () => <Select multiple data={Options('orphan_discrimination',this.state.lang)} label={Label('orphan_discrimination',this.state.lang)} key='orphan_discrimination' selected={this.state.orphan_discrimination} onChange={(orphan_discrimination) => this.setState({ orphan_discrimination })} /> },
        ];

        let c4b_0069 =[
            { component: () => <Select data={Options('yes_no_dk',this.state.lang)} label={Label('saving_money',this.state.lang)} key='saving_money' selected={this.state.saving_money} onChange={(saving_money) => this.setState({ saving_money })} /> },
        ];
        
        let c4b_21068 = [
            { component: () => <Select key='market_facilitator' selected={this.state.market_facilitator} onChange={market_facilitator => this.setState({ market_facilitator })} label={Label('market_facilitator', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Input key='group_leader' value={this.state.group_leader} onChange={group_leader => this.setState({ group_leader })} label={Label('group_leader', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Input key='location' value={this.state.location} onChange={location => this.setState({ location })} label={Label('location', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Input key='date' value={this.state.date} onChange={date => this.setState({ date })} label={Label('date', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Select key='production_group' selected={this.state.production_group} onChange={production_group => this.setState({ production_group })} label={Label('production_group', this.state.lang)} data={Options('production_group', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Select key='volumn' selected={this.state.volumn} onChange={volumn => this.setState({ volumn })} label={Label('volumn', this.state.lang)} data={Options('volumn', this.state.lang)} /> },
            this.state.market_facilitator == 1 && this.state.volumn == 77 && { component: () => <Input key='volumn_other' value={this.state.volumn_other} onChange={volumn_other => this.setState({ volumn_other })} label={Label('volumn_other', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Number key='land' value={this.state.land} onChange={land => this.setState({ land })} label={Label('land', this.state.lang)} /> },
            this.state.market_facilitator == 1 && { component: () => <Select key='area' selected={this.state.area} onChange={area => this.setState({ area })} label={Label('area', this.state.lang)} data={Options('area', this.state.lang)} /> },
            { component: () => <Select key='access_financial' selected={this.state.access_financial} onChange={access_financial => this.setState({ access_financial })} label={Label('access_financial', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            { component: () => <Select key='technical_service' selected={this.state.technical_service} onChange={technical_service => this.setState({ technical_service })} label={Label('technical_service', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            { component: () => <Select key='purchase_inputs' selected={this.state.purchase_inputs} onChange={purchase_inputs => this.setState({ purchase_inputs })} label={Label('purchase_inputs', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            { component: () => <Number key='collected_together' value={this.state.collected_together} onChange={collected_together => this.setState({ collected_together })} label={Label('collected_together', this.state.lang)} /> },
            { component: () => <Number key='processed_product' value={this.state.processed_product} onChange={processed_product => this.setState({ processed_product })} label={Label('processed_product', this.state.lang)} /> },
            { component: () => <Number key='processed_cost' value={this.state.processed_cost} onChange={processed_cost => this.setState({ processed_cost })} label={Label('processed_cost', this.state.lang)} /> },
            { component: () => <Number key='unprocessed_cost' value={this.state.unprocessed_cost} onChange={unprocessed_cost => this.setState({ unprocessed_cost })} label={Label('unprocessed_cost', this.state.lang)} /> },
            { component: () => <Number key='men_member' value={this.state.men_member} onChange={men_member => this.setState({ men_member })} label={Label('men_member', this.state.lang)} /> },
            { component: () => <Number key='women_member' value={this.state.women_member} onChange={women_member => this.setState({ women_member })} label={Label('women_member', this.state.lang)} /> },
            { component: () => <Number key='total_group_production' value={this.state.total_group_production} onChange={total_group_production => this.setState({ total_group_production })} label={Label('total_group_production', this.state.lang)} /> },
            { component: () => <Number key='total_cost_production' value={this.state.total_cost_production} onChange={total_cost_production => this.setState({ total_cost_production })} label={Label('total_cost_production', this.state.lang)} /> },
            { component: () => <Number key='total_growing_area' value={this.state.total_growing_area} onChange={total_growing_area => this.setState({ total_growing_area })} label={Label('total_growing_area', this.state.lang)} /> },
            { component: () => <Number key='quantity_individually' value={this.state.quantity_individually} onChange={quantity_individually => this.setState({ quantity_individually })} label={Label('quantity_individually', this.state.lang)} /> },
            { component: () => <Number key='goods' value={this.state.goods} onChange={goods => this.setState({ goods })} label={Label('goods', this.state.lang)} /> },
            { component: () => <Number key='average_price' value={this.state.average_price} onChange={average_price => this.setState({ average_price })} label={Label('average_price', this.state.lang)} /> },
        ];
        
        let c4b_21071 = [
            { component: () => <Select key='grow_food' selected={this.state.grow_food} onChange={grow_food => this.setState({ grow_food })} label={Label('grow_food', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.grow_food == 1 && { component: () => <Select multiple key='crops' selected={this.state.crops || []} onChange={crops => this.setState({ crops })} label={Label('crops', this.state.lang)} data={Options('crops', this.state.lang)} /> },
            this.state.crops && this.state.crops.includes(77) && { component: () => <Input key='crops_other' value={this.state.crops_other} onChange={crops_other => this.setState({ crops_other })} label={Label('crops_other', this.state.lang)} /> },
            ...(this.state.crops || []).map((crop) => (
                {
                    component: () => [
                        <Number key="1" value={this.state['crop_details_quantity_grown_' + crop]} onChange={quantity_grown => this.setState({ ['crop_details_quantity_grown_' + crop]: quantity_grown })} label={Label('quantity_grown', this.state.lang, Options('crops', this.state.lang, crop))} />,
                        <Input key="2" value={this.state['crop_details_unit_grown_' + crop]} onChange={unit_grown => this.setState({ ['crop_details_unit_grown_' + crop]: unit_grown })} label={Label('unit_grown', this.state.lang, Options('crops', this.state.lang, crop))} />,
                    ]
                }
            )),
            { component: () => <Select key='food_sale' selected={this.state.food_sale} onChange={food_sale => this.setState({ food_sale })} label={Label('food_sale', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.food_sale == 1 && { component: () => <Select multiple key='crops_sale' selected={this.state.crops_sale || []} onChange={crops_sale => this.setState({ crops_sale })} label={Label('crops_sale', this.state.lang)} data={Options('crops', this.state.lang)} /> },
            this.state.crops_sale && this.state.crops_sale.includes(77) && { component: () => <Input key='crops_sale_other' value={this.state.crops_sale_other} onChange={crops_sale_other => this.setState({ crops_sale_other })} label={Label('crops_sale_other', this.state.lang)} /> },
            ...(this.state.crops_sale || []).map((crop) => (
                {
                    component: () => [
                        <Number key="1" value={this.state['sale_details_quantity_sale_' + crop]} onChange={quantity_sale => this.setState({ ['sale_details_quantity_sale_' + crop]: quantity_sale })} label={Label('quantity_sale', this.state.lang, Options('crops', this.state.lang, crop))} />,
                        <Number key="1" value={this.state['sale_details_price_sale_' + crop]} onChange={price_sale => this.setState({ ['sale_details_price_sale_' + crop]: price_sale })} label={Label('price_sale', this.state.lang, Options('crops', this.state.lang, crop))} />,
                        <Input key="2" value={this.state['sale_details_unit_sale_' + crop]} onChange={unit_sale => this.setState({ ['sale_details_unit_sale_' + crop]: unit_sale })} label={Label('unit_sale', this.state.lang, Options('crops', this.state.lang, crop))} />,
                    ]
                }
            )),
            { component: () => <Select key='sale_location' selected={this.state.sale_location} onChange={sale_location => this.setState({ sale_location })} label={Label('sale_location', this.state.lang)} data={Options('sale_location', this.state.lang)} /> },
            this.state.sale_location == 77 && { component: () => <Input key='sale_location_other' value={this.state.sale_location_other} onChange={sale_location_other => this.setState({ sale_location_other })} label={Label('sale_location_other', this.state.lang)} /> },
            { component: () => <Select key='other_saving' selected={this.state.other_saving} onChange={other_saving => this.setState({ other_saving })} label={Label('other_saving', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            { component: () => <Select key='recent_loan' selected={this.state.recent_loan} onChange={recent_loan => this.setState({ recent_loan })} label={Label('recent_loan', this.state.lang)} data={Options('recent_loan', this.state.lang)} /> },
            this.state.recent_loan == 77 && { component: () => <Input key='recent_loan_other' value={this.state.recent_loan_other} onChange={recent_loan_other => this.setState({ recent_loan_other })} label={Label('recent_loan_other', this.state.lang)} /> },
            this.state.recent_loan != 1 && { component: () => <Select key='financing_require' selected={this.state.financing_require} onChange={financing_require => this.setState({ financing_require })} label={Label('financing_require', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.financing_require == 1 && { component: () => <Select key='collateral_type' selected={this.state.collateral_type} onChange={collateral_type => this.setState({ collateral_type })} label={Label('collateral_type', this.state.lang)} data={Options('collateral_type', this.state.lang)} /> },
            this.state.collateral_type == 77 && { component: () => <Input key='collateral_type_other' value={this.state.collateral_type_other} onChange={collateral_type_other => this.setState({ collateral_type_other })} label={Label('collateral_type_other', this.state.lang)} /> },
            this.state.recent_loan != 1 && { component: () => <Select key='loan' selected={this.state.loan} onChange={loan => this.setState({ loan })} label={Label('loan', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.loan == 2 && { component: () => <Select key='loan_apply' selected={this.state.loan_apply} onChange={loan_apply => this.setState({ loan_apply })} label={Label('loan_apply', this.state.lang)} data={Options('apply', this.state.lang)} /> },
            this.state.loan_apply == 77 && { component: () => <Input key='loan_apply_other' value={this.state.loan_apply_other} onChange={loan_apply_other => this.setState({ loan_apply_other })} label={Label('loan_apply_other', this.state.lang)} /> },
            { component: () => <Select key='crop_insurence' selected={this.state.crop_insurence} onChange={crop_insurence => this.setState({ crop_insurence })} label={Label('crop_insurence', this.state.lang)} data={Options('yes_no', this.state.lang)} /> },
            this.state.crop_insurence == 2 && { component: () => <Select key='crop_insurence_why' selected={this.state.crop_insurence_why} onChange={crop_insurence_why => this.setState({ crop_insurence_why })} label={Label('crop_insurence_why', this.state.lang)} data={Options('apply', this.state.lang)} /> },
            this.state.crop_insurence_why == 77 && { component: () => <Input key='insurence_why_other' value={this.state.insurence_why_other} onChange={insurence_why_other => this.setState({ insurence_why_other })} label={Label('insurence_why_other', this.state.lang)} /> },
            { component: () => <Select key='major_source' selected={this.state.major_source} onChange={major_source => this.setState({ major_source })} label={Label('major_source', this.state.lang)} data={Options('source', this.state.lang)} /> },
            this.state.major_source == 77 && { component: () => <Input key='major_source_other' value={this.state.major_source_other} onChange={major_source_other => this.setState({ major_source_other })} label={Label('major_source_other', this.state.lang)} /> },
            { component: () => <Select multiple key='alternative_source' selected={this.state.alternative_source} onChange={alternative_source => this.setState({ alternative_source })} label={Label('alternative_source', this.state.lang)} data={Options('source', this.state.lang)} /> },
            this.state.alternative_source && { component: () => <Input key='alternative_source_other' value={this.state.alternative_source_other} onChange={alternative_source_other => this.setState({ alternative_source_other })} label={Label('alternative_source_other', this.state.lang)} /> },
            { component: () => <Number key='estimated_earning' value={this.state.estimated_earning} onChange={estimated_earning => this.setState({ estimated_earning })} label={Label('estimated_earning', this.state.lang)} /> },
            { component: () => <Select multiple key='expenses' selected={this.state.expenses} onChange={expenses => this.setState({ expenses })} label={Label('expenses', this.state.lang)} data={Options('expenses', this.state.lang)} /> },
            this.state.expenses == 77 && { component: () => <Input key='expenses_other' value={this.state.expenses_other} onChange={expenses_other => this.setState({ expenses_other })} label={Label('expenses_other', this.state.lang)} /> },
            { component: () => <Number key='estimated_expenses' value={this.state.estimated_expenses} onChange={estimated_expenses => this.setState({ estimated_expenses })} label={Label('estimated_expenses', this.state.lang)} /> },
            { component: () => <Select key='food_security' selected={this.state.food_security} onChange={food_security => this.setState({ food_security })} label={Label('food_security', this.state.lang)} data={Options('food_security', this.state.lang)} /> },
            { component: () => <Number key='land_area' value={this.state.land_area} onChange={land_area => this.setState({ land_area })} label={Label('land_area', this.state.lang)} /> },
            { component: () => <Select key='area_production' selected={this.state.area_production} onChange={area_production => this.setState({ area_production })} label={Label('area_production', this.state.lang)} data={Options('area', this.state.lang)} /> },
            { component: () => <Number key='production_consumed' value={this.state.production_consumed} onChange={production_consumed => this.setState({ production_consumed })} label={Label('production_consumed', this.state.lang)} /> },
            { component: () => <Number key='production_wastage' value={this.state.production_wastage} onChange={production_wastage => this.setState({ production_wastage })} label={Label('production_wastage', this.state.lang)} /> },
            { component: () => <Number key='production_sold' value={this.state.production_sold} onChange={production_sold => this.setState({ production_sold })} label={Label('production_sold', this.state.lang)} /> },
            { component: () => <Number key='production_investment' value={this.state.production_investment} onChange={production_investment => this.setState({ production_investment })} label={Label('production_investment', this.state.lang)} /> },
            { component: () => <Number key='production_saving' value={this.state.production_saving} onChange={production_saving => this.setState({ production_saving })} label={Label('production_saving', this.state.lang)} /> },
        ];

        let collection = {
            c1a_0008,
            c1a_0013,
            c1a_0015,
            c1a_0018,
            c1a_0020,
            c1a_0022,
            c1a_0032,
            c1a_0033,
            c1a_0048,
            c1a_21338,
            c1a_24267,
            c1b_0065,
            c1b_0067,
            c1b_0072,
            c1b_0087,
            c1b_0091,
            c1b_0128,
            c1b_0130,
            c1b_0131,
            c1bsav,
            c1c_0152,
            c1c_0156,
            c1c_0160,
            c2a_21686,
            c2a_24023,
            c2b_22844,
            c2c_0293,
            c2d_23018p,
            c2d_23018s,
            c2d_24263,
            c2d_24264,
            c2d_24276,
            c3c_22869,
            c4a_0014,
            c4a_0165,
            c4a_0190,
            c4a_0196,
            c4a_0219,
            c4a_0221,
            c4a_21415,
            c4a_22917,
            c4a_22920,
            c4a_23441,
            c4a_23443,
            c4a_24265,
            c4b_0044,
            c4b_0069,
            c4b_21068,
            c4b_21071,
            c4b_24266,
            c4c_23075,
        }
        let main = [
        ].concat(_.flatten(_.values(_.map(this.props.navigation.state.params.state.indicators.sort(),f=>collection[f] ))));

        return main;
    }

    listRespondents(obj = {}, lang = 0){
        let listRespondents = [
            obj.respondent_type == 'family' && { label: ['HH Head ', 'घरमुली'], value: 1 },
            obj.tp == 3 && obj.respondent_type == 'member' && { label: ['Children 12-18 years ', '१२ बर्ष देखि १८ बर्षको बालबालिका'], value: 2 },
            obj.tp == 3 && obj.respondent_type == 'member' && { label: ['Youth 16-25 Years', '१६ बर्ष देखि २५ बर्षको युवा'], value: 3 },
            obj.tp == 7 && obj.respondent_type == 'member' && { label: ['Pregnant Woman', 'गर्भवती महिला'], value: 4 },
            obj.tp == 7 && obj.respondent_type == 'member' && { label: ['Mother with below 2 years child', '२ बर्ष भन्दा कम उमेरको बच्चाको आमा '], value: 5 },
            obj.tp == 7 && obj.respondent_type == 'member' && { label: ['Mother with below 5 years child', '५ बर्ष मुनिको बच्चाको आमा'], value: 6 },
            obj.respondent_type == 'group' && { label: ['Producer/farmer', 'कृषक/उत्पादक'], value: 7 },
            obj.respondent_type == 'institute' && { label: ['School teacher', 'बिद्यालय शिक्षक'], value: 8 },
            obj.tp == 4 && obj.respondent_type == 'member' && { label: ['Students', 'बिद्यार्थी '], value: 9 },
            obj.tp == 6 && obj.respondent_type == 'group' && { label: ['Students Parent', 'बिध्यर्थिको अविभावक '], value: 10 },
            obj.tp == 6 && obj.respondent_type == 'group' && { label: ['Reading Club facilitator', 'Reading Club सहजकर्ता'], value: 11 },
            obj.respondent_type == 'group' && { label: ['VCPPC Members', 'बाल संरक्षण समिति सदस्य '], value: 12 },
            obj.respondent_type == 'group' && { label: ['LDRMP Members ', 'स्थानीय विपद जोखिम ब्यबस्थापन समिति सदस्य हरु'], value: 13 },
            obj.respondent_type == 'group' && { label: ['Producer group', 'उत्पादन समूह'], value: 22 },
            obj.respondent_type == 'group' && { label: ['Skype Club members', 'SKY club  सदस्य '], value: 14 },
            obj.respondent_type == 'group' && { label: ['SMC', ' बिद्यालय ब्यबस्थापन समिति'], value: 15 },
            obj.respondent_type == 'institute' && { label: ['Head teacher', 'प्रधानाध्यापक'], value: 16 },
            obj.tp == 1 && obj.respondent_type == 'member' && { label: ['Care givers with 0-18 year children', '०-१८ बर्षको बालबालिकाको अविभावक/हेरचाह कर्ता'], value: 17 },
            obj.tp == 8 && obj.respondent_type == 'group' && { label: ['CP service user (community members)', 'बाल संरक्षण सेवाग्राही /समुदायका सदस्य'], value: 18 },
            obj.tp == 7 && obj.respondent_type == 'member' && { label: ['Mother with 6-59 months child', '६ देखि ५९ महिनाको बच्चाको आमा'], value: 19 },
            obj.tp == 7 && obj.respondent_type == 'member' && { label: ['Mother with 12-59 months child', '१२ देखि ५९ महिनाको बाचाको आमा '], value: 20 },
            obj.tp == 1 && obj.respondent_type == 'member' && { label: ['Care giver with 5-18 year children', '५-१८ बर्षको बच्चाको आमा '], value: 21 },
        ];
        
        return listRespondents.filter(Boolean).map(f=>{
            f.label = f.label[lang];
            return f;
        });
    }
} 