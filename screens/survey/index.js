 import Performs from './perform';
import React from 'react';
import Response from './response'
import {
    StackNavigator
} from "react-navigation";
import Surveys from './survey';
export default StackNavigator({
    Survey: {
        name: 'SurveyList',
        screen: Response
    },
    Perform: {
        name: 'Perform',
        screen: Performs
    },
    Ask: {
        name: 'Answer',
        screen: Surveys
    }
}, {
        headerMode: 'none',
        initialRouteName: 'Survey',
    })