import { Alert, Text, ToastAndroid } from "react-native";
import { Body, Button, Container, Content, Fab, Icon, List, ListItem, Right, View } from 'native-base';

import Header from '../../designs/header'
import { NavigationActions } from "react-navigation";
import React from "react";
import _ from 'lodash';
import {get} from '../../module/store';
import list from './list';
import {remove} from '../../module/store';

export default class Responses extends React.Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            models: [],
            is_ready: false,
        };
        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this)
        this.load = this.load.bind(this);
        this.news = this.news.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async load() {
        let models = await get() || [];
        let data = _.filter(models, { belongsTo: 'survey' })
        this.setState({
            data, is_ready: true
        });
    }

    news() {
        ToastAndroid.show('Loading...',ToastAndroid.SHORT);
        this.props.navigation.dispatch(NavigationActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({ routeName: 'Survey' }),
                NavigationActions.navigate({ routeName: 'Perform' }),
            ]
        }));
    }

    render() {
        let content = <ListItem style={{ marginLeft: 0, paddingLeft: 12 }}><Text> No response has been stored.</Text></ListItem>
        if (this.state.is_ready && this.state.data.length > 0) {
            content = this.state.data.map((da, index) => <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} key={da.index}><Body><Text>
                {da.question} - {da.respondent_name} - {_.capitalize(da.respondent_type)}</Text></Body><Right><Button danger onPress={() => 
                Alert.alert(
                    'Are you sure?',
                    'It will delete this entry from this device.',
                    [
                        { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                        { text: 'OK', onPress: () => remove(da.index) && this.setState({ data: _.reject(this.state.data, { index: da.index }) }) },
                    ],                                      
                    { cancelable: false }
                )}><Icon name='trash' /></Button></Right></ListItem>);
        }
        return <Container>
            <Header menu title="List of responses" {...this.props} />
            <Content>
                <List>
                    {content}
                </List>
            </Content>
            <Fab active={true}
                style={{ backgroundColor: 'orange' }}
                position="bottomRight"
                onPress={this.news.debounce()}>
                <Icon name="md-add" />
                <Button onPress={this.load}>
                    <Icon name="refresh" />
                </Button>
            </Fab>
        </Container>;
    }
}
