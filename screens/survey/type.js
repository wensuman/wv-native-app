export default [
    { label: 'Member', value: 'member' },
    { label: 'Family', value: 'family' },
    { label: 'Group', value: 'group' },
    { label: 'Institute', value: 'institute' },
]