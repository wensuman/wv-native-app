import List from "./list";
import New from "./new";
import NewGroupMember from '../family/member/new';
import Single from './single';
import SingleMember from '../family/member/single';
import {
  StackNavigator
} from "react-navigation";
export default StackNavigator({
  GroupList: {
    screen: List
  },
  NewGroup: {
    screen: New
  },
  SingleGroup:{
    screen: Single
  },
  SingleGroupMember:{
    screen: SingleMember
  },
  NewGroupMember:{
    screen: NewGroupMember
  }
},{
  headerMode:'none',
  initialRouteName: 'GroupList',
});