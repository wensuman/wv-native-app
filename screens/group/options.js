export const type_of_group = [
  { value: "Health Mothers group", label: ["Health Mothers group", "Health Mothers group"] },
  { value: "School Management Committee", label: ["School Management Committee", "School Management Committee"] },
  { value: "Parent Teacher Associations (PTA)", label: ["Parent Teacher Associations (PTA)", "Parent Teacher Associations (PTA)"] },
  { value: "Child Club", label: ["Child Club", "Child Club"] },
  { value: "School Based Disaster Risk Management Committee", label: ["School Based Disaster Risk Management Committee", "School Based Disaster Risk Management Committee"] },
  { value: "Citizen Voice and Action Working Group", label: ["Citizen Voice and Action Working Group", "Citizen Voice and Action Working Group"] },
  { value: "Life Skill group", label: ["Life Skill group", "Life Skill group"] },
  { value: "Producer group", label: ["Producer group", "Producer group"] },
  { value: "Business group", label: ["Business group", "Business group"] },
  { value: "Saving group", label: ["Saving group", "Saving group"] },
  { value: "Local Disaster Risk Management Committee", label: ["Local Disaster Risk Management Committee", "Local Disaster Risk Management Committee"] },
  { value: "Skill and Knowledge for Youth Economical Environment", label: ["Skill and Knowledge for Youth Economical Environment", "Skill and Knowledge for Youth Economical Environment"] },
  { value: "Producer group alliance", label: ["Producer group alliance", "निर्माता समूह गठबन्धन"] },
  { value: "Marketing Committee", label: ["Marketing Committee", "मार्केटिंग समिति"] },
];

export const group_type_1 = [
  { value: "Agriculture", label: ["Agriculture", "Agriculture"] },
  { value: "Livestock", label: ["Livestock", "Livestock"] },
  { value: "Handicraft", label: ["Handicraft", "Handicraft"] },
  { value: "Small Enterprise", label: ["Small Enterprise", "Small Enterprise"] }
];
export const type_of_group_formation = [
  { value: "Newly formed", label: ["Newly formed", "Newly formed"] },
  { value: "Existing", label: ["Existing", "Existing"] },
  { value: "Small enterprise group", label: ["Small enterprise group", "Small enterprise group"] },
];

export const group_type_2 = [
  { value: "Women only - General", label: ["Women only - General", "Women only - General"] },
  { value: "Men only - General", label: ["Men only - General", "Men only - General"] },
  { value: "Mixed - General", label: ["Mixed - General", "Mixed - General"] },
  { value: "Youth or adolescences - General", label: ["Youth or adolescences - General", "Youth or adolescences - General"] },
  { value: "Women only - Sector Based", label: ["Women only - Sector Based", "Women only - Sector Based"] },
  { value: "Men only - Sector Based", label: ["Men only - Sector Based", "Men only - Sector Based"] },
  { value: "Mixed - Sector Based", label: ["Mixed - Sector Based", "Mixed - Sector Based"] },
  { value: "Youth or adolescences - Sector Based", label: ["Youth or adolescences - Sector Based", "Youth or adolescences - Sector Based"] },
];

export default {
  type_of_group,
  group_type_1,
  group_type_2,
  type_of_group_formation,
}