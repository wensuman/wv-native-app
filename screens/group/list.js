import { ActivityIndicator, Platform, ScrollView, Text, ToastAndroid, TouchableOpacity } from "react-native";
import { Body, Button, Container, Content, Fab, Header, Icon, Input, Item, List, ListItem } from 'native-base';

import { NavigationActions } from "react-navigation";
import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import {get,file} from '../../module/store';

export default class Lists extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      models: [],
      loading: 'Loading...',
      chunk:10
    };
    this.componentDidMount = this.componentDidMount.bind(this);
    this.load = this.load.bind(this);
    this.newBind = this.newBind.bind(this);
    this.searches = this.searches.bind(this);
  }

  componentDidMount() {
    this.load();
  }

  async load() {
    ToastAndroid.show('Loading...', ToastAndroid.SHORT);
    let models = await get() || [];
    let downloadedModels = await file('group') || [];
    downloadedModels = downloadedModels.map(m => ({ ...m, ...m.details, uploaded: true, ...m.location, index: 'index_' + m.id, id: m.id })).reverse()
    models = _.filter(models, { belongsTo: 'group' }).concat(downloadedModels || []);
    this.setState({
      models, original: models || [], group_loaded: true, loading: '',
    });
  }

  newBind() {
    this.props.navigation.navigate("NewGroup");
  }

  searches(){
    if (!this.state.search) {
      this.setState({ models: this.state.original });
    }
    this.state.search && this.setState({ models: this.state.original.filter(f => f.id == parseInt(this.state.search) || f.name_of_group.includes(this.state.search) ) })
  }

  render() {
    return <Container>
      <Header style={{ backgroundColor: 'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded >
        <Item>
          <Icon name="ios-search" />
          <Input onChangeText={search => this.setState({ search })} placeholder="Search by ID, name" />
          <Button style={{ paddingHorizontal: 10 }} onPress={this.searches} success>
            <Text  style={{color:'white'}}>Search</Text>
          </Button>
        </Item>
      </Header>
      <Content>
        <ScrollView>
          {this.state.models && <List>
            {this.state.models.map((group, index) => (
              <ListItem key={index} onPress={() => {
                this.props.navigation.dispatch(NavigationActions.reset({
                  index:1,
                  actions:[
                    NavigationActions.navigate({ routeName: 'GroupList'}),
                    NavigationActions.navigate({ routeName: 'SingleGroup', params: { data: group }})
                  ]
                }))
              }} style={{ marginLeft: 0, paddingLeft: 10 }}>
                <Body>
                  <Text>ID: {_.capitalize(group.id)}</Text>
                  <Text>Name: {_.capitalize(group.name_of_group)}</Text>
                  <Text>Type: {group.type_of_group}</Text>
                </Body>
              </ListItem>
            ))}
          </List>}
       </ScrollView>
      </Content>
      <Fab
        active={true}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: 'orange' }}
        position="bottomRight"
        onPress={this.newBind.debounce()}>
        <Icon name="md-add" />
        <Button onPress={this.load}>
          <Icon name="refresh" />
        </Button>
      </Fab>
    </Container>;
  }
} 