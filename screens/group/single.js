import { ScrollView, Text, ToastAndroid } from 'react-native'
import { Body, Button, Container, Fab, Icon, Item, List, ListItem, Right, ScrollableTab, Tab, Tabs, View } from "native-base";

import Header from '../../designs/header';
import Label from "../../module/label";
import Location from '../../module/location'
import {
    NavigationActions
} from "react-navigation";
import React from "react";
import _ from "lodash";
import { remove } from '../../module/store';
import {get,file} from '../../module/store';

export default class Single extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    members = [];

    institutes = [];

    constructor(props) {
        super(props);
        this.state = { ...this.props.navigation.state.params.data };
        this.state.lang = 0;
        this.load = this.load.bind(this);
        this.edit = this.edit.bind(this);
        this.addse = this.addse.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async load() {
        ToastAndroid.show('Refreshing...', ToastAndroid.SHORT);
        this.members = await file('member');
        let data = await get() || [];
        let group = _.find(data, { belongsTo: 'group', index: this.state.index });
        let members = data.filter(m=>m.belongsTo=='member').filter(m=>m.groups_involved.includes(this.state.id) || m.group_index == this.state.index);
        members = members.concat(this.members.filter(f=>this.state.memberids && this.state.memberids.includes(f.id)));
        this.setState({ ...group, members });
    }

    viewsmembers(m) {
        let member = m;
        if(m.location){
            member = { ...m.location, ...member}
        }
        if(m.detail){
            member = { ...m.detail,...member}
        }
        this.props.navigation.navigate(this.props.navigation.state.params.SingleInstituteGroupMember || "SingleGroupMember", {
            member: member,
            from: 'group'
        });
    }

    addse() {
        this.props.navigation.navigate(this.props.navigation.state.params.newGroupMemberRoute || "NewGroupMember", {
            name: "Add new member",
            group_index: this.state.index,
            location: _.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole']),
            group_id: this.state.id
        });
    }

    edit() {
        this.props.navigation.navigate(this.props.navigation.state.params.editGroupRoute || "NewGroup", { state: {...this.state, members:undefined} })
    }

    render() {
        let content = false;

        if (this.state.type_of_group && this.state.type_of_group.includes('Health Post')) {
            content = [
                this.state.number_of_health_staff_on_health_post && <ListItem style={{marginLeft:0, paddingLeft:10}} key="number_of_health_staff_on_health_post"><Text>{Label.number_of_health_staff_on_health_post[this.state.lang]}: {this.state.number_of_health_staff_on_health_post}</Text></ListItem>,
                this.state.do_health_post_have_its_own_building && <ListItem style={{marginLeft:0, paddingLeft:10}} key="do_health_post_have_its_own_building"><Text>{Label.do_health_post_have_its_own_building[this.state.lang]}: {this.state.do_health_post_have_its_own_building}</Text></ListItem>,
                this.state.does_health_post_have_birthing_service && <ListItem style={{marginLeft:0, paddingLeft:10}} key="does_health_post_have_birthing_service"><Text>{Label.does_health_post_have_birthing_service[this.state.lang]}: {this.state.does_health_post_have_birthing_service}</Text></ListItem>,
                this.state.are_essential_birthing_equipment_is_in_place_for_birthing_service && <ListItem style={{marginLeft:0, paddingLeft:10}} key="are_essential_birthing_equipment_is_in_place_for_birthing_service"><Text>{Label.are_essential_birthing_equipment_is_in_place_for_birthing_service[this.state.lang]}: {this.state.are_essential_birthing_equipment_is_in_place_for_birthing_service}</Text></ListItem>,
                this.state.is_sba_available_24_hour_in_health_facility && <ListItem style={{marginLeft:0, paddingLeft:10}} key="is_sba_available_24_hour_in_health_facility"><Text>{Label.is_sba_available_24_hour_in_health_facility[this.state.lang]}: {this.state.is_sba_available_24_hour_in_health_facility}</Text></ListItem>,
                this.state.do_health_facility_have_toilet_and_water_supply && <ListItem style={{marginLeft:0, paddingLeft:10}} key="do_health_facility_have_toilet_and_water_supply"><Text>{Label.do_health_facility_have_toilet_and_water_supply[this.state.lang]}: {this.state.do_health_facility_have_toilet_and_water_supply}</Text></ListItem>,
                this.state.do_health_facility_have_all_medicine_as_per_the_government_provision && <ListItem style={{marginLeft:0, paddingLeft:10}} key="do_health_facility_have_all_medicine_as_per_the_government_provision"><Text>{Label.do_health_facility_have_all_medicine_as_per_the_government_provision[this.state.lang]}: {this.state.do_health_facility_have_all_medicine_as_per_the_government_provision}</Text></ListItem>,
                this.state.do_health_facility_have_health_facility_operation_management_committee && <ListItem style={{marginLeft:0, paddingLeft:10}} key="do_health_facility_have_health_facility_operation_management_committee"><Text>{Label.do_health_facility_have_health_facility_operation_management_committee[this.state.lang]}: {this.state.do_health_facility_have_health_facility_operation_management_committee}</Text></ListItem>,
                this.state.is_it_functional && <ListItem style={{marginLeft:0, paddingLeft:10}} key="is_it_functional"><Text>{Label.is_it_functional[this.state.lang]}: {this.state.is_it_functional}</Text></ListItem>,
                this.state.how_frequently_meeting_is_conducted && <ListItem style={{marginLeft:0, paddingLeft:10}} key="how_frequently_meeting_is_conducted"><Text>{Label.how_frequently_meeting_is_conducted[this.state.lang]}: {this.state.how_frequently_meeting_is_conducted}</Text></ListItem>,
                this.state.what_is_the_population_size_of_its_catchment_area && <ListItem style={{marginLeft:0, paddingLeft:10}} key="what_is_the_population_size_of_its_catchment_area"><Text>{Label.what_is_the_population_size_of_its_catchment_area[this.state.lang]}: {this.state.what_is_the_population_size_of_its_catchment_area}</Text></ListItem>,
                this.state.what_is_the_average_number_of_opd_visit_per_day && <ListItem style={{marginLeft:0, paddingLeft:10}} key="what_is_the_average_number_of_opd_visit_per_day"><Text>{Label.what_is_the_average_number_of_opd_visit_per_day[this.state.lang]}: {this.state.what_is_the_average_number_of_opd_visit_per_day}</Text></ListItem>
            ]
        }

        if (this.state.type_of_group == 'PHC OCR') {
            content = [
                this.state.date_when_it_runs && <ListItem style={{marginLeft:0, paddingLeft:10}} key="date_when_it_runs"><Text>{Label.date_when_it_runs[this.state.lang]} : {this.state.date_when_it_runs}</Text></ListItem>,
                this.state.does_it_have_it_owns_building && <ListItem style={{marginLeft:0, paddingLeft:10}} key="does_it_have_it_owns_building"><Text>{Label.does_it_have_it_owns_building[this.state.lang]} : {this.state.does_it_have_it_owns_building}</Text></ListItem>,
                this.state.does_it_have_essential_furniture_and_equipment_in_place && <ListItem style={{marginLeft:0, paddingLeft:10}} key="does_it_have_essential_furniture_and_equipment_in_place"><Text>{Label.does_it_have_essential_furniture_and_equipment_in_place[this.state.lang]} : {this.state.does_it_have_essential_furniture_and_equipment_in_place}</Text></ListItem>,
                this.state.does_it_have_toilet_and_water_supply && <ListItem style={{marginLeft:0, paddingLeft:10}} key="does_it_have_toilet_and_water_supply"><Text>{Label.does_it_have_toilet_and_water_supply[this.state.lang]} : {this.state.does_it_have_toilet_and_water_supply}</Text></ListItem>,
                this.state.does_have_health_facility_operation_management_committee && <ListItem style={{marginLeft:0, paddingLeft:10}} key="does_have_health_facility_operation_management_committee"><Text>{Label.does_have_health_facility_operation_management_committee[this.state.lang]} : {this.state.does_have_health_facility_operation_management_committee}</Text></ListItem>,
                this.state.is_it_functional && <ListItem style={{marginLeft:0, paddingLeft:10}} key="is_it_functional"><Text>{Label.is_it_functional[this.state.lang]} : {this.state.is_it_functional}</Text></ListItem>,
                this.state.how_frequently_meeting_is_conducted && <ListItem style={{marginLeft:0, paddingLeft:10}} key="how_frequently_meeting_is_conducted"><Text>{Label.how_frequently_meeting_is_conducted[this.state.lang]} : {this.state.how_frequently_meeting_is_conducted}</Text></ListItem>,
                this.state.what_is_the_number_of_population_served_by_it_in_an_month_average && <ListItem style={{marginLeft:0, paddingLeft:10}} key="what_is_the_number_of_population_served_by_it_in_an_month_average"><Text>{Label.what_is_the_number_of_population_served_by_it_in_an_month_average[this.state.lang]} : {this.state.what_is_the_number_of_population_served_by_it_in_an_month_average}</Text></ListItem>,
                this.state.does_have_all_medicine_as_per_the_government_provision && <ListItem style={{marginLeft:0, paddingLeft:10}} key="does_have_all_medicine_as_per_the_government_provision"><Text>{Label.does_have_all_medicine_as_per_the_government_provision[this.state.lang]} : {this.state.does_have_all_medicine_as_per_the_government_provision}</Text></ListItem>,
            ]
        }

        if (this.state.type_of_group == 'Mothers group') {
            content = [
                this.state.contact_detail_of_the_focal_person && <ListItem style={{marginLeft:0, paddingLeft:10}} key="contact_detail_of_the_focal_person"><Text>{Label.contact_detail_of_the_focal_person[this.state.lang]} : {this.state.contact_detail_of_the_focal_person}</Text></ListItem>,
                this.state.have_building_or_meeting_room && <ListItem style={{marginLeft:0, paddingLeft:10}} key="have_building_or_meeting_room"><Text>{Label.have_building_or_meeting_room[this.state.lang]} : {this.state.have_building_or_meeting_room}</Text></ListItem>,
                this.state.have_sitting_arrangement_for_meeting && <ListItem style={{marginLeft:0, paddingLeft:10}} key="have_sitting_arrangement_for_meeting"><Text>{Label.have_sitting_arrangement_for_meeting[this.state.lang]} : {this.state.have_sitting_arrangement_for_meeting}</Text></ListItem>,
                this.state.have_toilet_water_facility_nearby && <ListItem style={{marginLeft:0, paddingLeft:10}} key="have_toilet_water_facility_nearby"><Text>{Label.have_toilet_water_facility_nearby[this.state.lang]} : {this.state.have_toilet_water_facility_nearby}</Text></ListItem>,
                this.state.have_any_IEC_materials_for_display_on_the_meeting && <ListItem style={{marginLeft:0, paddingLeft:10}} key="have_any_IEC_materials_for_display_on_the_meeting"><Text>{Label.have_any_IEC_materials_for_display_on_the_meeting[this.state.lang]} : {this.state.have_any_IEC_materials_for_display_on_the_meeting}</Text></ListItem>,
            ]
        }

        if (this.state.type_of_group == 'Child Club') {
            this.state.have_received_any_training && (content = <ListItem style={{marginLeft:0, paddingLeft:10}}><Text>{Label.have_received_any_training[this.state.lang]} : {this.state.have_received_any_training}</Text></ListItem>)
        }

        if (this.state.type_of_group == 'Life Skill group') {
            this.state.name_of_facilitator && (content = <ListItem style={{marginLeft:0, paddingLeft:10}}><Text>{Label.name_of_facilitator[this.state.lang]} : {this.state.name_of_facilitator}</Text></ListItem>)
        }

        if (this.state.type_of_group == 'Producer group') {
            content = [
                this.state.year_of_establishment && <ListItem style={{marginLeft:0, paddingLeft:10}} key="year_of_establishment"><Text>{Label.year_of_establishment[this.state.lang]} : {this.state.year_of_establishment}</Text></ListItem>,
                this.state.meeting_venue && <ListItem style={{marginLeft:0, paddingLeft:10}} key="meeting_venue"><Text>{Label.meeting_venue[this.state.lang]} : {this.state.meeting_venue}</Text></ListItem>,
                this.state.general_fix_date && <ListItem style={{marginLeft:0, paddingLeft:10}} key="general_fix_date"><Text>{Label.general_fix_date[this.state.lang]} : {this.state.general_fix_date}</Text></ListItem>,
                this.state.group_type && <ListItem style={{marginLeft:0, paddingLeft:10}} key="group_type"><Text>{Label.group_type[this.state.lang]} : {this.state.group_type}</Text></ListItem>,
                this.state.type_of_group_formation && <ListItem style={{marginLeft:0, paddingLeft:10}} key="type_of_group_formation"><Text>{Label.type_of_group_formation[this.state.lang]} : {this.state.type_of_group_formation}</Text></ListItem>,
                this.state.saving_amount && <ListItem style={{marginLeft:0, paddingLeft:10}} key="saving_amount"><Text>{Label.saving_amount[this.state.lang]} : {this.state.saving_amount}</Text></ListItem>,
                this.state.have_received_any_training && <ListItem style={{marginLeft:0, paddingLeft:10}} key="have_received_any_training"><Text>{Label.have_received_any_training[this.state.lang]} : {this.state.have_received_any_training}</Text></ListItem>,
                this.state.where_was_this_group_registered && <ListItem style={{marginLeft:0, paddingLeft:10}} key="where_was_this_group_registered"><Text>{Label.where_was_this_group_registered[this.state.lang]} : {this.state.where_was_this_group_registered}</Text></ListItem>,
                this.state.when_was_this_group_registered && <ListItem style={{marginLeft:0, paddingLeft:10}} key="when_was_this_group_regisstered"><Text>{Label.when_was_this_group_registered[this.state.lang]} : {this.state.when_was_this_group_registered}</Text></ListItem>,
            ]
        }

        if (this.state.type_of_group == 'Business group') {
            content = [
                this.state.sale_volume && <ListItem style={{marginLeft:0, paddingLeft:10}} key="sale_volume"><Text>{Label.sale_volume[this.state.lang]}: {this.state.sale_volume}</Text></ListItem>,
                this.state.revenue && <ListItem style={{marginLeft:0, paddingLeft:10}} key="revenue"><Text>{Label.revenue[this.state.lang]}: {this.state.revenue}</Text></ListItem>,
                this.state.production_cost && <ListItem style={{marginLeft:0, paddingLeft:10}} key="production_cost"><Text>{Label.production_cost[this.state.lang]}: {this.state.production_cost}</Text></ListItem>,
                this.state.profit && <ListItem style={{marginLeft:0, paddingLeft:10}} key="profit"><Text>{Label.profit[this.state.lang]}: {this.state.profit}</Text></ListItem>,
            ]
        }

        if (this.state.type_of_group == 'Saving group') {
            content = [
                this.state.established_year && <ListItem style={{marginLeft:0, paddingLeft:10}} key="established_year"><Text>{Label.established_year[this.state.lang]} : {this.state.established_year}</Text></ListItem>,
                this.state.revenue && <ListItem style={{marginLeft:0, paddingLeft:10}} key="revenue"><Text>{Label.revenue[this.state.lang]} : {this.state.revenue}</Text></ListItem>,
                this.state.production_cost && <ListItem style={{marginLeft:0, paddingLeft:10}} key="production_cost"><Text>{Label.production_cost[this.state.lang]} : {this.state.production_cost}</Text></ListItem>,
                this.state.profit && <ListItem style={{marginLeft:0, paddingLeft:10}} key="profit"><Text>{Label.profit[this.state.lang]} : {this.state.profit}</Text></ListItem>,
                this.state.where_was_group_registered && <ListItem style={{marginLeft:0, paddingLeft:10}} key="where_was_group_registered"><Text>{Label.where_was_group_registered[this.state.lang]} : {this.state.where_was_group_registered}</Text></ListItem>,
                this.state.when_was_group_registered && <ListItem style={{marginLeft:0, paddingLeft:10}} key="when_was_group_registered"><Text>{Label.when_was_group_registered[this.state.lang]} : {this.state.when_was_group_registered}</Text></ListItem>,
                this.state.number_of_members && <ListItem style={{marginLeft:0, paddingLeft:10}} key="number_of_members"><Text>{Label.number_of_members[this.state.lang]} : {this.state.number_of_members}</Text></ListItem>,
                this.state.name_of_facilitators && <ListItem style={{marginLeft:0, paddingLeft:10}} key="name_of_facilitators"><Text>{Label.name_of_facilitators[this.state.lang]} : {this.state.name_of_facilitators}</Text></ListItem>,
                this.state.savings && <ListItem style={{marginLeft:0, paddingLeft:10}} key="savings"><Text>{Label.savings[this.state.lang]} : {this.state.savings}</Text></ListItem>,
                this.state.meeting_venue && <ListItem style={{marginLeft:0, paddingLeft:10}} key="meeting_venue"><Text>{Label.meeting_venue[this.state.lang]} : {this.state.meeting_venue}</Text></ListItem>,
                this.state.general_fix_date && <ListItem style={{marginLeft:0, paddingLeft:10}} key="general_fix_date"><Text>{Label.general_fix_date[this.state.lang]} : {this.state.general_fix_date}</Text></ListItem>,
                this.state.group_type && <ListItem style={{marginLeft:0, paddingLeft:10}} key="group_type"><Text>{Label.group_type[this.state.lang]} : {this.state.group_type}</Text></ListItem>,
            ];
        }

        if (this.state.type_of_group == 'Local Disaster Risk Management Committee') {
            content = [
                this.state.established_year && <ListItem style={{marginLeft:0, paddingLeft:10}} key="established_year"><Text>{Label.established_year[this.state.lang]} : {this.state.established_year}</Text></ListItem>,
                this.state.facilitated_by && <ListItem style={{marginLeft:0, paddingLeft:10}} key="facilitated_by"><Text>{Label.facilitated_by[this.state.lang]} : {this.state.facilitated_by}</Text></ListItem>,
                this.state.received_basic_training && <ListItem style={{marginLeft:0, paddingLeft:10}} key="received_basic_training"><Text>{Label.received_basic_training[this.state.lang]} : {this.state.received_basic_training}</Text></ListItem>,
            ];
        }

        if (this.state.type_of_group == 'Skill and Knowledge for Youth Economical Environment') {
            content = [
                this.state.established_year && <ListItem style={{marginLeft:0, paddingLeft:10}} key="established_year"><Text>{Label.established_year[this.state.lang]} : {this.state.established_year}</Text></ListItem>,
                this.state.name_of_designated_skye_coach && <ListItem style={{marginLeft:0, paddingLeft:10}} key="name_of_designated_skye_coach"><Text>{Label.name_of_designated_skye_coach[this.state.lang]} : {this.state.name_of_designated_skye_coach}</Text></ListItem>,
                this.state.meeting_venue && <ListItem style={{marginLeft:0, paddingLeft:10}} key="meeting_venue"><Text>{Label.meeting_venue[this.state.lang]} : {this.state.meeting_venue}</Text></ListItem>,
            ];
        }
        content = content && content.filter && content.filter(Boolean);

        return <Container>
            <Header hasTabs  title={'Group detail'} alertOnRight onRight={(this.state.uploaded || this.state.id) ? undefined : () => {
                remove(this.state.index);
                this.props.navigation.dispatch(NavigationActions.back());
            }} {...this.props} />
            <ScrollView>
                <List style={{ backgroundColor: 'white' }}>
                    <ListItem style={{marginLeft:0, paddingLeft:10}}><Text>ID: {this.state.id}</Text></ListItem>
                    <ListItem style={{marginLeft:0, paddingLeft:10}}><Text>Name: {this.state.name_of_group}</Text></ListItem>
                    <ListItem style={{marginLeft:0, paddingLeft:10}}><Text>Type: {this.state.type_of_group}</Text></ListItem>
                    <Location display data={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} />
                    {content}
                    {this.state.members && this.state.members.length > 0 && <ListItem itemDivider><Text>Members</Text></ListItem>}
                    {this.state.members && this.state.members.map(m => <ListItem onPress={_.debounce(this.viewsmembers.bind(this, m), 500)} style={{ marginLeft: 0, paddingLeft: 10 }} key={m.id || m.index}><Text>{m.full_name}, ID: {m.id}</Text></ListItem>)}
                </List>
            </ScrollView>
            <Fab active={true}
                direction="up"
                style={{ backgroundColor: 'orange' }}
                position="bottomRight"
                onPress={this.addse.debounce()}>
                <Icon name="md-add" />
                {(!this.state.uploaded && !this.state.id) && <Button onPress={this.edit.debounce()} style={{ backgroundColor: '#34A34F' }}>
                    <Icon name="md-color-filter" />
                </Button>}
                <Button onPress={this.load} warning>
                    <Icon name="refresh" />
                </Button>
            </Fab>
        </Container>
    }
};  

0