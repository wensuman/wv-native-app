import { Body, Button, Card, CardItem, Container, Content, DeckSwiper, Form, Icon, Item, Label, Left, List, ListItem, Right, Title } from 'native-base';
import { CheckList, Input, Number, Radio, Select } from '../../forms/input';
import { ScrollView, Text, ToastAndroid } from 'react-native'

import Create from '../../designs/create'
import Header from '../../designs/header';
import Labels from '../../module/label';
import { NavigationActions } from "react-navigation";
import Options from './options';
import React from "react";
import SingleLocation from '../../module/location'
import state from './state'
import store from '../../module/store';
import {get,file} from '../../module/store';

export default class New extends React.Component {

  static navigationOptions = {
    drawerLockMode: "locked-closed",
    header: null
  }

  gi = {
    'Health Mothers group':['Health Post','PHC ORC'],
    'School Management Committee':['School','Reading Camp'],
    'Parent Teacher Associations (PTA)':['School','Reading Camp'],
    'Child Club':['School','Reading Camp'],
    'School Based Disaster Risk Management Committee':['School','Reading Camp'],
  }

  state = {lang:0,belongsTo:'group'};

  institutes=[];

  constructor(props) {
    super(props);

    if (this.props.navigation.state.params && this.props.navigation.state.params.state){
      this.state = { ...this.state, ...this.props.navigation.state.params.state };
    }

    this.getQuestions = this.getQuestions.bind(this);
    this.setInstitutes = this.setInstitutes.bind(this);

  }

  componentDidMount() {
    this.setInstitutes();
  }

  async setInstitutes() {
    let loadedInstitutes = (await file('institute') || []).map(f => ({ type: f.institution_type,value: f.id, label: [f.name_of_institution, f.name_of_institution] }));
    let loaded = await get() || [];
    let institutes = _.filter(loaded,{belongsTo:'institute'});
    this.institutes =  _.sortBy(institutes.map(f => ({type: f.institution_type, value: f.id || f.index, label: [f.name_of_institution, f.name_of_institution] })).filter(f=>Boolean(f.value)).concat(loadedInstitutes), (i)=>i.label[0])
  }

  change(label, text) {
    this.setState({ label: text })
  }

  getQuestions() {
    
    let mothers_group = [
      { component: () => <Input label={Labels.contact_name_of_the_focal_person[this.state.lang]} value={this.state.contact_name_of_the_focal_person} key='contact_name_of_the_focal_person' onChange={(text) => { this.setState({ contact_name_of_the_focal_person: text }) }} />, validation: () => () => this.required(['contact_name_of_the_focal_person']) },
      { component: () => <Number label={Labels.contact_detail_of_the_focal_person[this.state.lang]} value={this.state.contact_detail_of_the_focal_person} key='contact_detail_of_the_focal_person' onChange={(text) => { this.setState({ contact_detail_of_the_focal_person: text }) }} />, validation: () => () => this.required(['contact_detail_of_the_focal_person']) },
      { component: () => <Select label={Labels.have_building_or_meeting_room[this.state.lang]} selected={this.state.have_building_or_meeting_room} key='have_building_or_meeting_room' onChange={(text) => this.setState({ have_building_or_meeting_room: text })} /> },
      { component: () => <Select label={Labels.have_sitting_arrangement_for_meeting[this.state.lang]} selected={this.state.have_sitting_arrangement_for_meeting} key='have_sitting_arrangement_for_meeting' onChange={(text) => this.setState({ have_sitting_arrangement_for_meeting: text })} /> },
      { component: () => <Select label={Labels.have_toilet_water_facility_nearby[this.state.lang]} selected={this.state.have_toilet_water_facility_nearby} key='have_toilet_water_facility_nearby' onChange={(text) => this.setState({ have_toilet_water_facility_nearby: text })} /> },
      { component: () => <Select label={Labels.have_any_IEC_materials_for_display_on_the_meeting[this.state.lang]} selected={this.state.have_any_IEC_materials_for_display_on_the_meeting} key='have_any_IEC_materials_for_display_on_the_meeting' onChange={(text) => this.setState({ have_any_IEC_materials_for_display_on_the_meeting: text })} /> },
    ];

    let child_club = [
    ]

    let life_skill_group = [
      { component: () => <Select label={Labels.have_received_any_training[this.state.lang]} selected={this.state.have_received_any_training} key='have_received_any_training' onChange={(text) => this.setState({ have_received_any_training: text })} /> },
      { component: () => <Input label={Labels.name_of_facilitator[this.state.lang]} value={this.state.name_of_facilitator} key='name_of_facilitator' onChange={(text) => { this.setState({ name_of_facilitator: text }) }} /> },
    ];

    let producer_group = [
      { component: () => <Input label={Labels.meeting_venue[this.state.lang]} value={this.state.meeting_venue} key='meeting_venue' onChange={(text) => { this.setState({ meeting_venue: text }) }} /> },
      { component: () => <Input label={Labels.general_fix_date[this.state.lang]} value={this.state.general_fix_date} key='general_fix_date' onChange={(text) => { this.setState({ general_fix_date: text }) }} /> },
      { component: () => <Select data={Options.group_type_1.ofLang(this.state.lang)} label={Labels.group_type[this.state.lang]} selected={this.state.group_type} key='group_type' onChange={(text) => this.setState({ group_type: text })} /> },
      { component: () => <Select data={Options.type_of_group_formation.ofLang(this.state.lang)} label={Labels.type_of_group_formation[this.state.lang]} selected={this.state.type_of_group_formation} key='type_of_group_formation' onChange={(text) => this.setState({ type_of_group_formation: text })} /> },
      { component: () => <Number label={Labels.saving_amount[this.state.lang]} value={this.state.saving_amount} key='saving_amount' onChange={(text) => { this.setState({ saving_amount: text }) }} />, validation: () => () => this.required(['saving_amount']) },
      { component: () => <Input label={Labels.where_was_this_group_registered[this.state.lang]} value={this.state.where_was_this_group_registered} key='where_was_this_group_registered' onChange={(text) => { this.setState({ where_was_this_group_registered: text }) }} /> },
      { component: () => <Number label={Labels.when_was_this_group_registered[this.state.lang]} value={this.state.when_was_this_group_registered} key='when_was_this_group_registered' onChange={(text) => { this.setState({ when_was_this_group_registered: text }) }} />, validation: () => () => this.required(['when_was_this_group_registered']) },
    ];

    let business_group = [
      { component: () => <Number label={Labels.sale_volume[this.state.lang]} value={this.state.sale_volume} key='sale_volume' onChange={(text) => { this.setState({ sale_volume: text }) }} />, validation: () => () => this.required(['sale_volume']) },
      { component: () => <Number label={Labels.revenue[this.state.lang]} value={this.state.revenue} key='revenue' onChange={(text) => { this.setState({ revenue: text }) }} />, validation: () => () => this.required(['revenue']) },
      { component: () => <Input label={Labels.production_cost[this.state.lang]} value={this.state.production_cost} key='production_cost' onChange={(text) => { this.setState({ production_cost: text }) }} /> },
      { component: () => <Input label={Labels.profit[this.state.lang]} value={this.state.profit} key='profit' onChange={(text) => { this.setState({ profit: text }) }} /> },
    ];

    let saving_group = [
      { component: () => <Input label={Labels.revenue[this.state.lang]} value={this.state.revenue} key='revenue' onChange={(text) => { this.setState({ revenue: text }) }} /> },
      { component: () => <Input label={Labels.production_cost[this.state.lang]} value={this.state.production_cost} key='production_cost' onChange={(text) => { this.setState({ production_cost: text }) }} /> },
      { component: () => <Input label={Labels.profit[this.state.lang]} value={this.state.profit} key='profit' onChange={(text) => { this.setState({ profit: text }) }} /> },
      { component: () => <Input label={Labels.where_was_group_registered[this.state.lang]} value={this.state.where_was_group_registered} key='where_was_group_registered' onChange={(text) => { this.setState({ where_was_group_registered: text }) }} /> },
      { component: () => <Number label={Labels.when_was_group_registered[this.state.lang]} value={this.state.when_was_group_registered} key='when_was_group_registered' onChange={(text) => { this.setState({ when_was_group_registered: text }) }} />, validation: () => () => this.required(['when_was_group_registered']) },
      { component: () => <Number label={Labels.number_of_members[this.state.lang]} value={this.state.number_of_members} key='number_of_members' onChange={(text) => { this.setState({ number_of_members: text }) }} />, validation: () => () => this.required(['number_of_members']) },
      { component: () => <Input label={Labels.name_of_facilitators[this.state.lang]} value={this.state.name_of_facilitators} key='name_of_facilitators' onChange={(text) => { this.setState({ name_of_facilitators: text }) }} />, validation: () => () => this.required(['name_of_facilitators']) },
      { component: () => <Input label={Labels.savings[this.state.lang]} value={this.state.savings} key='savings' onChange={(text) => { this.setState({ savings: text }) }} /> },
      { component: () => <Input label={Labels.meeting_venue[this.state.lang]} value={this.state.meeting_venue} key='meeting_venue' onChange={(text) => { this.setState({ meeting_venue: text }) }} /> },
      { component: () => <Input label={Labels.general_fix_date[this.state.lang]} value={this.state.general_fix_date} key='general_fix_date' onChange={(text) => { this.setState({ general_fix_date: text }) }} /> },
      { component: () => <Select data={Options.group_type_2.ofLang(this.state.lang)} label={Labels.group_type[this.state.lang]} selected={this.state.group_type} key='group_type' onChange={(text) => this.setState({ group_type: text })} /> },
    ];
    let local_disaster_risk_management_committee = [
      { component: () => <Input label={Labels.facilitated_by[this.state.lang]} value={this.state.facilitated_by} key='facilitated_by' onChange={(text) => { this.setState({ facilitated_by: text }) }} /> },
      { component: () => <Select label={Labels.received_basic_training[this.state.lang]} selected={this.state.received_basic_training} key='received_basic_training' onChange={(text) => this.setState({ received_basic_training: text })} /> },
    ];
    let skill_and_knowledge_for_youth_economical_environment = [
      { component: () => <Input label={Labels.name_of_designated_skye_coach[this.state.lang]} value={this.state.name_of_designated_skye_coach} key='name_of_designated_skye_coach' onChange={(text) => { this.setState({ name_of_designated_skye_coach: text }) }} /> },
      { component: () => <Input label={Labels.meeting_venue[this.state.lang]} value={this.state.meeting_venue} key='meeting_venue' onChange={(text) => { this.setState({ meeting_venue: text }) }} /> },
    ];
    let questions = [
      {
        component: () => <SingleLocation lang={this.state.lang} locations={this.state.location || _.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} onChange={state => {
          this.setState(state);
        }} />,
        validation: () => this.required(['state_id', 'district_id', 'municipality_id', 'ward_id'], 'Please fill up the location fields correctly')
      },
      { component: () => <Select data={Options.type_of_group.ofLang(this.state.lang)} label={Labels.type_of_group[this.state.lang]} selected={this.state.type_of_group} key='type_of_group' onChange={(text) => this.setState({ type_of_group: text })} />, validation: () => this.required(['type_of_group']) },
      { component: () => <Input label={Labels.name_of_group[this.state.lang]} value={this.state.name_of_group} key='name_of_group' onChange={(text) => { this.setState({ name_of_group: text }) }} />, validation: () => this.required(['name_of_group']) },
      { component: () => <Number minLength={4} maxLength={4} label={Labels.established_year[this.state.lang]} value={this.state.established_year} key='established_year' onChange={(text) => { this.setState({ established_year: text }) }} />, validation: ()=>{
        if(this.state.established_year && this.state.established_year.length == 4 && this.state.established_year <= new Date().getFullYear()){
          return true;
        }
        ToastAndroid.show('Established year should be less than or equal to current year', ToastAndroid.SHORT)
      } },
      this.gi[this.state.type_of_group] && this.gi[this.state.type_of_group].length > 0  && { component: () => <Select data={this.institutes.filter(i=>(this.gi[this.state.type_of_group] && this.gi[this.state.type_of_group].length > 0 && this.gi[this.state.type_of_group].includes(i.type))).ofLang(this.state.lang)} label={Labels.institute_id[this.state.lang]} selected={this.state.institute_id} key='institute_id' onChange={(text) => this.setState({ institute_id: text })} /> },
    ]
    if (this.state.type_of_group == 'Health Mothers group') questions = questions.concat(mothers_group)
    if (this.state.type_of_group == 'Child Club') questions = questions.concat(child_club)
    if (this.state.type_of_group == 'Life Skill group') questions = questions.concat(life_skill_group)
    if (this.state.type_of_group == 'Producer group') questions = questions.concat(producer_group)
    if (this.state.type_of_group == 'Business group') questions = questions.concat(business_group)
    if (this.state.type_of_group == 'Saving group') questions = questions.concat(saving_group)
    if (this.state.type_of_group == 'Local Disaster Risk Management Committee') questions = questions.concat(local_disaster_risk_management_committee)
    if (this.state.type_of_group == 'Skill and Knowledge for Youth Economical Environment') questions = questions.concat(skill_and_knowledge_for_youth_economical_environment)
    return questions;
  }

  validation(data) {
    this.criteria = data;
    return true;
  }

  required(checkIn, message) {
    if (checkIn.filter(state => {
      return (!this.state[state] || (this.state[state] || []).length == 0)
    }).length > 0) {
      ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
      return false;
    }
    return true;
  }

  render() {
    return <ScrollView>
      <Header icon={'md-book'} rightText={this.state.lang ? 'NP' : 'EN' } title={this.state.index ? 'Edit group' : 'Add new group'} alertOnBack {...this.props} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
      <Create onSave={() => {
        store({ ...this.state, lang:undefined, step:undefined }, () => {
          this.props.navigation.dispatch(NavigationActions.back());
        });
      }} {...this.props} exists={this.state.exists} questions={this.getQuestions()} />
    </ScrollView>;
  }
}