import { FileSystem } from 'expo';
import {
    ToastAndroid,
    AsyncStorage
} from 'react-native';
import Url from '../../module/url'
import download from '../../module/store';

let url = Url.server_url;

export const table = async (i, check = false) => {
    let token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    if(!token){
        return;
    }
    let fileUri = FileSystem.documentDirectory + i + token.id;
    let status = await FileSystem.getInfoAsync(fileUri)
    let uri = url + i + "?json=true&X-ID="+token.id;
    if ( check ) {
        return status.exists;
    }
    try {
        await FileSystem.downloadAsync(uri, fileUri)
        return true;
    } catch (error) {
        ToastAndroid.show('Could not download ' + i, ToastAndroid.SHORT)
        return false;
    }
}

export default {
    table
}