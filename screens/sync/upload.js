import { ToastAndroid, AsyncStorage } from 'react-native';
import Axios from 'axios';
import _ from 'lodash';
import retriever from './retriever';
import store, {RemoveUploaded} from '../../module/store';
import url from '../../module/url';
import {table} from './download';

let family_url = url.server_url + 'sync/family';
let group_url = url.server_url + 'sync/group';
let institute_url = url.server_url + 'sync/institute';
let survey_url = url.server_url + 'sync/survey';
let health_survey_url = url.server_url + 'sync/health';
let child_survey_url = url.server_url + 'sync/child-survey';
let att_url = url.server_url + 'sync/att/';
let pdherth_url = url.server_url + 'sync/pdherth';
let producer_url = url.server_url + 'sync/producer-group';

const token = null;

export const continous = async (url, items = 0) => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    if (items.length > 0) {
        let item = items.pop();
        await Axios.post(url, {...item, uploaded:undefined, belongsTo: undefined, lang:undefined, index:undefined, exists:undefined, preview:undefined, step:undefined, group_index:undefined, family_index: undefined, institute_index: undefined},{headers:{'POSTED-BY':token.id}}).then(async response => {
            await store({ ...item, uploaded: true, id: (response.data.id || item.id)  }) && await continous(url, items)
        }).catch(async error =>{
            await continous(url, items);
        })
    }
    return true;
}

export const continousMember = async (items = 0) => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    if (items.length > 0) {
        item = items.pop();
        let parent = item.family_id || 'group';
        let l = await Axios.post(family_url + '/' + parent + '/members', {...item,age:undefined, uploaded:undefined, belongsTo: undefined, lang:undefined, index:undefined, exists:undefined, preview:undefined, step:undefined, group_index:undefined, family_index: undefined, institute_index: undefined},{headers:{'POSTED-BY':token.id}}).then(async response => {
            await store({ ...item, uploaded: true, id: response.data.id }) && await continousMember(items)
        }).catch(async err => await continousMember(items))
    }
    return true;
}

export const continousATT = async (items = 0) => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    if (items.length > 0) {
        let item = items.pop();
        await Axios.post(att_url + item.location_id, { ids: item.rolled },{headers:{'POSTED-BY':token.id}}).then(async response => {
            await store({ ...item, uploaded: true }) && await continousATT(items)
        }).catch(async err => await continousATT(items))
    }
    return true;
}

export const family = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('family');
    return await continous(family_url, data.filter(filter => !filter.uploaded));
}

export const member = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let groups = await retriever('group');
    if(groups.filter(f=>!f.uploaded).length > 0){
        alert("Please upload group data first.");
        return;
    }

    let institute = await retriever('institute');
    if(institute.filter(f=>!f.uploaded).length > 0){
        alert("Please upload institute data first.");
        return;
    }

    let family = await retriever('family');
    if(family.filter(f=>!f.uploaded).length > 0){
        alert("Please upload family data first.");
        return;
    }

    let data = await retriever('member');
    return await continousMember(data.filter(filter => !filter.uploaded));
}

export const institute = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('institute');
    return await continous(institute_url, data.filter(filter => !filter.uploaded))
}

export const group = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let institute = await retriever('institute');
    if (institute.filter(f => !f.uploaded).length > 0) {
        alert("Please upload institute data first.");
        return;
    }
    let data = await retriever('group');
    return await continous(group_url, data.filter(filter => !filter.uploaded))
}

export const survey = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('survey');
    return await continous(survey_url, data.filter(filter => !filter.uploaded))
}

export const ATT = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('ATT');
    return await continousATT(data.filter(filter => !filter.uploaded))
}

export const health = async () => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('health');
    return await continous(health_survey_url, data.filter(f=>!f.uploaded));
}

export const childsurvey = async ()=>{
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('child survey');
    return await continous(child_survey_url, data.filter(f=>!f.uploaded));
}

export const pdherth = async()=>{
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('pdherth');
    return await continous(pdherth_url, data.filter(f=>!f.uploaded));
}

export const ProducerGroup =  async ()=>{
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));
    let data = await retriever('Producer Group');
    return await continous(producer_url, data.filter(f=>!f.uploaded));
}

export default async (i, callback) => {
    if(!token)
    token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));

    ToastAndroid.show('Uploading...',ToastAndroid.SHORT);

    i = i.replace(' ','');

    let collection = {
        family,
        member,
        institute,
        group,
        survey,
        ATT,
        health,
        childsurvey,
        pdherth,
        ProducerGroup
    }
    
    let status = await collection[i]();
    ToastAndroid.show('Updating local data...',ToastAndroid.SHORT)
    try {
        ['group','family'].includes(i) && await table('member');
        await table(i);
    } catch (error) {
        
    }
    
    await RemoveUploaded(i);
    
    return status;
}