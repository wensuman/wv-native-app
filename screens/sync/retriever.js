import _ from 'lodash';
import {get} from '../../module/store';

export default async (belongs) => {
    let data = await get();
    return _.filter(data, { belongsTo: belongs });
}
