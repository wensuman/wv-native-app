import { Alert, AsyncStorage, NetInfo, Text, ToastAndroid, TouchableOpacity } from "react-native";
import { Body, Button, Container, Content, Footer, Icon, List, ListItem, Right, Switch } from "native-base";

import Header from '../../designs/header';
import { NavigationActions } from "react-navigation";
import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import Upload from './upload';
import _ from 'lodash';
import { table } from './download';
import {get, clear} from '../../module/store'

export default class Sync extends React.Component {

    static navigationOptions = {
        title: 'Sync',
        header: null,
    }

    state={token_loaded:false};

    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this)
        this.updateSituation = this.updateSituation.bind(this)
        this.checkInternet = this.checkInternet.bind(this)
        this.setUndefined = this.setUndefined.bind(this)
    }

    componentDidMount() {
        this.updateSituation();
        NetInfo.addEventListener( 'connectionChange', this.checkInternet );
    }

    componentWillUnmount() {
        NetInfo.removeEventListener('connectionChange', this.checkInternet);
    }

    checkInternet() {
        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({ internet_loaded: isConnected, net_loaded: true });
        });
    }

    async updateSituation() {

        let token = JSON.parse(await AsyncStorage.getItem('@wv-data:token'));

        this.checkInternet();
        
        if(!token){
            return;
        }
        
        this.setState({token_loaded: true});

        ToastAndroid.show('Updating...', ToastAndroid.SHORT);

        let data = _.groupBy(await get(), 'belongsTo');

        data && this.setState({ ...data});
        
    }

    setUndefined(i){
        this.setState({[i]: undefined});
        return true;
    }

    render() {
        if (!this.state.internet_loaded) {
            return <Container>
                <Header menu {...this.props} title='No internet' />
                <Content>
                    <Button full light>
                        <Text>You are currently offline. Turn on your wifi first.</Text>
                    </Button>
                </Content>
            </Container>;
        }
        if(!this.state.token_loaded){
            return <Container>
                <Header menu {...this.props} title='Log In First' />
                <Content>
                    <Button full light>
                        <Text>This option is enabled only for logged in users.</Text>
                    </Button>
                </Content>
            </Container>;
        }

        let list = _.map(this.state, (item, key) => (_.endsWith(key, '_loaded') || key == 'updating' || key == 'undefined') ? false : <ListItem key={key} icon><Body><Text>{_.capitalize(key)}</Text></Body><Right><Button transparent onPress={async () => { 
            Alert.alert(
                'Are you sure?',
                'This will post data to server?',
                [
                    { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                    { text: 'OK', onPress: async () => {
                        this.setState({updating:'Uploading...'});
                        await Upload(key) && this.setUndefined(key);
                        this.setState({updating:undefined});
                    }
                    },
                ],
                { cancelable: true }
            )
            }}><Icon name={_.filter(item, (i) => !i.uploaded).length == 0 ? 'md-checkmark-circle' : 'arrow-up'} /></Button></Right></ListItem>).filter(Boolean);
        return <Container style={{backgroundColor:'white'}}>
            <Spinner visible={Boolean(this.state.updating)} textContent={this.state.updating || 'Updating'} />
            <Header menu {...this.props} title='Sync / Update' />
            <Content>
                {list.length > 0 && <List >
                    <ListItem itemDivider>
                        <Text>To Upload</Text>
                    </ListItem>
                    {list}
                </List>}
                <Button style={{margin:10}} block full onPress={() =>{
                            Alert.alert(
                            'Download latest data?',
                            '',
                            [
                                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                { text: 'OK', onPress: async () => {
                                    this.setState({updating: 'Downloading...'})
                                    await ['location:place', 'project', 'member', 'family', 'group', 'institute', 'activities','pdherth'].map(async (i)=>{
                                        let some = await table(i.split(':').pop());
                                        i == 'pdherth' && setTimeout(()=>(this.setState({updating: undefined}), alert('Updated successfully')), 10000);
                                    });
                                } },
                            ],
                            { cancelable: true }
                        )
                    } }>
                    <Text style={{color:'white'}}>Download latest data</Text>
                </Button>
            </Content>
            <Button full danger onPress={() => {
                Alert.alert(
                    'Are you sure?',
                    'It will delete all the data collected on this device.',
                    [
                        { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                        { text: 'OK', onPress: async () => { await clear(), this.props.navigation.dispatch(NavigationActions.reset({index:0,actions:[NavigationActions.navigate({routeName:'Dashboard'})]})), ToastAndroid.show('Data has been cleared',ToastAndroid.SHORT) } },
                    ],
                    { cancelable: true }
                )
            }}>
                <Text style={{color:'white'}}>Clear all data</Text>
            </Button>
        </Container>;
    }
}
