import React from 'react';
export default {
    state_id: '',
    district_id: '',
    municipality_id: '',
    ward_id: '',
    tole: '',
    step: 0,
    secondary_source_of_income: [],
    source_of_income: [],
    major_area_of_expenditure: [],
    main_construction_materials_of_outside_walls: [],
    type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day: [],
    belongsTo: 'family'
}