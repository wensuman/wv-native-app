export const ethnicity = [
    { label: ["Brahman / Chhetri", "Brahman / Chhetri"], value: "Brahman / Chhetri" },
    { label: ["Tarai / Madhesi Other Castes", "Tarai / Madhesi Other Castes"], value: "Tarai / Madhesi Other Castes" },
    { label: ["Dalits", "Dalits"], value: "Dalits" },
    { label: ["Newar", "Newar"], value: "Newar" },
    { label: ["Janajati", "Janajati"], value: "Janajati" },
    { label: ["Muslim", "Muslim"], value: "Muslim" },
    { label: ["Other", "Other"], value: "Other" }
];

export const house_type = [
    { label: ["Rented", "बहाल/भाडाको"], value: "Rented" },
    { label: ["Own", "आफ्नै"], value: "Own" }
];

export const expenditure_type = [
    { label: ["Borrow", "Borrow"], value: "Borrow" },
    { label: ["Loan from Institution", "Loan from Institution"], value: "Loan from Institution" },
    { label: ["Loan from Individual", "Loan from Individual"], value: "Loan from Individual" },
    { label: ["Donation from organization", "Donation from organization"], value: "Donation from organization" },
    { label: ["Other", "Other"], value: "Other" },
];

export const gender_headed  = [
    { label: ["Male", "पुरुष"], value: "Male" },
    { label: ["Female", "महिला"], value: "Female" },
    { label: ["Boy", "केटा"], value: "Boy" },
    { label: ["Girl", "केटी"], value: "Girl" },
];

export const source_of_income = [
    { label: ["Agriculture", "कृषी"], value: "Agriculture" },
    { label: ["Livestock", "पशुपालन"], value: "Livestock" },
    { label: ["Service", "नोकरी"], value: "Service" },
    { label: ["Skilled labor", "दक्ष श्रमिक"], value: "Skilled labor" },
    { label: ["Unskilled labor( Daily wage)", "अदक्ष श्रमिक( दैनिक ज्यालादारी)"], value: "Unskilled labor( Daily wage)" },
    { label: ["Small Businesses", "सानोतिनो व्यापार"], value: "Small Businesses" },
    { label: ["Foreign employment", "बैदेसिक रोजगारी"], value: "Foreign employment" },
    { label: ["Small and Medium Entrepreneurs", "साना तथा मझौला उद्यम"], value: "Small and Medium Entrepreneurs" },
	{ label: ["Social security support", "सामाजिक सुरक्षा सहयोग"], value: "Social security support" },
    { label: ["Other", "अन्य"], value: "Other" },
];

export const major_area_of_expenditure = [
    { label: ["Education", "Education"], value: "Education" },
    { label: ["Health", "Health"], value: "Health" },
    { label: ["Food", "Food"], value: "Food" },
    { label: ["Entertainment", "Entertainment"], value: "Entertainment" },
    { label: ["Clothing", "Clothing"], value: "Clothing" },
    { label: ["Household maintenance","Household maintenance"], value: "Household maintenance" },
    { label: ["Other", "Other"], value: "Other" },
];

export const number_of_household_members = [
    { label: ["Eight or more", "८ वा सोभन्दा बढी"], value: "Eight or more" },
    { label: ["Seven", "७ जना"], value: "Seven" },
    { label: ["Six", "६ जना"], value: "Six" },
    { label: ["Five", "५ जना"], value: "Five" },
    { label: ["Four", "४ जना"], value: "Four" },
    { label: ["Three", "३ जना"], value: "Three" },
    { label: ["Two or less than two", "१ वा २ जना"], value: "Two or less than two" },
]

export const type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day = [
    { label: ["No male head or spouse", "पति नभएको/महिला परिवार मुली भएको"], value: "No male head or spouse" },
	{ label: ["Paid wages on a daily basis or contract/piece-rate in non-agriculture", "गैर–कृषि क्षेत्रमा दैनिक ज्यालादारी/ठेक्कामा रोजगार"], value: "Paid wages on a daily basis or contract/piece-rate in non-agriculture" },
    { label: ["Does not work or Paid wages on daily basis or contract/piece-rate in agriculture", "काम नगर्ने/कृषि क्षेत्रमा दैनिक ज्यालादारी/ठेक्कामा रोजगार"], value: "Does not work or Paid wages on daily basis or contract/piece-rate in agriculture" },
    { label: ["Self-employed in agriculture", "कृषिमा स्वरोजगार"], value: "Self-employed in agriculture" },
    { label: ["Self-employed in non-agriculture", "गैर–कृषि क्षेत्रमा स्वरोजगार"], value: "Self-employed in non-agriculture" },
    { label: ["Paid wages on a long-term basis in agriculture or non-agriculture", "कृषि वा गैर–कृषि क्षेत्रमा लामो अवधिको ज्यालादारी"], value: "Paid wages on a long-term basis in agriculture or non-agriculture" },
]

export const number_of_bedroom_in_the_residence = [
    { label: ["none", "छुट्टै सुत्ने कोठा नभएको"], value: "none" },
    { label: ["one", "एउटा"], value: "one" },
    { label: ["two", "दुईवटा"], value: "two" },
    { label: ["three or more", "तिन वा सो भन्दा बढी"], value: "three or more" },
]

export const main_construction_materials_of_outside_walls = [
    { label: ["No outside walls", "भित्ता नभएको"], value: "No outside walls" },
    { label: ["Bamboos/leaves", "बास, स्याउला"], value: "Bamboos/leaves" },
    { label: ["Umbaked bricks", "काँचो ईटा"], value: "Umbaked bricks" },
    { label: ["Wood/CGI", "काठ/जस्ता"], value: "Wood/CGI" },
    { label: ["Mud-bounded brics/stones", "माटोको जोडाई भएको ईटा/ ढुंगा"], value: "Mud-bounded brics/stones" },
    { label: ["Cement-bounded bricks/ stones", "सिमेण्टले जोडेको ईटा/ढुंगा"], value: "Cement-bounded bricks/ stones" },

]

export const main_materials_on_the_roof = [
    { label: ["Straw/Thatch","पराल/खर/छवाली"], value: "Straw/Thatch" },
    { label: ["Earth/Mud","माटो"], value: "Earth/Mud" },
    { label: ["Titles/slate","टायल/स्लेट/खपडा वा झिंगाटी/ढुंगा"], value: "Titles/slate" },
    { label: ["Wood/planks, or galvanised iron", "काठ/फ्ल्याक वा जस्ता/टिन"], value: "Wood/planks, or galvanised iron" },
    { label: ["Concentrate/cement", "सिमेन्ट/ढलान"], value: "Concentrate/cement" },
]

export const type_of_stove_used_by_household_for_cooking = [
    { label: ["Open fireplaces", "अगेना"], value: "Open fireplaces" },
    { label: ["Mud", "माटोको चुल्हो"], value: "Mud" },
    { label: ["Kerosene stove", "मट्टीतेल चुल्हो"], value: "Kerosene stove" },
    { label: ["Gas stove", "ग्यास चुल्हो"], value: "Gas stove" },
    { label: ["Smokeless oven", "धुवांरहित चुल्हो"], value: "Smokeless oven" },
]

export const type_of_toilet_used_by_household = [
    { label: ["No toilet", "चर्पी नभएको"], value: "No toilet" },
    { label: ["Household non flush", "साधारण घरायसी फ्लस नभएको चर्पी"], value: "Household non flush" },
    { label: ["Community latrine", "सामुदायिक चर्पी"], value: "Community latrine" },
    { label: ["Household flush (connected to municipal sewerage)", "फ्ल्स भएको चर्पी (सार्वजनिक ढलमा जोडिएको"], value: "Household flush (connected to municipal sewerage)" },
    { label: ["Household flush and connected to septic tank", "फ्ल्स भएको चर्पी (सेपटिक टयांकमा जोडिएको"], value: "Household flush and connected to septic tank" },
]

export const number_of_telephone_sets_in_household = [
    { label: ["None", "नभएको"], value: "None" },
    { label: ["One", "एउटा भएको"], value: "One" },
    { label: ["Two or more", "दुई वा बढी"], value: "Two or more" },
]

export default {
    ethnicity,
    house_type,
    expenditure_type,
    source_of_income,
    gender_headed,
    major_area_of_expenditure,
    number_of_household_members,
    type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day,
    number_of_bedroom_in_the_residence,
    main_construction_materials_of_outside_walls,
    main_materials_on_the_roof,
    type_of_stove_used_by_household_for_cooking,
    type_of_toilet_used_by_household,
    number_of_telephone_sets_in_household
}