import {
  Button,
  Picker,
  StyleSheet,
  Text,
  View
} from "react-native";

import List from './list';
import New from './new';
import NewMember from './member/new';
import React from "react";
import Single from './single';
import SingleMember from './member/single';
import {
  StackNavigator
} from "react-navigation";

export default StackNavigator({
  FamilyList: {
    name: 'Family',
    screen: List
  },
  New: {
    name: 'New Family',
    screen: New
  },
  FamilySingle: {
    name: 'Single Family',
    screen: Single,
  },
  NewMember: {
    name: 'New Member',
    screen: NewMember
  },
  ViewMember: {
    name: 'View Member',
    screen: SingleMember
  }
}, {
    headerMode: 'none',
    initialRouteName: 'FamilyList',
  })