import { Body, Button, Card, CardItem, Container, Content, DeckSwiper, Form, Icon, Item, Left, List, ListItem, Right, Title } from 'native-base';
import { Input, Number, Radio, Select } from '../../forms/input';
import { ScrollView, Text, ToastAndroid } from 'react-native'

import Create from '../../designs/create'
import Header from '../../designs/header';
import Label from '../../module/questions';
import Labels from '../../module/label'
import { NavigationActions } from "react-navigation";
import Option from '../../module/options';
import Options from './options';
import React from "react";
import SingleLocation from '../../module/location'
import state from './state'
import store from '../../module/store';

export default class New extends React.Component {

  static navigationOptions = {
    drawerLockMode: "locked-closed",
    header: null
  }

  constructor(props) {
    super(props);
    this.getQuestions = this.getQuestions.bind(this);
    this.state = state;
    if (this.props.navigation.state.params && this.props.navigation.state.params.state)
      this.state = { ...this.state, ...this.props.navigation.state.params.state };
    this.state.lang = 0;
  }

  getQuestions() {
    return [
      {
        component: () => <SingleLocation lang={this.state.lang} locations={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} onChange={state => {
          this.setState(state);
        }} />,
        validation: () => this.required(['state_id', 'district_id', 'municipality_id', 'ward_id'], 'Please fill up the location fields correctly')
      },
      { component: () => <Input label={Labels.family_name[this.state.lang]} value={this.state.family_name} key='family_name' onChange={(text) => { this.setState({ family_name: text }) }} />, validation: () => this.required(['family_name']) },
      { component: () => <Input label={Labels.head_name[this.state.lang]} value={this.state.head_name} key='head_name' onChange={(text) => { this.setState({ head_name: text }) }} />, validation: () => this.required(['head_name']) },
      { component: () => <Select data={Options.gender_headed.ofLang(this.state.lang)} label={Labels.gender_headed[this.state.lang]} selected={this.state.gender_headed} key='head_name' onChange={(gender_headed) => { this.setState({ gender_headed }) }} />, validation: () => this.required(['gender_headed']) },
      { component: () => <Input label={Labels.language[this.state.lang]} value={this.state.language} key='language' onChange={(text) => { this.setState({ language: text }) }} />, validation: () => this.required(['language']) },
      { component: () => <Number label={Labels.contact_number[this.state.lang]} value={this.state.contact_number} key='contact_number' onChange={(text) => { this.setState({ contact_number: text }) }} />, validation: () => () => this.required(['contact_number']) },
      { component: () => <Select data={Options.house_type.ofLang(this.state.lang)} label={Labels.house_type[this.state.lang]} selected={this.state.house_type} key='house_type' onChange={(text) => this.setState({ house_type: text })} />, validation: () => this.required(['house_type']) },
      this.state.house_type == 'Own' && { component: () => <Input label={Labels.name_of_person_who_legally_owns_the_house[this.state.lang]} value={this.state.name_of_person_who_legally_owns_the_house} key='name_of_person_who_legally_owns_the_house' onChange={(text) => { this.setState({ name_of_person_who_legally_owns_the_house: text }) }} /> },
      { component: () => <Input label={Labels.house_number[this.state.lang]} value={this.state.house_number} key='house_number' onChange={(text) => { this.setState({ house_number: text }) }} /> },
      { component: () => <Select label={Labels.family_have_registered_children[this.state.lang]} selected={this.state.family_have_registered_children} key='family_have_registered_children' onChange={(text) => { this.setState({ family_have_registered_children: text }) }} />, validation: () => this.required(['family_have_registered_children']) },
      { component: () => <Select data={Options.ethnicity.ofLang(this.state.lang)} label={Labels.ethnicity_or_cast[this.state.lang]} selected={this.state.ethnicity_or_cast} key='ethnicity_or_cast' onChange={(text) => { this.setState({ ethnicity_or_cast: text }) }} />, validation: () => this.required(['ethnicity_or_cast']) },
      { component: () => <Select label={Labels.does_family_own_land[this.state.lang]} selected={this.state.does_family_own_land} key='does_family_own_land' onChange={(text) => { this.setState({ does_family_own_land: text }) }} />, validation: () => this.required(['does_family_own_land']) },
      this.state.does_family_own_land == 'yes' && { component: () => <Number label={Labels.own_land_area_in_bigha[this.state.lang]} value={this.state.own_land_area_in_bigha} key='own_land_area_in_bigha' onChange={(text) => { this.setState({ own_land_area_in_bigha: text }) }} /> },
      this.state.does_family_own_land == 'yes' && { component: () => <Number label={Labels.own_land_area_in_kattha[this.state.lang]} value={this.state.own_land_area_in_kattha} key='own_land_area_in_kattha' onChange={(text) => { this.setState({ own_land_area_in_kattha: text }) }} /> },
      this.state.does_family_own_land == 'yes' && { component: () => <Number label={Labels.own_land_area_in_dhur[this.state.lang]} value={this.state.own_land_area_in_dhur} key='own_land_area_in_dhur' onChange={(text) => { this.setState({ own_land_area_in_dhur: text }) }} /> },
      this.state.does_family_own_land == 'yes' && { component: () => <Number label={Labels.own_land_area_in_ropani[this.state.lang]} value={this.state.own_land_area_in_ropani} key='own_land_area_in_ropani' onChange={(text) => { this.setState({ own_land_area_in_ropani: text }) }} /> },
      this.state.does_family_own_land == 'yes' && { component: () => <Number label={Labels.own_land_area_in_aana[this.state.lang]} value={this.state.own_land_area_in_aana} key='own_land_area_in_aana' onChange={(text) => { this.setState({ own_land_area_in_aana: text }) }} /> },
      this.state.does_family_own_land == 'yes' && { component: () => <Number label={Labels.own_land_area_in_hector[this.state.lang]} value={this.state.own_land_area_in_hector} key='own_land_area_in_hector' onChange={(text) => { this.setState({ own_land_area_in_hector: text }) }} /> },
      { component: () => <Select label={Labels.does_family_have_lease_land[this.state.lang]} selected={this.state.does_family_have_lease_land} key='does_family_have_lease_land' onChange={(text) => { this.setState({ does_family_have_lease_land: text }) }} />, validation: () => this.required(['does_family_have_lease_land']) },
      this.state.does_family_have_lease_land == 'yes' && { component: () => <Number label={Labels.lease_land_area_in_bigha[this.state.lang]} value={this.state.lease_land_area_in_bigha} key='lease_land_area_in_bigha' onChange={(text) => { this.setState({ lease_land_area_in_bigha: text }) }} /> },
      this.state.does_family_have_lease_land == 'yes' && { component: () => <Number label={Labels.lease_land_area_in_kattha[this.state.lang]} value={this.state.lease_land_area_in_kattha} key='lease_land_area_in_kattha' onChange={(text) => { this.setState({ lease_land_area_in_kattha: text }) }} /> },
      this.state.does_family_have_lease_land == 'yes' && { component: () => <Number label={Labels.lease_land_area_in_dhur[this.state.lang]} value={this.state.lease_land_area_in_dhur} key='lease_land_area_in_dhur' onChange={(text) => { this.setState({ lease_land_area_in_dhur: text }) }} /> },
      this.state.does_family_have_lease_land == 'yes' && { component: () => <Number label={Labels.lease_land_area_in_ropani[this.state.lang]} value={this.state.lease_land_area_in_ropani} key='lease_land_area_in_ropani' onChange={(text) => { this.setState({ lease_land_area_in_ropani: text }) }} /> },
      this.state.does_family_have_lease_land == 'yes' && { component: () => <Number label={Labels.lease_land_area_in_aana[this.state.lang]} value={this.state.lease_land_area_in_aana} key='lease_land_area_in_aana' onChange={(text) => { this.setState({ lease_land_area_in_aana: text }) }} /> },
      this.state.does_family_have_lease_land == 'yes' && { component: () => <Number label={Labels.lease_land_area_in_hector[this.state.lang]} value={this.state.lease_land_area_in_hector} key='lease_land_area_in_hector' onChange={(text) => { this.setState({ lease_land_area_in_hector: text }) }} /> },
      { component: () => <Select multiple data={Options.source_of_income.ofLang(this.state.lang)} label={Labels.sources_of_income[this.state.lang]} selected={this.state.sources_of_income} key='sources_of_income' onChange={sources_of_income => this.setState({ sources_of_income })} /> , validation: () => this.required(['sources_of_income'])},
      { component: () => <Select data={Options.source_of_income.filter(s=>this.state.sources_of_income && this.state.sources_of_income.includes(s.value)).ofLang(this.state.lang)} label={Labels.primary_source_of_income[this.state.lang]} selected={this.state.primary_source_of_income} key='primary_source_of_income' onChange={(text) => { this.setState({ primary_source_of_income: text }) }} />, validation: () => this.required(['primary_source_of_income']) },
      this.state.primary_source_of_income == 'Other' && { component: () => <Input label={Labels.specify_other_primary_source_income[this.state.lang]} value={this.state.specify_other_primary_source_income} key='specify_other_primary_source_income' onChange={(text) => { this.setState({ specify_other_primary_source_income: text }) }} /> },
      //PPI Question
      { component: () => <Select data={Options.number_of_household_members.ofLang(this.state.lang)} label={Labels.number_of_household_members[this.state.lang]} selected={this.state.number_of_household_members} key='number_of_household_members' onChange={(text) => { this.setState({ number_of_household_members: text }) }} />, validation: () => this.required(['number_of_household_members']) },
      { component: () => <Select data={Options.type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day.ofLang(this.state.lang)} label={Labels.type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day[this.state.lang]} selected={this.state.type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day} key='type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day' onChange={type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day => this.setState({ type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day })} />, validation: () => this.required(['type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day']) },
      { component: () => <Select data={Options.number_of_bedroom_in_the_residence.ofLang(this.state.lang)} label={Labels.number_of_bedroom_in_the_residence[this.state.lang]} selected={this.state.number_of_bedroom_in_the_residence} key='number_of_bedroom_in_the_residence' onChange={(text) => { this.setState({ number_of_bedroom_in_the_residence: text }) }} />, validation: () => this.required(['number_of_bedroom_in_the_residence']) },
      { component: () => <Select data={Options.main_construction_materials_of_outside_walls.ofLang(this.state.lang)} label={Labels.main_construction_materials_of_outside_walls[this.state.lang]} selected={this.state.main_construction_materials_of_outside_walls} key='main_construction_materials_of_outside_walls' onChange={main_construction_materials_of_outside_walls => this.setState({ main_construction_materials_of_outside_walls })} />, validation: () => this.required(['main_construction_materials_of_outside_walls']) },
      { component: () => <Select data={Options.main_materials_on_the_roof.ofLang(this.state.lang)} label={Labels.main_materials_on_the_roof[this.state.lang]} selected={this.state.main_materials_on_the_roof} key='main_materials_on_the_roof' onChange={(text) => { this.setState({ main_materials_on_the_roof: text }) }} />, validation: () => this.required(['main_materials_on_the_roof']) },
      { component: () => <Select label={Labels.residence_has_kitchen[this.state.lang]} selected={this.state.residence_has_kitchen} key='residence_has_kitchen' onChange={(text) => { this.setState({ residence_has_kitchen: text }) }} />, validation: () => this.required(['residence_has_kitchen']) },
      { component: () => <Select data={Options.type_of_stove_used_by_household_for_cooking.ofLang(this.state.lang)} label={Labels.type_of_stove_used_by_household_for_cooking[this.state.lang]} selected={this.state.type_of_stove_used_by_household_for_cooking} key='type_of_stove_used_by_household_for_cooking' onChange={(text) => { this.setState({ type_of_stove_used_by_household_for_cooking: text }) }} />, validation: () => this.required(['type_of_stove_used_by_household_for_cooking']) },
      { component: () => <Select data={Options.type_of_toilet_used_by_household.ofLang(this.state.lang)} label={Labels.type_of_toilet_used_by_household[this.state.lang]} selected={this.state.type_of_toilet_used_by_household} key='type_of_toilet_used_by_household' onChange={(text) => { this.setState({ type_of_toilet_used_by_household: text }) }} />, validation: () => this.required(['type_of_toilet_used_by_household']) },
      { component: () => <Select label={Labels.does_household_own_agricultural_land[this.state.lang]} selected={this.state.does_household_own_agricultural_land} key='does_household_own_agricultural_land' onChange={(text) => { this.setState({ does_household_own_agricultural_land: text }) }} />, validation: () => this.required(['does_household_own_agricultural_land']) },
      { component: () => <Select data={Options.number_of_telephone_sets_in_household.ofLang(this.state.lang)} label={Labels.number_of_telephone_sets_in_household[this.state.lang]} selected={this.state.number_of_telephone_sets_in_household} key='number_of_telephone_sets_in_household' onChange={(text) => { this.setState({ number_of_telephone_sets_in_household: text }) }} />, validation: () => this.required(['number_of_telephone_sets_in_household']) },
      // Food Insecurity
      { component: () => <Select selected={this.state.saving_money} key='saving_money' onChange={saving_money => this.setState({ saving_money })} label={Label('saving_money', this.state.lang)} /> },
      { component: () => <Select selected={this.state.food_worry} key='food_worry' onChange={food_worry => this.setState({ food_worry })} label={Label('food_worry', this.state.lang)} /> },
      this.state.food_worry == 'yes' && { component: () => <Select data={Option('food_worry', this.state.lang)} selected={this.state.happen1} key='happen1' onChange={happen1 => this.setState({ happen1 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.lack_food} key='lack_food' onChange={lack_food => this.setState({ lack_food })} label={Label('lack_food', this.state.lang)} /> },
      this.state.lack_food == 'yes' && { component: () => <Select data={Option('lack_food', this.state.lang)} selected={this.state.happen2} key='happen2' onChange={happen2 => this.setState({ happen2 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.variety_food} key='variety_food' onChange={variety_food => this.setState({ variety_food })} label={Label('variety_food', this.state.lang)} /> },
      this.state.variety_food == 'yes' && { component: () => <Select data={Option('variety_food', this.state.lang)} selected={this.state.happen3} key='happen3' onChange={happen3 => this.setState({ happen3 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.want_food} key='want_food' onChange={want_food => this.setState({ want_food })} label={Label('want_food', this.state.lang)} /> },
      this.state.want_food == 'yes' && { component: () => <Select data={Option('want_food', this.state.lang)} selected={this.state.happen4} key='happen4' onChange={happen4 => this.setState({ happen4 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.small_meal} key='small_meal' onChange={small_meal => this.setState({ small_meal })} label={Label('small_meal', this.state.lang)} /> },
      this.state.small_meal == 'yes' && { component: () => <Select data={Option('small_meal', this.state.lang)} selected={this.state.happen5} key='happen5' onChange={happen5 => this.setState({ happen5 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.few_meal} key='few_meal' onChange={few_meal => this.setState({ few_meal })} label={Label('few_meal', this.state.lang)} /> },
      this.state.few_meal == 'yes' && { component: () => <Select data={Option('few_meal', this.state.lang)} selected={this.state.happen6} key='happen6' onChange={happen6 => this.setState({ happen6 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.no_food} key='no_food' onChange={no_food => this.setState({ no_food })} label={Label('no_food', this.state.lang)} /> },
      this.state.no_food == 'yes' && { component: () => <Select data={Option('no_food', this.state.lang)} selected={this.state.happen7} key='happen7' onChange={happen7 => this.setState({ happen7 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.hungry_night} key='hungry_night' onChange={hungry_night => this.setState({ hungry_night })} label={Label('hungry_night', this.state.lang)} /> },
      this.state.hungry_night == 'yes' && { component: () => <Select data={Option('hungry_night', this.state.lang)} selected={this.state.happen8} key='happen8' onChange={happen8 => this.setState({ happen8 })} label={Label('happen', this.state.lang)} /> },
      { component: () => <Select selected={this.state.hungry_day} key='hungry_day' onChange={hungry_day => this.setState({ hungry_day })} label={Label('hungry_day', this.state.lang)} /> },
      this.state.hungry_day == 'yes' && { component: () => <Select data={Option('hungry_day', this.state.lang)} selected={this.state.happen9} key='happen9' onChange={happen9 => this.setState({ happen9 })} label={Label('happen', this.state.lang)} /> },
      {component:()=><Select label={Labels.does_family_have_any_malnourished_child_under_five[this.state.lang]} onChange={does_family_have_any_malnourished_child_under_five => this.setState({does_family_have_any_malnourished_child_under_five})} selected={this.state.does_family_have_any_malnourished_child_under_five} key='does_family_have_any_malnourished_child_under_five' />},
      {component:()=><Select label={Labels.does_family_have_any_out_of_school_children_aged_six_to_eighteen[this.state.lang]} selected={this.state.does_family_have_any_out_of_school_children_aged_six_to_eighteen} key='does_family_have_any_out_of_school_children_aged_six_to_eighteen' onChange={does_family_have_any_out_of_school_children_aged_six_to_eighteen=>this.setState({does_family_have_any_out_of_school_children_aged_six_to_eighteen})} />},
      {component:()=><Select data={Option('agreement_real',this.state.lang)} label={Labels.house_is_safe_from_disasters[this.state.lang]} selected={this.state.house_is_safe_from_disasters} key='house_is_safe_from_disasters' onChange={house_is_safe_from_disasters=>this.setState({house_is_safe_from_disasters})} />},
      {component:()=><Select data={Option('agreement_real',this.state.lang)} label={Labels.can_family_cope_with_disasters[this.state.lang]} selected={this.state.can_family_cope_with_disasters} key='can_family_cope_with_disasters' onChange={can_family_cope_with_disasters=>this.setState({can_family_cope_with_disasters})} />},
    ]
  }

  validation(data) {
    this.criteria = data;
    return true;
  }

  required(checkIn, message) {
    if (checkIn.filter((state) => {
      return (!this.state[state] || (this.state[state] || []).length == 0)
    }).length > 0) {
      ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
      return false;
    }
    return true;
  }

  render() {
    return <ScrollView>
      <Header icon={'md-book'} rightText={this.state.lang ?  'NP' : 'EN'} title="Add new family" alertOnBack {...this.props} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
      <Create exists={this.state.exists} onSave={() => {
        store({...this.state, lang:undefined, preview:undefined, step:undefined}, () => {
          this.state.index ? this.props.navigation.dispatch(NavigationActions.back()):
          this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'FamilyList' }),
            ]
          })) });
      }} {...this.props} questions={this.getQuestions()} />
    </ScrollView>;
  }
}