import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    paddingH: {
        marginLeft: 20,
        marginRight: 20
    }
})