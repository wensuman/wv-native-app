import { Body, Button, Card, CardItem, Col, Container, Content, DeckSwiper, Form, Grid, Icon, Item, Label, Left, List, ListItem, Picker, Right, Title, View } from 'native-base';
import { DatePick, Input, Number, Photo, Select } from '../../../forms/input';
import { ScrollView, Text, ToastAndroid } from 'react-native';
import { bs2ad, ad2bs } from 'ad-bs-converter';

import Create from '../../../designs/create';
import Header from '../../../designs/header';
import Labels from '../../../module/label';
import { NavigationActions } from "react-navigation";
import Options from './options';
import Option from '../../../module/options';
import React from "react";
import SingleLocation from '../../../module/location';
import _ from 'lodash';
import state from './state';
import store from '../../../module/store';
import { get, file } from '../../../module/store';

export default class New extends React.Component {

    static navigationOptions = {
        drawerLockMode: "locked-closed",
        header: null
    }

    fm = [];
        
    mm = [];

    ffm = [];

    state = {
        belongsTo:'member',
    };

    constructor(props) {

        super(props);

        if(this.props.navigation.state.params){

            if (this.props.navigation.state.params && this.props.navigation.state.params.location) {
                this.state = { ...this.state, ...this.props.navigation.state.params.location }
            }
            
            if (this.props.navigation.state.params.family_index){
                this.state.family_index = this.props.navigation.state.params.family_index;
            }
            
            if (this.props.navigation.state.params.group_index){
                this.state.group_index = this.props.navigation.state.params.group_index;
            }
                
            if (this.props.navigation.state.params && this.props.navigation.state.params.state){
                this.state = { ...state, ...this.props.navigation.state.params.state };
            }       

            if (this.props.navigation.state.params.family_id){
                this.state.family_id = this.props.navigation.state.params.family_id;
            }

            if (this.props.navigation.state.params.group_id){
                this.state.groups_involved = [this.props.navigation.state.params.group_id];
            }

        }
       
        if (!this.state.lang) {
            this.state.lang = 0;
        }
        
        this.groupList = this.groupList.bind(this);
        this.calculateAge = this.calculateAge.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.getQuestions = this.getQuestions.bind(this);
        
    }

    async componentDidMount() {
        
        this.groupList();
        
        let listy = await file('member');
        let store = _.filter(await get(), {belongsTo:'member'});
        
        const whole = listy.concat(store);

        let byIndex = _.filter(whole, {family_index: this.state.family_index});
        let byId = _.filter(whole, {family_id: this.state.family_id});
        
        this.fm = _.uniqBy(byIndex.concat(byId), f=>{if(f.id) return f.id; if(f.index) return f.index;});
        this.mm = this.fm.filter(m=>m.sex == 'male').map(m=>({label:m.full_name,value: (m.id || m.full_name)}));
        this.fmm = this.fm.filter(m=>m.sex == 'female').map(m=>({label:m.full_name,value: (m.id || m.full_name)}));
        this.setState({'some':'value'});
    
    }

    async groupList() {
        
        let listy = await file('group');
        let parsed = listy.map(m => ({ value: m.id, label: m.name_of_group }));
        let store = _.filter(await get(), {belongsTo:'group'}).map(m=>({value:m.id || m.index, label: m.name_of_group}));
        
        this.groups= _.sortBy(store.concat(parsed), f=>f.label);
    
    }

    required(checkIn, message) {
        
        if (checkIn.filter((state) => {
          return (!this.state[state] || (this.state[state] || []).length == 0)
        }).length > 0) {
          ToastAndroid.show(message || 'Fill up the field correctly', ToastAndroid.SHORT);
          return false;
        }
        return true;
    }

    calculateAge(birthday) { // birthday is a date
        var ageDifMs = Date.now() - (new Date(birthday)).getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        this.setState({ age: Math.abs(ageDate.getUTCFullYear() - 1970) });
        return true;
    }

    getQuestions() {
        const state = this.state;
        const mm = this.mm;
        const fmm = this.fmm;
        let questions = [
            { component: () => <SingleLocation locations={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} onChange={state => { this.setState(state); }} />,validation: () => this.required(['state_id', 'district_id', 'municipality_id', 'ward_id'], 'Please fill up the location fields correctly')},
            { component: () => <Input value={state.full_name} label={Labels.full_name[state.lang]} onChange={(text) => { this.setState({ full_name: text }) }} />, validation: () => this.required(['full_name']) },
            { component: () => <DatePick value={ state.date_of_birth_np } label={Labels.date_of_birth[state.lang]} onChange={(date_of_birth) => { 
                let date_eng = _.values(_.pick(bs2ad(date_of_birth),['year','month','day'])).join('/');
                this.setState({ date_of_birth_np: date_of_birth, date_of_birth: date_eng })
                this.calculateAge(date_eng);
            }} />, validation: () => {
                if(this.required(['date_of_birth']) && (new Date(this.state.date_of_birth)).getTime() >= (new Date()).getTime()){
                    ToastAndroid.show('Date of birth must be less than current date', ToastAndroid.SHORT);
                    return false;
                }
                return this.required(['date_of_birth']);
            } },
            { component: () => <Select selected={state.sex} key="sex" label={Labels.sex[state.lang]} data={Option('gender',state.lang)} onChange={sex => { this.setState({ sex }) }} />, validation: () => this.required(['sex']) },
            state.age > 16 && { component: () => <Select selected={state.head_of_family} key="head_of_family" label={Labels.head_of_family[state.lang]} onChange={head_of_family => { this.setState({ head_of_family }) }} />, validation: () => this.required(['head_of_family']) },
            { component: () => <Select selected={state.father_id} key="father_id" data={mm} label={Labels.father_id[state.lang]} onChange={father_id =>  this.setState({ father_id })} /> },
            { component: () => <Select selected={state.mother_id} key="mother_id" data={fmm} label={Labels.mother_id[state.lang]} onChange={mother_id => this.setState({ mother_id })} /> },
            state.age > 5 && { component: () => <Select selected={state.has_mobile_number} key="has_mobile_number" label={Labels.has_mobile_number[state.lang]} onChange={(text) => { this.setState({ has_mobile_number: text }) }} />, validation: () => this.required(['has_mobile_number']) },
            state.age > 5 && state.has_mobile_number == 'yes' && { component: () => <Number value={state.mobile_number} label={Labels.mobile_number[state.lang]} onChange={(text) => { this.setState({ mobile_number: text }) }} /> },
            state.age > 16 && { component: () => <Select selected={state.has_citizenship} key="has_citizenship" label={Labels.has_citizenship[state.lang]} onChange={(text) => { this.setState({ has_citizenship: text }) }} /> },
            state.age > 16 && state.has_citizenship == 'yes' && { component: () => <Input value={state.citizenship_number} label={Labels.citizenship_number[state.lang]} onChange={(text) => { this.setState({ citizenship_number: text }) }} />},
            state.age <= 16 && state.has_citizenship != 'yes' && { component: () => <Select selected={state.birth_registration} key="birth_registration" label={Labels.birth_registration[state.lang]} onChange={(text) => { this.setState({ birth_registration: text }) }} />},
            state.age <= 16 && state.has_citizenship != 'yes' && state.birth_registration == 'yes' && { component: () => <Select selected={state.birth_registration_certificate} key="birth_registration_certificate" label={Labels.birth_registration_certificate[state.lang]} onChange={(text) => { this.setState({ birth_registration_certificate: text }) }} />},
            state.age <= 16 && state.has_citizenship != 'yes' && state.birth_registration_certificate == 'yes' && { component: () => <Input value={state.birth_registration_certificate_number} label={Labels.birth_registration_certificate_number[state.lang]} onChange={(text) => { this.setState({ birth_registration_certificate_number: text }) }} /> },
            state.age < 5 && { component: () => <Number value={state.height} label={Labels.height[state.lang]} onChange={height => { this.setState({ height }) }} />, validation: ()=>{
                if(!this.state.height){
                    return true;
                }
                return ( this.state.height <= 130 && this.state.height >= 20 ) || ToastAndroid.show('Height must be between 20 and 130', ToastAndroid.SHORT);
            } },
            state.age < 5 && { component: () => <Number value={state.weight} label={Labels.weight[state.lang]} onChange={weight => { this.setState({ weight }) }} />, validation: ()=>{
                if(!this.state.weight){
                    return true;
                }
                return this.state.weight <= 30 || ToastAndroid.show('Weight must be less than 30', ToastAndroid.SHORT);
            }},
            { component: () => <Select selected={state.has_disabilities} key="has_disabilities" label={Labels.has_disabilities[state.lang]} onChange={(text) => { this.setState({ has_disabilities: text }) }} />, validation: () => this.required(['has_disabilities']) },
            state.has_disabilities == 'yes' && { component: () => <Select data={Options.type_of_disabilities.ofLang(state.lang)} selected={state.type_of_disabilities} key="type_of_disabilities" label={Labels.type_of_disabilities[state.lang]} onChange={type_of_disabilities => this.setState({ type_of_disabilities })} />, validation: () => this.required(['type_of_disabilities']) },
            state.age >= 1 && state.age <=25 && { component: () => <Select selected={state.is_registered} key="is_registered" label={Labels.is_registered[state.lang]} onChange={(text) => { this.setState({ is_registered: text }) }} />, validation: () => this.required(['is_registered']) },
            state.age >= 1 && state.age <=25 && state.is_registered == 'yes' &&  { component: () => <Number value={state.rc_no} label={Labels.rc_no[state.lang]} onChange={rc_no => { this.setState({ rc_no }) }} />},
            state.age > 5 && { component: () => <Select selected={state.are_you_able_to_read_and_write} key="are_you_able_to_read_and_write" label={Labels.are_you_able_to_read_and_write[state.lang]} onChange={(text) => { this.setState({ are_you_able_to_read_and_write: text }) }} />, validation: () => this.required(['are_you_able_to_read_and_write']) },
            state.age > 5 && { component: () => <Select selected={state.has_been_to_school} key="has_been_to_school" label={Labels.has_been_to_school[state.lang]} onChange={(text) => { this.setState({ has_been_to_school: text }) }} />, validation: () => this.required(['has_been_to_school']) },
            state.age > 5 && state.has_been_to_school == 'yes' && { component: () => <Select selected={state.completed_education_level} key="completed_education_level" label={Labels.completed_education_level[state.lang]} data={Options.completed_education_level.ofLang(state.lang)} onChange={(text) => { this.setState({ completed_education_level: text }) }} />, validation: () => this.required(['completed_education_level']) },
            state.age > 5 && state.has_been_to_school == 'yes' && { component: () => <Select selected={state.currently_enrolled_to_school_or_college} key="currently_enrolled_to_school_or_college" label={Labels.currently_enrolled_to_school_or_college[state.lang]} onChange={(text) => { this.setState({ currently_enrolled_to_school_or_college: text }) }} />, validation: () => this.required(['currently_enrolled_to_school_or_college']) },
            state.age > 5 && { component: () => <Select data={Options.yes_no_dnt.ofLang(this.state.lang)} selected={state.have_you_ever_taken_skill_based_training} key="have_you_ever_taken_skill_based_training" label={Labels.have_you_ever_taken_skill_based_training[state.lang]} onChange={(text) => { this.setState({ have_you_ever_taken_skill_based_training: text }) }} />, validation: () => this.required(['have_you_ever_taken_skill_based_training']) },
            state.age > 5 && { component: () => <Select selected={state.is_person_working} key="is_person_working" label={Labels.is_person_working[state.lang]} onChange={(text) => { this.setState({ is_person_working: text }) }} />, validation: () => this.required(['is_person_working']) },
            state.age > 5 && state.age <= 18 && state.is_person_working == 'yes' && { component: () => <Select multiple data={Options.reason_for_child_working.ofLang(state.lang)} selected={state.reason_for_child_working} key="reason_for_child_working" label={Labels.reason_for_child_working[state.lang]} onChange={reason_for_child_working => this.setState({ reason_for_child_working })} />, validation: () => this.required(['reason_for_child_working']) },
            state.age > 5 && { component: () => <Select selected={state.marital_status} key="marital_status" label={Labels.marital_status[state.lang]} data={Options.marital_status.ofLang(state.lang)} onChange={(text) => { this.setState({ marital_status: text }) }} />, validation: () => this.required(['marital_status']) },
            state.age < 50 && state.marital_status && state.marital_status.toLowerCase() != 'unmarried' && state.sex == 'female' && { component: () => <Select selected={state.is_pregnant} key="is_pregnant" label={Labels.is_pregnant[state.lang]} onChange={(text) => { this.setState({ is_pregnant: text }) }} />, validation: () => this.required(['is_pregnant']) },
            state.age < 50 && state.marital_status && state.marital_status.toLowerCase() != 'unmarried' && state.sex == 'female' && { component: () => <Select selected={state.is_lactating} key="is_lactating" label={Labels.is_lactating[state.lang]} onChange={(text) => { this.setState({ is_lactating: text }) }} />, validation: () => this.required(['is_lactating']) },
            state.marital_status && state.marital_status.toLowerCase() != 'unmarried' && state.sex == 'female' && { component: () => <Select selected={state.husband_id} key="husband_id" data={this.fm.filter(m=>m.sex=='male').map(m=>({label: m.full_name, value: m.id || m.full_name}))} label={Labels.husband_name[state.lang]} onChange={(husband_id) => { this.setState({ husband_id }) }} /> },
            state.age > 16 && { component: () => <Select data={Options.yes_no_dnt.ofLang(this.state.lang)} selected={state.has_bank_account} key="has_bank_account" label={Labels.has_bank_account[state.lang]} onChange={(text) => { this.setState({ has_bank_account: text }) }} />, validation: () => this.required(['has_bank_account']) },
            state.age > 5 && { component: () => <Select selected={state.group_associate} key="group_associate" label={Labels.group_associate[state.lang]} onChange={group_associate => { this.setState({  group_associate }) }} />, validation: () => this.required(['group_associate']) },
            state.age > 5 && state.group_associate == 'yes' && { component: () => <Select multiple data={this.groups || []} searchable limit={5} label={Labels.groups_involved[state.lang]} onChange={groups_involved => { this.setState({ groups_involved: groups_involved })}} selected={state.groups_involved} key="groups_involved" /> },
        ];
        this.props.navigation.state.params.group_index && !this.props.navigation.state.params.group_id && questions.push({ component: () =>  <Select data={Options.designation.ofLang(this.state.lang)} label={['Designation of member in this group']} selected={this.state['group_' + this.props.navigation.state.params.group_index + '_designation']} onChange={designation => this.setState({ ['group_' + this.props.navigation.state.params.group_index + '_designation']: designation})} />})
        state.groups_involved && state.groups_involved.map(g=>{
            let group = _ .find(this.groups, { value: parseInt(g) });
            let options = Options.designation.ofLang(this.state.lang);
            group && questions.push({ component: () => <Select key={'group_design_'+g} data={options} label={['Designation of member in ' + group.label]} selected={this.state['group_' + g + '_designation']} onChange={designation => {console.log(designation),this.setState({ ['group_' + g + '_designation']: designation})}} />})
        });
        return questions;
    }

    render() {
        return <ScrollView>
            <Header icon={'md-book'} title={this.state.index ? 'Edit member' : 'Add member'} alertOnBack {...this.props} rightText={this.state.lang ? 'NP' : 'EN'} onRight={() => { this.setState({ lang: (this.state.lang + 1) % 2 }) }} />
            <Create exists={this.state.exists} onSave={() => {
                store(this.state, () => { this.props.navigation.dispatch(NavigationActions.back()) });
            }} {...this.props} questions={this.getQuestions()} />
        </ScrollView>;
    }
}