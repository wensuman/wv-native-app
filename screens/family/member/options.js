export const gender = [
    { label: ['Male', 'पुरुष'], value: 'male' },
    { label: ['Female', 'महिला'], value: 'female' },
    { label: ['Third Gender', 'Third Gender'], value: 'third gender' },
]
export const type_of_disabilities = [
    { label: ['Physical', 'Physical'], value: 'Physical' },
    { label: ['Blindness / low vision', 'Blindness / low vision'], value: 'Blindness / low vision' },
    { label: ['Deaf / hard to hearing', 'Deaf / hard to hearing'], value: 'Deaf / hard to hearing' },
    { label: ['Deaf - blind', 'Deaf - blind'], value: 'Deaf - blind' },
    { label: ['Speech problem', 'Speech problem'], value: 'Speech problem' },
    { label: ['Mental disable', 'Mental disable'], value: 'Mental disable' },
    { label: ['Intellectual disable', 'Intellectual disable'], value: 'Intellectual disable' },
    { label: ['Multiple disable', 'Multiple disable'], value: 'Multiple disable' },
]

export const yes_no_dnt = [
    {
        label: ['Yes', 'छ'],
        value: 'Yes'
    },
    {
        label: ['No', 'छैन'],
        value: 'No'
    },
    {
        label: ['Don\'t Know', 'थाहा छैन'],
        value: 'Don\'t Know'
    },
]

export const completed_education_level = [
    { label: ['ECD', 'ECD'], value: 'ECD' },
    { label: ['Nursery', 'Nursery'], value: 'Nursery' },
    { label: ['Lower Kinder Garden', 'Lower Kinder Garden'], value: 'Lower Kinder Garden' },
    { label: ['Upper Kinder Garden', 'Upper Kinder Garden'], value: 'Upper Kinder Garden' },
    { label: ['Class 1', 'Class 1'], value: 'Class 1' },
    { label: ['Class 2', 'Class 2'], value: 'Class 2' },
    { label: ['Class 3', 'Class 3'], value: 'Class 3' },
    { label: ['Class 4', 'Class 4'], value: 'Class 4' },
    { label: ['Class 5', 'Class 5'], value: 'Class 5' },
    { label: ['Class 6', 'Class 6'], value: 'Class 6' },
    { label: ['Class 7', 'Class 7'], value: 'Class 7' },
    { label: ['Class 8', 'Class 8'], value: 'Class 8' },
    { label: ['Class 9', 'Class 9'], value: 'Class 9' },
    { label: ['Class 10', 'Class 10'], value: 'Class 10' },
    { label: ['Class 11', 'Class 11'], value: 'Class 11' },
    { label: ['Class 12', 'Class 12'], value: 'Class 12' },
    { label: ['Bachelor First year', 'Bachelor First year'], value: 'Bachelor First year' },
    { label: ['Bachelor Second year', 'Bachelor Second year'], value: 'Bachelor Second year' },
    { label: ['Bachelor Third year', 'Bachelor Third year'], value: 'Bachelor Third year' },
    { label: ['Bachelor Fourth year', 'Bachelor Fourth year'], value: 'Bachelor Fourth year' },
    { label: ['Bachelor Fifth year', 'Bachelor Fifth year'], value: 'Bachelor Fifth year' },
    { label: ['Master First year', 'Master First year'], value: 'Master First year' },
    { label: ['Master Second year', 'Master Second year'], value: 'Master Second year' },
    { label: ['P.H.D.', 'P.H.D.'], value: 'P.H.D.' },
];

export const reason_for_dropout = [
    { label: ['School fees too expensive', 'School fees too expensive'], value: 'School fees too expensive' },
    { label: ['Uniform/shoes/clothes for school too expensive', 'Uniform/shoes/clothes for school too expensive'], value: 'Uniform/shoes/clothes for school too expensive' },
    { label: ['Books or other supplies for school to expensive', 'Books or other supplies for school to expensive'], value: 'Books or other supplies for school to expensive' },
    { label: ['Lack of transport', 'Lack of transport'], value: 'Lack of transport' },
    { label: ['No female teachers', 'No female teachers'], value: 'No female teachers' },
    { label: ['Parents/ Guardians did not want me to go', 'Parents/ Guardians did not want me to go'], value: 'Parents/ Guardians did not want me to go' },
    { label: ['School quality issue', 'School quality issue'], value: 'School quality issue' },
    { label: ['Lack of interest', 'Lack of interest'], value: 'Lack of interest' },
    { label: ['Got married', 'Got married'], value: 'Got married' },
    { label: ['Illness: family', 'Illness: family'], value: 'Illness: family' },
    { label: ['Illness: Self', 'Illness: Self'], value: 'Illness: Self' },
    { label: ['Had to work outside home', 'Had to work outside home'], value: 'Had to work outside home' },
    { label: ['Has disability', 'Has disability'], value: 'Has disability' },
    { label: ['Does not understand language used in school', 'Does not understand language used in school'], value: 'Does not understand language used in school' },
    { label: ['Unsafe to travel to school', 'Unsafe to travel to school'], value: 'Unsafe to travel to school' },
    { label: ['Protection Issues (Abuse, Bulling, etc)', 'Protection Issues (Abuse, Bulling, etc)'], value: 'Protection Issues (Abuse, Bulling, etc)' },
    { label: ['Stigma/discrimination at school', 'Stigma/discrimination at school'], value: 'Stigma/discrimination at school' },
    { label: ['School conflict with beliefs', 'School conflict with beliefs'], value: 'School conflict with beliefs' },
    { label: ['No separate toilets for girls at school', 'No separate toilets for girls at school'], value: 'No separate toilets for girls at school' },
    { label: ['Disaster in community', 'Disaster in community'], value: 'Disaster in community' },
    { label: ['Conflict in community', 'Conflict in community'], value: 'Conflict in community' },
    { label: ['Labor migration', 'Labor migration'], value: 'Labor migration' },
    { label: ['Not interested', 'Not interested'], value: 'Not interested' },
    { label: ['Not smart enough, not worthwhile', 'Not smart enough, not worthwhile'], value: 'Not smart enough, not worthwhile' },
    { label: ['Don’t know', 'Don’t know'], value: 'Don’t know' },
    { label: ['No response', 'No response'], value: 'No response' },
]

export const type_of_technical_education = [
    { label: ['CA', 'CA'], value: 'CA' },
    { label: ['Pilot', 'Pilot'], value: 'Pilot' },
    { label: ['Sub Engineer', 'Sub Engineer'], value: 'Sub Engineer' },
    { label: ['Health Assistant', 'Health Assistant'], value: 'Health Assistant' },
    { label: ['ANM', 'ANM'], value: 'ANM' },
    { label: ['CMA', 'CMA'], value: 'CMA' },
    { label: ['JT', 'JT'], value: 'JT' },
    { label: ['JTA (Junior Technical Agriculture)', 'JTA (Junior Technical Agriculture)'], value: 'JTA (Junior Technical Agriculture)' },
    { label: ['Others', 'Others'], value: 'Others' },
]
export const type_of_vocational_education = [
    { label: ['Tailoring', 'Tailoring'], value: 'Tailoring' },
    { label: ['Beautician/parlor', 'Beautician/parlor'], value: 'Beautician/parlor' },
    { label: ['Driving', 'Driving'], value: 'Driving' },
    { label: ['Mobile Repairing', 'Mobile Repairing'], value: 'Mobile Repairing' },
    { label: ['Mobile Shop', 'Mobile Shop'], value: 'Mobile Shop' },
    { label: ['Electrician', 'Electrician'], value: 'Electrician' },
    { label: ['Motor Bike Repairing', 'Motor Bike Repairing'], value: 'Motor Bike Repairing' },
    { label: ['Solar Maintenance', 'Solar Maintenance'], value: 'Solar Maintenance' },
    { label: ['Vegetable Farming', 'Vegetable Farming'], value: 'Vegetable Farming' },
    { label: ['Meat Shop', 'Meat Shop'], value: 'Meat Shop' },
    { label: ['Mason Training', 'Mason Training'], value: 'Mason Training' },
    { label: ['Dental chair side assistant', 'Dental chair side assistant'], value: 'Dental chair side assistant' },
    { label: ['Computer hardware', 'Computer hardware'], value: 'Computer hardware' },
    { label: ['Accounting package', 'Accounting package'], value: 'Accounting package' },
    { label: ['Early child care traininge', 'Early child care traininge'], value: 'Early child care traininge' },
    { label: ['Cook', 'Cook'], value: 'Cook' },
    { label: ['ECD/Montessori', 'ECD/Montessori'], value: 'ECD/Montessori' },
    { label: ['Other', 'Other'], value: 'Other' },

]
export const reason_for_child_working = [
    { label: ['Poor family economic condition', 'Poor family economic condition'], value: 'Poor family economic condition' },
    { label: ['Pressure from parents', 'Pressure from parents'], value: 'Pressure from parents' },
    { label: ['Peer pressure', 'Peer pressure'], value: 'Peer pressure' },
    { label: ['Interested on working', 'Interested on working'], value: 'Interested on working' },
    { label: ['Tailoring', 'Tailoring'], value: 'Tailoring' },
]
export const marital_status = [
    { label: ['Unmarried', 'Unmarried'], value: 'unmarried' },
    { label: ['Married', 'Married'], value: 'married' },
    { label: ['Widow', 'Widow'], value: 'widow' },
    { label: ['Divorced', 'Divorced'], value: 'divorced' },
]

export const designation = [
    { label: ['Chair person', 'अध्यक्ष'], value: 'Chair person' },
    { label: ['Vice-chair person', 'उपाध्यक्ष'], value: 'Vice-chair person' },
    { label: ['Secretary', 'सचिव'], value: 'Secretary' },
    { label: ['Vice-Secretary', 'सह सचिव'], value: 'Vice-Secretary' },
    { label: ['Treasurer', 'कोषाध्यक्ष'], value: 'Treasurer' },
    { label: ['Member', 'सदस्य'], value: 'Member' },
    { label: ['Others', 'अन्य'], value: 'Others' },
]

export default {
    gender,
    type_of_disabilities,
    type_of_vocational_education,
    type_of_technical_education,
    reason_for_dropout,
    completed_education_level,
    reason_for_child_working,
    yes_no_dnt,
    designation,
    marital_status
}