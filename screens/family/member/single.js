import { ScrollView, Text, ToastAndroid } from 'react-native'
import { Body, Button, Container, Fab, Footer, H3, Icon, Item, Left, List, ListItem, Right, ScrollableTab, Separator, Tab, Tabs } from "native-base";

import Header from '../../../designs/header';
import {get,file,remove} from '../../../module/store';
import Label from "../../../module/label";
import Location from '../../../module/location'
import {
    NavigationActions
} from "react-navigation";
import React from "react";
import _ from "lodash";

export default class Single extends React.Component {
    static navigationOptions = {
        header: null,
        drawerLockMode: "locked-closed",
    }

    groups = []

    constructor(props) {
        super(props);
        this.state = { lang: 0, ...this.props.navigation.state.params.member };
        this.load = this.load.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.newmember = this.newmember.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async load() {
        ToastAndroid.show('Refreshing...', ToastAndroid.SHORT);
        let data = await get() || [];
        let member = _.find(data, { belongsTo: 'member', index: this.state.index })
        member && this.setState(member);
        if(member && member.groups_involved){
            let listy = await file('group');
            let parsed = listy.map(m => ({ value: m.id, label: m.name_of_group }));
            let store = _.filter(await get(), {belongsTo:'group'}).map(m=>({value:m.id || m.index, label: m.name_of_group}));
            this.groups= _.sortBy(store.concat(parsed), f=>f.label).filter(g=>member.groups_involved.includes((g.value)));
        }
    }

    render() {
        return (
            <Container>
                <Header hasTabs title="Member detail" alertOnRight onRight={this.state.id ? undefined : () => {
                    remove(this.state.index);
                    this.props.navigation.dispatch(NavigationActions.back());
                }} {...this.props} />
                <ScrollView>
                <List>
                            {this.state.id && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>ID: {this.state.id}</Text>
                            </ListItem>}
                            {this.state.full_name && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.full_name[this.state.lang]}: {_.capitalize(this.state.full_name)}</Text>
                            </ListItem>}
                            {this.state.date_of_birth && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.date_of_birth[this.state.lang]}: {_.capitalize(this.state.date_of_birth)} {this.state.date_of_birth_np && '('+this.state.date_of_birth_np+')'}</Text>
                            </ListItem>}
                            <Location display data={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} />
                            {this.state.sex && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.sex[this.state.lang]}: {_.capitalize(this.state.sex)}</Text>
                            </ListItem>}
                            {this.state.has_mobile_number && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.has_mobile_number[this.state.lang]}: {_.capitalize(this.state.has_mobile_number)}</Text>
                            </ListItem>}
                            {this.state.has_mobile_number && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.mobile_number[this.state.lang]}: {_.capitalize(this.state.mobile_number)}</Text>
                            </ListItem>}
                            {this.state.has_citizenship && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.has_citizenship[this.state.lang]}: {_.capitalize(this.state.has_citizenship)}</Text>
                            </ListItem>}
                            {this.state.has_citizenship == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.citizenship_number[this.state.lang]}: {_.capitalize(this.state.citizenship_number)}</Text>
                            </ListItem>}
                            {this.state.has_citizenship != 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.birth_registration[this.state.lang]}: {_.capitalize(this.state.birth_registration)}</Text>
                            </ListItem>}
                            {this.state.has_citizenship != 'yes' && this.state.birth_registration == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.birth_registration_certificate[this.state.lang]}: {_.capitalize(this.state.birth_registration_certificate)}</Text>
                            </ListItem>}
                            {this.state.has_citizenship != 'yes' && this.state.birth_registration_certificate == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.birth_registration_certificate_number[this.state.lang]}: {_.capitalize(this.state.birth_registration_certificate_number)}</Text>
                            </ListItem>}
                            {this.state.is_registered && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.is_registered[this.state.lang]}: {_.capitalize(this.state.is_registered)}</Text>
                            </ListItem>}
                            {this.state.height && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.height[this.state.lang]}: {this.state.height || 'Not available'}</Text>
                            </ListItem>}
                            {this.state.weight && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.weight[this.state.lang]}: {this.state.weight || 'Not available'}</Text>
                            </ListItem>}
                            {this.state.marital_status && this.state.marital_status.toLowerCase() != 'unmarried' && this.state.sex == 'female' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.is_pregnant[this.state.lang]}: {_.capitalize(this.state.is_pregnant)}</Text>
                            </ListItem>}
                            {this.state.marital_status && this.state.marital_status.toLowerCase() != 'unmarried' && this.state.sex == 'female' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.is_lactating[this.state.lang]}: {_.capitalize(this.state.is_lactating)}</Text>
                            </ListItem>}
                            {this.state.has_disabilities && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.has_disabilities[this.state.lang]}: {_.capitalize(this.state.has_disabilities)}</Text>
                            </ListItem>}
                            {this.state.has_disabilities == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.type_of_disabilities[this.state.lang]}: {(this.state.type_of_disabilities || []).display()}</Text>
                            </ListItem>}
                            {this.state.are_you_able_to_read_and_write && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.are_you_able_to_read_and_write[this.state.lang]}: {_.capitalize(this.state.are_you_able_to_read_and_write)}</Text>
                            </ListItem>}
                            {this.state.has_been_to_school && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.has_been_to_school[this.state.lang]}: {_.capitalize(this.state.has_been_to_school)}</Text>
                            </ListItem>}
                            {this.state.has_been_to_school == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.completed_education_level[this.state.lang]}: {_.capitalize(this.state.completed_education_level)}</Text>
                            </ListItem>}
                            {this.state.has_been_to_school == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.currently_enrolled_to_school_or_college[this.state.lang]}: {_.capitalize(this.state.currently_enrolled_to_school_or_college)}</Text>
                            </ListItem>}
                            {this.state.have_you_ever_taken_skill_based_training && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Text>{Label.have_you_ever_taken_skill_based_training[this.state.lang]}: {_.capitalize(this.state.have_you_ever_taken_skill_based_training)}</Text>
                        </ListItem>}
                        {this.groups.length > 0 && this.groups.map(g=><ListItem key={g.value} style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Text>{g.label}:{this.state['group_'+g.value+'_designation']}</Text>
                        </ListItem>) }
                        {this.state.is_per && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.is_person_working[this.state.lang]}: {this.state.is_person_working || 'No'}</Text>
                            </ListItem>}
                            {this.state.reason_for_child_working && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                                <Text>{Label.reason_for_child_working[this.state.lang]}: {(this.state.reason_for_child_working || []).display()}</Text>
                            </ListItem>}
                            {this.state.marital_status && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Text>{Label.marital_status[this.state.lang]}: {_.capitalize(this.state.marital_status)}</Text>
                        </ListItem>}
                        {this.state.marital_status && this.state.marital_status.toLowerCase() != 'unmarried' && this.state.sex == 'female' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Text>{Label.husband_name[this.state.lang]}: {_.capitalize(this.state.husband_name)}</Text>
                        </ListItem>}
                        {this.state.has_bank_account && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
                            <Text>{Label.has_bank_account[this.state.lang]}: {_.capitalize(this.state.has_bank_account)}</Text>
                        </ListItem>}
                        </List>
                </ScrollView>
                {!this.state.uploaded && !this.state.id && <Fab
                    active={true}
                    direction="up"
                    containerStyle={{}}
                    style={{ backgroundColor: 'orange' }}
                    position="bottomRight"
                    onPress={this.newmember.debounce()}>
                    <Icon name="md-color-filter" />
                    {!this.state.uploaded && !this.state.id && <Button onPress={this.load}><Icon name="refresh" /></Button>}
                </Fab>}
            </Container>
        )
    }
    newmember() {
        if (this.props.navigation.state.routeName == 'SingleInstituteGroupMember') {
            this.props.navigation.navigate('NewInstituteGroupMember', {
                state: this.state
            });
            return;
        }
        this.props.navigation.navigate(this.props.navigation.state.params.from == 'group' ? 'NewGroupMember' : 'NewMember', {
            state: this.state
        });
    }
};