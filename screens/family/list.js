import { ActivityIndicator, Platform, RefreshControl, ScrollView, Text, ToastAndroid, TouchableNativeFeedback, TouchableOpacity } from "react-native";
import { Body, Button, Col, Container, Content, Fab, Header, Icon, Input, Item, Left, List, ListItem, Right, View } from 'native-base';

import { NavigationActions } from "react-navigation";
import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import {get,file} from '../../module/store';

export default class Lists extends React.Component {
  static navigationOptions = {
    title: "Family",
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      models: [],
      loading: 'Loading...',
      chunk:1
    };
    this.componentDidMount = this.componentDidMount.bind(this);
    this.load = this.load.bind(this);
    this.new = this.new.bind(this);
    this.search = this.search.bind(this)
  }

  componentDidMount() {
    this.load();
  }

  async load() {
    ToastAndroid.show('Loading...', ToastAndroid.SHORT);
    let models = await get() || [];
    let downloadedModels = await file('family') || [];
    this.setState({loading:''});
    downloadedModels = downloadedModels.map(m => ({ ...m, ...m.detail, uploaded: true, ...m.location, index: 'index_' + m.id, id: m.id })).reverse()
    models = _.filter(models, { belongsTo: 'family' }).concat(downloadedModels || []);
    this.setState({
      models, original: models || [], family_loaded: true, refreshing: false
    });
  }

  new() {
    this.props.navigation.navigate("New")
  }

  search() {
    if (!this.state.search) {
      this.setState({ models: this.state.original });
    }
    
    this.state.search && this.setState({ models: this.state.original.filter(f => (f.head_name && f.head_name.includes(this.state.search)) || f.id == parseInt(this.state.search) || f.contact_number == this.state.search || (f.detail && f.detail.house_number == this.state.search)) })
  }
  
  render() {
    return <Container>
      <Content>
        <Header style={{ backgroundColor:'orange', marginTop: Platform.Version < 20 ? 24 : 0 }} searchBar rounded >
          <Item>
            <Icon name="ios-search" />
            <Input onChangeText={search => this.setState({ search })} placeholder="Search families by ID, contact number" />
            <Button style={{ paddingHorizontal: 10 }} onPress={this.search} success>
              <Text style={{color:'white'}}>Search</Text>
            </Button>
          </Item>
        </Header>
        <ScrollView horizontal={false} >
          {this.state.models && <List>
            {this.state.models.slice(0,10).map((family, index) => (
              <ListItem key={index} onPress={()=>{
                this.props.navigation.dispatch(NavigationActions.reset({
                  index: 1,
                  actions: [
                    NavigationActions.navigate({ routeName: 'FamilyList' }),
                    NavigationActions.navigate({
                      routeName: 'FamilySingle', params: {
                        data: family
                      }
                    })
                  ]
                }))
              }} style={{ marginLeft: 0, paddingLeft: 10 }}>
                <Body >
                  <Text>ID: {family.id}</Text>
                  <Text>Family Name: {family.family_name}</Text>
                  <Text>Head Name: {family.head_name}</Text>
                  <Text>Contact number: {family.contact_number}</Text>
                  <Text>Ethncity: {family.ethnicity_or_cast}</Text>
                  <Text>House Number: {(family.detail && family.detail.house_number) || family.house_number}</Text>
                  <Text>Number of member: {(family.members && family.members.length)}</Text>
                </Body>
              </ListItem>
            ))}
          </List>}
        </ScrollView>
      </Content>
      <Fab active={true}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: 'orange' }}
        position="bottomRight"
        onPress={this.new.debounce()}>
        <Icon name="md-add" />
        <Button onPress={this.load}><Icon name="refresh" /></Button>
      </Fab>
    </Container>;
  }
}
