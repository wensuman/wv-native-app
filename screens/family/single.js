import { ScrollView, Text, ToastAndroid} from 'react-native';
import { Body, Button, Container, Fab, Icon, Item, List, ListItem, Right, ScrollableTab, Tab, Tabs, View } from 'native-base';

import Header from '../../designs/header';
import Label from '../../module/label';
import Labels from '../../module/questions';
import Location from '../../module/location';
import { NavigationActions } from 'react-navigation';
import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import { remove } from '../../module/store';
import { Input, Number } from '../../forms/input';
import {get,file} from '../../module/store';

export default class Single extends React.Component {

  static navigationOptions = {
    drawerLockMode: 'locked-closed',
    header: null
  }

  constructor(props) {
    super(props);
    this.state = { ...this.props.navigation.state.params.data };
    this.load = this.load.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.edits = this.edits.bind(this);
    this.adds = this.adds.bind(this);
  }

  componentDidMount() {
    this.load();
  }

  async load() {
    ToastAndroid.show('Refreshing...', ToastAndroid.SHORT);
    
    let data = await get() || [];
    
    let members = _.filter(data, { belongsTo: 'member', family_index: this.state.index });

    let downloadedMembers = await file('member') || [];
    
    if (this.state.id && downloadedMembers) {
      downloadedMembers = downloadedMembers.filter(f => f.family_id == this.state.id).map(m => ({ ...m.detail, uploaded: true, ...m.location, index: 'index_mem_' + m.id, ...m}))
      members = downloadedMembers.concat(members)
    }

    let family = _.find(data, { belongsTo: 'family', index: this.state.index });

    this.setState({ members, ...family, is_ready: true });
    
  }

  render() {
    if(!this.state.is_ready){
      return <Spinner visible={true} textContent="Loading..." />
    }
    return <Container>
      <Header hasTabs title='Family detail' alertOnRight onRight={this.state.uploaded ? undefined : () => {
        remove(this.state.index);
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'FamilyList' }),
          ]
        }));
      }} {...this.props} />
      <Tabs renderTabBar={() => <ScrollableTab tabStyle={{ backgroundColor: 'orange' }} />}>
        <Tab heading='Details'>
          <List>
            {this.state.id && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>ID of family : {_.capitalize(this.state.id)}</Text>
            </ListItem>}
            {this.state.family_name && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>Name of family : {_.capitalize(this.state.family_name)}</Text>
            </ListItem>}
            {this.state.language && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>Language of family : {_.capitalize(this.state.language)}</Text>
            </ListItem>}
            {this.state.family_have_registered_children && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>Do family have registered children ? : {_.capitalize(this.state.family_have_registered_children)}</Text>
            </ListItem>}
            {this.state.contact_number && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>Contact No. : {this.state.contact_number}</Text>
            </ListItem>}
            {this.state.house_type && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>House type : {this.state.house_type}</Text>
            </ListItem>}
            {this.state.house_type == 'Own' && this.state.name_of_person_who_legally_owns_the_house &&  <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>Owner Name : {_.capitalize(this.state.name_of_person_who_legally_owns_the_house)}</Text>
            </ListItem>}
            {this.state.house_number && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>House Number :{this.state.house_number}</Text>
            </ListItem>}
            {this.state.ethnicity_or_cast &&<ListItem style={{ marginLeft: 0, paddingLeft: 10 }} >
              <Text>Cast or ethnicity of family : {this.state.ethnicity_or_cast}</Text>
            </ListItem>}
          </List>
        </Tab>
        <Tab heading='Location'>
          <Location display data={_.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole'])} />
        </Tab>
        <Tab heading='Property'>
          <List>
            {this.state.does_family_own_land && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Does your family own the land?: {_.capitalize(this.state.does_family_own_land)}</Text></ListItem>}
            {this.state.does_family_own_land == 'yes' && <View>
              {this.state.own_land_area_in_dhur && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In dhur : {this.state.own_land_area_in_dhur}</Text></ListItem>}
              {this.state.own_land_area_in_kattha && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In kattha : {this.state.own_land_area_in_kattha}</Text></ListItem>}
              {this.state.own_land_area_in_bigha && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In bigha : {this.state.own_land_area_in_bigha}</Text></ListItem>}
              {this.state.own_land_area_in_aana && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In aana : {this.state.own_land_area_in_aana}</Text></ListItem>}
              {this.state.own_land_area_in_ropani && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In ropani : {this.state.own_land_area_in_ropani}</Text></ListItem>}
              {this.state.own_land_area_in_hector && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In hector : {this.state.own_land_area_in_hector}</Text></ListItem>}
            </View>}
            {this.state.does_family_have_lease_land && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Does your family own Lease land? : {_.capitalize(this.state.does_family_have_lease_land)}</Text></ListItem>}
            {this.state.does_family_have_lease_land == 'yes' && <View>
              {this.state.lease_land_area_in_dhur && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In dhur : {this.state.lease_land_area_in_dhur}</Text></ListItem>}
              {this.state.lease_land_area_in_kattha && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In kattha : {this.state.lease_land_area_in_kattha}</Text></ListItem>}
              {this.state.lease_land_area_in_bigha && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In bigha : {this.state.lease_land_area_in_bigha}</Text></ListItem>}
              {this.state.lease_land_area_in_aana && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In aana : {this.state.lease_land_area_in_aana}</Text></ListItem>}
              {this.state.lease_land_area_in_ropani && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In ropani : {this.state.lease_land_area_in_ropani}</Text></ListItem>}
              {this.state.lease_land_area_in_hector && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In hector : {this.state.lease_land_area_in_hector}</Text></ListItem>}
            </View>}
          </List>
        </Tab>
        <Tab heading='Income/Vulnerability'>
          <List>
            {this.state.sources_of_income && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Sources of income : {(this.state.sources_of_income || []).display()}</Text></ListItem>}
            {this.state.primary_source_of_income && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Primary source of income : {this.state.primary_source_of_income}</Text></ListItem>}
            {this.state.does_family_have_any_malnourished_child_under_five && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Does family have any malnourished child under 5?: {(this.state.does_family_have_any_malnourished_child_under_five || []).display()}</Text></ListItem>}
            {this.state.does_family_have_any_out_of_school_children_aged_6_to_18 && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Does family have any out of school children aged 6 to 18?(drop out/not enrolled) : {(this.state.does_family_have_any_out_of_school_children_aged_6_to_18 || []).display()}</Text></ListItem>}
            {this.state.house_is_safe_from_disasters && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>My house is safe from possible flood,landslide,fire hazard?: {(this.state.house_is_safe_from_disasters || []).display()}</Text></ListItem>}
            {this.state.can_family_cope_with_disasters && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>My family can cope with disasters as and when it happens?: {(this.state.can_family_cope_with_disasters || []).display()}</Text></ListItem>}
          </List>
        </Tab>
        <Tab heading='PPI Questions'>
          <ScrollView>
            {this.state.number_of_household_members && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>How many Household members are there ?  : {(this.state.number_of_household_members || []).display()}</Text></ListItem>}
            {this.state.type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>In what types of job did the male head/spouse work the most hours in the past seven days?  : {(this.state.type_of_jobs_where_male_or_spouse_worked_the_most_hours_in_past_seven_day || []).display()}</Text></ListItem>}
            {this.state.number_of_bedroom_in_the_residence && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>How many bedroom does your residence have? : {(this.state.number_of_bedroom_in_the_residence || []).display()}</Text></ListItem>}
            {this.state.main_construction_materials_of_outside_walls && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Main construction materials of outside walls : {(this.state.main_construction_materials_of_outside_walls || []).display()}</Text></ListItem>}
            {this.state.main_materials_on_the_roof && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>What is the main materials of the roof? : {(this.state.main_materials_on_the_roof || []).display()}</Text></ListItem>}
            {this.state.residence_has_kitchen && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Does your residence have kitchen?  : {(this.state.residence_has_kitchen || []).display()}</Text></ListItem>}
            {this.state.type_of_stove_used_by_household_for_cooking && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>What types of stove does your household mainly use for cooking? : {(this.state.type_of_stove_used_by_household_for_cooking || []).display()}</Text></ListItem>}
            {this.state.type_of_toilet_used_by_household && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>What type of toilet is used by your household? : {(this.state.type_of_toilet_used_by_household || []).display()}</Text></ListItem>}
            {this.state.does_household_own_agricultural_land && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>Does your household own, sharecrops-in or mortgage-in any agricultural land? : {(this.state.does_household_own_agricultural_land || []).display()}</Text></ListItem>}
            {this.state.number_of_telephone_sets_in_household && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>How many telephone sets/cordless/mobile does your household own?  : {(this.state.number_of_telephone_sets_in_household || []).display()}</Text></ListItem>}
          </ScrollView>
        </Tab>
        <Tab heading='Food Insecurity'>
            <ScrollView>
              {this.state.saving_money && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('saving_money')}: {(this.state.saving_money|| []).display()}</Text></ListItem>}
              {this.state.food_worry && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('food_worry')}: {(this.state.food_worry|| []).display()}</Text></ListItem>}
              {this.state.food_worry == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen1|| []).display()}</Text></ListItem>}
              {this.state.lack_food && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('lack_food')}: {(this.state.lack_food|| []).display()}</Text></ListItem>}
              {this.state.happen2 && this.state.lack_food == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen2|| []).display()}</Text></ListItem>}
              {this.state.variety_food && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('variety_food')}: {(this.state.variety_food|| []).display()}</Text></ListItem>}
              {this.state.happen3 && this.state.variety_food == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen3 || []).display()}</Text></ListItem>}
              {this.state.want_food && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('want_food')}: {(this.state.want_food || []).display()}</Text></ListItem>}
              {this.state.want_food == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen4|| []).display()}</Text></ListItem>}
              {this.state.small_meal && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('small_meal')}: {(this.state.small_meal || []).display()}</Text></ListItem>}
              {this.state.small_meal == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen5|| []).display()}</Text></ListItem>}
              {this.state.few_meal && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('few_meal')}: {(this.state.few_meal || []).display()}</Text></ListItem>}
              {this.state.no_food && this.state.few_meal == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('no_food')}: {(this.state.no_food || []).display()}</Text></ListItem>}
              {this.state.hungry_night && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('hungry_night')}: {(this.state.hungry_night || []).display()}</Text></ListItem>}
              {this.state.happen8 && this.state.hungry_night == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen8 || []).display()}</Text></ListItem>}
              {this.state.hungry_day && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('hungry_day')}: {(this.state.hungry_day || []).display()}</Text></ListItem>}
              {this.state.happen9 && this.state.hungry_day == 'yes' && <ListItem style={{ marginLeft: 0, paddingLeft: 10 }} ><Text>{Labels('happen')}: {(this.state.happen9 || []).display()}</Text></ListItem>}
            </ScrollView>
        </Tab>
        <Tab heading='Members'>
          <ScrollView>
            <List>
              {this.state.members && this.state.members.map((member, index) => <ListItem onPress={_.debounce(this.viewmember.bind(this, member), 500)} style={{ marginLeft: 0, paddingLeft: 10 }} key={index}>
                <Body>
                  <Text>{member.full_name} {member.head_of_family && member.head_of_family == 'yes' && '( Head )'}, ID: {member.id}</Text>
                </Body>
                <Right>
                  <Icon name='arrow-forward' />
                </Right>
              </ListItem>)}
            </List>
          </ScrollView>
        </Tab>
      </Tabs>
      <Fab active={true}
        direction='up'
        containerStyle={{}}
        style={{ backgroundColor: 'orange' }}
        position='bottomRight'
        onPress={this.edits.debounce()}>
        <Icon name='md-add' />
        {!this.state.uploaded && <Button onPress={this.adds.debounce()} style={{ backgroundColor: '#34A34F' }}>
          <Icon name='md-color-filter' />
        </Button>}
        <Button onPress={this.load}>
          <Icon name='refresh' />
        </Button>
      </Fab>
    </Container>
  }

  viewmember(member) {
    this.props.navigation.navigate('ViewMember', {
      name: member.full_name,
      member: member
    });
  }

  edits() {
    // this.state.id ? this.props.navigation.dispatch(NavigationActions.back()) :
      this.props.navigation.navigate('NewMember', {
        name: 'Add new member',
        family_index: this.state.index,
        members: this.state.members,
        location: _.pick(this.state, ['state_id', 'district_id', 'municipality_id', 'ward_id', 'tole']),
        family_id: this.state.id,
        members: this.state.members
      });
  }

  adds() {
    let state = this.state;
    state.members = undefined;
    this.props.navigation.navigate('New', { state: state })
  }
}