import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    paddingH: {
        paddingLeft: 20,
        paddingRight: 20
    }
})
