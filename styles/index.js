import {StyleSheet} from 'react-native';

const paddingLR = {
    paddingLeft: 20,
    paddingRight: 20
}

export const marginLR = {
    marginLeft: 20,
    marginRight: 20
}

export default paddingLR